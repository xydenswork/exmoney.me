<?php
if( !defined( 'ABSPATH')){ exit(); }



        
		
function adddeposit_shortcode($atts, $content) { 
global $wpdb, $investbox;
           
			
	$temp = '';

	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
	//$temp .= apply_filters('before_toinvest_page','');
	
	if($user_id){

		$temp .= '
		<!--<div class="systemdiv">-->
			<div class="systemdiv_ins">Нажмите на название валюты, в которой желаете сделать депозит.<br><br>';
		
			$systems = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."inex_system ORDER BY title ASC, valut ASC");
			foreach($systems as $sys){ /* выводим системы */
				$gid = $sys->gid;
				if($investbox->check_ps($gid)){
				
					$tarifs = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."inex_tars WHERE gid='$gid' AND status='1' ORDER BY cdays ASC");
					if(count($tarifs) > 0){
					
						$item = $tarifs[0];
					
						$maxsum_st = 'style="display: none;"';
						if($item->maxsum > 0){
							$maxsum_st = '';
							$defsumm = is_sum($item->maxsum);
						} else {
							$defsumm = 10000;
						}

						$minsum_st = 'style="display: none;"';
						if($item->minsum > 0){
							$minsum_st = '';
							if($defsumm < $item->minsum){
								$defsumm = is_sum($item->minsum);
							}
						}				
					
						$pers = pn_strip_input($item->mpers);
						$dohod = is_sum($defsumm / 100 * $pers,2);
					
						$before_toinvest_one = '
						<div class="onesystem">
							<div class="onesystem_ins">
								<div class="onesystemtitle">'. __('Invest','inex') .' '. pn_strip_input($sys->title .' '. $sys->valut) .'</div>
						';
					
						$temp .= apply_filters('before_toinvest_one',$before_toinvest_one, $sys);
					
						$temp .= '
						<form action="'. get_ajax_link('invest_createdeposit','get') .'" method="post">
						
							<div class="onesystembody">
								<div class="inv_table">
									<div class="inv_tr">
										<div class="inv_td labeltd">'. __('Your wallet','inex') .':</div>
										<div class="inv_td"><input type="text" name="account" class="inex_input depositkow" value="" /></div>
										<div class="inv_td helptd">'. __('Wallet we are to send money on','inex') .'</div>
									</div>							
									<div class="inv_tr">
										<div class="inv_td labeltd">'. __('Amount','inex') .':</div>
										<div class="inv_td"><input type="text" name="sum" class="inex_input depositchangesumm" value="'. $defsumm .'" /></div>
										<div class="inv_td helptd">'. __('Amount of deposit','inex') .'
											<span class="change_min_sum_line" '. $minsum_st .'><br /><strong>'. __('min.','inex') .'</strong> <span class="change_min_sum">'. is_sum($item->minsum) .'</span> '. pn_strip_input($sys->valut).'</span>
											<span class="change_max_sum_line" '. $maxsum_st .'><br /><strong>'. __('max.','inex') .'</strong> <span class="change_max_sum">'. is_sum($item->maxsum) .'</span> '. pn_strip_input($sys->valut).'</span>
										</div>
									</div>
									<div class="inv_tr">
										<div class="inv_td labeltd">'. __('Period','inex') .':</div>
										<div class="inv_td"><select name="tarid" class="inex_select depositchangetar" autocomplete="off">';
										
										foreach($tarifs as $tar){
											$temp .= '<option value="'. $tar->id .'">'. pn_strip_input($tar->cdays) .' '. __('days','inex') .'</option>';	
										}
										
										$temp .= '
										</select>
										</div>
										<div class="inv_td helptd">'. __('Terms and conditions may vary due to a period','inex') .'</div>
									</div>
									<div class="inv_tr">
										<div class="inv_td labeltd">'. __('Percent','inex') .':</div>
										<div class="inv_td datatd"><span class="changepers">'. $pers .'</span>%</div>
										<div class="inv_td helptd">'. __('Percent for period','inex') .'</div>
									</div>

									<div class="inv_tr">
										<div class="inv_td labeltd">'. __('Profit','inex') .':</div>
										<div class="inv_td datatd"><span class="changedohod">'. $dohod .'</span> '. pn_strip_input($sys->valut) .'</div>
										<div class="inv_td helptd"></div>
									</div>
									<div class="inv_tr">
										<div class="inv_td labeltd"></div>
										<div class="inv_td datatd"><input type="submit" name="submit" class="inex_submit goinvest" value="'. __('Invest','inex') .'" /></div>
										<div class="inv_td helptd"></div>
									</div>								
									';								
							
						$temp .='
								</div>
							</div>
						</form>
						';
						
						foreach($tarifs as $tar){
							$temp .= '
							<div class="tars_'. $tar->id .'" style="display: none;">
								<input type="hidden" name="" class="the_minsum" value="'. is_sum($tar->minsum) .'" />
								<input type="hidden" name="" class="the_maxsum" value="'. is_sum($tar->maxsum) .'" />
								<input type="hidden" name="" class="the_pers" value="'. is_sum($tar->mpers) .'" />
								<input type="hidden" name="" class="the_days" value="'. is_sum($tar->cdays) .'" />
							</div>
							';
						}
					
						$after_toinvest_one = '
							</div>
						</div>	
						';
					
						$temp .= apply_filters('after_toinvest_one',$after_toinvest_one, $sys);
					
					}
				}
			}	
		
		$temp .= '
			</div>
		<!--</div>-->';
	
	
						
		
		//$temp .= apply_filters('after_toinvest_form',$after_toinvest_form);
		
		$temp .= get_pagenavi($pagenavi);			
	
	} else {
		$temp .= '<div class="inex_resultfalse">[ '. __('Page is available after log in only','inex') .' ]</div>';
	}
	
	//$temp .= apply_filters('after_toinvest_page','');

	return $temp;
}
add_shortcode('adddeposit', 'adddeposit_shortcode');