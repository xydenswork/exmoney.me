<?php 
if( !defined( 'ABSPATH')){ exit(); }

add_filter('placed_captcha', 'def_placed_captcha', 0);
function def_placed_captcha(){
	$placed = array(
		'loginform' => __('Authourization form','pn'),
		'registerform' => __('Registration form','pn'),
		'lostpass1form' => __('Lost password form','pn'),
		'contactform' => __('Contact form','pn'),
		'reservform' => __('When available reserve','pn'),
		'exchangeform' => __('Exchange order','pn'),
	);	
	return $placed;
}

function get_contact_form_filelds($place='shortcode'){
	$ui = wp_get_current_user();

	$items = array();
	$items['name'] = array(
		'name' => 'name',
		'title' => __('Your name', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => pn_strip_input(is_isset($ui,'first_name')),
		'type' => 'input',
		'not_auto' => 0,
		'classes' => 'notclear',
	);
	$items['email'] = array(
		'name' => 'email',
		'title' => __('Your e-mail', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => is_email(is_isset($ui,'user_email')),
		'type' => 'input',
		'not_auto' => 0,
		'classes' => 'notclear',
	);	
	$items['exchange_id'] = array(
		'name' => 'exchange_id',
		'title' => __('Exchange ID', 'pn'),
		'placeholder' => '',
		'req' => 0,
		'value' => '', 
		'type' => 'input',
		'not_auto' => 0,
	);	
	$items['text'] = array(
		'name' => 'text',
		'title' => __('Message', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => '', 
		'type' => 'text',
		'not_auto' => 0,
	);		
	$items = apply_filters('get_form_filelds', $items, 'contactform', $ui, $place);	
	$items = apply_filters('contact_form_filelds', $items, $ui, $place);
	
	return $items;
}	

function get_zreserv_form_filelds($place='shortcode'){
	$ui = wp_get_current_user();

	$items = array();
	$items['sum'] = array(
		'name' => 'sum',
		'title' => '',
		'placeholder' => __('Required amount', 'pn'),
		'req' => 1,
		'value' => '',
		'type' => 'input',
		'not_auto' => 0,
	);
	$items['email'] = array(
		'name' => 'email',
		'title' => '',
		'placeholder' => __('E-mail', 'pn'),
		'req' => 1,
		'value' => is_email(is_isset($ui,'user_email')),
		'type' => 'input',
		'not_auto' => 0,
		'classes' => 'notclear',
	);		
	$items['comment'] = array(
		'name' => 'comment',
		'title' => '',
		'placeholder' => __('Comment', 'pn'),
		'req' => 0,
		'value' => '', 
		'type' => 'text',
		'not_auto' => 0,
	);	
	$items = apply_filters('get_form_filelds',$items, 'reservform', $ui, $place);
	$items = apply_filters('reserv_form_filelds',$items, $ui, $place);	
	
	return $items;
}

function get_login_form_filelds($place='shortcode'){
	$ui = wp_get_current_user();

	$items = array();
	$items['logmail'] = array(
		'name' => 'logmail',
		'title' => __('Login or email', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => '',
		'type' => 'input',
		'not_auto' => 0,
	);
	$items['pass'] = array(
		'name' => 'pass',
		'title' => __('Password', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => '',
		'type' => 'password',
		'not_auto' => 0,
	);		
	$items = apply_filters('get_form_filelds',$items, 'loginform', $ui, $place);
	$items = apply_filters('login_form_filelds',$items, $ui, $place);	
	
	return $items;
}

function get_register_form_filelds($place='shortcode'){
	$ui = wp_get_current_user();

	$items = array();
	$items['login'] = array(
		'name' => 'login',
		'title' => __('Login', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => '',
		'type' => 'input',
		'not_auto' => 0,
	);
	$items['email'] = array(
		'name' => 'email',
		'title' => __('E-mail', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => '',
		'type' => 'input',
		'not_auto' => 0,
	);	
	$items['pass'] = array(
		'name' => 'pass',
		'title' => __('Password', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => '',
		'type' => 'password',
		'not_auto' => 0,
	);
	$items['pass2'] = array(
		'name' => 'pass2',
		'title' => __('Password again', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => '',
		'type' => 'password',
		'not_auto' => 0,
	);
	$items = apply_filters('get_form_filelds',$items, 'registerform', $ui, $place);
	$items = apply_filters('register_form_filelds',$items, $ui, $place);	
	
	return $items;
}

function get_lostpass1_form_filelds($place='shortcode'){
	$ui = wp_get_current_user();

	$items = array();
	$items['email'] = array(
		'name' => 'email',
		'title' => __('E-mail', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => '',
		'type' => 'input',
		'not_auto' => 0,
	);
	$items = apply_filters('get_form_filelds',$items, 'lostpass1form', $ui, $place);
	$items = apply_filters('lostpass1_form_filelds',$items, $ui, $place);	
	
	return $items;
}

function get_lostpass2_form_filelds($place='shortcode'){
	$ui = wp_get_current_user();

	$items = array();
	$items['pass'] = array(
		'name' => 'pass',
		'title' => __('New password', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => '',
		'type' => 'password',
		'not_auto' => 1,
	);
	$items['pass2'] = array(
		'name' => 'pass2',
		'title' => __('New password again', 'pn'),
		'placeholder' => '',
		'req' => 1,
		'value' => '',
		'type' => 'password',
		'not_auto' => 1,
	);
	$items = apply_filters('get_form_filelds',$items, 'lostpass2form', $ui, $place);
	$items = apply_filters('lostpass2_form_filelds',$items, $ui, $place);	
	
	return $items;
}

function get_account_form_filelds($place='shortcode'){
global $exchangebox;	
	$ui = wp_get_current_user();

	$items = array();
	if(pn_allow_uv('login')){
		$items['login'] = array(
			'name' => 'login',
			'title' => __('Login', 'pn'),
			'placeholder' => '',
			'req' => 0,
			'value' => is_user(is_isset($ui,'user_login')),
			'type' => 'input',
			'not_auto' => 0,
			'disable' => 1,
		);
	}
	if(pn_allow_uv('last_name')){
		$items['last_name'] = array(
			'name' => 'last_name',
			'title' => __('Last name', 'pn'),
			'placeholder' => '',
			'req' => 0,
			'value' => pn_strip_input(is_isset($ui,'last_name')),
			'type' => 'input',
			'not_auto' => 0,
			'disable' => apply_filters('disabled_account_form_line', 0, 'last_name', $ui),
		);
	}
	if(pn_allow_uv('first_name')){
		$items['first_name'] = array(
			'name' => 'first_name',
			'title' => __('First name', 'pn'),
			'placeholder' => '',
			'req' => 0,
			'value' => pn_strip_input(is_isset($ui,'first_name')),
			'type' => 'input',
			'not_auto' => 0,
			'disable' => apply_filters('disabled_account_form_line', 0, 'first_name', $ui),
		);
	}
	if(pn_allow_uv('second_name')){
		$items['second_name'] = array(
			'name' => 'second_name',
			'title' => __('Second name', 'pn'),
			'placeholder' => '',
			'req' => 0,
			'value' => pn_strip_input(is_isset($ui,'second_name')),
			'type' => 'input',
			'not_auto' => 0,
			'disable' => apply_filters('disabled_account_form_line', 0, 'second_name', $ui),
		);	
	}	
	if(pn_allow_uv('user_phone')){	
		$items['user_phone'] = array(
			'name' => 'user_phone',
			'title' => __('Phone no.', 'pn'),
			'placeholder' => '',
			'req' => 0,
			'value' => pn_strip_input(is_isset($ui,'user_phone')),
			'type' => 'input',
			'not_auto' => 0,
			'disable' => apply_filters('disabled_account_form_line', 0, 'user_phone', $ui),
		);
	}
	if(pn_allow_uv('user_skype')){
		$items['user_skype'] = array(
			'name' => 'user_skype',
			'title' => __('Skype', 'pn'),
			'placeholder' => '',
			'req' => 0,
			'value' => pn_strip_input(is_isset($ui,'user_skype')),
			'type' => 'input',
			'not_auto' => 0,
			'disable' => apply_filters('disabled_account_form_line', 0, 'user_skype', $ui),
		);
	}
	$items['user_email'] = array(
		'name' => 'user_email',
		'title' => __('E-mail', 'pn'),
		'placeholder' => '',
		'req' => 0,
		'value' => is_email(is_isset($ui,'user_email')),
		'type' => 'input',
		'not_auto' => 0,
		'disable' => apply_filters('disabled_account_form_line', 0, 'user_email', $ui),
	);	
	if(pn_allow_uv('website')){
		$items['website'] = array(
			'name' => 'website',
			'title' => __('Website', 'pn'),
			'placeholder' => '',
			'req' => 0,
			'value' => esc_url(is_isset($ui,'user_url')),
			'type' => 'input',
			'not_auto' => 0,
			'disable' => apply_filters('disabled_account_form_line', 0, 'website', $ui),
		);
	}
	if(pn_allow_uv('user_passport')){	
		$items['user_passport'] = array(
			'name' => 'user_passport',
			'title' => __('Passport number', 'pn'),
			'placeholder' => '',
			'req' => 0,
			'value' => pn_strip_input(is_isset($ui,'user_passport')),
			'type' => 'input',
			'not_auto' => 0,
			'disable' => apply_filters('disabled_account_form_line', 0, 'user_passport', $ui),
		);
	}	
	$items['pass'] = array(
		'name' => 'pass',
		'title' => __('New password', 'pn'),
		'placeholder' => '',
		'req' => 0,
		'value' => '',
		'type' => 'password',
		'not_auto' => 0,
		'disable' => 0,
	);
	$items['pass2'] = array(
		'name' => 'pass2',
		'title' => __('New password again', 'pn'),
		'placeholder' => '',
		'req' => 0,
		'value' => '',
		'type' => 'password',
		'not_auto' => 0,
		'disable' => 0,
	);	
	$items = apply_filters('get_form_filelds',$items, 'accountform', $ui, $place);
	$items = apply_filters('account_form_filelds',$items, $ui, $place);	
	return $items;
}

function get_vtitle($valut_id){
global $wpdb;

	$valut_id = intval($valut_id);
	$valut_data = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut_id'");
	if(isset($valut_data->vname)){
		return pn_strip_input($valut_data->vname.' '.$valut_data->vtype);
	} else {
		return __('No item','pn');
	}
} 

function get_bid_status($status){
	$status_title = __('Not known','pn');
	if($status == 'new'){
		$status_title = __('new application','pn');
	} elseif($status == 'payed'){
		$status_title = __('marked bid by user as paid','pn');
	} elseif($status == 'realpay'){
		$status_title = __('paid bid','pn');		
	} elseif($status == 'delete'){
		$status_title = __('delete application','pn');
	} elseif($status == 'returned'){
		$status_title = __('money will be returned','pn');
	} elseif($status == 'verify'){
		$status_title = __('paid from another purse','pn');
	} elseif($status == 'error'){
		$status_title = __('error application','pn');
	} elseif($status == 'success'){
		$status_title = __('success application','pn');
	}
	return $status_title;
}

add_filter('list_naps_temp','def_list_naps_temp',0); 
function def_list_naps_temp($list_naps_temp){
	
	$list_naps_temp = array(
		'description_txt' => __('Exchange description','pn'),
		'timeline_txt' => __('Deadline for exchange','pn'),
		'status_new' => __('Payment instruction','pn'),
		'status_returned' => __('Return bid','pn'),
		'status_delete' => __('Deleted bid','pn'),
		'status_payed' => __('Marked bid by user as paid','pn'),
		'status_realpay' => __('Paid bid','pn'),
		'status_verify' => __('Paid bid from another purse','pn'),
		'status_error' => __('Error bid','pn'),
		'status_success' => __('Success bid','pn'),
	);		
							
	return $list_naps_temp;
}

function get_comis_text($com_ps, $dop_com, $psys, $vtype, $vid, $gt){
	$comis_text = '';
	
	if($com_ps > 0 or $dop_com > 0){
		$comis_text = __('Including','pn').' ';
	}		

	if($com_ps > 0 and $dop_com > 0){
		$comis_text .= __('add. service fee','pn');
		$comis_text .= ' ('. $dop_com .' '. $vtype .')';
		$comis_text .= __('and','pn');
		$comis_text .= ' ';		
		$comis_text .= __('payment system fee','pn');
		$comis_text .= ' <span class="wvalname">'. $psys . ' <span class="wvalsumm">('. $com_ps .' '. $vtype .')</span> ';
	} elseif($com_ps > 0){
		$comis_text .= __('payment system fee','pn');
		$comis_text .= ' <span class="wvalname">'. $psys . ' <span class="wvalsumm">('. $com_ps .' '. $vtype .')</span> ';	
	} elseif($dop_com > 0){
		$comis_text .= __('add. service fee','pn');
		$comis_text .= ' <span class="wvalsumm">('. $dop_com .' '. $vtype .')</span>';
	}	
	
	if($gt == 1){
		if($com_ps > 0 or $dop_com > 0){
			$comis_text .= ', ';
			if($vid == 1){
				$comis_text .= __('you give','pn');
			} else {
				$comis_text .= __('you get','pn');
			}
		}
	}
	
	return pn_strip_input($comis_text);
}

function get_exchangestep_title(){
global $wpdb, $bids_id, $bids_data;	
	$title = '';
	if(isset($bids_data->id)){
		if($bids_data->status == 'auto'){
			$valut1 = pn_strip_input($bids_data->valut1).' '.pn_strip_input($bids_data->valut1type);
			$valut2 = pn_strip_input($bids_data->valut2).' '.pn_strip_input($bids_data->valut2type);
		    $title = sprintf(__('Exchange %1$s to %2$s','pn'),$valut1,$valut2);
			return apply_filters('get_exchange_title', $title, $bids_data->naprid, $valut1, $valut2);
		} else {
			$title = __('Bid id','pn') . ' '. $bids_data->id;
			return apply_filters('get_exchangestep_title', $title, $bids_data->id);
		}
	}
}

function get_exchange_title(){
global $naps_id, $naps_data;	
	if(isset($naps_data->item1) and isset($naps_data->item2)){
		$item_title1 = pn_strip_input($naps_data->item1);
		$item_title2 = pn_strip_input($naps_data->item2);
								
		$title = sprintf(__('Exchange %1$s to %2$s','pn'),$item_title1,$item_title2);	
		return apply_filters('get_exchange_title', $title, $naps_id, $item_title1, $item_title2);
	}
}

function update_bids_meta($id, $key, $value){ 
	return update_pn_meta('bids_meta', $id, $key, $value);
}

function get_bids_meta($id, $key){
	return get_pn_meta('bids_meta', $id, $key);
}

function delete_bids_meta($id, $key){
	return delete_pn_meta('bids_meta', $id, $key);
}

function update_naps_meta($id, $key, $value){ 
	return update_pn_meta('naps_meta', $id, $key, $value);
}

function get_naps_meta($id, $key){
	return get_pn_meta('naps_meta', $id, $key);
}

function delete_naps_meta($id, $key){
	return delete_pn_meta('naps_meta', $id, $key);
}

function update_valuts_meta($id, $key, $value){ 
	return update_pn_meta('valuts_meta', $id, $key, $value);
}

function get_valuts_meta($id, $key){
	return get_pn_meta('valuts_meta', $id, $key);
}

function delete_valuts_meta($id, $key){
	return delete_pn_meta('valuts_meta', $id, $key);
}

function copy_naps_txtmeta($data_id, $new_id){
	copy_txtmeta('napsmeta', $data_id, $new_id);
}

function delete_naps_txtmeta($data_id){
	delete_txtmeta('napsmeta', $data_id);
}

function get_naps_txtmeta($data_id, $key){
	return get_txtmeta('napsmeta', $data_id, $key);
}

function update_naps_txtmeta($data_id, $key, $value){
	return update_txtmeta('napsmeta', $data_id, $key, $value);
}

function delete_vaccs_txtmeta($data_id){
	delete_txtmeta('vaccsmeta', $data_id);
}

function get_vaccs_txtmeta($data_id, $key){
	return get_txtmeta('vaccsmeta', $data_id, $key);
}

function update_vaccs_txtmeta($data_id, $key, $value){
	return update_txtmeta('vaccsmeta', $data_id, $key, $value);
}