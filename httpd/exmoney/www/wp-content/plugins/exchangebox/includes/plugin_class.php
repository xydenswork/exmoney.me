<?php
if( !defined( 'ABSPATH')){ exit(); }

if(!class_exists('ExchangeBox')){
	class ExchangeBox extends Premium {

		function __construct($debug_mode=0)
		{
		
			$this->debug_constant_name = 'PN_RICH_TEST';
			$this->plugin_version = '7.0';
			$this->plugin_name = 'ExchangeBox';
			$this->plugin_path = PN_PLUGIN_NAME;
			$this->plugin_dir = PN_PLUGIN_DIR;
			$this->plugin_url = PN_PLUGIN_URL;
			$this->plugin_prefix = 'pn';
			$this->plugin_option_name = 'pn_options';
			$this->plugin_page_name = 'the_pages';
			$this->plugin_usersess_day = PN_USERSESS_DAY;
			
			parent::__construct($debug_mode);
			
			add_filter('all_plugins', array($this, 'title_this_plugin'));
			
			add_action('init', array($this,'deprecated_pages')); /* off next version */
			
			add_action('template_redirect', array($this, 'exchange_redirect'),0);
			
			add_filter('query_vars', array($this, 'query_vars'));
			add_filter('generate_rewrite_rules', array($this, 'generate_rewrite_rules'));			
			
		}	
		
		public function title_this_plugin($plugins){
			global $locale;	
				
			$plugin_path = $this->plugin_path;	
				
			if($locale == 'ru_RU'){ /* русское описание */
				$name = 'ExchangeBox';
				$description = 'Полуавтоматический обменный пункт';
					
				$plugins[$plugin_path]['Name'] = $name;
				$plugins[$plugin_path]['Description'] = $description;	
			} 	
				
			return $plugins;
		}	

		public function deprecated_pages(){
			
			$plugin = basename($this->plugin_path,'.php');
			$wp_content = ltrim(str_replace(ABSPATH,'',WP_CONTENT_DIR),'/');
			
			add_rewrite_rule('rtest.html$', $wp_content . '/plugins/'. $plugin .'/sitepage/test.php', 'top');

			/* old */
			add_rewrite_rule('blackping.html$', $wp_content . '/plugins/'.$plugin.'/sitepage/blackping.php', 'top');
			add_rewrite_rule('logout.html$', $wp_content . '/plugins/'.$plugin.'/sitepage/logout.php', 'top');
			add_rewrite_rule('curscron.html$', $wp_content . '/plugins/'. $plugin .'/sitepage/curscron.php', 'top');
			add_rewrite_rule('sitemap.xml$', $wp_content . '/plugins/'.$plugin.'/sitepage/sitemap.php', 'top');
			add_rewrite_rule('exportxml.xml$', $wp_content .'/plugins/'.$plugin.'/sitepage/exportxml.php', 'top');
			add_rewrite_rule('exporttxt.txt$', $wp_content .'/plugins/'.$plugin.'/sitepage/exporttxt.php', 'top');
		}
		
		public function list_tech_pages($pages){
			
			$pages[] = array(
				'post_name'      => 'home',
				'post_title'     => 'Главная',
				'post_content'   => '[changetable]',
				'post_template'   => 'exb-pluginpage.php',
			);
			$pages[] = array(
				'post_name'      => 'news',
				'post_title'     => 'Новости',
				'post_content'   => '',
				'post_template'   => '',
			);	
			$pages[] = array( 
				'post_name'      => 'tos',
				'post_title'     => 'Правила сайта',
				'post_content'   => '',
				'post_template'   => '',
			);					
			$pages[] = array(
				'post_name'      => 'login',
				'post_title'     => 'Авторизация',
				'post_content'   => '[login_form]',
				'post_template'   => '',
			);
			$pages[] = array(
				'post_name'      => 'register',
				'post_title'     => 'Регистрация',
				'post_content'   => '[register_form]',
				'post_template'   => '',
			);
			$pages[] = array(
				'post_name'      => 'lostpass',
				'post_title'     => 'Восстановление пароля',
				'post_content'   => '[lostpass_page]',
				'post_template'   => '',
			);
			$pages[] = array(
				'post_name'      => 'account',
				'post_title'     => 'Личный кабинет',
				'post_content'   => '[account_page]',
				'post_template'   => 'exb-pluginpage.php',
			);							
			$page_text='
			<p>Мы всегда готовы ответить на интересующие Вас вопросы, а также выслушать Ваши предложения по улучшению нашего сервиса.</p>
			<p>Используйте данную форму, если хотите задать нам вопрос, или сообщить о ошибке. Пожалуйста, делайте Ваше сообщение как можно более развернутым, тогда мы сможем решить проблему намного быстрее.</p>

			[exb_contact_form]
			';	
			$pages[] = array(
				'post_name'      => 'feedback',
				'post_title'     => 'Контакты',
				'post_content'   => $page_text,
				'post_template'   => '',
			);
			$pages[] = array(
				'post_name'      => 'payouts',
				'post_title'     => 'Вывод партнерских средств',
				'post_content'   => '[exb_payouts]',
				'post_template'   => 'exb-pluginpage.php',
			);			
			$pages[] = array(
				'post_name'      => 'partnersFAQ',
				'post_title'     => 'Партнерский FAQ',
				'post_content'   => '',
				'post_template'   => '',
			);
			$pages[] = array(
				'post_name'      => 'banners',
				'post_title'     => 'Рекламные материалы',
				'post_content'   => '[exb_banners]',
				'post_template'   => 'exb-pluginpage.php',
			);
			$pages[] = array(
				'post_name'      => 'partnerstats',
				'post_title'     => 'Партнерский аккаунт',
				'post_content'   => '[exb_partnerstats]',
				'post_template'   => 'exb-pluginpage.php',
			);
			$pages[] = array(
				'post_name'      => 'xchange',
				'post_title'     => 'Обмен',
				'post_content'   => '[xchange]',
				'post_template'   => 'exb-pluginpage.php',
			);
			$pages[] = array(
				'post_name'      => 'exchangestep',
				'post_title'     => 'Обмен шаги',
				'post_content'   => '[exchangestep]',
				'post_template'   => 'exb-pluginpage.php',
			);			
			$pages[] = array(
				'post_name'      => 'usersobmen',
				'post_title'     => 'Ваши операции',
				'post_content'   => '[exb_usersobmen]',
				'post_template'   => 'exb-pluginpage.php',
			);			
			$pages[] = array(
				'post_name'      => 'tarifs',
				'post_title'     => 'Тарифы',
				'post_content'   => '[exb_tarifs]',
				'post_template'   => 'exb-pluginpage.php',
			);			
			
			return $pages;
		}	
		
		public function admin_menu(){ 
			
			add_submenu_page("index.php", __('Migration','pn'), __('Migration','pn'), 'administrator', "pn_migrate", array($this, 'admin_temp'));
			
			if(current_user_can('administrator') or current_user_can('pn_change_notify')){
				add_menu_page(__('Message settings', $this->plugin_prefix), __('Message settings', $this->plugin_prefix), 'read', 'pn_mail_temps', array($this, 'admin_temp'), $this->get_icon_link('mails'));
				add_submenu_page("pn_mail_temps", __('E-mail templates', $this->plugin_prefix), __('E-mail templates', $this->plugin_prefix), 'read', "pn_mail_temps", array($this, 'admin_temp'));
				add_submenu_page("pn_mail_temps", __('SMS templates', $this->plugin_prefix), __('SMS templates', $this->plugin_prefix), 'read', "pn_sms_temps", array($this, 'admin_temp'));
			}			
			
			$hook = add_menu_page(__('Applications','pn'), __('Applications','pn'), 'administrator', "pn_bids", array($this, 'admin_temp'), $this->get_icon_link('exchange'), 3);
			add_action( "load-$hook", 'pn_trev_hook' );
			
			add_menu_page(__('Exchanger settings','pn'), __('Exchanger settings','pn'), 'administrator', 'pn_config', array($this, 'admin_temp'), $this->get_icon_link('settings'));
			add_submenu_page("pn_config", __('Exchange settings','pn'), __('Exchange settings','pn'), 'administrator', "pn_exchange_config", array($this, 'admin_temp'));
			add_submenu_page("pn_config", __('Field helps','pn'), __('Field helps','pn'), 'administrator', "pn_fhelps", array($this, 'admin_temp'));
			add_submenu_page("pn_config", __('Settings XML sitemap','pn'), __('Settings XML sitemap','pn'), 'administrator', "pn_xmlmap", array($this, 'admin_temp'));
			add_submenu_page("pn_config", __('Cron','pn'), __('Cron','pn'), 'administrator', "pn_cron", array($this, 'admin_temp'));
			add_submenu_page("pn_config", __('Language settings','pn'), __('Language settings','pn'), 'administrator', "pn_lang", array($this, 'admin_temp'));
			add_submenu_page("pn_config", __('Admin panel','pn'), __('Admin panel','pn'), 'administrator', "pn_admin", array($this, 'admin_temp'));
			
			add_menu_page(__('Theme settings','pn'), __('Theme settings','pn'), 'administrator', 'pn_themeconfig', array($this, 'admin_temp'), $this->get_icon_link('theme'));	
			
			$hook = add_menu_page(__('Merchants','pn'), __('Merchants','pn'), 'administrator', 'pn_merchants', array($this, 'admin_temp'), $this->get_icon_link('merchants'));
			add_action( "load-$hook", 'pn_trev_hook' );
			add_submenu_page("pn_merchants", __('Merchants','pn'), __('Merchants','pn'), 'administrator', "pn_merchants", array($this, 'admin_temp'));
			add_submenu_page("pn_merchants", __('Merchants settings','pn'), __('Merchants settings','pn'), 'administrator', "pn_data_merchants", array($this, 'admin_temp'));
			$hook = add_submenu_page("pn_merchants", __('SMS gates','pn'), __('SMS gates','pn'), 'administrator', "pn_smsgate", array($this, 'admin_temp'));
			add_action( "load-$hook", 'pn_trev_hook' );
			
			$hook = add_menu_page(__('Moduls','pn'), __('Moduls','pn'), 'administrator', 'pn_moduls', array($this, 'admin_temp'), $this->get_icon_link('moduls'));
			add_action( "load-$hook", 'pn_trev_hook' );
			
			$hook = add_menu_page( __('Currency','pn'), __('Currency','pn'), 'administrator', "pn_valuts", array($this, 'admin_temp'), $this->get_icon_link('valuts'));	
			add_action( "load-$hook", 'pn_trev_hook' );
			add_submenu_page("pn_valuts", __('Add currency','pn'), __('Add currency','pn'), 'administrator', "pn_add_valuts", array($this, 'admin_temp'));
			add_submenu_page("pn_valuts", __('Sort currency','pn'), __('Sort currency','pn'), 'read', "pn_sort_valuts", array($this, 'admin_temp'));
			add_submenu_page("pn_valuts", __('Sort reserve','pn'), __('Sort reserve','pn'), 'read', "pn_sort_valuts_reserve", array($this, 'admin_temp'));			
			
			$hook = add_menu_page(__('Currency codes','pn'), __('Currency codes','pn'), 'administrator', "pn_vtypes", array($this, 'admin_temp'), $this->get_icon_link('vtypes'));	
			add_action( "load-$hook", 'pn_trev_hook' );
			add_submenu_page("pn_vtypes", __('Add currency code','pn'), __('Add currency code','pn'), 'administrator', "pn_add_vtypes", array($this, 'admin_temp'));
			
			$hook = add_menu_page(__('Currency accounts','pn'), __('Currency accounts','pn'), 'administrator', 'pn_vaccounts', array($this, 'admin_temp'), $this->get_icon_link('accounts'));  
			add_action( "load-$hook", 'pn_trev_hook' );
			add_submenu_page("pn_vaccounts", __('Add','pn'), __('Add','pn'), 'administrator', "pn_add_vaccounts", array($this, 'admin_temp'));	
			add_submenu_page("pn_vaccounts", __('Add list','pn'), __('Add list','pn'), 'administrator', "pn_add_vaccounts_many", array($this, 'admin_temp'));
			
			$hook = add_menu_page( __('Custom currency fields','pn'), __('Custom currency fields','pn'), 'administrator', "pn_cfc", array($this, 'admin_temp'), $this->get_icon_link('vfield'));	
			add_action( "load-$hook", 'pn_trev_hook' );
			add_submenu_page("pn_cfc", __('Add custom field','pn'), __('Add custom field','pn'), 'administrator', "pn_add_cfc", array($this, 'admin_temp'));
			add_submenu_page("pn_cfc", __('Sort custom fields','pn'), __('Sort custom fields','pn'), 'administrator', "pn_sort_cfc", array($this, 'admin_temp'));
			
			$hook = add_menu_page(__('Directions exchanges','pn'), __('Directions exchanges','pn'), 'administrator', "pn_naps", array($this, 'admin_temp'), $this->get_icon_link('naps'));	
			add_action( "load-$hook", 'pn_trev_hook' );
			add_submenu_page("pn_naps", __('Add direction exchange','pn'), __('Add direction exchange','pn'), 'administrator', "pn_add_naps", array($this, 'admin_temp'));
			add_submenu_page("pn_naps", __('Sort directions exchanges','pn'), __('Sort directions exchanges','pn'), 'administrator', "pn_sort_naps", array($this, 'admin_temp'));
			add_submenu_page("pn_naps", __('Templates exchange directions','pn'), __('Templates exchange directions','pn'), 'administrator', "pn_naps_temp", array($this, 'admin_temp'));
			
			$hook = add_menu_page(__('Adjustment reserve','pn'), __('Adjustment reserve','pn'), 'administrator', "pn_reserv", array($this, 'admin_temp'), $this->get_icon_link('reserv'));	
			add_action( "load-$hook", 'pn_trev_hook' );
			add_submenu_page("pn_reserv", __('Add reserv transaction','pn'), __('Add reserv transaction','pn'), 'administrator', "pn_add_reserv", array($this, 'admin_temp'));	
			
			$hook = add_menu_page( __('Requests reserve','pn'), __('Requests reserve','pn'), 'administrator', "pn_zreserv", array($this, 'admin_temp'), $this->get_icon_link('zreserv'));	
			add_action( "load-$hook", 'pn_trev_hook' );		
			
			$hook = add_menu_page(__('Black list','pn'), __('Black list','pn'), 'administrator', 'pn_blacklist', array($this, 'admin_temp'), $this->get_icon_link('blacklist'));  
			add_action( "load-$hook", 'pn_trev_hook' );
			add_submenu_page("pn_blacklist", __('Add','pn'), __('Add','pn'), 'administrator', "pn_add_blacklist", array($this, 'admin_temp'));	
			add_submenu_page("pn_blacklist", __('Add list','pn'), __('Add list','pn'), 'administrator', "pn_add_blacklist_many", array($this, 'admin_temp'));
			add_submenu_page("pn_blacklist", __('Settings','pn'), __('Settings','pn'), 'administrator', "pn_config_blacklist", array($this, 'admin_temp'));	
			
			add_menu_page(__('SEO','pn'), __('SEO','pn'), 'read', 'pn_seo', array($this, 'admin_temp'), $this->get_icon_link('seo'));
			
			add_menu_page(__('Partner program','pn'), __('Partner program','pn'), 'administrator', 'pn_pp', array($this, 'admin_temp'), $this->get_icon_link('pp'));
			add_submenu_page("pn_pp", __('Stats','pn'), __('Stats','pn'), 'administrator', "pn_pp", array($this, 'admin_temp'));
			$hook = add_submenu_page("pn_pp", __('Partnership exchanges','pn'), __('Partnership exchanges','pn'), 'administrator', "pn_pexch", array($this, 'admin_temp'));
			add_action( "load-$hook", 'pn_trev_hook' );		
			$hook = add_submenu_page("pn_pp", __('Payments','pn'), __('Payments','pn'), 'administrator', "pn_payouts", array($this, 'admin_temp'));	
			add_action( "load-$hook", 'pn_trev_hook' );	
			$hook = add_submenu_page("pn_pp", __('Transitions','pn'), __('Transitions','pn'), 'administrator', "pn_plinks", array($this, 'admin_temp'));
			add_action( "load-$hook", 'pn_trev_hook' );		
			$hook = add_submenu_page("pn_pp", __('Referals','pn'), __('Referals','pn'), 'administrator', "pn_preferals", array($this, 'admin_temp'));
			add_action( "load-$hook", 'pn_trev_hook' );	
			$hook = add_submenu_page("pn_pp", __('Royalties','pn'), __('Royalties','pn'), 'administrator', "pn_partnpers", array($this, 'admin_temp'));	
			add_action( "load-$hook", 'pn_trev_hook' );
			add_submenu_page("pn_pp", __('Add expulsion','pn'), __('Add expulsion','pn'), 'administrator', "pn_add_partnpers", array($this, 'admin_temp'));	
			add_submenu_page("pn_pp", __('Banners','pn'), __('Banners','pn'), 'administrator', "pn_pbanners", array($this, 'admin_temp'));	
			add_submenu_page("pn_pp", __('Settings','pn'), __('Settings','pn'), 'administrator', "pn_psettings", array($this, 'admin_temp'));			
			
		}
		
		public function exchange_redirect(){
			global $wpdb;
			
			if(isset($_GET['cur_from']) and isset($_GET['cur_to'])){
				$cur_from = is_xml_value(is_param_get('cur_from'));
				$cur_to = is_xml_value(is_param_get('cur_to'));
				if($cur_from and $cur_to and $cur_to != $cur_from){
					$vd1 = $wpdb->get_row("SELECT xname FROM ". $wpdb->prefix ."valuts WHERE xml_value='$cur_from'");
					$vd2 = $wpdb->get_row("SELECT xname FROM ". $wpdb->prefix ."valuts WHERE xml_value='$cur_to'");
					if(isset($vd1->xname) and isset($vd2->xname)){
						wp_redirect(get_exchange_link($vd1->xname, $vd2->xname));
						exit;
					}
				}
			}	
			
		}
		
		public function query_vars( $query_vars ){
			$query_vars[] = 'valut1';
			$query_vars[] = 'valut2';
			$query_vars[] = 'hashed';

			return $query_vars;
		}

		public function generate_rewrite_rules($wp_rewrite){		
			$rewrite_rules = array (
				'([\-_A-Za-z0-9]+)_([A-Za-z0-9]+)_to_([A-Za-z0-9]+)$' => 'index.php?pagename=$matches[1]&valut1=$matches[2]&valut2=$matches[3]',
				'([\-_A-Za-z0-9]+)/hst_([A-Za-z0-9]{35})$' => 'index.php?pagename=$matches[1]&hashed=$matches[2]',
			);
			$wp_rewrite->rules = array_merge($rewrite_rules, $wp_rewrite->rules);		
		}		
		
	}    
}