<?php
if( !defined( 'ABSPATH')){ exit(); }
 
add_action('pn_adminpage_title_pn_exchange_config', 'pn_adminpage_title_pn_exchange_config');
function pn_adminpage_title_pn_exchange_config($page){
	_e('Exchange settings','pn');
} 

add_action('pn_adminpage_content_pn_exchange_config','def_pn_adminpage_content_pn_exchange_config');
function def_pn_adminpage_content_pn_exchange_config(){
global $wpdb, $exchangebox;

	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('Exchange settings','pn'),
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);	
	$techreg = $exchangebox->get_option('exchange','techreg'); 
	$options['techreg'] = array(
		'view' => 'select',
		'title' => __('maintenance mode','pn'),
		'options' => array('0'=>__('No','pn'),'1'=>__('Yes','pn'),'2'=>__('Yes, special','pn'),'3'=>__('Yes, special 2','pn')),
		'default' => $exchangebox->get_option('exchange','techreg'),
		'name' => 'techreg',
	);	
	
	$mhide = 'pn_hide';
	if($techreg == 1 or $techreg == 2 or $techreg == 3){
		$mhide = '';	
	}			
	$mhide .= ' techregtext';

	$options['techregtext'] = array(
		'view' => 'textarea',
		'title' => __('Text for maintenance','pn'),
		'default' => $exchangebox->get_option('exchange','techregtext'),
		'name' => 'techregtext',
		'width' => '',
		'height' => '100px',
		'work' => 'text',
		'class' => $mhide
	);	
	
	$options['techreg_from'] = array(
		'view' => 'select',
		'title' => __('Apply mode','pn'),
		'options' => array('0'=>__('for admin and users','pn'),'1'=>__('for users only','pn')),
		'default' => $exchangebox->get_option('exchange','techreg_from'),
		'name' => 'techreg_from',
		'class' => $mhide
	);
	
	$options['gostnaphide'] = array(
		'view' => 'select',
		'title' => __('Hide exchange direction from the guests?','pn'),
		'options' => array('0'=>__('No','pn'),'1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('exchange','gostnaphide'),
		'name' => 'gostnaphide',
	);
	
	$options[] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['an1_hidden'] = array(
		'view' => 'select',
		'title' => __('Data visibility in bid for account Give','pn'),
		'options' => array('0'=>__('show data','pn'),'1'=>__('hide data','pn'),'2'=>__('show first 4 symbols','pn'),'3'=>__('show last 4 symbols','pn'),'4'=>__('show first 4 symbols and show last 4 symbols','pn')),
		'default' => $exchangebox->get_option('exchange','an1_hidden'),
		'name' => 'an1_hidden',
	);
	$options['an2_hidden'] = array(
		'view' => 'select',
		'title' => __('Data visibility in bid for account Get','pn'),
		'options' => array('0'=>__('show data','pn'),'1'=>__('hide data','pn'),'2'=>__('show first 4 symbols','pn'),'3'=>__('show last 4 symbols','pn'),'4'=>__('show first 4 symbols and show last 4 symbols','pn')),
		'default' => $exchangebox->get_option('exchange','an2_hidden'),
		'name' => 'an2_hidden',
	);	
	$options['an3_hidden'] = array(
		'view' => 'select',
		'title' => __('Visibility in the application personal data','pn'),
		'options' => array('0'=>__('show data','pn'),'1'=>__('hide data','pn'),'2'=>__('show first 4 symbols','pn'),'3'=>__('show last 4 symbols','pn'),'4'=>__('show first 4 symbols and show last 4 symbols','pn')),
		'default' => $exchangebox->get_option('exchange','an3_hidden'),
		'name' => 'an3_hidden',
	);
	$options[] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options[] = array(
		'view' => 'user_func',
		'name' => 'check_new_user',
		'func_data' => array(),
		'func' => 'pn_newuser_option1',
	);	
	$options[] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['admin_mail'] = array(
		'view' => 'select',
		'title' => __('Send email notifications to admin if admin changes status of bid on his own','pn'),
		'options' => array('0'=>__('No','pn'),'1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('exchange','admin_mail'),
		'name' => 'admin_mail',
	);
	$options[] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['allow_dev'] = array(
		'view' => 'select',
		'title' => __('Allow to manage order using another browser','pn'),
		'options' => array('0'=>__('No','pn'),'1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('exchange','allow_dev'),
		'name' => 'allow_dev',
	);
	$options['ipuserhash'] = array(
		'view' => 'select',
		'title' => __('Forbid managing an order from another IP address','pn'),
		'options' => array('0'=>__('No','pn'),'1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('exchange','ipuserhash'),
		'name' => 'ipuserhash',
	); 
	$options[] = array(
		'view' => 'line',
		'colspan' => 2,
	);
	$options['autodelete'] = array(
		'view' => 'select',
		'title' => __('Automatic deletion of unpaid claims','pn'),
		'options' => array('0'=>__('No','pn'),'1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('exchange','autodelete'),
		'name' => 'autodelete',
	);
	$options['ad_h'] = array(
		'view' => 'input',
		'title' => __('How many hours?','pn'),
		'default' => $exchangebox->get_option('exchange','ad_h'),
		'name' => 'ad_h',
	);	
	$options['ad_m'] = array(
		'view' => 'input',
		'title' => __('How many minutes?','pn'),
		'default' => $exchangebox->get_option('exchange','ad_m'),
		'name' => 'ad_m',
	);	
	
	$form = new PremiumForm();
	$params_form = array(
		'filter' => 'pn_exchange_config_option',
		'method' => 'post',
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);
?>
<script type="text/javascript">
$(function(){
	$('#pn_techreg').change(function(){
		if($(this).val() == '0'){
			$('.techregtext').hide();	
		} else {
			$('.techregtext').show();
		}
	});
});
</script>
<?php	
}  

function pn_newuser_option1(){
	$check_new_user = get_option('check_new_user');
	if(!is_array($check_new_user)){ $check_new_user = array(); }
				
	$fields = array(
		'0'=> __('Account give','pn'),
		'1'=> __('Account get','pn'),
		'2'=> __('Phone','pn'),
		'3'=> __('Skype','pn'),
		'4'=> __('E-mail','pn'),
	);
	?>
	<tr>
		<th><?php _e('Checker for new user','pn'); ?></th>
		<td>
			<div class="premium_wrap_standart">
				<?php 
				if(is_array($fields)){
					foreach($fields as $key => $val){ 
					?>
						<div><label><input type="checkbox" name="check_new_user[]" <?php if(in_array($key,$check_new_user)){ ?>checked="checked"<?php } ?> value="<?php echo $key; ?>" /> <?php echo $val; ?></label></div>
					<?php 
					} 
				}
				?>							
			</div>
		</td>		
	</tr>				
	<?php				
}   

add_action('premium_action_pn_exchange_config','def_premium_action_pn_exchange_config');
function def_premium_action_pn_exchange_config(){
global $wpdb, $exchangebox;	

	only_post();
	pn_only_caps(array('administrator'));
	
	$form = new PremiumForm();

	$options = array('techreg_from', 'techregtext','techreg', 'gostnaphide', 'an1_hidden','an2_hidden','an3_hidden','autodelete','ad_h','ad_m','admin_mail', 'allow_dev','ipuserhash');
	foreach($options as $key){
		$val = pn_strip_input(is_param_post($key));
		$exchangebox->update_option('exchange',$key,$val);
	}	

	$check_new_user = is_param_post('check_new_user');
	update_option('check_new_user', $check_new_user);	
			
	do_action('pn_exchange_config_option_post');
	
	$url = admin_url('admin.php?page=pn_exchange_config&reply=true');
	$form->answer_form($url);
}