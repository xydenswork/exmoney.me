<?php
if( !defined( 'ABSPATH')){ exit(); }

/* news and pages */
add_action("admin_menu", "seo_metabox");
function seo_metabox($post_id) {
	if (function_exists("add_meta_box")) {
		$args = array('public' => true);
		$post_types = get_post_types($args,'names');
		if(is_array($post_types)){
			foreach($post_types as $pt){
				if($pt != 'attachment'){
					add_meta_box("pn_seo_id", __('Seo','pn'), "pn_seo_box", $pt, "normal");
				}
			}
		}
	}
}

function pn_seo_box($post){
	$post_id = $post->ID;
	
	$form = new PremiumForm();
			
	$seo_title = get_post_meta($post_id, 'seo_title', true); 
	$seo_key = get_post_meta($post_id, 'seo_key', true); 
	$seo_descr = get_post_meta($post_id, 'seo_descr', true);
	
	$atts_input = array();
	$atts_input['class'] = 'big_input';	
	?>
	<input type="hidden" name="pn_seo_box" value="1" />
		
		<p><strong><?php _e('Page title','pn'); ?></strong>
		<?php $form->input('seo_title' , $seo_title, $atts_input, 0); ?>
		</p>
		
		<p><strong><?php _e('Page keywords','pn'); ?></strong>
		<?php $form->textarea('seo_key', $seo_key, '', '50px', array(), 0); ?>
		</p>		
		
		<p><strong><?php _e('Page description','pn'); ?></strong>
		<?php $form->textarea('seo_descr', $seo_descr, '', '100px', array(), 0); ?>
		</p>		
	<?php
}

add_action("edit_post", "edit_post_seo");
function edit_post_seo($post_id){
	if(!current_user_can('edit_post', $post_id )){
		return $post_id;
	}
		
	if(isset($_POST['pn_seo_box'])){					
		$seo_title = pn_strip_input(is_param_post('seo_title'));
		update_post_meta($post_id, 'seo_title', $seo_title) or add_post_meta($post_id, 'seo_title', $seo_title, true);	
		
		$seo_key = pn_strip_input(is_param_post('seo_key'));
		update_post_meta($post_id, 'seo_key', $seo_key) or add_post_meta($post_id, 'seo_key', $seo_key, true);

		$seo_descr = pn_strip_input(is_param_post('seo_descr'));
		update_post_meta($post_id, 'seo_descr', $seo_descr) or add_post_meta($post_id, 'seo_descr', $seo_descr, true);			
	}
		
}
/* end news and pages */

/* category and tags */
add_action('init','all_posttypes_set_seo', 10000);
function all_posttypes_set_seo(){
	$taxonomies=get_taxonomies('','objects');
	if(is_array($taxonomies)){
		$not = array('nav_menu','link_category','post_format');
	    foreach($taxonomies as $tax){    
		    $name = $tax->name;
		    if(!in_array($name,$not)){	
				add_action($name . '_add_form_fields', 'add_form_fields_seo');
				add_action($name . '_edit_form', 'edit_form_fields_seo');
				add_action('edit_' . $name, 'edit_tags_seo');
				add_action('created_' . $name, 'edit_tags_seo');
			}
		}
	}
}
		 
function add_form_fields_seo($tag){
	$form = new PremiumForm();
	
	$atts_input = array();
	$atts_input['class'] = 'big_input';	
?>
	<input type="hidden" name="tag_seo_filter" value="1" />
		
	<div class="form-field term-name-wrap">
		<label><?php _e('Page title','pn'); ?></label>
		<?php $form->input('seo_title' , '', $atts_input, 0); ?>
	</div>
	<div class="form-field term-name-wrap">
		<label><?php _e('Page keywords','pn'); ?></label>
		<?php $form->textarea('seo_key', '', '', '50px', array(), 0); ?>
	</div>
	<div class="form-field term-name-wrap">
		<label><?php _e('Page description','pn'); ?></label>
		<?php $form->textarea('seo_descr', '', '', '100px', array(), 0); ?>
	</div>		
<?php 	
}

function edit_form_fields_seo($tag){
	$form = new PremiumForm();
	
	$term_id = $tag->term_id;
	$seo_title = get_term_meta($term_id, 'seo_title', true); 
	$seo_key = get_term_meta($term_id, 'seo_key', true); 
	$seo_descr = get_term_meta($term_id, 'seo_descr', true);
	
	$atts_input = array();
	$atts_input['class'] = 'big_input';	
?>
	<input type="hidden" name="tag_seo_filter" value="1" />
		
	<table class="form-table">
		<tr class="form-field term-name-wrap">
			<th scope="row"><label><?php _e('Page title','pn'); ?></label></th>
			<td>
				<?php $form->input('seo_title' , $seo_title, $atts_input, 0); ?>
			</td>
		</tr>
		<tr class="form-field term-name-wrap">
			<th scope="row"><label><?php _e('Page keywords','pn'); ?></label></th>
			<td>
				<?php $form->textarea('seo_key', $seo_key, '', '50px', array(), 0); ?>
			</td>
		</tr>
		<tr class="form-field term-name-wrap">
			<th scope="row"><label><?php _e('Page description','pn'); ?></label></th>
			<td>
				<?php $form->textarea('seo_descr', $seo_descr, '', '100px', array(), 0); ?>
			</td>
		</tr>		
	</table>
<?php	
} 

function edit_tags_seo($id){

	if(isset($_POST['tag_seo_filter'])){
			
		$seo_title = pn_strip_input(is_param_post('seo_title'));
		update_term_meta($id, 'seo_title', $seo_title);	
		
		$seo_key = pn_strip_input(is_param_post('seo_key'));
		update_term_meta($id, 'seo_key', $seo_key);

		$seo_descr = pn_strip_input(is_param_post('seo_descr'));
		update_term_meta($id, 'seo_descr', $seo_descr);
			
	}
	
}
/* end category and tags */

/* canonical */
remove_action('wp_head', 'rel_canonical');
add_action( 'wp_head', 'seo_rel_canonical');
function seo_rel_canonical() {
	if ( !is_singular() ){
		return;
	}	

	global $wp_the_query;
	if ( !$id = $wp_the_query->get_queried_object_id() ){
		return;
	}	

	if(is_exchange_page()){
		$site_value1 = get_query_var('valut1');
		$site_value2 = get_query_var('valut2');	
		$link = get_exchange_link($site_value1, $site_value2);
		echo "<link rel='canonical' href='$link' />\n";
	} else {
		if(!is_exchangestep_page()){
			$link = get_permalink( $id );
			echo "<link rel='canonical' href='$link' />\n";
		}	
	}
}

/* keywords */
add_filter('wp_head' , 'wp_head_seo');
function wp_head_seo() {
global $exchangebox;

	$key = '';
	$descr = '';
	
	$sitename = get_option('blogname');
	
	if(is_front_page()){
		$key = pn_strip_input($exchangebox->get_option('seo','home_key'));
		$descr = pn_strip_input($exchangebox->get_option('seo','home_descr'));		
	} elseif(is_home()){
		$key = pn_strip_input($exchangebox->get_option('seo','news_key'));
		$descr = pn_strip_input($exchangebox->get_option('seo','news_descr'));	
	} elseif(is_category() or is_tag()){
		
		$term_data = get_queried_object();
		$term_id = $term_data->term_id;
		
		$key = pn_strip_input(get_term_meta($term_id, 'seo_key', true)); 
		$descr = pn_strip_input(get_term_meta($term_id, 'seo_descr', true));

	} elseif(is_singular('post')){
		global $post;
		$post_id = intval($post->ID);
		$key = pn_strip_input(get_post_meta($post_id, 'seo_key', true)); 
		$descr = pn_strip_input(get_post_meta($post_id, 'seo_descr', true));
		
	} elseif(is_page()){		
		if(is_exchange_page()){
			global $naps_id;
			$naps_id = intval($naps_id);
			$key = pn_strip_input(get_naps_meta($naps_id, 'seo_key')); 
			$descr = pn_strip_input(get_naps_meta($naps_id, 'seo_descr'));					
		} else {
			global $post;
			$post_id = intval($post->ID);
			$key = pn_strip_input(get_post_meta($post_id, 'seo_key', true)); 
			$descr = pn_strip_input(get_post_meta($post_id, 'seo_descr', true));		
		}
	}
?><meta name="keywords" content="<?php echo $key; ?>" />
<meta name="description" content="<?php echo $descr; ?>" />
<?php	
}
/* end keywords */

/* title */
add_filter('wp_title' , 'wp_title_seo',99);
function wp_title_seo($title) {
global $wpdb, $exchangebox;	
	
	if(is_front_page()){
		$new_title = pn_strip_input($exchangebox->get_option('seo','home_title'));
		if($new_title){
			return $new_title;
		}
	} elseif(is_home()){
		$new_title = pn_strip_input($exchangebox->get_option('seo','news_title'));
		if($new_title){
			return $new_title;
		}		
	} elseif(is_singular('post')){
		global $post;
		
		$item_id = intval($post->ID);
		
		$seo_title = pn_strip_input(get_post_meta($item_id, 'seo_title', true));
		if($seo_title){
			return $seo_title;
		}
		
		$item_title = pn_strip_input($post->post_title);
		$news_temp = pn_strip_input($exchangebox->get_option('seo','news_temp'));
		$new_title = str_replace('[title]',$item_title,$news_temp);
		if($new_title){
			return $new_title;
		}
		
	} elseif(is_category()){
		
		global $cat;
		$item_id = intval($cat);
		
		$seo_title = pn_strip_input(get_term_meta($item_id, 'seo_title', true));
		if($seo_title){
			return $seo_title;
		}		
		
	} elseif(is_tag()){	
		
		global $tag_id;
		$item_id = intval($tag_id);
		
		$seo_title = pn_strip_input(get_term_meta($item_id, 'seo_title', true));
		if($seo_title){
			return $seo_title;
		}		
		
	} elseif(is_page()){	
	
		if(is_exchange_page()){
			global $naps_id, $naps_data;
			if($naps_id > 0){
				$item_id = intval($naps_id);
			
				$seo_title = pn_strip_input(get_naps_meta($item_id, 'seo_title'));
				if($seo_title){
					return $seo_title;
				}

				$item_title1 = pn_strip_input($naps_data->item1);
				$item_title2 = pn_strip_input($naps_data->item2);
				$new_title = pn_strip_input($exchangebox->get_option('seo','exch_temp'));
				$new_title = str_replace('[title1]',$item_title1,$new_title);
				$new_title = str_replace('[title2]',$item_title2,$new_title);
				if($new_title){
					return $new_title;
				}			
			}
		} elseif(is_exchangestep_page()){
		
			return get_exchangestep_title();
		
		} else {
			
			global $post;
			
			$item_id = intval($post->ID);
			
			$seo_title = pn_strip_input(get_post_meta($item_id, 'seo_title', true));
			if($seo_title){
				return $seo_title;
			}
			
			$item_title = pn_strip_input($post->post_title);
			$new_title = pn_strip_input($exchangebox->get_option('seo','page_temp'));
			$new_title = str_replace('[title]',$item_title,$new_title);
			if($new_title){
				return $new_title;
			}			
			
		}
	}		
				
	return $title;
} 
/* end title */	