<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_seo', 'pn_adminpage_title_pn_seo');
function pn_adminpage_title_pn_seo($page){
	_e('SEO','pn');
} 

add_action('pn_adminpage_content_pn_seo','def_pn_adminpage_content_pn_seo');
function def_pn_adminpage_content_pn_seo(){
global $exchangebox;	
	
	$form = new PremiumForm();
	
	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('Homepage','pn'),
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);	
	$options['home_title'] = array(
		'view' => 'inputbig',
		'title' => __('Page title','pn'),
		'default' => $exchangebox->get_option('seo','home_title'),
		'name' => 'home_title',
		'ml' => 1,
	);	
	$options['home_key'] = array( 
		'view' => 'textarea',
		'title' => __('Page keywords','pn'),
		'default' => $exchangebox->get_option('seo','home_key'),
		'name' => 'home_key',
		'width' => '',
		'height' => '50px',
		'ml' => 1,
	);
	$options['home_descr'] = array( 
		'view' => 'textarea',
		'title' => __('Page description','pn'),
		'default' => $exchangebox->get_option('seo','home_descr'),
		'name' => 'home_descr',
		'width' => '',
		'height' => '100px',
		'ml' => 1,
	);
	$options['news_h3'] = array(
		'view' => 'h3',
		'title' => __('News','pn'),
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);
	$options['news_title'] = array(
		'view' => 'inputbig',
		'title' => __('Page title','pn'),
		'default' => $exchangebox->get_option('seo','news_title'),
		'name' => 'news_title',
		'ml' => 1,
	);
	$options['news_key'] = array( 
		'view' => 'textarea',
		'title' => __('Page keywords','pn'),
		'default' => $exchangebox->get_option('seo','news_key'),
		'name' => 'news_key',
		'width' => '',
		'height' => '50px',
		'ml' => 1,
	);
	$options['news_descr'] = array( 
		'view' => 'textarea',
		'title' => __('Page description','pn'),
		'default' => $exchangebox->get_option('seo','news_descr'),
		'name' => 'news_descr',
		'width' => '',
		'height' => '100px',
		'ml' => 1,
	);	
	$options['news_title_h3'] = array(
		'view' => 'h3',
		'title' => __('News title template','pn'),
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);
	$options['news_temp'] = array(
		'view' => 'textareatags',
		'title' => __('Template','pn'),
		'default' => $exchangebox->get_option('seo','news_temp'),
		'tags' => array('title'=>__('Title','pn')),
		'width' => '',
		'height' => '50px',
		'prefix1' => '[',
		'prefix2' => ']',
		'name' => 'news_temp',
		'ml' => 1,
	);
	$options['pages_title_h3'] = array(
		'view' => 'h3',
		'title' => __('Pages title template','pn'),
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);				
	$options['page_temp'] = array(
		'view' => 'textareatags',
		'title' => __('Template','pn'),
		'default' => $exchangebox->get_option('seo','page_temp'),
		'tags' => array('title'=>__('Title','pn')),
		'width' => '',
		'height' => '50px',
		'prefix1' => '[',
		'prefix2' => ']',
		'name' => 'page_temp',
		'ml' => 1,
	);				
	$options['exchange_title_h3'] = array(
		'view' => 'h3',
		'title' => __('Exchange page title template (title)','pn'),
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);				
	$options['exch_temp'] = array(
		'view' => 'textareatags',
		'title' => __('Template','pn'),
		'default' => $exchangebox->get_option('seo','exch_temp'),
		'tags' => array('title1'=>__('Currency title 1','pn'), 'title2'=>__('Currency title 2','pn')),
		'width' => '',
		'height' => '50px',
		'prefix1' => '[',
		'prefix2' => ']',
		'name' => 'exch_temp',
		'ml' => 1,
	);								
	$params_form = array(
		'filter' => 'seo_changeform',
		'method' => 'post',
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);	
} 

add_action('premium_action_pn_seo','def_premium_action_pn_seo');
function def_premium_action_pn_seo(){
global $wpdb, $exchangebox;	

	only_post();
	pn_only_caps(array('administrator'));
	
	$form = new PremiumForm();
	
	$options = array(
		'home_title','home_key','home_descr','news_title','news_key','news_descr','news_temp','page_temp','exch_temp',
	);	
					
	foreach($options as $key){
		$val = pn_strip_input(is_param_post_ml($key));
		$exchangebox->update_option('seo',$key, $val);
	}				

	do_action('seo_changeform_post');
	
	$url = admin_url('admin.php?page=pn_seo&reply=true');
	$form->answer_form($url);
} 