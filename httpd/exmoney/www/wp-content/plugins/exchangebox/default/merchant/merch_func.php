<?php
if( !defined( 'ABSPATH')){ exit(); }

add_filter('list_merchants', 'def_list_merchants', 100);
function def_list_merchants($list){
	asort($list);
	return $list;
}

function is_enable_merchant($id){
	$merchants = get_option('merchants');
	if(!is_array($merchants)){ $merchants = array(); }
	
	return intval(is_isset($merchants,$id));
}

/* конструктор примечания */
function get_text_pay($m_id, $item, $pay_sum){
	if($m_id and isset($item->id)){
		$merch_data = get_option('merch_data');
		$data = is_isset($merch_data,$m_id);
		$text = trim(is_isset($data,'note'));
		
		$fio_arr = array($item->fname, $item->iname, $item->oname);
		$fio_arr = array_unique($fio_arr);
		$fio = pn_strip_input(join(' ',$fio_arr));
		
		$text = apply_filters('get_text_pay', $text, $m_id, $item);
		$text = str_replace('[id]',$item->id, $text);
		$text = str_replace('[sum1]', is_sum($item->summ1), $text);
		$text = str_replace('[valut1]', pn_strip_input($item->valut1) .' '. pn_strip_input($item->valut1type),$text);	
		$text = str_replace('[sum2]', is_sum($item->summz2),$text);
		$text = str_replace('[valut2]', pn_strip_input($item->valut2) .' '. pn_strip_input($item->valut2type),$text);
		$text = str_replace('[account_give]', pn_strip_input($item->schet1),$text);
		$text = str_replace('[account_get]', pn_strip_input($item->schet2),$text);
		$text = str_replace('[account]', pn_strip_input($item->schet2),$text);
		$text = str_replace('[fio]',$fio,$text);
		$text = str_replace('[last_name]',$item->fname,$text);
		$text = str_replace('[first_name]',$item->iname,$text);
		$text = str_replace('[second_name]',$item->oname,$text);		
		$text = str_replace('[ip]', pn_strip_input($item->userip),$text);
		$text = str_replace('[skype]', pn_strip_input($item->skype),$text);
		$text = str_replace('[phone]', pn_strip_input($item->tel),$text);
		$text = str_replace('[email]', is_email($item->email),$text);	
		$text = str_replace('[passport]', pn_strip_input($item->pnomer),$text);		
		
		return esc_attr(trim($text));
	}
}
	
/* автоматический вывод инструкции мерчанта */
add_action('instruction_merchant','def_instruction_merchant',1,2);
function def_instruction_merchant($instruction,$m_id){
	
	if($m_id and is_enable_merchant($m_id)){	
		$merch_data = get_option('merch_data');
		$data = is_isset($merch_data,$m_id); 
		$text = trim(is_isset($data,'text'));
		return $text;
	}
	
	return $instruction;
} 

function get_merch_data($m_id){
global $pn_merch_data;
	if(!is_array($pn_merch_data)){
		$pn_merch_data = (array)get_option('merch_data');
	}
	return is_isset($pn_merch_data,$m_id);
}

/* идентификатор */
function get_payment_id($arg){
	$id = intval(is_param_post($arg));
	if(!$id){ $id = intval(is_param_get($arg)); }
	return $id;
}

/* действие, при отказе от оплаты */
function the_merchant_bid_delete($id){
global $wpdb;
	$id = intval($id);	
	if($id){
		$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE id='$id' AND status != 'auto'");
		if(isset($item->id)){
			
			$hashed = is_bid_hash($item->hashed);
			$url = get_bids_url($hashed);
			wp_redirect($url);
			exit;		

		} else {
			pn_display_mess(__('You have refused payment','pn'));
		}
	} else {
		pn_display_mess(__('You have refused payment','pn'));
	}	
}

/* действие, при успешной оплате */
function the_merchant_bid_success($id){
global $wpdb;
	$id = intval($id);	
	if($id){
		$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE id='$id' AND status != 'auto'");
		if(isset($item->id)){
			
			$hashed = is_bid_hash($item->hashed);
			$url = get_bids_url($hashed);
			wp_redirect($url);
			exit;		

		} else {
			pn_display_mess(__('You have successfully paid','pn'),__('You have successfully paid','pn'),'true');
		}
	} else {
		pn_display_mess(__('You have successfully paid','pn'),__('You have successfully paid','pn'),'true');
	}	
}

/* помечаем заявку как оплаченную */
function the_merchant_bid_payed($id, $sum=0, $purse='', $naschet='', $payment_id='', $system='user'){
global $wpdb;

	$id = intval($id);
	$system = trim($system);
	if($system != 'system'){ $system = 'user'; }
	
	if($id){
		$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE id='$id' AND status IN('new','payed')");
		if(isset($item->id)){
			
			$purse = str_replace(' ','',pn_maxf(pn_strip_input($purse),500));
			$sum = is_sum($sum);
			$naschet = pn_maxf_mb(pn_strip_input($naschet),500);
			$payment_id = pn_maxf_mb(pn_strip_input($payment_id),500);
			$id = $item->id;
			
			$account = str_replace(' ','',$item->schet1);
			
			$arr = array(
				'editdate'=> current_time('mysql') 
			);			
			
			if($naschet){
				$arr['naschet'] = $naschet;
				$arr['naschet_h'] = pn_crypt_data($naschet);
			}
			
			if($purse and $account and $purse != $account){
				$arr['status'] = 'verify';
				
				$result = $wpdb->update($wpdb->prefix.'bids', $arr, array('id'=>$item->id));
				if($result == 1){
					do_action('change_bidstatus_all', 'verify', $item->id, $item, 'site', $system);
					do_action('change_bidstatus_verify', $item->id, $item, 'site', $system);	
				}
				
			} else {
				$arr['status'] = 'realpay';
				
				$result = $wpdb->update($wpdb->prefix.'bids', $arr, array('id'=>$item->id));
				if($result == 1){
					do_action('change_bidstatus_all', 'realpay', $item->id, $item, 'site', $system);
					do_action('change_bidstatus_realpay', $item->id, $item, 'site', $system); 
				}
			}
			
			update_bids_meta($item->id, 'pay_sum', $sum);
			update_bids_meta($item->id, 'pay_ac', $purse);			
		
		}
	}
}

function get_data_merchant_for_id($id, $item=''){
global $wpdb;	

    $id = intval($id);
	$array = array();
	$array['err'] = 0;
	$array['status'] = $array['summ'] = $array['valut'] = $array['vtype'] = $array['hashed'] = $array['m_id'] = '';
	
	if($id){
		if(!is_object($item)){
			$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE id='$id'");
		}
		if(isset($item->id)){
			
			$array['err'] = 0;
			$array['status'] = is_status_name($item->status);
			$array['summ'] = is_sum($item->summ1);
			$array['valut'] = $array['vtype'] = $item->valut1type;
			$array['hashed'] = is_bid_hash($item->hashed);
			
			$valut1i = $item->valut1i;
			$valut1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1i'");
			if(isset($valut1->xzt)){ 
				if(is_enable_merchant($valut1->xzt)){
					$array['m_id'] = $valut1->xzt;
				}
			}
	
		} else {
			$array['err'] = 2;	
		}
	} else {
		$array['err'] = 1;
	}
	
	return $array;
}

if(!class_exists('Merchant_ExchangeBox')){
	class Merchant_ExchangeBox{
		public $name = "";
		public $m_data = "";
		public $title = "";		
		
		function __construct($file, $map, $title)
		{
			$path = get_extension_file($file);
			$name = get_extension_name($path);
			$numeric = get_extension_num($name);

			$data = set_extension_data($path . '/dostup/index', $map);
			
			file_safe_include($path . '/class');
			
			$this->name = trim($name);
			$this->m_data = $data;
			$this->title = $title.' '.$numeric;
			
			add_filter('list_merchants',array($this, 'list_merchants'));
			add_filter('get_merchant_id',array($this, 'get_merchant_id'),1,3);
		}	
		
		public function list_merchants($list_merchants){
			$list_merchants[] = array(
				'id' => $this->name,
				'title' => $this->title .' ('. $this->name .')',
			);
			return $list_merchants;
		}

		public function get_merchant_id($now, $m_id, $item){
			if($m_id and $m_id == $this->name){
				if(is_enable_merchant($m_id)){
					return $m_id;
				} 
			}
			return $now;
		}			
	}
}