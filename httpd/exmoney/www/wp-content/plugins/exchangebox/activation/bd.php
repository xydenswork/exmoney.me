<?php
if( !defined( 'ABSPATH')){ exit(); }		
			
global $wpdb;
$prefix = $wpdb->prefix;

	$table_name = $wpdb->prefix ."pn_options";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		`meta_key` varchar(250) NOT NULL,
		`meta_key2` varchar(250) NOT NULL,
		`meta_value` longtext NOT NULL,
		PRIMARY KEY ( `id` ),
		INDEX (`meta_key`),
		INDEX (`meta_key2`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);	

	$table_name = $wpdb->prefix ."auth_logs";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		`auth_date` datetime NOT NULL,
		`user_id` bigint(20) NOT NULL,
		`user_login` varchar(250) NOT NULL,
		`old_user_ip` varchar(250) NOT NULL,
		`old_user_browser` varchar(250) NOT NULL,
		`now_user_ip` varchar(250) NOT NULL,
		`now_user_browser` varchar(250) NOT NULL,
		`auth_status` int(1) NOT NULL default '0',
		`auth_status_text` longtext NOT NULL,
		PRIMARY KEY ( `id` ),
		INDEX (`user_id`),
		INDEX (`auth_date`),
		INDEX (`auth_status`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);			
	
	/* users */
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'sec_lostpass'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `sec_lostpass` int(1) NOT NULL default '1'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'sec_login'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `sec_login` int(1) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'email_login'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `email_login` int(1) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'enable_ips'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `enable_ips` longtext NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'auto_login1'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `auto_login1` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'auto_login2'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `auto_login2` varchar(250) NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'user_browser'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `user_browser` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'user_ip'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `user_ip` varchar(250) NOT NULL");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'user_bann'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `user_bann` int(1) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'admin_comment'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `admin_comment` longtext NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'last_adminpanel'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `last_adminpanel` varchar(50) NOT NULL");
    }	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'user_discount'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `user_discount` varchar(50) NOT NULL default '0'");
    }	
	/* end users */
	
	/* archive */
	$table_name= $wpdb->prefix ."archive_data";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		`meta_key` varchar(250) NOT NULL,
		`meta_key2` varchar(250) NOT NULL,
		`meta_key3` varchar(250) NOT NULL,
		`item_id` bigint(20) NOT NULL default '0',
		`meta_value` varchar(20) NOT NULL default '0',
		PRIMARY KEY ( `id` ),
		INDEX (`meta_key`),
		INDEX (`meta_key2`),
		INDEX (`meta_key3`),
		INDEX (`meta_value`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);
	/* end archive */							
	
/*
коды валют

xname - значение
vncurs - внутренний курс за 1 доллар
parser - id парсера
nums - число
*/	
	
	$table_name= $wpdb->prefix ."vtypes";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`xname` longtext NOT NULL,
        `vncurs` varchar(50) NOT NULL default '0',
		`parser` bigint(20) NOT NULL default '0',
		`nums` varchar(50) NOT NULL default '0',
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);

	/* создаем коды валют */
	$vtypes = array('RUB','EUR','USD','UAH','AMD','KZT','GLD','BYN','UZS','BTC','TRY');
	if(is_array($vtypes)){
		foreach($vtypes as $type){
			$cc = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."vtypes WHERE xname='$type'");
			if($cc == 0){
				$wpdb->insert($wpdb->prefix ."vtypes", array('xname'=>$type, 'vncurs'=>'1'));
			}
		}
	}
	/* end создаем коды валют */		
	
	$table_name= $wpdb->prefix ."valuts_meta";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`item_id` bigint(20) NOT NULL default '0',
		`meta_key` longtext NOT NULL,
		`meta_value` longtext NOT NULL,
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);		
	
/*
валюты

vname - название ПС
vlogo - логотип
vtype - название кода валюты
firstzn - первые буквы
xname - значения для сайта
xml_value - значение для XML
valut_reserv - резерв (автосумма)
xzt - мерчант
rxzt - быстрая оплата
numleng - минимальное кол-во символов
maxnumleng - максимальное кол-во символов
helps - подсказка при заполнении
txt1 - название отдаете
show1 - выводить при отдаете
txt2 - название получаете
show2 - выводить при получаете
pvivod - вывод партнерских средств
vactive - активность валюты (1 - активна, 0 - не активна)
*/

	$table_name= $wpdb->prefix ."valuts";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`vname` longtext NOT NULL,
		`vlogo` longtext NOT NULL,
		`xname` longtext NOT NULL,
		`vtype` longtext NOT NULL,
		`numleng` int(5) NOT NULL default '0',
		`maxnumleng` int(5) NOT NULL default '100',
		`xzt` varchar(150) NOT NULL default '',
		`helps` longtext NOT NULL,
		`txt1` longtext NOT NULL,
		`txt2` longtext NOT NULL,
		`rxzt` int(5) NOT NULL default '0',
		`vactive` int(1) NOT NULL default '1',
		`pvivod` int(1) NOT NULL default '1',
		`show1` int(1) NOT NULL default '1',
		`show2` int(1) NOT NULL default '1',
		`xml_value` varchar(250) NOT NULL,
		`firstzn` varchar(20) NOT NULL,
		`valut_reserv` varchar(50) NOT NULL default '0',
		`site_order` bigint(20) NOT NULL default '0',
		`reserv_order` bigint(20) NOT NULL default '0',
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);

/* blacklist 

meta_key - ключ (0-счет, 1-e-mail, 2-телефон,3-скайп, 4-ip
meta_value - значение
*/	
	$table_name= $wpdb->prefix ."blacklist";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
        `meta_key` varchar(12) NOT NULL default '0',
		`meta_value` tinytext NOT NULL,
		`comment_text` longtext NOT NULL,
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);					

    $query = $wpdb->query("SHOW COLUMNS FROM ". $wpdb->prefix ."users LIKE 'ref_id'");
    if ($query == 0) {
        $wpdb->query("ALTER TABLE ". $wpdb->prefix ."users ADD `ref_id` bigint(20) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."users LIKE 'partner_pers'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."users ADD `partner_pers` varchar(10) NOT NULL default '0'");
    }	
	
	$table_name= $wpdb->prefix ."plinks";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`user_id` bigint(20) NOT NULL default '0',
		`user_login` varchar(250) NOT NULL,
		`pdate` datetime NOT NULL,
		`pbrowser` longtext NOT NULL,
		`pip` longtext NOT NULL,
		`prefer` longtext NOT NULL,
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);	

/* 	
partner_pers
*/	
	$table_name= $wpdb->prefix ."partner_pers";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
        `sumec` varchar(50) NOT NULL default '0',
		`pers` varchar(50) NOT NULL default '0',
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);		

/*
0 - в ожидании
1 - выполнена
2 - отказано
3 - отменена пользователем
*/		
	$table_name= $wpdb->prefix ."payoutuser";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`bdate` datetime NOT NULL,
		`userid` bigint(20) NOT NULL default '0',
		`user_login` varchar(250) NOT NULL,
		`bsumm` varchar(250) NOT NULL default '0',
		`valuts` varchar(250) NOT NULL,
		`valsid` bigint(20) NOT NULL default '0',
		`schet` varchar(250) NOT NULL,
		`bstatus` int(1) NOT NULL default '0',
		`bcomment` longtext NOT NULL,
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);			
	
	$table_name= $wpdb->prefix ."transactionrezerv";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`vdate` datetime NOT NULL,
		`valsid` bigint(20) NOT NULL default '0',
		`vsumm` varchar(250) NOT NULL default '0',
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);		
	
/*
Дополнительные поля валют
 
tech_name - техническое название
cf_name - название
vid - 0 текст, 1- select
cf_req - 0-не обязательно, 1-обязательно
minzn - мин.длинна
maxzn - макс длинна
firstzn - начальное значение
helps - подсказка
datas - если селект, то массив выборки
cf_hidden - видимость на сайте
valut_id - id валюты
place_id - 0 - и там и там, 1 - отдаете, 2 - получаете
uniqueid - идентификатор
*/	
	$table_name= $wpdb->prefix ."custom_fields_valut";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`tech_name` longtext NOT NULL,
		`cf_name` longtext NOT NULL,
		`vid` int(1) NOT NULL default '0',
		`valut_id` bigint(20) NOT NULL default '0',
		`cf_req` int(1) NOT NULL default '0',
		`place_id` int(1) NOT NULL default '0',
		`minzn` int(2) NOT NULL default '0',
		`maxzn` int(5) NOT NULL default '100',
		`firstzn` varchar(20) NOT NULL,	
		`uniqueid` varchar(250) NOT NULL,
		`helps` longtext NOT NULL,
		`datas` longtext NOT NULL,
		`status` int(2) NOT NULL default '1',
		`cf_hidden` int(2) NOT NULL default '0',
		`cf_order` bigint(20) NOT NULL default '0',
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);
	
/*
направления обменов

rorder - сортировка в колонке 1
rorder2 - сортировка в колонке 2
valsid1 - валюта отдаете
valsid2 - валюта получаете
curs1 - курс 1
curs2 - курс 2
parser - id парсера
parcommis1 - число к парсеру 1
parcommis2 - число к парсеру 2
prsum1 - сумма коммисси 1
prproc1 - процент коммисси 1
prsum2 - сумма коммисси 2
prproc2 - процент коммисси 2
minsumm1com - минимальная сумма комиссии
maxsumm1com - максимальная сумма комиссии
minsumm1 - Мин. сумма обмена (для Отдаю)
minsumm2 - Мин. сумма обмена (для Получаю)
maxsumm1 - Макс. сумма обмена (для Отдаю)
maxsumm2 - Макс. сумма обмена (для Получаю)
combox - доп.комиссия с отправителя сумма
comboxpers - доп.комиссия с отправителя процент
status - статус направления (1-активно, 0-неактивно)
delsk - скидка пользователей (0- есть, 1-отключена)
hidegost - статус гостей (0 - не скрывать, 1 - запретить, 2 - не скрывать, но запретить)
partmax - макс партнерский процент
parthide - (0-выплачивать, 1-не выплачивать)
*/		
	$table_name= $wpdb->prefix ."napobmens";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`rorder` bigint(20) NOT NULL default '0',
		`rorder2` bigint(20) NOT NULL default '0',
		`valsid1` bigint(20) NOT NULL default '0',
		`valsid2` bigint(20) NOT NULL default '0',
		`curs1` varchar(250) NOT NULL default '0',
		`curs2` varchar(250) NOT NULL default '0',
		`prproc1` varchar(250) NOT NULL default '0',
		`prproc2` varchar(250) NOT NULL default '0',
		`prsum1` varchar(250) NOT NULL default '0',
		`prsum2` varchar(250) NOT NULL default '0',		
		`maxsumm1com` varchar(250) NOT NULL default '0', 
		`maxsumm2com` varchar(250) NOT NULL default '0',
		`minsumm1com` varchar(50) NOT NULL default '0',  
		`minsumm2com` varchar(50) NOT NULL default '0',		
		`minsumm1` varchar(250) NOT NULL default '0',
		`minsumm2` varchar(250) NOT NULL default '0',
		`maxsumm1` varchar(250) NOT NULL default '0',
		`maxsumm2` varchar(250) NOT NULL default '0',		
		`combox` varchar(50) NOT NULL default '0',
		`comboxpers` varchar(50) NOT NULL default '0',		
		`parser` bigint(20) NOT NULL default '0',
		`parcommis1` varchar(250) NOT NULL default '0',
		`parcommis2` varchar(250) NOT NULL default '0',				
		`status` int(1) NOT NULL default '1',
		`delsk` int(1) NOT NULL default '0',
		`hidegost` int(1) NOT NULL default '0',
		`partmax` varchar(25) NOT NULL default '0',
		`parthide` int(1) NOT NULL default '0',
		`client_verify` int(1) NOT NULL default '0',
		`x19mod` bigint(20) NOT NULL default '0',
		`uf` longtext NOT NULL,
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);			
	
	$table_name = $wpdb->prefix ."naps_meta";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`item_id` bigint(20) NOT NULL default '0',
		`meta_key` longtext NOT NULL,
		`meta_value` longtext NOT NULL,
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);			
		
/*
Запрос резерва

zdate - дата запроса
email - e-mail
valid - id валюты
summk - сумма запроса
comment - комментарий
*/	
	$table_name = $wpdb->prefix ."zapros_rezerv";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		`zdate` datetime NOT NULL,
		`email` varchar(250) NOT NULL,
		`valid` bigint(20) NOT NULL default '0',
        `summk` varchar(250) NOT NULL default '0',
		`comment` longtext NOT NULL,
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);	
	
/*
Счета валют

text_comment - просто комментарий
valid - id валюты, он же шорткод
title - номер счета
visib - макс кол-во просмотров
prosm - текущее кол-во просмотров
*/	
	$table_name = $wpdb->prefix ."vschets";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		`valid` bigint(20) NOT NULL default '0',
        `title` varchar(250) NOT NULL default '0',
		`visib` int(1) NOT NULL default '0',
		`prosm` int(5) NOT NULL default '0',
		`text_comment` longtext NOT NULL,
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);		

/* обмены 

schet1 - счет отдаете
schet2 - счет получаете
partpr - партнерский процент
ref_sum - сумма партнера
pcalc - оплачено партнеру или нет (0-нет, 1-да)

user_hash - хэш пользователя
status:
	auto - автоматический статус
	new - новая заявка
	error - ошибка
	delete - удалена

	payed - юзер сказал что оплачена(я оплатил!)
	realpay - реально оплачена	
	verify - оплачена с другого кошелька

	success - выполнена
	returned - возвращена	
*/
	$table_name = $wpdb->prefix ."bids";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
		`schet1` varchar(250) NOT NULL,
		`schet2` varchar(250) NOT NULL,
		`schet1_hash` longtext NOT NULL,
		`schet2_hash` longtext NOT NULL,		
		`fname` longtext NOT NULL, 
		`iname` longtext NOT NULL,
		`oname` longtext NOT NULL,	
		`email` longtext NOT NULL,
		`tel` longtext NOT NULL,
		`skype` longtext NOT NULL,	
		`pnomer` longtext NOT NULL,
		`curs1` varchar(35) NOT NULL default '0',
		`curs2` varchar(35) NOT NULL default '0',		
		`summ1` varchar(35) NOT NULL default '0',
		`summ2` varchar(35) NOT NULL default '0',
		`summz1` varchar(35) NOT NULL default '0',
		`summz2` varchar(35) NOT NULL default '0',		
		`exsum` varchar(35) NOT NULL default '0',	
		`tpersent1` varchar(35) NOT NULL default '0',
		`tpersent2` varchar(35) NOT NULL default '0',
		`tcommis1` varchar(35) NOT NULL default '0',
		`tcommis2` varchar(35) NOT NULL default '0',
		`naschet` varchar(150) NOT NULL,		
		`naschet_h` varchar(250) NOT NULL,
		`naprid` bigint(20) NOT NULL,
		`hashed` longtext NOT NULL,
		`user_hash` varchar(150) NOT NULL,
		`valut1` longtext NOT NULL,
		`valut2` longtext NOT NULL,
		`valut1i` bigint(20) NOT NULL default '0',
		`valut2i` bigint(20) NOT NULL default '0',		
		`valut1type` longtext NOT NULL,
		`valut2type` longtext NOT NULL,
		`dmetas` longtext NOT NULL,
		`dop1` longtext NOT NULL,
		`dop2` longtext NOT NULL,
		`tdate` datetime NOT NULL,
		`editdate` datetime NOT NULL,
		`status` varchar(35) NOT NULL,
		`acomments` longtext NOT NULL,
		`ucomments` longtext NOT NULL,
		`user_id` bigint(20) NOT NULL default '0',
		`user_skidka` varchar(35) NOT NULL default '0',		
		`user_sksumm` varchar(35) NOT NULL default '0',
		`userip` longtext NOT NULL,
		`ref_id` bigint(20) NOT NULL default '0',	
		`partmax` varchar(25) NOT NULL default '0',
		`parthide` int(1) NOT NULL default '0',
		`partpr` varchar(25) NOT NULL default '0',
		`ref_sum` varchar(25) NOT NULL default '0',
		`p_sum` varchar(25) NOT NULL default '0',
		`pcalc` int(1) NOT NULL default '0',
		PRIMARY KEY ( `id` )
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);
	
	$table_name= $wpdb->prefix ."bids_meta";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`item_id` bigint(20) NOT NULL default '0',
		`meta_key` longtext NOT NULL,
		`meta_value` longtext NOT NULL,
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);						
	
	do_action('pn_bd_activated');	