<?php
if( !defined( 'ABSPATH')){ exit(); }

function sitemap_shortcode($atts, $content) {
global $wpdb, $post, $exchangebox;
	
	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);	
	
    $temp = '';
    $temp .= apply_filters('exb_before_sitemap_page','');
	
	if($exchangebox->get_option('htmlmap','pages') == 1){
	
		$temp .= '
		<div class="sitemapblock"><div class="sitemapblockvn">
			<div class="sitemaptitle">
				<div class="sitemaptitlevn">
					'. __('Pages','pn') .'
				</div>
			</div>
				<div class="clear"></div>
			
			<div class="simapbl">
				<div class="simapblvn">
					<ul class="smap">';

						$exclude_pages = $exchangebox->get_option('htmlmap','exclude_page');
						if(!is_array($exclude_pages)){ $exclude_pages = array(); } 
						$exclude = join(',',$exclude_pages);				
				
						$args = array(
							'post_type' => 'page',
							'posts_per_page' => '-1',
							'exclude' => $exclude
						);
						$mposts = get_posts($args);
						foreach($mposts as $mpos){ 
							$temp .= '<li><a href="'. get_permalink($mpos->ID) .'">'. pn_strip_input(ctv_ml($mpos->post_title)) .'</a></li>';
						}
					
				$temp .= '</ul>
				<div class="clear"></div>
			</div>
			</div>
			
		</div></div>';				
		
	}
		 
	if($exchangebox->get_option('htmlmap','exchanges') == 1){
			
		$techreg = get_technical_mode(); 
		if($techreg == 0 or $techreg == 2 or $techreg == 3){
			
			$ui = wp_get_current_user();
			$user_id = intval($ui->ID);
			
			$gostnaphide = intval($exchangebox->get_option('exchange','gostnaphide')); 
			if($gostnaphide != 1 or $gostnaphide == 1 and $user_id){ 
  
				$v = get_valuts_data();
				
				$where = get_naps_where('sm');
				$cc = $wpdb->get_var("SELECT COUNT(id) FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder ASC"); 
				if($cc > 0){
					$limit1 = ceil($cc / 2);
					$limit2 = $cc - $limit1;
					$inicio = $limit1;
				}  else {
					$limit1 = 0;
					$limit2 = 0;
				}
  
				$temp .= '
				<div class="sitemapblock"><div class="sitemapblockvn">
	
					<div class="sitemaptitle">
						<div class="sitemaptitlevn">
							'. __('Directions exchanges','pn') .'
						</div>
					</div>
						<div class="clear"></div>
		
					<div class="sitemapleft">
						<ul class="smap">';
							$obmens = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder ASC LIMIT $limit1");
							foreach($obmens as $obm){
								$temp .= '<li><a href="'. get_exchange_link($v[$obm->valsid1]->xname, $v[$obm->valsid2]->xname) .'">'. pn_strip_input($v[$obm->valsid1]->vname .' '. $v[$obm->valsid1]->vtype) .' <span class="sm_arr">»</span> '. pn_strip_input($v[$obm->valsid2]->vname .' '. $v[$obm->valsid2]->vtype) .'</a></li>';
							}
						$temp .= '</ul>
							<div class="clear"></div>			
					</div>
					<div class="sitemapright">
						<ul class="smap">';
							$obmens = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder ASC LIMIT $inicio, $limit2");
							foreach($obmens as $obm){
								$temp .= '<li><a href="'. get_exchange_link($v[$obm->valsid1]->xname, $v[$obm->valsid2]->xname) .'">'. pn_strip_input($v[$obm->valsid1]->vname .' '. $v[$obm->valsid1]->vtype) .' <span class="sm_arr">»</span> '. pn_strip_input($v[$obm->valsid2]->vname .' '. $v[$obm->valsid2]->vtype) .'</a></li>';
							}	
						$temp .= '</ul>
						<div class="clear"></div>		
					</div>		
						<div class="clear"></div>
					</div>
				</div>
				';
	
			}
		}			
			
	}		
		
	if($exchangebox->get_option('htmlmap','news') == 1){
	
		$temp .= '
		<div class="sitemapblock"><div class="sitemapblockvn">
	
			<div class="sitemaptitle">
				<div class="sitemaptitlevn">
					'. __('News','pn') .'
				</div>
			</div>
				<div class="clear"></div>	

			<div class="simapbl">
				<div class="simapblvn">
					<ul class="smap">';
					
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => '-1'
					);
					$mposts = get_posts($args);
					foreach($mposts as $mpos){ 
						$temp .= '<li><a href="'. get_permalink($mpos->ID) .'">'. pn_strip_input(ctv_ml($mpos->post_title)) .'</a></li>';
					}
				
			$temp .= '</ul>
			<div class="clear"></div>
			</div>
		</div>		
		
		</div></div>';		
		
	}					

    $temp .= apply_filters('exb_after_sitemap_page','');		
	
	return $temp;
}
add_shortcode('sitemap', 'sitemap_shortcode');
add_shortcode('exb_sitemap', 'sitemap_shortcode'); /* deprecated */