<?php
if( !defined( 'ABSPATH')){ exit(); }
	
/*
title: [en_US:]Individual rate[:en_US][ru_RU:]Свой курс ЦБ[:ru_RU]
description: [en_US:]Individual rate[:en_US][ru_RU:]Свой курс ЦБ[:ru_RU]
version: 1.1
category: [en_US:]Exchange directions[:en_US][ru_RU:]Направления обменов[:ru_RU]
cat: directions
*/	
	
$path = get_extension_file(__FILE__);
$name = get_extension_name($path);		
	
add_action('pn_moduls_active_'.$name, 'bd_pn_moduls_active_masschange');
add_action('pn_bd_activated', 'bd_pn_moduls_active_masschange');
function bd_pn_moduls_active_masschange(){
global $wpdb;	
		
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'masschange'");
    if ($query == 0) {
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `masschange` bigint(20) NOT NULL default '0'");
    }

	$table_name= $wpdb->prefix ."masschange";
    $sql = "CREATE TABLE IF NOT EXISTS $table_name(
		`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT ,
		`title` tinytext NOT NULL,
		`curs1` varchar(50) NOT NULL default '0',
		`curs2` varchar(50) NOT NULL default '0',
		PRIMARY KEY ( `id` )	
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	$wpdb->query($sql);
	
}	
	
add_action('admin_menu', 'pn_adminpage_masschange');
function pn_adminpage_masschange(){
global $exchangebox;
	
	$hook = add_menu_page( __('Individual rate','pn'), __('Individual rate','pn'), 'administrator', "pn_masschange", array($exchangebox, 'admin_temp'), $exchangebox->get_icon_link('cbr'));	
	add_action( "load-$hook", 'pn_trev_hook' );
	add_submenu_page("pn_masschange", __('Add rate','pn'), __('Add rate','pn'), 'administrator', "pn_add_masschange", array($exchangebox, 'admin_temp'));		
	
}

global $exchangebox;
$exchangebox->include_patch(__FILE__, 'add');
$exchangebox->include_patch(__FILE__, 'list');
$exchangebox->include_patch(__FILE__, 'filters');