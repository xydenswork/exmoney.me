<?php
if( !defined( 'ABSPATH')){ exit(); }

/*
title: [en_US:]Show Exchange direction settings in XML/TXT file[:en_US][ru_RU:]Настройка вывода направлений обмена в XML/TXT файле[:ru_RU]
description: [en_US:]Show Exchange direction settings in XML/TXT file[:en_US][ru_RU:]Настройка вывода направлений обмена в XML/TXT файле[:ru_RU]
version: 1.1
category: [en_US:]Exchange directions[:en_US][ru_RU:]Направления обменов[:ru_RU]
cat: directions
*/

$path = get_extension_file(__FILE__);
$name = get_extension_name($path);

/* BD */
add_action('pn_moduls_active_'.$name, 'bd_pn_moduls_active_napsfiles');
add_action('pn_bd_activated', 'bd_pn_moduls_active_napsfiles');
function bd_pn_moduls_active_napsfiles(){
global $wpdb;	
	
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'show_file'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `show_file` int(1) NOT NULL default '1'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'xml_city'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `xml_city` longtext NOT NULL");
    } 
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'xml_juridical'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `xml_juridical` int(1) NOT NULL default '0'");
    }
	$query = $wpdb->query("SHOW COLUMNS FROM ".$wpdb->prefix ."napobmens LIKE 'xml_param'");
    if ($query == 0){
		$wpdb->query("ALTER TABLE ".$wpdb->prefix ."napobmens ADD `xml_param` longtext NOT NULL");
    }	
	
}
/* end BD */

add_action('pn_adminpage_content_pn_naps','txtxml_pn_admin_content_pn_directions', 0);
function txtxml_pn_admin_content_pn_directions(){
global $exchangebox;
	
	$form = new PremiumForm();
	$hash = '';
	if($exchangebox->get_option('txtxml','hash') == 1){
		$hash = get_hash_cron('?');
	}
	$text = __('Links to files containing rates','pn').': <a href="'. get_site_url_or() .'/request-exporttxt.txt'. $hash .'" target="_blank">TXT</a> | <a href="'. get_site_url_or() .'/request-exportxml.xml'. $hash .'" target="_blank">XML</a>';
	$form->substrate($text);
	
}

add_action('admin_menu', 'pn_adminpage_txtxml');
function pn_adminpage_txtxml(){
global $exchangebox;	

	add_submenu_page("pn_config", __('TXT and XML export settings','pn'), __('TXT and XML export settings','pn'), 'administrator', "pn_txtxml", array($exchangebox, 'admin_temp'));
}

add_action('pn_adminpage_title_pn_txtxml', 'def_adminpage_title_pn_txtxml');
function def_adminpage_title_pn_txtxml($page){
	_e('TXT and XML export settings','pn');
} 

add_action('pn_adminpage_content_pn_txtxml','pn_adminpage_content_pn_txtxml');
function pn_adminpage_content_pn_txtxml(){
global $wpdb, $exchangebox;

	$form = new PremiumForm(); 

	$options = array();
	$options['top_title'] = array(
		'view' => 'h3',
		'title' => __('TXT and XML export settings','pn'),
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);
	$options['txt'] = array(
		'view' => 'select',
		'title' => __('TXT file','pn'),
		'options' => array('0'=>__('No','pn'), '1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('txtxml','txt'),
		'name' => 'txt',
	);	
	$options['line0'] = array(
		'view' => 'line',
		'colspan' => 2,
	);
	$options['xml'] = array(
		'view' => 'select',
		'title' => __('XML file','pn'),
		'options' => array('0'=>__('No','pn'), '1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('txtxml','xml'),
		'name' => 'xml',
	);	
	$options['line1'] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['hash'] = array(
		'view' => 'select',
		'title' => __('Add hash to XML/TXT files URL','pn'),
		'options' => array('0'=>__('No','pn'), '1'=>__('Yes','pn')),
		'default' => $exchangebox->get_option('txtxml','hash'),
		'name' => 'hash',
	);	
	$options['hash_help'] = array(
		'view' => 'help',
		'title' => __('More info','pn'),
		'default' => __('Personal hash is set in file /wp-content/plugins/exchangebox/userdata.php','pn'),
	);	
	$options['line2'] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options['decimal'] = array(
		'view' => 'input',
		'title' => __('Number of simbols after comma (forcibly)','pn'),
		'default' => $exchangebox->get_option('txtxml','decimal'),
		'name' => 'decimal',
	);	
	$params_form = array(
		'filter' => 'pn_txtxml_option',
		'method' => 'post',
		'button_title' => __('Save','pn'),
	);
	$form->init_form($params_form, $options);
	
} 

add_action('premium_action_pn_txtxml','premium_action_pn_txtxml');
function premium_action_pn_txtxml(){
global $wpdb, $exchangebox;	

	only_post();
	pn_only_caps(array('administrator'));
	
	$form = new PremiumForm();
	
	$options = array('txt','xml','decimal','hash');			
	foreach($options as $key){
		$val = intval(is_param_post($key));
		$exchangebox->update_option('txtxml',$key, $val);
	}				

	do_action('pn_txtxml_option_post');
	
	$url = admin_url('admin.php?page=pn_txtxml&reply=true');
	$form->answer_form($url);
}

add_action('myaction_request_exporttxt','def_myaction_request_exporttxt');
function def_myaction_request_exporttxt(){
global $wpdb, $exchangebox;	
	
	header("Content-type: text/txt; charset=utf-8");
	
	if($exchangebox->get_option('up_mode') == 1){
		_e('Maintenance','pn');
		exit;	
	}
	if(!check_hash_cron() and $exchangebox->get_option('txtxml','hash') == 1){	
		_e('Maintenance','pn');
		exit;	
	}
	$techreg = get_technical_mode();
	if($techreg == 3){
		die(get_technical_mode_text());
		exit;		
	}

	if($exchangebox->get_option('txtxml','txt') == 1){
			
		$decimal = intval($exchangebox->get_option('txtxml','decimal'));
				
		$v = get_valuts_data();

		$where = get_naps_where("files");
		$naps = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder ASC");
		foreach($naps as $ob){ 
			$output = apply_filters('show_txtxml_line', 1, $ob, 'txt');
			if($output == 1){
				$valid1 = $ob->valsid1;
				$valid2 = $ob->valsid2;
						
				if(isset($v[$valid1]) and isset($v[$valid2])){
					$vd1 = $v[$valid1];
					$vd2 = $v[$valid2];
					$decimal1 = $decimal2 = $decimal;

					echo is_xml_value($vd1->xml_value) .';'. is_xml_value($vd2->xml_value) .';'. is_sum($ob->curs1, $decimal1).';'.is_sum($ob->curs2, $decimal2).';'. is_sum($vd2->valut_reserv , $decimal2) .";\n";
				}
			}
		} 
			
		exit;
	} 

	_e('File containing rates disabled','pn');	
}

add_action('myaction_request_exportxml','def_myaction_request_exportxml');
function def_myaction_request_exportxml(){
global $wpdb, $exchangebox;
	header("Content-Type: text/xml; charset=utf-8");
	
	if($exchangebox->get_option('up_mode') != 1){
		if(check_hash_cron() or $exchangebox->get_option('txtxml','hash') != 1){
			$techreg = get_technical_mode();
			if($techreg == 0 or $techreg == 1 or $techreg == 2){
	?>
<?php echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
	<?php
		if($exchangebox->get_option('txtxml','xml') == 1){
			
			$decimal = intval($exchangebox->get_option('txtxml','decimal'));

			$v = get_valuts_data();

			$where = get_naps_where("files");
			$naps = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."napobmens WHERE $where ORDER BY rorder ASC");
	?>
	<rates>
	<?php		
			foreach($naps as $ob){	
				$output = apply_filters('show_txtxml_line', 1, $ob, 'xml');	
				if($output == 1){
					$valid1 = $ob->valsid1;
					$valid2 = $ob->valsid2;
					
					if(isset($v[$valid1]) and isset($v[$valid2])){
						$vd1 = $v[$valid1];
						$vd2 = $v[$valid2];
						
						$decimal1 = $decimal2 = $decimal;
						
						$lines = array();
						$lines['from'] = is_xml_value($vd1->xml_value);
						$lines['to'] = is_xml_value($vd2->xml_value);
						$lines['in'] = is_sum($ob->curs1, $decimal1);
						$lines['out'] = is_sum($ob->curs2, $decimal2);
						$lines['amount'] = is_sum($vd2->valut_reserv ,$decimal2);
						
						$tofee = array();
						if($ob->combox){
							$tofee[] = is_sum($ob->combox, $decimal2) . ' ' . pn_strip_input($vd2->vtype);
						} elseif($ob->comboxpers){ 
							$tofee[] = is_sum($ob->comboxpers, $decimal2). ' %';
						}						
						if(count($tofee) > 0){
							$lines['tofee'] = join(', ',$tofee);
						}
						
						if($ob->minsumm1 > 0){
							$lines['minamount_give'] = is_sum($ob->minsumm1, $decimal1).' '.pn_strip_input($vd1->vtype);
						}
						if($ob->minsumm2 > 0){
							$lines['minamount_get'] = is_sum($ob->minsumm2, $decimal2).' '.pn_strip_input($vd2->vtype);
						}						
						if($ob->maxsumm1 > 0){
							$lines['maxamount_give'] = is_sum($ob->maxsumm1, $decimal1).' '.pn_strip_input($vd1->vtype);
						}
						// if($ob->maxsumm2 > 0){
							// $lines['maxamount_get'] = is_sum($ob->maxsumm2, $decimal2).' '.pn_strip_input($vd2->vtype);
						// }
						
						$params = array();
						$xml_param = trim(is_isset($ob,'xml_param'));
						if($xml_param){
							$params[] = $xml_param;
						}
						
						$params[] = 'manual';
				
						$xml_juridical = intval(is_isset($ob,'xml_juridical'));
						if($xml_juridical){
							$params[] = 'juridical';
						}
						if(count($params) > 0){
							$lines['param'] = join(', ',$params);
						} 						
						$lines = apply_filters('file_xml_lines', $lines, $ob, $vd1, $vd2);

						$cities = pn_strip_input(is_isset($ob,'xml_city'));
						$num_item = 1;
						$cities_arr = array();
						if($cities){
							$cities_arr = explode(',',$cities);
							$num_item = count($cities_arr);
						}
						$r=0;
						while($r++<$num_item){
							$s = $r-1;
							$city = trim(is_isset($cities_arr,$s));
							if($city){
								$lines['city'] = $city;
							}						
		?>
			<item>
				<?php foreach($lines as $line_key => $line_value){ 
					$line_key = str_replace(array('minamount_give','minamount_get'),'minamount',$line_key);
					$line_key = str_replace(array('maxamount_give','maxamount_get'),'maxamount',$line_key);
				?>
				<<?php echo $line_key; ?>><?php echo $line_value; ?></<?php echo $line_key; ?>>	
				<?php } ?>
			</item>
		<?php		
						}
					}
				}
			}
	?>
	</rates>
	<?php	
			exit;
		}
	}	
	} 	
	}	
	?>
	<error><?php _e('File containing rates disabled','pn'); ?></error>	
	<?php
}


add_action('exb_napobmens_add_other', 'napsfiles_tab_direction_tab12', 1); 
function napsfiles_tab_direction_tab12($data){ 
?>
	<tr>
		<td colspan="3">
			<div class="premium_h3"><?php _e('XML files settings','pn'); ?></div>
			<div class="premium_h3submit">
				<input type="submit" name="" class="button" value="<?php _e('Save', 'pn'); ?>" />
			</div>
		</td>
	</tr>
	<tr>
		<th><?php _e('Show in file','pn'); ?></th>
		<td colspan="2">
			<div class="premium_wrap_standart">
				<select name="show_file" autocomplete="off">
					<?php 
					$show_file = is_isset($data, 'show_file'); 
					if(!is_numeric($show_file)){ $show_file = 1; }
					?>						
					<option value="1" <?php selected($show_file,1); ?>><?php _e('Yes','pn');?></option>
					<option value="0" <?php selected($show_file,0); ?>><?php _e('No','pn');?></option>						
				</select>
			</div>
		</td>
	</tr>
	<tr>
		<th><?php _e('City where exchanges with cash is available','pn'); ?></th>
		<td colspan="2">
			<div class="premium_wrap_standart">
				<input type="text" name="xml_city" style="width: 200px;" value="<?php echo pn_strip_input(is_isset($data, 'xml_city')); ?>" />
			</div>
		</td>
	</tr>
	<tr>
		<th><?php _e('Tags for parameter param','pn'); ?></th>
		<td colspan="2">
			<div class="premium_wrap_standart">
				<input type="text" name="xml_param" style="width: 200px;" value="<?php echo pn_strip_input(is_isset($data, 'xml_param')); ?>" />
			</div>
		</td>
	</tr>	
	<tr>
		<th><?php _e('Other options','pn'); ?></th>
		<td>
			<div class="premium_wrap_standart">
				<select name="xml_juridical" autocomplete="off">
					<?php 
						$xml_juridical = is_isset($data, 'xml_juridical'); 
					?>						
					<option value="0" <?php selected($xml_juridical,0); ?>><?php _e('Individual transfer','pn');?></option>
					<option value="1" <?php selected($xml_juridical,1); ?>><?php _e('Legal entity transfer','pn');?></option>
				</select>
			</div>
		</td>
		<td>
		</td>		
	</tr>	
<?php	
}

add_filter('pn_naps_addform_post', 'napsfiles_pn_direction_addform_post');
function napsfiles_pn_direction_addform_post($array){
	$array['show_file'] = intval(is_param_post('show_file'));
	$array['xml_city'] = pn_strip_input(is_param_post('xml_city'));
	$array['xml_param'] = pn_strip_input(is_param_post('xml_param'));
	$array['xml_juridical'] = intval(is_param_post('xml_juridical'));
	
	return $array;
}

add_filter('get_naps_where', 'napsfiles_get_directions_where', 10, 2);
function napsfiles_get_directions_where($where, $place){
	if($place == 'files'){
		$where .= "AND show_file = '1' ";	
	}
	return $where;
} 	