<?php
if( !defined( 'ABSPATH')){ exit(); }

add_action('pn_adminpage_title_pn_finstats', 'pn_admin_title_pn_finstats');
function pn_admin_title_pn_finstats(){
	_e('Financial statistics','pn');
}

add_action('pn_adminpage_content_pn_finstats','def_pn_admin_content_pn_finstats');
function def_pn_admin_content_pn_finstats(){
global $wpdb;
?>
<form action="<?php pn_the_link_post('finstats_form'); ?>" class="finstats_form" method="post">
	<div class="finfiletrs">
		<div class="fin_list">
			<div class="fin_label"><?php _e('Start date','pn'); ?></div>
			<input type="search" name="startdate" autocomplete="off" class="pn_datepicker" value="" />
		</div>
		<div class="fin_list">
			<div class="fin_label"><?php _e('End date','pn'); ?></div>
			<input type="search" name="enddate" autocomplete="off" class="pn_datepicker" value="" />
		</div>		
			<div class="premium_clear"></div>
			
		<?php
		$valuts = array();
		$vals = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."valuts ORDER BY site_order ASC");
		foreach($vals as $val){
			$valuts[$val->id] = pn_strip_input($val->vname .' '. $val->vtype);
		}		
		?>
					
		<div class="fin_list">
			<div class="fin_label"><?php _e('Currency name','pn'); ?></div>

			<select name="valut_id" autocomplete="off">
				<option value="0">--<?php _e('No item','pn'); ?>--</option>
				<?php foreach($valuts as $key => $currency){ ?>
					<option value="<?php echo $key; ?>"><?php echo $currency; ?></option>
				<?php } ?>
			</select>
		</div>

		<?php
		$vtype = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."vtypes ORDER BY xname ASC");
		?>		
		<div class="fin_list">
			<div class="fin_label"><?php _e('Currency code','pn'); ?></div>

			<select name="vtype_id" autocomplete="off">
				<option value="0">--<?php _e('No item','pn'); ?>--</option>
				<?php foreach($vtype as $item){ ?>
					<option value="<?php echo $item->id; ?>"><?php echo pn_strip_input($item->xname); ?></option>
				<?php } ?>
			</select>

		</div>		
			<div class="premium_clear"></div>
		
		<div class="fin_list">
			<div class="fin_label"><?php _e('Convert to','pn'); ?></div>

			<select name="convert" autocomplete="off">
				<option value="0">--<?php _e('not to convert','pn'); ?>--</option>
				<?php foreach($vtype as $item){ ?>
					<option value="<?php echo $item->id; ?>"><?php echo pn_strip_input($item->xname); ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="fin_list">
			<div class="fin_label"><?php _e('Individual Central Bank rate','pn'); ?></div>
			<input type="text" name="curs" value="" />
		</div>		
			<div class="premium_clear"></div>		
			
		<div class="fin_line"><label><input type="checkbox" name="share" value="1" /> <?php _e('multiplied by individual rate','pn'); ?></label></div>	
		<div class="fin_line"><label><input type="checkbox" name="ppay" value="1" /> <?php _e('consider affiliate payments','pn'); ?></label></div>
		<div class="fin_line"><label><input type="checkbox" name="trans" value="1" /> <?php _e('consider corrections of reserve','pn'); ?></label></div>
			
		<input type="submit" name="submit" class="finstat_link" value="<?php _e('Display statistics','pn'); ?>" />
		<div class="finstat_ajax"></div>
			
			<div class="premium_clear"></div>
	</div>
</form>

<div id="finres"></div>

<script type="text/javascript">
jQuery(function($){
	
	$('.finstats_form').ajaxForm({
	    dataType:  'json',
        beforeSubmit: function(a,f,o) {
			
			$('.finstat_link').attr('disabled',true);
		    $('.finstat_ajax').show();
			
        },
		error: function(res, res2, res3){
			<?php do_action('pn_js_error_response'); ?>
		},		
        success: function(res) {
			
			$('.finstat_link').attr('disabled',false);
		    $('.finstat_ajax').hide();
			
			if(res['status'] == 'error'){
				<?php do_action('pn_js_alert_response'); ?>
			} else if(res['status'] == 'success') {
				$('#finres').html(res['table']);
			}
        }
    });
	
});
</script>	
	
<?php
} 

add_action('premium_action_finstats_form', 'pn_premium_action_finstats_form');
function pn_premium_action_finstats_form(){
global $wpdb;

	only_post();
	
	$log = array();
	$log['status'] = 'success';
	$log['response'] = '';
	$log['status_code'] = 0; 
	$log['status_text'] = '';

	$ui = wp_get_current_user();
	$user_id = intval($ui->ID);
	if(current_user_can('administrator')){
		
		$where1 = $where2 = $where3 = $where4 = '';		
		
		$pr = $wpdb->prefix;
		
		$startdate = is_my_date(is_param_post('startdate'));
		if($startdate){
			$startdate = get_mydate($startdate,'Y-m-d 00:00');
			$where1 .= " AND tdate >= '$startdate'";
			$where2 .= " AND tdate >= '$startdate'";
			$where3 .= " AND bdate >= '$startdate'";
			$where4 .= " AND vdate >= '$startdate'";
		}
		$enddate = is_my_date(is_param_post('enddate'));
		if($enddate){
			$enddate = get_mydate($enddate,'Y-m-d 00:00');
			$where1 .= " AND tdate <= '$enddate'";
			$where2 .= " AND tdate <= '$enddate'";
			$where3 .= " AND bdate <= '$enddate'";
			$where4 .= " AND vdate <= '$enddate'";
		}	

		$type = 1;
		$vtype_convert = '';
		
		$valut_id = intval(is_param_post('valut_id'));
		if($valut_id){
			$data = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut_id'");
			if(isset($data->id)){
				$where1 .= " AND valut1i = '$valut_id'";
				$where2 .= " AND valut2i = '$valut_id'";
				$where3 .= " AND valsid = '$valut_id'";
				$where4 .= " AND valsid = '$valut_id'";	
				$vtype_convert = $data->vtype;
			}
		}
		
		$vtype_id = intval(is_param_post('vtype_id'));
		if($vtype_id){
			$data = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."vtypes WHERE id='$vtype_id'");
			if(isset($data->id)){
				$type = 0;
				$vtype_convert = $data->xname;
				$where1 .= " AND valut1type = '$vtype_convert'";
				$where2 .= " AND valut1type = '$vtype_convert'";
			}
		}	
		
		if(!$vtype_convert){
			$log['table'] = '<div class="finresults">'. __('You choose the type of currency or currency','pn') .'</div>';
			echo json_encode($log);	
			exit;
		}

		$c_oper = 0;
		$bou = 0;
		$ac_bou = 0;
		$sol = 0;
		$ac_sol = 0;
		$profit = 0;		
		$ac_profit = 0;
		
		$c_oper = $wpdb->get_var("SELECT COUNT(id) FROM ". $wpdb->prefix ."bids WHERE status IN('payed','realpay','success','verify') $where1 OR status IN('success') $where2");
		
		$bou_c = $wpdb->get_var("SELECT SUM(summ1) FROM ".$wpdb->prefix."bids WHERE status IN('payed','realpay','success','verify') $where1");
		$bou = $bou + $bou_c;
		
		$ac_bou_c = $wpdb->get_var("SELECT SUM(summz1) FROM ".$wpdb->prefix."bids WHERE status IN('payed','realpay','success','verify') $where1");
		$ac_bou = $ac_bou + $ac_bou_c;		
		
		$sol_c = $wpdb->get_var("SELECT SUM(summ2) FROM ".$wpdb->prefix."bids WHERE status = 'success' $where2");
		$sol = $sol + $sol_c;
		
		$ac_sol_c = $wpdb->get_var("SELECT SUM(summz2) FROM ".$wpdb->prefix."bids WHERE status = 'success' $where2");
		$ac_sol = $ac_sol + $ac_sol_c;

		$ppay = intval(is_param_post('ppay'));
		if($ppay == 1 and $type == 1){
			$partn = $wpdb->get_var("SELECT SUM(bsum) FROM ".$wpdb->prefix."payoutuser WHERE bstatus = '1' $where3");
			$bou = $bou - $partn;
			$ac_bou = $ac_bou - $partn;
		}
		
		$trans = intval(is_param_post('trans'));
		if($trans == 1 and $type == 1){
			$tr = $wpdb->get_var("SELECT SUM(vsumm) FROM ".$wpdb->prefix."transactionrezerv WHERE id > 0 $where4");
			$bou = $bou + $tr;
			$ac_bou = $ac_bou + $tr;
		}			
		
		$convert = intval(is_param_post('convert'));
		$curs = is_sum(is_param_post('curs'));
		if($convert){ 
			$data = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."vtypes WHERE id='$convert'");
			if(isset($data->id)){
				$bou = convert_sum($bou, $vtype_convert, $data->xname);
				$sol = convert_sum($sol, $vtype_convert, $data->xname);
				$ac_bou = convert_sum($ac_bou, $vtype_convert, $data->xname);
				$ac_sol = convert_sum($ac_sol, $vtype_convert, $data->xname);
				$vtype_convert = $data->xname;
			}
		} elseif($curs > 0){

			$share = intval(is_param_post('share'));
		
			$bou = convert_mycurs($bou, $curs, $share);
			$sol = convert_mycurs($sol, $curs, $share);
			$ac_bou = convert_mycurs($ac_bou, $curs, $share);
			$ac_sol = convert_mycurs($ac_sol, $curs, $share);			
			
			$vtype_convert = 'S';
		}
		
		$profit = $bou - $sol;
		$ac_profit = $ac_bou - $ac_sol;		
		
		$profit = get_sum_color($profit);		
		$ac_profit = get_sum_color($ac_profit);
		
		$table = '
		<div class="finresults">
			<div class="finline"><strong>'. __('Total exchange operations','pn') .'</strong>: '. $c_oper .'</div>
			<div class="finline"><strong>'. __('Bought','pn') .'</strong>: '. $bou .' '. $vtype_convert .'</div>
			<div class="finline"><strong>'. __('Actually bought','pn') .'</strong>: '. $ac_bou .' '. $vtype_convert .'</div>
			<div class="finline"><strong>'. __('Sold','pn') .'</strong>: '. $sol .' '. $vtype_convert .'</div>
			<div class="finline"><strong>'. __('Actually sold','pn') .'</strong>: '. $ac_sol .' '. $vtype_convert .'</div>
			<div class="finline"><strong>'. __('Profit','pn') .'</strong>: '. $profit .' '. $vtype_convert .'</div>
			<div class="finline"><strong>'. __('Actually profit','pn') .'</strong>: '. $ac_profit .' '. $vtype_convert .'</div>
		</div>		
		';
		
		$log['table'] = $table;
	} else {
		$log['status'] = 'error';
		$log['status_code'] = 1;
		$log['status_text'] = __('You do not have permission','pn');
	}	
	
	echo json_encode($log);	
	exit;
}