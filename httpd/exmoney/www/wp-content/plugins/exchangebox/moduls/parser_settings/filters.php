<?php
if( !defined( 'ABSPATH')){ exit(); }

/* currency codes */
add_filter('pn_vtypes_addform', 'parser_pn_currency_code_addform', 10, 2);
function parser_pn_currency_code_addform($options, $data){
		
	$options[] = array(
		'view' => 'line',
		'colspan' => 2,
	);	
	$options[] = array(
		'view' => 'h3',
		'title' => '',
		'submit' => __('Save','pn'),
		'colspan' => 2,
	);
	$parsers = array();
	$parsers[0] = '-- '. __('No item','pn') .' --';
	$en_parsers = array();
	if(function_exists('get_list_parsers')){
		$en_parsers = get_list_parsers();
	}
	if(is_array($en_parsers)){
		foreach($en_parsers as $key => $val){
			$parsers[$key] = $val['title'];
		}
	}
	$options['parser'] = array(
		'view' => 'select',
		'title' => __('Automatic change of rate','pn'),
		'options' => $parsers,
		'default' => is_isset($data, 'parser'),
		'name' => 'parser',
		'work' => 'input',
	);	
	$options['nums'] = array(
		'view' => 'inputbig',
		'title' => __('Add to rate','pn'),
		'default' => is_isset($data, 'nums'),
		'name' => 'nums',
	);	
	
	return $options;
}

add_filter('pn_vtypes_addform_post', 'parser_pn_currency_code_addform_post');
function parser_pn_currency_code_addform_post($array){
	
	$array['parser'] = intval(is_param_post('parser'));
	$array['nums'] = pn_parser_num(is_param_post('nums'));
	
	return $array;
}

add_action('pn_vtypes_edit','parser_pn_currency_code_edit', 10, 2);
add_action('pn_vtypes_add','parser_pn_currency_code_edit', 10, 2);
function parser_pn_currency_code_edit($data_id, $array){
	if($data_id){
		update_vtypes_to_parser($data_id);		
	}	
}

add_filter('vtypes_manage_ap_columns', 'parser_currency_codes_manage_ap_columns');
function parser_currency_codes_manage_ap_columns($columns){
	$columns['parser'] = __('Rate automatic adjustment','pn');
	return $columns;
}

add_filter('vtypes_manage_ap_col', 'parser_currency_codes_manage_ap_col', 10, 3);
function parser_currency_codes_manage_ap_col($html, $column_name, $item){
	
	if($column_name == 'parser'){	
		$en_parsers = array();
		if(function_exists('get_list_parsers')){
			$en_parsers = get_list_parsers();
		}
		$html = '
		<div style="width: 200px;">
		';
			$html = '
			<select name="parser['. $item->id .']" autocomplete="off" id="currency_code_parser_'. $item->id .'" class="currency_code_parser" style="width: 200px; display: block; margin: 0 0 10px;"> 
			';
				$enable = 0;
				$html .= '<option value="0" '. selected($item->parser,0,false) .'>-- '. __('No item','pn') .' --</option>';
				if(is_array($en_parsers)){
					foreach($en_parsers as $parser_key => $parser_data){
						if($item->parser == $parser_key){
							$enable = 1;
						}
							
						$html .= '<option value="'. $parser_key .'" '. selected($item->parser,$parser_key,false) .'>'. is_isset($parser_data,'title') .'</option>';
					}
				}
			$style = 'style="display: none;"';	
			if($enable == 1){
				$style = '';
			}
				$html .= '
					</select>
					<div id="the_currency_code_parser_'. $item->id .'" '. $style .'>
						<input type="text" name="nums['. $item->id .']" value="'. pn_strip_input($item->nums) .'" />
					</div>		
				';
		$html .= '</div>';	
	}
	
	return $html;
}

add_action('pn_vtypes_save','parser_pn_currency_codes_save');
function parser_pn_currency_codes_save(){
global $wpdb;
	
	$work_parser = 0;
	if(isset($_POST['parser']) and is_array($_POST['parser'])){	
		foreach($_POST['parser'] as $id => $parser_id){		
			$id = intval($id);
			$parser = intval($parser_id);
			$parser_actions = pn_parser_num($_POST['nums'][$id]);							
					
			$array = array();
			if($parser > 0){
				$work_parser = 1;
				$array['parser'] = $parser;
				$array['nums'] = $parser_actions;			
			} else {
				$array['parser'] = 0;
				$array['nums'] = 0;										
			}	
			$wpdb->update($wpdb->prefix.'vtypes', $array, array('id'=>$id));		
		}		
	}	
	if($work_parser == 1){
		update_vtypes_to_parser();
	}
}

add_action('pn_adminpage_content_pn_vtypes','js_pn_admin_content_pn_currency_codes');
function js_pn_admin_content_pn_currency_codes(){
?>
	<style>
	.column-parser{ width: 200px!important; }
	</style>	
	<script type="text/javascript">
	$(function(){
		$('.currency_code_parser').on('change', function(){
			var id = $(this).attr('id').replace('currency_code_parser_','');
			var vale = $(this).val();
			if(vale > 0){
				$('#the_currency_code_parser_'+id).show();
			} else {
				$('#the_currency_code_parser_'+id).hide();
			}
		});		
	});
	</script>
<?php	
}
/* end currency codes */

/* directions */
add_action('pn_adminpage_content_pn_naps', 'parser_pn_adminpage_content_pn_directions');
function parser_pn_adminpage_content_pn_directions(){
?>	
<style>
.column-parser{ width: 230px!important; }
</style>
<script type="text/javascript">
jQuery(function($){
	$('.directions_parser').change(function(){
		var id = $(this).attr('id').replace('directions_parser_','');
		var vale = $(this).val();
		if(vale > 0){
			$('#the_directions_parser_'+id).show();
			$('#the_directions_masschange_'+id).hide();
			$('#directions_masschange_'+id).val('0');
		} else {
			$('#the_directions_parser_'+id).hide();
		}
	});			
});
</script>
<?php
}

add_filter('naps_manage_ap_columns', 'parser_directions_manage_ap_columns');
function parser_directions_manage_ap_columns($columns){
	
	$new_columns = array();
	foreach($columns as $k => $v){
		$new_columns[$k] = $v;
		if($k == 'course2'){
			$new_columns['parser'] = __('Auto adjust rate','pn');
		}
	}
	
	return $new_columns;
}

add_action('pn_naps_save', 'parser_pn_directions_save');
function parser_pn_directions_save(){
global $wpdb;	
	
	$work_parser = 1;
	
	if(isset($_POST['parser']) and is_array($_POST['parser'])){		
		foreach($_POST['parser'] as $id => $parser_id){
						
			$id = intval($id);
			$parser = intval($parser_id);
			$parcommis1 = pn_parser_num($_POST['parcommis1'][$id]);			
			$parcommis2 = pn_parser_num($_POST['parcommis2'][$id]);
						
			$array = array();
			if($parser > 0){
				$work_parser = 1;
				$array['parser'] = $parser;
				$array['parcommis1'] = $parcommis1;			
				$array['parcommis2'] = $parcommis2;
			} else {
				$array['parser'] = 0;
				$array['parcommis1'] = 0;			
				$array['parcommis2'] = 0;							
			}
								
			$wpdb->update($wpdb->prefix.'napobmens', $array, array('id'=>$id));
						
		}			
	}	
	if($work_parser == 1){
		update_naps_to_parser();
	}
}

add_filter('naps_manage_ap_col', 'parser_directions_manage_ap_col', 10, 3);
function parser_directions_manage_ap_col($show, $column_name, $item){
	if($column_name == 'parser'){
		$en_parsers = get_list_parsers('work','[para] [[birg]]');			
			
		$html = '
		<div style="width: 230px;">
		';
			
		$html .= '
		<select name="parser['. $item->id .']" autocomplete="off" id="directions_parser_'. $item->id .'" class="directions_parser" style="width: 230px; display: block; margin: 0 0 10px;"> 
		';
			$enable = 0;
				$html .= '<option value="0" '. selected($item->parser,0,false) .'>-- '. __('No item','pn') .' --</option>';
			if(is_array($en_parsers)){
				foreach($en_parsers as $parser_key => $parser_data){
					if($item->parser == $parser_key){
						$enable = 1;
					}
						
					$html .= '<option value="'. $parser_key .'" '. selected($item->parser,$parser_key,false) .'>'. $parser_data['title'] .'</option>';
				}
			}
				
		$style = 'style="display: none;"';	
		if($enable == 1){
			$style = '';
		}
				
		$html .= '
		</select>
			
		<div id="the_directions_parser_'. $item->id .'" '. $style .'>
			<input type="text" name="parcommis1['. $item->id .']" style="width: 95px; float: left; margin: 0px 0px 0 0;" value="'. pn_strip_input($item->parcommis1) .'" />
			<div style="float: left; margin: 3px 2px 0 2px;">=></div>
			<input type="text" name="parcommis2['. $item->id .']" style="width: 95px; float: left; margin: 0px 0px 0 0;" value="'. pn_strip_input($item->parcommis2) .'" />				
				<div class="premium_clear"></div>
		</div>		
		';
			
		$html .= '</div>';
			return $html;
	}
	
	return $show;
}

add_action('exb_napobmens_add_rate', 'parser_tab_direction_tab3', 1);
function parser_tab_direction_tab3($data){

	$parsers = array();
	$parsers[0] = '-- '. __('No item','pn') .' --';
	$en_parsers = get_list_parsers('work','[para] [[birg]]');
	if(is_array($en_parsers)){
		foreach($en_parsers as $key => $val){
			$parsers[$key] = $val['title'];
		}
	}
	?>
	<tr>
		<th><?php _e('Auto adjust rate','pn'); ?></th>
		<td colspan="2">
			<div class="premium_wrap_standart">
				<select name="parser" id="the_parser_select" autocomplete="off"> 
					<?php foreach($parsers as $parser_key => $parser_title){ ?>
						<option value="<?php echo $parser_key; ?>" <?php selected(is_isset($data, 'parser'),$parser_key,true); ?>><?php echo $parser_title; ?></option>
					<?php } ?>
				</select>
			</div>
		</td>
	</tr>
	<tr>
		<th><?php _e('Add to rate','pn'); ?></th>
		<td>
			<div class="premium_wrap_standart">
				<input type="text" name="parcommis1" value="<?php echo pn_strip_input(is_isset($data, 'parcommis1'));?>" />										
			</div>			
		</td>
		<td>
			<div class="premium_wrap_standart">
				<input type="text" name="parcommis2" value="<?php echo pn_strip_input(is_isset($data, 'parcommis2'));?>" />											
			</div>	
		</td>
	</tr>
	<script type="text/javascript">
	jQuery(function(){
		$('#the_parser_select').change(function(){
			$('#the_masschange_select').val('0');
		});
	});
	</script>	
<?php	
}

add_filter('pn_naps_addform_post', 'parser_pn_direction_addform_post');
function parser_pn_direction_addform_post($array){

	$array['parser'] = $parser = intval(is_param_post('parser'));
	if($parser > 0){
		$array['parcommis1'] = pn_parser_num(is_param_post('parcommis1'));			
		$array['parcommis2'] = pn_parser_num(is_param_post('parcommis2'));
	} else {
		$array['parcommis1'] = 0;			
		$array['parcommis2'] = 0;				
	}	
	
	return $array;
}

add_action('pn_naps_edit', 'parser_pn_direction_edit',1,2);
add_action('pn_naps_add', 'parser_pn_direction_edit',1,2);
function parser_pn_direction_edit($data_id, $array){
	if($data_id){
		$parser = intval(is_param_post('parser'));
		if($parser > 0 and function_exists('update_naps_to_parser')){
			update_naps_to_parser($data_id);
		}
	}	
}
/* end directions */

/* best */
add_action('pn_adminpage_content_bcb_bc_adjs','parser_pn_admin_content_pn_bc_adjs');
function parser_pn_admin_content_pn_bc_adjs(){
?>	
<style>
.column-parser{ width: 230px!important; }
</style>
<script type="text/javascript">
jQuery(function($){
	$('.bcadjs_parser').change(function(){
		var id = $(this).attr('id').replace('bcadjs_parser_','');
		var vale = $(this).val();
		if(vale > 0){
			$('#the_bcadjs_parser_'+id).show();
		} else {
			$('#the_bcadjs_parser_'+id).hide();
		}
	});			
});
</script>
<?php
}

add_filter('bcadjs_manage_ap_columns', 'parser_bcadjs_manage_ap_columns');
function parser_bcadjs_manage_ap_columns($columns){
	
	$new_columns = array();
	foreach($columns as $k => $v){
		$new_columns[$k] = $v;
		if($k == 'giveget'){
			$new_columns['parser'] = __('Auto adjust rate','pn');
		}
	}
	
	return $new_columns;
}

add_action('pn_bcadjs_save', 'parser_pn_bcadjs_save');
function parser_pn_bcadjs_save(){
global $wpdb;	
	
	if(isset($_POST['standart_parser']) and is_array($_POST['standart_parser'])){ 		
		foreach($_POST['standart_parser'] as $id => $parser_id){
						
			$id = intval($id);
			$parser = intval($parser_id);
			$standart_parser_actions_give = bcb_parser_num($_POST['standart_parser_actions_give'][$id]);			
			$standart_parser_actions_get = bcb_parser_num($_POST['standart_parser_actions_get'][$id]);
						
			$array = array();
			if($parser > 0){
				$array['parser'] = $parser;
				$array['nums1'] = $standart_parser_actions_give;			
				$array['nums2'] = $standart_parser_actions_get;
			} else {
				$array['parser'] = 0;
				$array['nums1'] = 0;			
				$array['nums2'] = 0;							
			}
								
			$wpdb->update($wpdb->prefix.'bcbroker_naps', $array, array('id'=>$id));
						
		}			
	}	

}

add_filter('bcadjs_manage_ap_col', 'parser_bcadjs_manage_ap_col', 10, 3);
function parser_bcadjs_manage_ap_col($show, $column_name, $item){
	if($column_name == 'parser'){
		$en_parsers = get_list_parsers('work','[para] [[birg]]');			
			
		$html = '
		<div style="width: 230px;">
		';
			
		$html .= '
		<select name="standart_parser['. $item->id .']" autocomplete="off" id="bcadjs_parser_'. $item->id .'" class="bcadjs_parser" style="width: 230px; display: block; margin: 0 0 10px;"> 
		';
			$enable = 0;
				$html .= '<option value="0" '. selected($item->parser,0,false) .'>-- '. __('No item','pn') .' --</option>';
			if(is_array($en_parsers)){
				foreach($en_parsers as $parser_key => $parser_data){
					if($item->parser == $parser_key){
						$enable = 1;
					}
						
					$html .= '<option value="'. $parser_key .'" '. selected($item->parser,$parser_key,false) .'>'. $parser_data['title'] .'</option>';
				}
			}
				
		$style = 'style="display: none;"';	
		if($enable == 1){
			$style = '';
		}
				
		$html .= '
		</select>
			
		<div id="the_bcadjs_parser_'. $item->id .'" '. $style .'>
			<input type="text" name="standart_parser_actions_give['. $item->id .']" style="width: 95px; float: left; margin: 0px 0px 0 0;" value="'. pn_strip_input($item->nums1) .'" />
			<div style="float: left; margin: 3px 2px 0 2px;">=></div>
			<input type="text" name="standart_parser_actions_get['. $item->id .']" style="width: 95px; float: left; margin: 0px 0px 0 0;" value="'. pn_strip_input($item->nums2) .'" />				
				<div class="premium_clear"></div>
		</div>		
		';
			
		$html .= '</div>';
			return $html;
	}
	
	return $show;
}

add_filter('pn_bcadjs_addform', 'parser_pn_bcadjs_addform', 10, 2);
function parser_pn_bcadjs_addform($options, $data){
	
	$parsers = array();
	$parsers[0] = '-- '. __('No item','pn') .' --';
	$en_parsers = array();
	if(function_exists('get_list_parsers')){
		$en_parsers = get_list_parsers();
	}
	if(is_array($en_parsers)){
		foreach($en_parsers as $key => $val){
			$parsers[$key] = $val['title'];
		}
	}	
	$new_options = array();
	foreach($options as $opt_key => $opt_val){
		$new_options[$opt_key] = $opt_val;
		if($opt_key == 'min_sum'){
			$new_options['minsum_parser_l1'] = array(
				'view' => 'line',
				'colspan' => 2,
			);			
			$new_options['minsum_parser'] = array(
				'view' => 'select',
				'title' => __('Auto adjust for min rate','pn'),
				'options' => $parsers,
				'default' => is_isset($data, 'minsum_parser'),
				'name' => 'minsum_parser',
				'work' => 'input',
			);	
			$new_options['minsum_parser_actions'] = array(
				'view' => 'inputbig',
				'title' => __('Add to min rate','pn'),
				'default' => is_isset($data, 'minsum_parser_actions'),
				'name' => 'minsum_parser_actions',
			);
			// $new_options['minsum_parser_l2'] = array(
				// 'view' => 'line',
				// 'colspan' => 2,
			// );			
		}
		if($opt_key == 'max_sum'){
			$new_options['maxsum_parser_l1'] = array(
				'view' => 'line',
				'colspan' => 2,
			);			
			$new_options['maxsum_parser'] = array(
				'view' => 'select',
				'title' => __('Auto adjust for max rate','pn'),
				'options' => $parsers,
				'default' => is_isset($data, 'maxsum_parser'),
				'name' => 'maxsum_parser',
				'work' => 'input',
			);	
			$new_options['maxsum_parser_actions'] = array(
				'view' => 'inputbig',
				'title' => __('Add to max rate','pn'),
				'default' => is_isset($data, 'maxsum_parser_actions'),
				'name' => 'maxsum_parser_actions',
			);
			// $new_options['maxsum_parser_l2'] = array(
				// 'view' => 'line',
				// 'colspan' => 2,
			// );			
		}
		if($opt_key == 'line4'){			
			$new_options['parser'] = array(
				'view' => 'select',
				'title' => __('Automatic change of rate','pn'),
				'options' => $parsers,
				'default' => is_isset($data, 'parser'),
				'name' => 'parser',
				'work' => 'input',
			);	
			$new_options['nums1'] = array(
				'view' => 'inputbig',
				'title' => __('Add to Send rate','pn'),
				'default' => is_isset($data, 'nums1'),
				'name' => 'nums1',
			);
			$new_options['nums2'] = array(
				'view' => 'inputbig',
				'title' => __('Add to Receive rate','pn'),
				'default' => is_isset($data, 'nums2'),
				'name' => 'nums2',
			);
			// $new_options['standart_parser_l1'] = array(
				// 'view' => 'line',
				// 'colspan' => 2,
			// );			
		}
	}
	
	return $new_options;
} 

add_filter('pn_bcadjs_addform_post', 'parser_pn_bcadjs_addform_post');
function parser_pn_bcadjs_addform_post($array){
	
	$array['parser'] = intval(is_param_post('parser'));
	$array['nums1'] = bcb_parser_num(is_param_post('nums1'));
	$array['nums2'] = bcb_parser_num(is_param_post('nums2'));
	$array['minsum_parser'] = intval(is_param_post('minsum_parser'));
	$array['minsum_parser_actions'] = bcb_parser_num(is_param_post('minsum_parser_actions'));
	$array['maxsum_parser'] = intval(is_param_post('maxsum_parser'));
	$array['maxsum_parser_actions'] = bcb_parser_num(is_param_post('maxsum_parser_actions'));
	
	return $array;
}
 
add_action('tab_bcbroker_min_sum', 'parser_tab_bcbroker_min_sum', 10, 2);
function parser_tab_bcbroker_min_sum($data, $broker){
	$parsers = array();
	$parsers[0] = '-- '. __('No item','pn') .' --';
	$en_parsers = get_list_parsers('work','[para] [[birg]]');
	if(is_array($en_parsers)){
		foreach($en_parsers as $key => $val){
			$parsers[$key] = $val['title'];
		}
	}
	?>
	<div class="premium_wrap_standart">
		<select name="bcadjs_minsum_parser" autocomplete="off"> 
			<?php foreach($parsers as $parser_key => $parser_title){ ?>
				<option value="<?php echo $parser_key; ?>" <?php selected(is_isset($broker, 'minsum_parser'),$parser_key,true); ?>><?php echo $parser_title; ?></option>
			<?php } ?>
		</select>
	</div>
	<div class="premium_wrap_standart">
		<input type="text" name="bcadjs_minsum_parser_actions" value="<?php echo pn_strip_input(is_isset($broker, 'minsum_parser_actions'));?>" /> <?php _e('Add to rate','pn'); ?>										
	</div>				
<?php	
}

add_action('tab_bcbroker_max_sum', 'parser_tab_bcbroker_max_sum', 10, 2);
function parser_tab_bcbroker_max_sum($data, $broker){
	$parsers = array();
	$parsers[0] = '-- '. __('No item','pn') .' --';
	$en_parsers = get_list_parsers('work','[para] [[birg]]');
	if(is_array($en_parsers)){
		foreach($en_parsers as $key => $val){
			$parsers[$key] = $val['title'];
		}
	}
	?>
	<div class="premium_wrap_standart">
		<select name="bcadjs_maxsum_parser" autocomplete="off"> 
			<?php foreach($parsers as $parser_key => $parser_title){ ?>
				<option value="<?php echo $parser_key; ?>" <?php selected(is_isset($broker, 'maxsum_parser'),$parser_key,true); ?>><?php echo $parser_title; ?></option>
			<?php } ?>
		</select>
	</div>
	<div class="premium_wrap_standart">
		<input type="text" name="bcadjs_maxsum_parser_actions" value="<?php echo pn_strip_input(is_isset($broker, 'maxsum_parser_actions'));?>" /> <?php _e('Add to rate','pn'); ?>									
	</div>				
<?php	
}

add_action('tab_bcbroker_standart_course', 'parser_tab_bcbroker_standart_course', 10, 2);
function parser_tab_bcbroker_standart_course($data, $broker){

	$parsers = array();
	$parsers[0] = '-- '. __('No item','pn') .' --';
	$en_parsers = get_list_parsers('work','[para] [[birg]]');
	if(is_array($en_parsers)){
		foreach($en_parsers as $key => $val){
			$parsers[$key] = $val['title'];
		}
	}
	?>
	<tr>
		<th><?php _e('Auto adjust rate','pn'); ?></th>
		<td>
				<div class="premium_wrap_standart">
					<select name="bcadjs_standart_parser" autocomplete="off"> 
						<?php foreach($parsers as $parser_key => $parser_title){ ?>
							<option value="<?php echo $parser_key; ?>" <?php selected(is_isset($broker, 'parser'),$parser_key,true); ?>><?php echo $parser_title; ?></option>
						<?php } ?>
					</select>
				</div>
		</td>
		<td></td>
	</tr>
	<tr>
		<th><?php _e('Add to rate','pn'); ?></th>
		<td>
			<div class="premium_wrap_standart">
				<input type="text" name="bcadjs_standart_parser_actions_give" value="<?php echo pn_strip_input(is_isset($broker, 'nums1'));?>" />										
			</div>			
		</td>
		<td>
			<div class="premium_wrap_standart">
				<input type="text" name="bcadjs_standart_parser_actions_get" value="<?php echo pn_strip_input(is_isset($broker, 'nums2'));?>" />											
			</div>	
		</td>
	</tr>	
<?php	
}

add_filter('pn_bcadjs_tab_addform_post', 'parser_pn_bcadjs_tab_addform_post');
function parser_pn_bcadjs_tab_addform_post($array){
	
	$array['parser'] = intval(is_param_post('bcadjs_standart_parser'));
	$array['nums1'] = pn_parser_num(is_param_post('bcadjs_standart_parser_actions_give'));
	$array['nums2'] = pn_parser_num(is_param_post('bcadjs_standart_parser_actions_get'));
	$array['minsum_parser'] = intval(is_param_post('bcadjs_minsum_parser'));
	$array['minsum_parser_actions'] = pn_parser_num(is_param_post('bcadjs_minsum_parser_actions'));
	$array['maxsum_parser'] = intval(is_param_post('bcadjs_maxsum_parser'));
	$array['maxsum_parser_actions'] = pn_parser_num(is_param_post('bcadjs_maxsum_parser_actions'));
	
	return $array;
}

add_filter('bcparser_def_course', 'parser_bcparser_def_course', 10, 6);
function parser_bcparser_def_course($darr, $item, $options, $direction, $vd1, $vd2){
	$curs_parser = get_option('curs_parser');
	if(!is_array($curs_parser)){ $curs_parser = array(); }
	
	$name_column = intval($item->name_column);
	$partofrate = intval(is_isset($options,'partofrate'));
	$conversion = intval(is_isset($options,'conversion'));	
	
	$minsum_parser = intval($item->minsum_parser);
	$minsum_parser_actions = pn_strip_input($item->minsum_parser_actions);
	if($minsum_parser > 0){
		$curs_data = is_isset($curs_parser,$minsum_parser);
		$curs1 = is_sum(is_isset($curs_data,'curs1'));
		$curs2 = is_sum(is_isset($curs_data,'curs2'));
		$curs = 0;
		if($curs1 and $curs2){
			if($name_column == 0 and $partofrate == 1){
				if($conversion == 0){
					$curs = is_sum($curs1/$curs2);
				} else {
					$curs = is_sum($curs1);
				}
			} else {
				if($conversion == 0){
					$curs = is_sum($curs2/$curs1);
				} else {
					$curs = is_sum($curs2);
				}
			}
		}		
		$ncurs = plus_persent_curs($curs, $minsum_parser_actions);				
		if($ncurs > 0){
			$darr['min_sum'] = $ncurs;
		}
	}

	$maxsum_parser = intval($item->maxsum_parser);
	$maxsum_parser_actions = pn_strip_input($item->maxsum_parser_actions);
	if($maxsum_parser > 0){
		$curs_data = is_isset($curs_parser, $maxsum_parser);
		$curs1 = is_sum(is_isset($curs_data,'curs1'));
		$curs2 = is_sum(is_isset($curs_data,'curs2'));
		$curs = 0;
		if($curs1 and $curs2){
			if($name_column == 0 and $partofrate == 1){
				if($conversion == 0){
					$curs = is_sum($curs1/$curs2);
				} else {
					$curs = is_sum($curs1);
				}
			} else {
				if($conversion == 0){
					$curs = is_sum($curs2/$curs1);
				} else {
					$curs = is_sum($curs2);
				}
			}
		}		
		$ncurs = plus_persent_curs($curs, $maxsum_parser_actions);				
		if($ncurs > 0){
			$darr['max_sum'] = $ncurs;
		}
	}	
	
	$standart_parser = intval($item->parser);
	$standart_parser_actions_give = pn_strip_input($item->nums1);
	$standart_parser_actions_get = pn_strip_input($item->nums2);
	if($standart_parser > 0){
		$curs_data = is_isset($curs_parser,$standart_parser);
		$curs1 = is_sum(is_isset($curs_data,'curs1'));
		$curs2 = is_sum(is_isset($curs_data,'curs2'));	
		$n_course_give = is_sum(plus_persent_curs($curs1, $standart_parser_actions_give));
		$n_course_get = is_sum(plus_persent_curs($curs2, $standart_parser_actions_get));				
		if($n_course_give > 0 and $n_course_get > 0){
			$darr['standart_course_give'] = $n_course_give;
			$darr['standart_course_get'] = $n_course_get;
		}
	}				
	
	return $darr;
}
/* end best */