<?php
if( !defined( 'ABSPATH')){ exit(); }

function parser_upload_data(){	
global $wpdb;

	$work_parser = get_option('work_parser');
	if(!is_array($work_parser)){ $work_parser = array(); }
	
	$config_parser = get_option('config_parser');
	if(!is_array($config_parser)){ $config_parser = array(); }
	
	$curs_parser = get_option('curs_parser');
	if(!is_array($curs_parser)){ $curs_parser = array(); }
	
	update_option('lcurs_parser', $curs_parser);
	
	$parsers = apply_filters('get_pn_parser', array());
	
	$time_parser = current_time('timestamp'); 

	/* CBR */
	$time = $time_parser + (24*60*60);
 	$date = date('d.m.Y', $time);
	if(
		is_isset($work_parser,1) == 1 or is_isset($work_parser,3) == 1 or 
		is_isset($work_parser,5) == 1 or is_isset($work_parser,7) == 1 or 
		is_isset($work_parser,8) == 1 or is_isset($work_parser,10) == 1 or
		is_isset($work_parser,11) == 1 or is_isset($work_parser,12) == 1
	){

		$curl = get_curl_parser('http://www.cbr.ru/scripts/XML_daily.asp?date_req='.$date, '', 'parser', 'cbr');
		if(is_array($curl) and !$curl['err'] and strstr($curl['output'],'<?xml')){
			$string = $curl['output'];
			$res = @simplexml_load_string($string);
			if(is_object($res)){
				if(isset($res->Valute)){
					$valuts = $res->Valute;
					foreach($valuts as $v_obj){
					
						$CharCode = (string)$v_obj->CharCode;
						$CharCode = trim($CharCode);
					
						if($CharCode == 'USD'){
					
							$on1 = (string)$v_obj->Value;
							$on1 = is_sum($on1);
							$zn = (string)$v_obj->Nominal;
							
							if($on1 > 0){
								$curs1 = def_parser_curs($parsers, '1', 1);
								$curs_parser[1]['curs1'] = $curs1; // USD
								$curs_parser[1]['curs2'] = is_sum($on1 / $zn * $curs1); // RUB
								
								$curs1 = def_parser_curs($parsers, '2', 1000);
								$curs_parser[2]['curs1'] = $curs1; // RUB
								$curs_parser[2]['curs2'] = is_sum($curs1 / $on1 * $zn); // USD							
							}				
						
						}
						
						if($CharCode == 'EUR'){
					
							$on1 = (string)$v_obj->Value;
							$on1 = is_sum($on1);
							$zn = (string)$v_obj->Nominal;
							
							if($on1 > 0){
								$curs1 = def_parser_curs($parsers, '3', 1);
								$curs_parser[3]['curs1'] = $curs1; // EUR
								$curs_parser[3]['curs2'] = is_sum($on1 / $zn * $curs1); // RUB
								
								$curs1 = def_parser_curs($parsers, '4', 1000);
								$curs_parser[4]['curs1'] = $curs1; // RUB
								$curs_parser[4]['curs2'] = is_sum($curs1 / $on1 * $zn); // EUR							
							}				
						
						}

						if($CharCode == 'UAH'){
					
							$on1 = (string)$v_obj->Value;
							$on1 = is_sum($on1);
							$zn = (string)$v_obj->Nominal;
							
							if($on1 > 0){
								$curs1 = def_parser_curs($parsers, '5', 100);
								$curs_parser[5]['curs1'] = $curs1; // UAH
								$curs_parser[5]['curs2'] = is_sum($on1 / $zn * $curs1); // RUB
								
								$curs1 = def_parser_curs($parsers, '6', 100);							
								$curs_parser[6]['curs1'] = $curs1; // RUB
								$curs_parser[6]['curs2'] = is_sum($curs1 / $on1 * $zn); // UAH	
							}				
						
						}

						if($CharCode == 'KZT'){
					
							$on1 = (string)$v_obj->Value;
							$on1 = is_sum($on1);
							$zn = (string)$v_obj->Nominal;
							
							if($on1 > 0){ 
								$curs1 = def_parser_curs($parsers, '7', 100);
								$curs_parser[7]['curs1'] = $curs1; // KZT
								$curs_parser[7]['curs2'] = is_sum($on1 / $zn * $curs1); // RUB
							}				
						
						}

						if($CharCode == 'AMD'){
					
							$on1 = (string)$v_obj->Value;
							$on1 = is_sum($on1);
							$zn = (string)$v_obj->Nominal;
							
							if($on1 > 0){ //1000 => 7480.77441
								$curs1 = def_parser_curs($parsers, '8', 100);							
								$curs_parser[8]['curs1'] = $curs1; // AMD
								$curs_parser[8]['curs2'] = is_sum($on1 / $zn * $curs1); // RUB
								
								$curs1 = def_parser_curs($parsers, '9', 1000);
								$curs_parser[9]['curs1'] = $curs1; // RUB
								$curs_parser[9]['curs2'] = is_sum($curs1 / $on1 * $zn); // AMD	
							}				
						
						}

						if($CharCode == 'BYN'){
					
							$on1 = (string)$v_obj->Value;
							$on1 = is_sum($on1);
							$zn = (string)$v_obj->Nominal;
							
							if($on1 > 0){
								$curs1 = def_parser_curs($parsers, '10', 1);								
								$curs_parser[10]['curs1'] = $curs1; // BYN
								$curs_parser[10]['curs2'] = is_sum($on1 / $zn * $curs1); // RUB
							}				
						
						}

						if($CharCode == 'CNY'){
					
							$on1 = (string)$v_obj->Value;
							$on1 = is_sum($on1);
							$zn = (string)$v_obj->Nominal;
							
							if($on1 > 0){ 
								$curs1 = def_parser_curs($parsers, '11', 1);							
								$curs_parser[11]['curs1'] = $curs1; // CNY
								$curs_parser[11]['curs2'] = is_sum($on1 / $zn * $curs1); // RUB
								
								$curs1 = def_parser_curs($parsers, '12', 1);
								$curs_parser[12]['curs1'] = $curs1; // RUB
								$curs_parser[12]['curs2'] = is_sum($curs1 / $on1 * $zn); // CNY	
							}				
						
						}						
					}
				}
			}
		}
	}  
	/* end CBR */
	
	/* ECB */
 	if(is_isset($work_parser,51) == 1 or is_isset($work_parser,52) == 1){
		$curl = get_curl_parser('http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml', '', 'parser', 'ecb');
		if(is_array($curl) and !$curl['err'] and strstr($curl['output'],'<?xml')){		
			$string = $curl['output'];
			$res = @simplexml_load_string($string);
			if(is_object($res) and isset($res->Cube)){
				$on1 = $res->Cube->Cube->Cube[0]['rate'];
				$on1 = (string)$on1;
				$on1 = is_sum($on1);
				if($on1 > 0){
					
					$curs1 = def_parser_curs($parsers, '51', 1);	
					$curs_parser[51]['curs1'] = $curs1; // EUR
					$curs_parser[51]['curs2'] = is_sum($on1 * $curs1); // USD

					$curs1 = def_parser_curs($parsers, '52', 1);	
					$curs_parser[52]['curs1'] = $curs1; // USD
					$curs_parser[52]['curs2'] = is_sum($curs1 / $on1); // EUR				

				}			
			}
		}
	} 
	/* end ECB */
	
	/* privatbank */
  	if(is_isset($work_parser,101) == 1 or is_isset($work_parser,103) == 1){
		$curl = get_curl_parser('https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=3', '', 'parser', 'privatbank');
		if(is_array($curl) and !$curl['err'] and strstr($curl['output'],'<exchangerates')){		
			$string = $curl['output'];
			$res = @simplexml_load_string($string);
			if(is_object($res) and isset($res->row)){
				
				foreach($res->row as $val){
					$v_data = (array)$val->exchangerate;
					$ccy = $v_data['@attributes']['ccy'];
					if($ccy == 'USD'){
						
						$key = trim(is_isset($config_parser,101));
						if($key != 'sale'){ $key='buy'; }
						$usduah = (string)$v_data['@attributes'][$key];
						
						$curs1 = def_parser_curs($parsers, '101', 100);	
						$curs_parser[101]['curs1'] = $curs1; // USD
						$curs_parser[101]['curs2'] = is_sum($usduah * $curs1); // UAH
				
						$curs1 = def_parser_curs($parsers, '102', 1000);					
						$curs_parser[102]['curs1'] = $curs1; // UAH
						$curs_parser[102]['curs2'] = is_sum($curs1 / $usduah); // USD							

					} elseif($ccy == 'EUR'){
					
						$key = trim(is_isset($config_parser,103));
						if($key != 'sale'){ $key='buy'; }			
						$euruah = (string)$v_data['@attributes'][$key];
						
						$curs1 = def_parser_curs($parsers, '103', 100);	
						$curs_parser[103]['curs1'] = $curs1; // EUR
						$curs_parser[103]['curs2'] = is_sum($euruah * $curs1); // UAH
				
						$curs1 = def_parser_curs($parsers, '104', 1000);	
						$curs_parser[104]['curs1'] = $curs1; // UAH
						$curs_parser[104]['curs2'] = is_sum($curs1 / $euruah); // EUR
					
					} elseif($ccy == 'RUR'){
					
						$key = trim(is_isset($config_parser,103));
						if($key != 'sale'){ $key='buy'; }			
						$euruah = (string)$v_data['@attributes'][$key];
						
						$curs1 = def_parser_curs($parsers, '103', 100);	
						$curs_parser[103]['curs1'] = $curs1; // EUR
						$curs_parser[103]['curs2'] = is_sum($euruah * $curs1); // UAH
				
						$curs1 = def_parser_curs($parsers, '104', 1000);	
						$curs_parser[104]['curs1'] = $curs1; // UAH
						$curs_parser[104]['curs2'] = is_sum($curs1 / $euruah); // EUR					
					
					}
				}				
										
			}
		}
	} 
  	if(is_isset($work_parser,105) == 1 or is_isset($work_parser,107) == 1){
		$curl = get_curl_parser('https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5', '', 'parser', 'privatbank');
		if(is_array($curl) and !$curl['err'] and strstr($curl['output'],'<exchangerates')){		
			$string = $curl['output'];
			$res = @simplexml_load_string($string);
			if(is_object($res) and isset($res->row)){
				
				foreach($res->row as $val){
					$v_data = (array)$val->exchangerate;
					$ccy = $v_data['@attributes']['ccy'];
					if($ccy == 'USD'){
						
						$key = trim(is_isset($config_parser,105));
						if($key != 'sale'){ $key='buy'; }
						$usduah = (string)$v_data['@attributes'][$key];
						
						$curs1 = def_parser_curs($parsers, '105', 100);	
						$curs_parser[105]['curs1'] = $curs1; // USD
						$curs_parser[105]['curs2'] = is_sum($usduah * $curs1); // UAH
				
						$curs1 = def_parser_curs($parsers, '106', 1000);
						$curs_parser[106]['curs1'] = $curs1; // UAH
						$curs_parser[106]['curs2'] = is_sum($curs1 / $usduah); // USD							

					} elseif($ccy == 'EUR'){
					
						$key = trim(is_isset($config_parser,107));
						if($key != 'sale'){ $key='buy'; }			
						$euruah = (string)$v_data['@attributes'][$key];
						
						$curs1 = def_parser_curs($parsers, '107', 100);
						$curs_parser[107]['curs1'] = $curs1; // EUR
						$curs_parser[107]['curs2'] = is_sum($euruah * $curs1); // UAH
				
						$curs1 = def_parser_curs($parsers, '108', 1000);
						$curs_parser[108]['curs1'] = $curs1; // UAH
						$curs_parser[108]['curs2'] = is_sum($curs1 / $euruah); // EUR
					
					}
				}
									
			}
		}
	} 	
	/* end privatbank */

	/* national */
 	if(is_isset($work_parser,151) == 1 or is_isset($work_parser,153) == 1 or is_isset($work_parser,155) == 1){
		$curl = get_curl_parser('http://www.nationalbank.kz/rss/rates_all.xml', '', 'parser', 'nationalbank');
		if(is_array($curl) and !$curl['err'] and strstr($curl['output'],'<?xml')){		
			$string = $curl['output'];
			$res = @simplexml_load_string($string);
			if(is_object($res) and isset($res->channel)){
				foreach($res->channel->item as $data){
				
					$CharCode = $data->title;
				
					if($CharCode == 'USD'){
					
						$on1 = (string)$data->description;
						$on1 = is_sum($on1);
						if($on1 > 0){
							$curs1 = def_parser_curs($parsers, '151', 1);
							$curs_parser[151]['curs1'] = $curs1; // USD
							$curs_parser[151]['curs2'] = is_sum($on1 * $curs1); // KZT
								
							$curs1 = def_parser_curs($parsers, '152', 1000);	
							$curs_parser[152]['curs1'] = $curs1; // KZT
							$curs_parser[152]['curs2'] = is_sum($curs1 / $on1); // USD	
						}				
						
					}

					if($CharCode == 'EUR'){
					
						$on1 = (string)$data->description;
						$on1 = is_sum($on1);
						if($on1 > 0){
							$curs1 = def_parser_curs($parsers, '153', 1);
							$curs_parser[153]['curs1'] = $curs1; // EUR
							$curs_parser[153]['curs2'] = is_sum($on1 * $curs1); // KZT
								
							$curs1 = def_parser_curs($parsers, '154', 1000);	
							$curs_parser[154]['curs1'] = $curs1; // KZT
							$curs_parser[154]['curs2'] = is_sum($curs1 / $on1); // EUR	
						}				
						
					}

					if($CharCode == 'RUB'){
					
						$on1 = (string)$data->description;
						$on1 = is_sum($on1);
						if($on1 > 0){
							$curs1 = def_parser_curs($parsers, '155', 1);
							$curs_parser[155]['curs1'] = $curs1; // RUB
							$curs_parser[155]['curs2'] = is_sum($on1 * $curs1); // KZT
								
							$curs1 = def_parser_curs($parsers, '156', 100);	
							$curs_parser[156]['curs1'] = $curs1; // KZT
							$curs_parser[156]['curs2'] = is_sum($curs1 / $on1); // RUB	
						}				
						
					}				
							
				}
			}
		}
	}	 
	/* end national */

	/* nbrb */
	if(is_isset($work_parser,201) == 1 or is_isset($work_parser,203) == 1 or is_isset($work_parser,205) == 1){
		$date = date('m/d/Y', $time_parser);
		$curl = get_curl_parser('http://www.nbrb.by/Services/XmlExRates.aspx?ondate='.$date, '', 'parser', 'nbrb');
		if(is_array($curl) and !$curl['err'] and strstr($curl['output'],'<?xml')){		
			$string = $curl['output'];
			$res = @simplexml_load_string($string);
			if(is_object($res) and isset($res->Currency)){
				foreach($res->Currency as $data){
				
					$CharCode = $data->CharCode;
				
					if($CharCode == 'USD'){
					
						$on1 = (string)$data->Rate;
						$on1 = is_sum($on1);
						if($on1 > 0){
							$curs1 = def_parser_curs($parsers, '201', 1);
							$curs_parser[201]['curs1'] = $curs1; // USD
							$curs_parser[201]['curs2'] = is_sum($on1 * $curs1); // BYN
								
							$curs1 = def_parser_curs($parsers, '202', 10);	
							$curs_parser[202]['curs1'] = $curs1; // BYN
							$curs_parser[202]['curs2'] = is_sum($curs1 / $on1); // USD	
						}				
						
					}

					if($CharCode == 'EUR'){
					
						$on1 = (string)$data->Rate;
						$on1 = is_sum($on1);
						if($on1 > 0){
							$curs1 = def_parser_curs($parsers, '203', 1);
							$curs_parser[203]['curs1'] = $curs1; // EUR
							$curs_parser[203]['curs2'] = is_sum($on1 * $curs1); // BYN
								
							$curs1 = def_parser_curs($parsers, '204', 10);	
							$curs_parser[204]['curs1'] = $curs1; // BYN
							$curs_parser[204]['curs2'] = is_sum($curs1 / $on1); // EUR	
						}				
						
					}

					if($CharCode == 'RUB'){
					
						$on1 = (string)$data->Rate;
						$on1 = is_sum($on1);
						if($on1 > 0){ //100 => 3.0398
							
							$curs1 = def_parser_curs($parsers, '205', 100);
							$curs_parser[205]['curs1'] = $curs1; // RUB
							$curs_parser[205]['curs2'] = is_sum($on1 / 100 * $curs1); // BYN
		
						}				
						
					}				
							
				}
			}
		}
	} 	
	/* end nbrb */	

	$curs_parser = apply_filters('before_load_curs_parser', $curs_parser, $work_parser, $config_parser, $parsers);
	
	update_option('curs_parser', $curs_parser);

	do_action('load_parser_courses', $curs_parser, $work_parser, $config_parser, $parsers);
	
	update_vtypes_to_parser();
	update_naps_to_parser();
	
	update_option('time_parser', $time_parser);
}

add_filter('list_cron_func', 'parser_list_cron_func');
function parser_list_cron_func($filters){
global $exchangebox;	

	if(!$exchangebox->is_up_mode()){
		$filters['parser_upload_data'] = array(
			'title' => __('Rates parser','pn'),
			'site' => '1hour',
		);
	}
	
	return $filters;
}