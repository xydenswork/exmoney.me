<?php
/*
title: Capitalist
description: мерчант Capitalist
version: 1.2
*/

if(!class_exists('merchant_capitalist')){
	class merchant_capitalist extends Merchant_ExchangeBox {

		function __construct($file, $title)
		{
			$map = array(
				'CAPITALIST_SHOP_ID', 'CAPITALIST_SECRETKEY', 
			);
			parent::__construct($file, $map, $title);
			
			add_filter('merchants_settingtext_'. $this->name, array($this, 'merchants_settingtext'));
			add_filter('exchangebox_merchant_gmerchant',array($this, 'merchant_formstep_autocheck'),1,2);
			add_action('get_merchant_admin_options_'. $this->name, array($this, 'get_merchant_admin_options'), 10, 2);
			add_filter('exchangebox_merchant_paybutton_'.$this->name, array($this,'merchant_paybutton'),99,3);
			add_action('myaction_merchant_'. $this->name .'_fail', array($this,'myaction_merchant_fail'));
			add_action('myaction_merchant_'. $this->name .'_success', array($this,'myaction_merchant_success'));
			add_action('myaction_merchant_'. $this->name .'_status', array($this,'myaction_merchant_status'));
		}
		
		function merchants_settingtext(){
			$text = '| <span class="bred">'. __('Not config merchant file','pn') .'</span>';
			if(
				is_deffin($this->m_data,'CAPITALIST_SHOP_ID') 
				and is_deffin($this->m_data,'CAPITALIST_SECRETKEY') 
			){
				$text = '';
			}
			
			return $text;
		}	

		function merchant_formstep_autocheck($autocheck, $m_id){
			
			if($m_id and $m_id == $this->name){
				$autocheck = 1;
			}
			
			return $autocheck;
		}	

		function get_merchant_admin_options($options, $data){ 
			
			$text = '
			<strong>Status URL:</strong> <a href="'. get_merchant_link($this->name.'_status') .'" target="_blank">'. get_merchant_link($this->name.'_status') .'</a><br />
			<strong>Success URL:</strong> <a href="'. get_merchant_link($this->name.'_success') .'" target="_blank">'. get_merchant_link($this->name.'_success') .'</a><br />
			<strong>Fail URL:</strong> <a href="'. get_merchant_link($this->name.'_fail') .'" target="_blank">'. get_merchant_link($this->name.'_fail') .'</a>			
			';

			$options[] = array(
				'view' => 'textfield',
				'title' => '',
				'default' => $text,
			);									
			
			return $options;	
		}

		function merchant_paybutton($temp, $naps, $item){

			$vtype = pn_strip_input($item->valut1type);		
			$currency = str_replace('RUB','RUR',$vtype);			
			$en_array = array('USD','EUR','RUR');
			if(in_array($currency,$en_array)){
				
				$order_number = $item->id;
				$amount = round($item->summ1,2);
				
				$text_pay = get_text_pay($this->name, $item, $amount);	
				if(!$text_pay){ $text_pay = __('Order id','pn').' '.$order_number; }
				$text_pay = substr($text_pay,0, 145);
				
				$merchant_id = is_deffin($this->m_data,'CAPITALIST_SHOP_ID');
				$merchant_secret = is_deffin($this->m_data,'CAPITALIST_SECRETKEY');
						
				$str = implode(':', array($amount, $currency, $text_pay, $merchant_id, $order_number)); 
				$sign = hash_hmac('md5', $str, $merchant_secret);
				
					$temp = '
					<form name="payment" method="post" action="https://capitalist.net/merchant/pay" accept-charset="UTF-8"> 
					
						<input type="hidden" name="merchantid" value="'. $merchant_id .'" /> 
						<input type="hidden" name="number" value="'. $order_number .'" /> 
						<input type="hidden" name="amount" value="'. $amount .'" /> 
						<input type="hidden" name="currency" value="'. $currency .'" /> 
						<input type="hidden" name="description" value="'. $text_pay .'" /> 
						<input type="hidden" name="sign" value="'. $sign .'" />

						<input type="submit" value="'. __('Proceed to checkout','pn') .'" />
					
					</form>															
					';				
					
					return $temp;			
			}						
			
			return $temp;
		}

		function myaction_merchant_fail(){
	
			$id = get_payment_id('order_number');
			the_merchant_bid_delete($id);
	
		}

		function myaction_merchant_success(){
	
			$id = get_payment_id('order_number');
			the_merchant_bid_success($id);
	
		}

		function myaction_merchant_status(){
	
			do_action('merchant_logs', $this->name);
	
			$payment_state = is_param_req('payment_state');	
			$merchant_id = is_param_req('merchant_id');		
			$order_number = intval(is_param_req('order_number'));
			$amount = is_param_req('payment_amount');
			$currency = is_param_req('payment_currency'); 
				
			if(!$order_number){
				die( 'No id' );
			}
			if ($payment_state != 'success'){
				die( 'Not success payment' );
			}
			
			try{
						
				$oClass = new Capitalist( is_deffin($this->m_data,'CAPITALIST_SHOP_ID'), is_deffin($this->m_data,'CAPITALIST_SECRETKEY') );
				$result = $oClass->check_state($order_number, $amount, $currency);
				
			}
			catch (Exception $e)
			{
				die( 'Error Capitalist class' );
			}	
			
			if(!isset($result->success) or !isset($result->data->order)){
				die( 'Not success payment for API' );
			}
			
			if ($order_number != $result->data->order->order_number){
				die( 'incorrect order id' );
			}
			
			if ('paid' != $result->data->order->state){
				die( 'incorrect order state' );
			}

			if ($merchant_id != $result->data->order->merchant_id){
				die( 'incorrect order merchant_id' );
			}			

			$id = $result->data->order->order_number;
			$data = get_data_merchant_for_id($id);
			$in_summ = $result->data->order->amount;
			$currency = trim($result->data->order->currency);
			$pay_purse = '';
			
			$err = $data['err'];
			$status = $data['status'];
			$m_id = $data['m_id'];
			$vtype = $data['vtype'];
			$vtype = str_replace('RUB','RUR',$vtype);
			
			if($status == 'new'){ 
				if($err == 0){
					if($m_id and $m_id == $this->name){
						if($vtype == $currency){
							$summ1 = summ_for_z($data['summ'],2);
							$summ2 = summ_for_z($in_summ,2);
							if($summ2 >= $summ1){		
					
								the_merchant_bid_payed($id, $summ2, $pay_purse, '', '', 'user');
											
								die( 'ok' );
								
							} else {
								die('The payment amount is less than the provisions');
							}
						} else {
							die('Wrong type of currency');
						}
					} else {
						die('At the direction of off merchant');
					}
				} else {
					die( 'The application does not exist or the wrong ID' );
				}
			} else {
				die( 'In the application the wrong status' );
			}			
	
		}		
	}
}

new merchant_capitalist(__FILE__, 'Capitalist');		