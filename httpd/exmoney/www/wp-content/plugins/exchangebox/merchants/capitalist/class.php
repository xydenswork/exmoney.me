<?php
if(!class_exists('Capitalist')){
class Capitalist {
    private $merchant_id, $secret_key;
    
    # Конструктор, принимает id аккаунта и пароль.
    public function __construct( $merchant_id, $secret_key ) {
        $this->merchant_id = intval( $merchant_id );
        $this->secret_key = trim( $secret_key );
    }
    
	public function check_state($order_number, $amount, $currency) {
		
		$merchant_id = $this->merchant_id;
		$secret_key = $this->secret_key;
		
		$str = implode(':', array($amount, $currency, $merchant_id, $order_number)); 
		$sign = hash_hmac('md5', $str, $secret_key);		
		
        $out = $this->request( 
            'https://capitalist.net/merchant/payGate/checkstate', 
            array( 
                'merchant_id' => $merchant_id,
                'order_number' => $order_number,
				'amount' => $amount,
				'currency' => $currency,
				'sign' => $sign,
            ) 
        );		
		
		return $res = @json_decode($out);	
	
	}
	
    # Метод отправки запроса и получения ответа.
    private static function request( $url, array $data = array() ) {
        
		if($curl = curl_init()){
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36');
			curl_setopt($curl, CURLOPT_TIMEOUT, 25);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
			$err  = curl_errno($curl);
			$out = curl_exec($curl);
			curl_close($curl);
			if(!$err){
					
				return $out;
					
			} 
		}		
		
    }
}
}