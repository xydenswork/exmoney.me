<?php
/*
title: Nixmoney
description: мерчант Nixmoney
version: 1.2
*/

if(!class_exists('merchant_nixmoney')){
	class merchant_nixmoney extends Merchant_ExchangeBox {

		function __construct($file, $title)
		{
			$map = array(
				'NIXMONEY_PASSWORD', 'NIXMONEY_USD', 'NIXMONEY_EUR', 
				'NIXMONEY_BTC', 'NIXMONEY_LTC', 'NIXMONEY_PPC', 
				'NIXMONEY_FTC','NIXMONEY_CRT', 'NIXMONEY_CLR',
			);
			parent::__construct($file, $map, $title);
			
			add_filter('merchants_settingtext_'. $this->name, array($this, 'merchants_settingtext'));
			add_filter('exchangebox_merchant_gmerchant',array($this, 'merchant_formstep_autocheck'),1,2);
			add_action('get_merchant_admin_options_'. $this->name, array($this, 'get_merchant_admin_options'), 10, 2);
			add_filter('exchangebox_merchant_paybutton_'.$this->name, array($this,'merchant_paybutton'),99,3);
			add_action('myaction_merchant_'. $this->name .'_fail', array($this,'myaction_merchant_fail'));
			add_action('myaction_merchant_'. $this->name .'_success', array($this,'myaction_merchant_success'));
			add_action('myaction_merchant_'. $this->name .'_status', array($this,'myaction_merchant_status'));
		}
		
		function merchants_settingtext(){
			$text = '| <span class="bred">'. __('Not config merchant file','pn') .'</span>';
			if(
				is_deffin($this->m_data,'NIXMONEY_PASSWORD') 
			){
				$text = '';
			}
			
			return $text;
		}	

		function merchant_formstep_autocheck($autocheck, $m_id){
			
			if($m_id and $m_id == $this->name){
				$autocheck = 1;
			}
			
			return $autocheck;
		}	

		function get_merchant_admin_options($options, $data){ 
			
			$text = '
			<strong>Status URL:</strong> <a href="'. get_merchant_link($this->name.'_status') .'" target="_blank">'. get_merchant_link($this->name.'_status') .'</a><br />
			<strong>Success URL:</strong> <a href="'. get_merchant_link($this->name.'_success') .'" target="_blank">'. get_merchant_link($this->name.'_success') .'</a><br />
			<strong>Fail URL:</strong> <a href="'. get_merchant_link($this->name.'_fail') .'" target="_blank">'. get_merchant_link($this->name.'_fail') .'</a>			
			';

			$options[] = array(
				'view' => 'textfield',
				'title' => '',
				'default' => $text,
			);									
			
			return $options;	
		}

		function merchant_paybutton($temp, $naps, $item){

			$vtype = pn_strip_input($item->valut1type);		
			$amount = pn_strip_input(round($item->summ1,2));
			$PAYEE_ACCOUNT = 0;
				
				if($vtype == 'USD'){
					$PAYEE_ACCOUNT = is_deffin($this->m_data,'NIXMONEY_USD');
				} elseif($vtype == 'EUR'){
					$PAYEE_ACCOUNT = is_deffin($this->m_data,'NIXMONEY_EUR');
				} elseif($vtype == 'BTC'){
					$PAYEE_ACCOUNT = is_deffin($this->m_data,'NIXMONEY_BTC');
				} elseif($vtype == 'LTC'){
					$PAYEE_ACCOUNT = is_deffin($this->m_data,'NIXMONEY_LTC');
				} elseif($vtype == 'PPC'){
					$PAYEE_ACCOUNT = is_deffin($this->m_data,'NIXMONEY_PPC');
				} elseif($vtype == 'FTC'){
					$PAYEE_ACCOUNT = is_deffin($this->m_data,'NIXMONEY_FTC');	
				} elseif($vtype == 'CRT'){
					$PAYEE_ACCOUNT = is_deffin($this->m_data,'NIXMONEY_CRT');	
				} elseif($vtype == 'CLR'){
					$PAYEE_ACCOUNT = is_deffin($this->m_data,'NIXMONEY_CLR');
				}			
			
			if($PAYEE_ACCOUNT){
				$text_pay = get_text_pay($this->name, $item, $amount);	
				
					$temp = '
					<form action="https://nixmoney.com/merchant.jsp" method="post" target="_blank">
						<input type="hidden" name="PAYEE_ACCOUNT" value="'. $PAYEE_ACCOUNT .'" />
						<input type="hidden" name="PAYEE_NAME" value="'. $text_pay .'" />
						<input type="hidden" name="PAYMENT_AMOUNT" value="'. $amount .'" />
						<input type="hidden" name="PAYMENT_URL" value="'. get_merchant_link($this->name.'_success') .'" />
						<input type="hidden" name="NOPAYMENT_URL" value="'. get_merchant_link($this->name.'_fail') .'" />
						<input type="hidden" name="BAGGAGE_FIELDS" value="PAYMENT_ID" />
						<input type="hidden" name="PAYMENT_ID" value="'. $item->id .'" />
						<input type="hidden" name="STATUS_URL" value="'. get_merchant_link($this->name.'_status') .'" />
						<input type="submit" formtarget="_top" value="'. __('Proceed to checkout','pn') .'" />
					</form>										
					';							
			}						
			
			return $temp;
		}

		function myaction_merchant_fail(){
	
			$id = get_payment_id('PAYMENT_ID');
			the_merchant_bid_delete($id);
	
		}

		function myaction_merchant_success(){
	
			$id = get_payment_id('PAYMENT_ID');
			the_merchant_bid_success($id);
	
		}

		function myaction_merchant_status(){
	
			do_action('merchant_logs', $this->name);
	
			if(!isset($_POST['PAYMENT_ID'])){
				die('no id');
			}
			if(!isset($_POST['V2_HASH'])){
				die('no hash');
			}
			
			$string= $_POST['PAYMENT_ID'].':'.$_POST['PAYEE_ACCOUNT'].':'.$_POST['PAYMENT_AMOUNT'].':'.$_POST['PAYMENT_UNITS'].':'.$_POST['PAYMENT_BATCH_NUM'].':'.$_POST['PAYER_ACCOUNT'].':'.strtoupper(md5(is_deffin($this->m_data,'NIXMONEY_PASSWORD'))).':'.$_POST['TIMESTAMPGMT'];

			$v2key = $_POST['V2_HASH'];
			$hash=strtoupper(md5($string));
			
			if($hash != $v2key){
				die('no hash');
			}	
			
			$id = $_POST['PAYMENT_ID'];
			$data = get_data_merchant_for_id($id);
			$in_summ = $_POST['PAYMENT_AMOUNT'];
			$pay_purse = $_POST['PAYER_ACCOUNT'];
			$currency = strtoupper($_POST['PAYMENT_UNITS']);
			
			$err = $data['err'];
			$status = $data['status'];
			$m_id = $data['m_id'];
			$vtype = $data['vtype'];
			$vtype = str_replace('RUB','RUR',$vtype);
			
			if($status == 'new'){ 
				if($err == 0){
					if($m_id and $m_id == $this->name){
						if($vtype == $currency){
							$summ1 = summ_for_z($data['summ'],2);
							$summ2 = summ_for_z($in_summ,2);
							if($summ2 >= $summ1){		
					
								the_merchant_bid_payed($id, $summ2, $pay_purse, '', $_POST['PAYMENT_BATCH_NUM'], 'user');
											
								die( 'Completed' );
								
							} else {
								die('The payment amount is less than the provisions');
							}
						} else {
							die('Wrong type of currency');
						}
					} else {
						die('At the direction of off merchant');
					}
				} else {
					die( 'The application does not exist or the wrong ID' );
				}
			} else {
				die( 'In the application the wrong status' );
			}			
	
		}		
	}
}

new merchant_nixmoney(__FILE__, 'Nixmoney');		