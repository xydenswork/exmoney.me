<?php
/*
title: PayKassa
description: мерчант PayKassa
version: 1.2
*/

if(!class_exists('merchant_paykassa')){
	class merchant_paykassa extends Merchant_ExchangeBox {

		function __construct($file, $title)
		{
			$map = array(
				'SHOP_ID', 'SHOP_PASS',
			);
			parent::__construct($file, $map, $title);
			
			add_filter('merchants_settingtext_'. $this->name, array($this, 'merchants_settingtext'));
			add_filter('exchangebox_merchant_gmerchant',array($this, 'merchant_formstep_autocheck'),1,2);
			add_action('get_merchant_admin_options_'. $this->name, array($this, 'get_merchant_admin_options'), 10, 2);
			add_filter('exchangebox_merchant_paybutton_'.$this->name, array($this,'merchant_paybutton'),99,3);
			add_action('myaction_merchant_'. $this->name .'_fail', array($this,'myaction_merchant_fail'));
			add_action('myaction_merchant_'. $this->name .'_success', array($this,'myaction_merchant_success'));
			add_action('myaction_merchant_'. $this->name .'_status', array($this,'myaction_merchant_status'));
		}
		
		function merchants_settingtext(){
			$text = '| <span class="bred">'. __('Not config merchant file','pn') .'</span>';
			if(
				is_deffin($this->m_data,'SHOP_ID') 
				and is_deffin($this->m_data,'SHOP_PASS')
			){
				$text = '';
			}
			
			return $text;
		}	

		function merchant_formstep_autocheck($autocheck, $m_id){
			
			if($m_id and $m_id == $this->name){
				$autocheck = 1;
			}
			
			return $autocheck;
		}	

		function get_merchant_admin_options($options, $data){ 

			$paymethods = array(
				'1' => 'payeer',
				'2' => 'perfectmoney',
				'4' => 'advcash',
				'7' => 'berty',
				'11' => 'bitcoin',
				'12' => 'ethereum',
				'14' => 'litecoin',
				'15' => 'dogecoin',
				'16' => 'dash',
				'18' => 'bitcoincash',
				'19' => 'zcash',
				'20' => 'monero',
				'21' => 'ethereumclassic',
				'22' => 'ripple',
				'23' => 'neo',
				'24' => 'gas',
			);			
			
			$options['paymethod'] = array(
				'view' => 'select',
				'title' => __('Payment method','pn'),
				'options' => $paymethods,
				'default' => is_isset($data, 'paymethod'),
				'name' => 'paymethod',
				'work' => 'int',
			);						
			
			$text = '
			<strong>Status URL:</strong> <a href="'. get_merchant_link($this->name.'_status') .'" target="_blank">'. get_merchant_link($this->name.'_status') .'</a><br />
			<strong>Success URL:</strong> <a href="'. get_merchant_link($this->name.'_success') .'" target="_blank">'. get_merchant_link($this->name.'_success') .'</a><br />
			<strong>Fail URL:</strong> <a href="'. get_merchant_link($this->name.'_fail') .'" target="_blank">'. get_merchant_link($this->name.'_fail') .'</a>			
			';

			$options[] = array(
				'view' => 'textfield',
				'title' => '',
				'default' => $text,
			);		
			
			return $options;	
		}

		function merchant_paybutton($temp, $naps, $item){

			$amount = $item->summ1;
			$text_pay = get_text_pay($this->name, $item, $amount);
			$currency = pn_strip_input($item->valut1type);
			
			$m_data = get_merch_data($this->name);
			
			$system_id = intval(is_isset($m_data, 'paymethod'));
			if(!$system_id){ $system_id = 1; }
			
			$res = '';
			
			try {
				$paykassa = new PayKassaSCI(is_deffin($this->m_data,'SHOP_ID'),is_deffin($this->m_data,'SHOP_PASS'));
				
				$res = $paykassa->sci_create_order(
					$amount,
					$currency,  
					$item->id,  
					$text_pay,   
					$system_id 
				);				
			}
			catch( Exception $e ) {
				if(current_user_can('administrator')){
					die( __('Error!','pn') . ' ' .$e->getMessage() );
				} else {
					die(__('Error!','pn'));
				}
			}				
				
			if(isset($res["data"]) and isset($res["data"]["url"])){ 
				$temp = '
				<form action="'. $res["data"]["url"] .'" method="POST">
					<input type="submit" formtarget="_top" value="'. __('Proceed to checkout','pn') .'" />
				</form>
				';
			} else {
				if(current_user_can('administrator') and isset($res['message'])){
					print_r($res);
					$temp = '';
				} else {
					$temp = __('Error!','pn');
				}				
			}							
			
			return $temp;
		}

		function myaction_merchant_fail(){
	
			$id = get_payment_id('PAYMENT_ID');
			the_merchant_bid_delete($id);
	
		}

		function myaction_merchant_success(){
	
			$id = get_payment_id('PAYMENT_ID');
			the_merchant_bid_success($id);
	
		}

		function myaction_merchant_status(){
	
			$m_data = get_merch_data($this->name);
			do_action('merchant_logs', $this->name, $m_data);
	
			try {
				
				$paykassa = new PayKassaSCI(is_deffin($this->m_data,'SHOP_ID'),is_deffin($this->m_data,'SHOP_PASS'));

				$res = $paykassa->sci_confirm_order();

				if (isset($res['error']) and $res['error']) {         // $res['error'] - true если ошибка
					echo $res['message']; 	// $res['message'] - текст сообщения об ошибке					
				} elseif(isset($res['data'])) {
					$id = (int)$res["data"]["order_id"];        // уникальный числовой идентификатор платежа в вашем системе, пример: 150800
					$transaction = $res["data"]["transaction"]; // номер транзакции в системе paykassa: 96401
					$hash = $res["data"]["hash"];               // hash, пример: bde834a2f48143f733fcc9684e4ae0212b370d015cf6d3f769c9bc695ab078d1
					$currency = $res["data"]["currency"];       // валюта платежа, пример: RUB, USD
					$amount = $res["data"]["amount"];           // сумма платежа, пример: 100.50
					$system = $res["data"]["system"];           // система, пример: AdvCash

					$data = get_data_merchant_for_id($id);
					$in_summ = $amount;
					$pay_purse = '';
			
					$err = $data['err'];
					$status = $data['status'];
					$m_id = $data['m_id'];
					$vtype = $data['vtype'];

					if($status == 'new'){ 
						if($err == 0){
							if($m_id and $m_id == $this->name){
								if($vtype == $currency){
									$summ1 = summ_for_z($data['summ'],2);
									$summ2 = summ_for_z($in_summ,2);
									if($summ2 >= $summ1){		
							
										the_merchant_bid_payed($id, $summ2, $pay_purse, '', $transaction, 'user');
													
										die( 'Completed' );
										
									} else {
										die('The payment amount is less than the provisions');
									}
								} else {
									die('Wrong type of currency');
								}
							} else {
								die('At the direction of off merchant');
							}
						} else {
							die( 'The application does not exist or the wrong ID' );
						}
					} else {
						die( 'In the application the wrong status' );
					}										
				}

			}
			catch( Exception $e ) {
				die( __('Error!','pn') . ' ' .$e->getMessage() );
			}	
		}		
	}
}

new merchant_paykassa(__FILE__, 'PayKassa');		