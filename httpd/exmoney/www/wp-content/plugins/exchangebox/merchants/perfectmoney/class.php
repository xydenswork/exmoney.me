<?php
if(!class_exists('PerfectMoney')){
class PerfectMoney {
    private $iAccountID, $sPassPhrase;
    
    # Конструктор, принимает id аккаунта и пароль.
    public function __construct( $iAccountID, $sPassPhrase ) {
        $this->iAccountID = intval( $iAccountID );
        $this->sPassPhrase = trim( $sPassPhrase );
    }
    
    public function getHistory( $sStartDate, $sEndDate, $batch_id = null ) {
        
        $date1 = explode('.', $sStartDate);
		$date2 = explode('.', $sEndDate);
        
		
		
        $aResponse = explode( "\n", self::request( 
            'https://perfectmoney.is/acct/historycsv.asp', 
            array( 
                'AccountID' => $this->iAccountID,
                'PassPhrase' => $this->sPassPhrase,
                'startday' => $date1[0] - 0,
                'startmonth' => $date1[1] - 0,
                'startyear' => $date1[2] - 0,
                'endday' => $date2[0] - 0,
                'endmonth' => $date2[1] - 0,
                'endyear' => $date2[2] - 0,
                //'paymentsmade' => true, //расход
				'paymentsreceived' => true, //приход
                'batchfilter' => $batch_id,
                //'payment_id' => $payment_id
            ) 
        ));
		
		$data = array();
		$data['error'] = 1;
        if($aResponse[0] == 'Time,Type,Batch,Currency,Amount,Fee,Payer Account,Payee Account,Payment ID,Memo'){
			if( isset($aResponse[1]) and $aResponse[1] != 'No Records Found.' ){
				
				$arr_data = explode(',',$aResponse[1]);
				if(count($arr_data) >= 9){
					if($arr_data[2] == $batch_id){
						$data['error'] = 0;
						$data['date'] = $arr_data[0];
						$data['type'] = $arr_data[1];
						$data['batch'] = $arr_data[2];
						$data['currency'] = $arr_data[3];
						$data['amount'] = $arr_data[4];
						$data['fee'] = $arr_data[5];
						$data['receiver'] = $arr_data[6];
						$data['sender'] = $arr_data[7];
						$data['payment_id'] = $arr_data[8];	
					}
				}
			}
		}
		
		return $data;
    }
    
    # Метод отправки запроса и получения ответа.
    private static function request( $url, array $data = array() ) {
        
		$c_options = array(
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query($data)
		);
		$result = get_curl_parser($url, $c_options, 'merchant', 'perfectmoney');
		$err  = $result['err'];
		$out = $result['output'];
		if(!$err){		
			return $out;		
		}		
		
    }
}
}