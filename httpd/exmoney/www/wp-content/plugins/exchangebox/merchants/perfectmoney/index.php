<?php
/*
title: Perfect Money
description: мерчант Perfect Money
version: 1.2
*/

if(!class_exists('merchant_perfectmoney')){
	class merchant_perfectmoney extends Merchant_ExchangeBox {

		function __construct($file, $title)
		{
			$map = array(
				'PM_ACCOUNT_ID', 'PM_PHRASE', 'PM_U_ACCOUNT', 
				'PM_E_ACCOUNT', 'PM_G_ACCOUNT', 'PM_PAYEE_NAME',
				'PM_ALTERNATE_PHRASE','PM_B_ACCOUNT'
			);
			parent::__construct($file, $map, $title);
			
			add_filter('merchants_settingtext_'. $this->name, array($this, 'merchants_settingtext'));
			add_filter('exchangebox_merchant_gmerchant',array($this, 'merchant_formstep_autocheck'),1,2);
			add_action('get_merchant_admin_options_'. $this->name, array($this, 'get_merchant_admin_options'), 10, 2);
			add_filter('exchangebox_merchant_paybutton_'.$this->name, array($this,'merchant_paybutton'),99,3);
			add_action('myaction_merchant_'. $this->name .'_fail', array($this,'myaction_merchant_fail'));
			add_action('myaction_merchant_'. $this->name .'_success', array($this,'myaction_merchant_success'));
			add_action('myaction_merchant_'. $this->name .'_status', array($this,'myaction_merchant_status'));
		}
		
		function merchants_settingtext(){
			$text = '| <span class="bred">'. __('Not config merchant file','pn') .'</span>';
			if(
				is_deffin($this->m_data,'PM_U_ACCOUNT') 
				or is_deffin($this->m_data,'PM_E_ACCOUNT') 
				or is_deffin($this->m_data,'PM_G_ACCOUNT') 
				or is_deffin($this->m_data,'PM_B_ACCOUNT')
			){
				$text = '';
			}
			
			return $text;
		}	

		function merchant_formstep_autocheck($autocheck, $m_id){
			
			if($m_id and $m_id == $this->name){
				$autocheck = 1;
			}
			
			return $autocheck;
		}	

		function get_merchant_admin_options($options, $data){ 
			
			$options['check_api'] = array(
				'view' => 'select',
				'title' => __('Check payment history by API','pn'),
				'options' => array('0'=>__('No','pn'), '1'=>__('Yes','pn')),
				'default' => is_isset($data, 'check_api'),
				'name' => 'check_api',
				'work' => 'int',
			);			
			
			$text = '
			<strong>Status URL:</strong> <a href="'. get_merchant_link($this->name.'_status') .'" target="_blank">'. get_merchant_link($this->name.'_status') .'</a><br />
			<strong>Success URL:</strong> <a href="'. get_merchant_link($this->name.'_success') .'" target="_blank">'. get_merchant_link($this->name.'_success') .'</a><br />
			<strong>Fail URL:</strong> <a href="'. get_merchant_link($this->name.'_fail') .'" target="_blank">'. get_merchant_link($this->name.'_fail') .'</a>			
			';

			$options[] = array(
				'view' => 'textfield',
				'title' => '',
				'default' => $text,
			);			
			
			return $options;	
		}

		function merchant_paybutton($temp, $naps, $item){

			$vtype = pn_strip_input($item->valut1type);
			
			$PAYEE_ACCOUNT = 0;	
			if($vtype == 'USD'){
				$PAYMENT_UNITS = 'USD';
				$PAYEE_ACCOUNT = is_deffin($this->m_data,'PM_U_ACCOUNT');
			} elseif($vtype == 'EUR'){
				$PAYMENT_UNITS = 'EUR';
				$PAYEE_ACCOUNT = is_deffin($this->m_data,'PM_E_ACCOUNT');
			} elseif($vtype == 'OAU'){
				$PAYMENT_UNITS = 'OAU';
				$PAYEE_ACCOUNT = is_deffin($this->m_data,'PM_G_ACCOUNT');			
			} elseif($vtype == 'BTC'){
				$PAYMENT_UNITS = 'BTC';
				$PAYEE_ACCOUNT = is_deffin($this->m_data,'PM_B_ACCOUNT');			
			}
				
			if($PAYEE_ACCOUNT){		
				$amount = pn_strip_input(round($item->summ1,2));		
						
				$text_pay = get_text_pay($this->name, $item, $amount);
						
				$temp = '
				<form name="MerchantPay" action="https://perfectmoney.is/api/step1.asp" method="post">
					<input name="SUGGESTED_MEMO" type="hidden" value="'. $text_pay .'" />
					<input name="sEmail" type="hidden" value="'. $item->email .'" />
					<input name="PAYMENT_AMOUNT" type="hidden" value="'. $amount .'" />
					<input name="PAYEE_ACCOUNT" type="hidden" value="'. $PAYEE_ACCOUNT .'" />								
								
					<input type="hidden" name="PAYEE_NAME" value="'. is_deffin($this->m_data,'PM_PAYEE_NAME') .'" />
					<input type="hidden" name="PAYMENT_UNITS" value="'. $PAYMENT_UNITS .'" />
					<input type="hidden" name="PAYMENT_ID" value="'. $item->id .'" />
					<input type="hidden" name="STATUS_URL" value="'. get_merchant_link($this->name.'_status') .'" />
					<input type="hidden" name="PAYMENT_URL" value="'. get_merchant_link($this->name.'_success') .'" />
					<input type="hidden" name="PAYMENT_URL_METHOD" value="POST" />
					<input type="hidden" name="NOPAYMENT_URL" value="'. get_merchant_link($this->name.'_fail') .'" />
					<input type="hidden" name="NOPAYMENT_URL_METHOD" value="POST" />
					<input type="hidden" name="SUGGESTED_MEMO_NOCHANGE" value="1" />
					<input type="hidden" name="BAGGAGE_FIELDS" value="sEmail" />

					<input type="submit" formtarget="_top" value="'. __('Proceed to checkout','pn') .'" />
				</form>				
				';
				
			}				
			
			return $temp;
		}

		function myaction_merchant_fail(){
	
			$id = get_payment_id('PAYMENT_ID');
			the_merchant_bid_delete($id);
	
		}

		function myaction_merchant_success(){
	
			$id = get_payment_id('PAYMENT_ID');
			the_merchant_bid_success($id);
	
		}

		function myaction_merchant_status(){
	
			do_action('merchant_logs', $this->name);
	
			$sPayeeAccount = isset( $_POST['PAYEE_ACCOUNT'] ) ? trim( $_POST['PAYEE_ACCOUNT'] ) : null;
			$iPaymentID = isset( $_POST['PAYMENT_ID'] ) ? $_POST['PAYMENT_ID'] : null;
			$dPaymentAmount = isset( $_POST['PAYMENT_AMOUNT'] ) ? trim( $_POST['PAYMENT_AMOUNT'] ) : null;
			$sPaymentUnits = isset( $_POST['PAYMENT_UNITS'] ) ? trim( $_POST['PAYMENT_UNITS'] ) : null;
			$iPaymentBatch = isset( $_POST['PAYMENT_BATCH_NUM'] ) ? trim( $_POST['PAYMENT_BATCH_NUM'] ) : null;
			$sPayerAccount = isset( $_POST['PAYER_ACCOUNT'] ) ? trim( $_POST['PAYER_ACCOUNT'] ) : null;
			$sTimeStampGMT = isset( $_POST['TIMESTAMPGMT'] ) ? trim( $_POST['TIMESTAMPGMT'] ) : null;
			$sV2Hash = isset( $_POST['V2_HASH'] ) ? trim( $_POST['V2_HASH'] ) : null;
			
			if( !in_array( $sPaymentUnits, array( 'USD', 'EUR', 'OAU', 'BTC' ) ) ){
				die( 'Invalid currency of payment' );
			}

			if( $sV2Hash != strtoupper( md5( $iPaymentID.':'.$sPayeeAccount.':'.$dPaymentAmount.':'.$sPaymentUnits.':'.$iPaymentBatch.':'.$sPayerAccount.':'.strtoupper( md5( is_deffin($this->m_data,'PM_ALTERNATE_PHRASE') ) ).':'.$sTimeStampGMT ) ) ){
				die( 'Invalid control signature' );
			}

			$constant = is_deffin($this->m_data,'PM_'.substr( $sPayeeAccount, 0, 1 ).'_ACCOUNT');
			if( $sPayeeAccount != $constant ){
				die( 'Invalid the seller s account' );
			}
			
			$data = get_merch_data($this->name);
			$check_history = intval(is_isset($data, 'check_api'));
			if($check_history == 1){
			
				try {
					$oClass = new PerfectMoney( is_deffin($this->m_data,'PM_ACCOUNT_ID'), is_deffin($this->m_data,'PM_PHRASE') );
					$res = $oClass->getHistory( date( 'd.m.Y', strtotime( '-2 day' ) ), date( 'd.m.Y', strtotime( '+2 day' ) ), $iPaymentBatch );
					if($res['error'] == 1){
						die( 'Wrong pay' );
					}
				}
				catch( Exception $e ) {
					die( 'Фатальная ошибка: '.$e->getMessage() );
				}		
			
			}

			# $sPayerAccount - счет плательщика
			# $dPaymentAmount - сумма платежа
			# $sPaymentUnits - валюта платежа (USD/EUR/OAU)			

			$id = $iPaymentID;
			$data = get_data_merchant_for_id($id);
			$in_summ = $dPaymentAmount;
			$pay_purse = $sPayerAccount;
			
			$err = $data['err'];
			$status = $data['status'];
			$m_id = $data['m_id'];
			$vtype = $data['vtype'];
			$vtype = str_replace(array('GLD'),'OAU',$vtype);
			
			if($status == 'new'){ 
				if($err == 0){
					if($m_id and $m_id == $this->name){
						if($vtype == $sPaymentUnits){
							$summ1 = summ_for_z($data['summ'],2);
							$summ2 = summ_for_z($in_summ,2);
							if($summ2 >= $summ1){		
					
								the_merchant_bid_payed($id, $summ2, $pay_purse, '', $iPaymentBatch, 'user');
											
								die( 'Completed' );
								
							} else {
								die('The payment amount is less than the provisions');
							}
						} else {
							die('Wrong type of currency');
						}
					} else {
						die('At the direction of off merchant');
					}
				} else {
					die( 'The application does not exist or the wrong ID' );
				}
			} else {
				die( 'In the application the wrong status' );
			}			
	
		}		
	}
}

new merchant_perfectmoney(__FILE__, 'Perfect Money');		