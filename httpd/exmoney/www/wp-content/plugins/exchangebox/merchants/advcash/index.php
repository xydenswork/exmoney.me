<?php
/*
title: AdvCash
description: мерчант AdvCash
version: 1.2
*/

if(!class_exists('merchant_advcash')){
	class merchant_advcash extends Merchant_ExchangeBox {

		function __construct($file, $title)
		{
			$map = array(
				'ADVANCED_CASH_ACCOUNT_EMAIL', 'ADVANCED_CASH_SCI_NAME', 'ADVANCED_CASH_SCI_SECRET', 
			);
			parent::__construct($file, $map, $title);
			
			add_filter('merchants_settingtext_'. $this->name, array($this, 'merchants_settingtext'));
			add_filter('exchangebox_merchant_gmerchant',array($this, 'merchant_formstep_autocheck'),1,2);
			add_action('get_merchant_admin_options_'. $this->name, array($this, 'get_merchant_admin_options'), 10, 2);
			add_filter('exchangebox_merchant_paybutton_'.$this->name, array($this,'merchant_paybutton'),99,3);
			add_action('myaction_merchant_'. $this->name .'_fail', array($this,'myaction_merchant_fail'));
			add_action('myaction_merchant_'. $this->name .'_success', array($this,'myaction_merchant_success'));
			add_action('myaction_merchant_'. $this->name .'_status', array($this,'myaction_merchant_status'));
		}
		
		function merchants_settingtext(){
			$text = '| <span class="bred">'. __('Not config merchant file','pn') .'</span>';
			if(
				is_deffin($this->m_data,'ADVANCED_CASH_ACCOUNT_EMAIL') 
				and is_deffin($this->m_data,'ADVANCED_CASH_SCI_NAME') 
				and is_deffin($this->m_data,'ADVANCED_CASH_SCI_SECRET') 
			){
				$text = '';
			}
			
			return $text;
		}	

		function merchant_formstep_autocheck($autocheck, $m_id){
			
			if($m_id and $m_id == $this->name){
				$autocheck = 1;
			}
			
			return $autocheck;
		}	

		function get_merchant_admin_options($options, $data){ 
			
			$text = '
			<strong>Status URL:</strong> <a href="'. get_merchant_link($this->name.'_status') .'" target="_blank">'. get_merchant_link($this->name.'_status') .'</a><br />
			<strong>Success URL:</strong> <a href="'. get_merchant_link($this->name.'_success') .'" target="_blank">'. get_merchant_link($this->name.'_success') .'</a><br />
			<strong>Fail URL:</strong> <a href="'. get_merchant_link($this->name.'_fail') .'" target="_blank">'. get_merchant_link($this->name.'_fail') .'</a>			
			';

			$options[] = array(
				'view' => 'textfield',
				'title' => '',
				'default' => $text,
			);									
			
			return $options;	
		}

		function merchant_paybutton($temp, $naps, $item){

			$currency = pn_strip_input(str_replace('RUB','RUR',$item->valut1type));		
			$amount = pn_strip_input(round($item->summ1,2));
			$orderId = $item->id;
			$ac_account_email = is_deffin($this->m_data,'ADVANCED_CASH_ACCOUNT_EMAIL');
			$ac_sci_name = is_deffin($this->m_data,'ADVANCED_CASH_SCI_NAME');
			$sign = hash('sha256', $ac_account_email . ":" . $ac_sci_name . ":" . $amount . ":" . $currency . ":" . is_deffin($this->m_data,'ADVANCED_CASH_SCI_SECRET') . ":" . $orderId);
		
			$text_pay = get_text_pay($this->name, $item, $amount);		
			
					
			$temp = '
			<form name="MerchantPay" target="_blank" action="https://wallet.advcash.com/sci/" method="post">
				<input type="hidden" name="ac_account_email" value="'. $ac_account_email .'" /> 
				<input type="hidden" name="ac_sci_name" value="'. $ac_sci_name .'" />  
				<input type="hidden" name="ac_order_id" value="'. $orderId .'" /> 
				<input type="hidden" name="ac_sign" value="'. $sign .'" />			
						
				<input type="hidden" name="ac_amount" value="'. $amount .'" />
				<input type="hidden" name="ac_currency" value="'. $currency .'" />
				<input type="hidden" name="ac_comments" value="'. $text_pay .'" />
						
				<input type="submit" value="'. __('Go to payment','pn') .'" />
			</form>												
			';				
			
			return $temp;
		}

		function myaction_merchant_fail(){
	
			$id = get_payment_id('ac_order_id');
			the_merchant_bid_delete($id);
	
		}

		function myaction_merchant_success(){
	
			$id = get_payment_id('ac_order_id');
			the_merchant_bid_success($id);
	
		}

		function myaction_merchant_status(){
	
			do_action('merchant_logs', $this->name);
	
			# Получение внешних данных :
			$transactionId = isset( $_REQUEST['ac_transfer'] ) ? trim( $_REQUEST['ac_transfer'] ) : null;
			$paymentDate = isset( $_REQUEST['ac_start_date'] ) ? $_REQUEST['ac_start_date'] : null;
			$sciName = isset( $_REQUEST['ac_sci_name'] ) ? trim( $_REQUEST['ac_sci_name'] ) : null;
			$payer = isset( $_REQUEST['ac_src_wallet'] ) ? trim( $_REQUEST['ac_src_wallet'] ) : null;
			$destWallet = isset( $_REQUEST['ac_dest_wallet'] ) ? trim( $_REQUEST['ac_dest_wallet'] ) : null;
			$orderId = isset( $_REQUEST['ac_order_id'] ) ? trim( $_REQUEST['ac_order_id'] ) : null;
			$amount = isset( $_REQUEST['ac_amount'] ) ? trim( $_REQUEST['ac_amount'] ) : null;
			$currency = isset( $_REQUEST['ac_merchant_currency'] ) ? trim( $_REQUEST['ac_merchant_currency'] ) : null;
			$hash = isset( $_REQUEST['ac_hash'] ) ? trim( $_REQUEST['ac_hash'] ) : null;
			
			if( !in_array( $currency, array( 'USD', 'EUR', 'RUR', 'GBP' ) ) ){
				die( 'Неверная валюта платежа' );
			}

			# Проверка контрольной подписи :
			if( $hash != strtolower( hash('sha256', $transactionId.':'.$paymentDate.':'.$sciName.':'.$payer.':'.$destWallet.':'.$orderId.':'.$amount.':'.$currency.':'. is_deffin($this->m_data,'ADVANCED_CASH_SCI_SECRET') ) ) ){
				die( 'Неверная контрольная подпись' );	
			}	

			$id = $orderId;
			$data = get_data_merchant_for_id($id);
			$in_summ = $amount;
			$pay_purse = $payer;
			
			$err = $data['err'];
			$status = $data['status'];
			$m_id = $data['m_id'];
			$vtype = $data['vtype'];
			$vtype = str_replace('RUB','RUR',$vtype);
			
			if($status == 'new'){ 
				if($err == 0){
					if($m_id and $m_id == $this->name){
						if($vtype == $currency){
							$summ1 = summ_for_z($data['summ'],2);
							$summ2 = summ_for_z($in_summ,2);
							if($summ2 >= $summ1){		
					
								the_merchant_bid_payed($id, $summ2, $pay_purse, is_deffin($this->m_data,'ADVANCED_CASH_ACCOUNT_EMAIL'), $transactionId, 'user');
											
								die( 'Completed' );
								
							} else {
								die('The payment amount is less than the provisions');
							}
						} else {
							die('Wrong type of currency');
						}
					} else {
						die('At the direction of off merchant');
					}
				} else {
					die( 'The application does not exist or the wrong ID' );
				}
			} else {
				die( 'In the application the wrong status' );
			}			
	
		}		
	}
}

new merchant_advcash(__FILE__, 'AdvCash');		