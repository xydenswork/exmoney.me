<?php
/*
title: LiveCoin
description: мерчант LiveCoin
version: 1.2
*/

if(!class_exists('merchant_livecoin')){
	class merchant_livecoin extends Merchant_Exchangebox{
	
		function __construct($file, $title)
		{
			$map = array(
				'LIVECOIN_KEY', 'LIVECOIN_SECRET',
			);
			parent::__construct($file, $map, $title);
			
			add_filter('merchants_settingtext_'.$this->name, array($this, 'merchants_settingtext'));
			add_filter('exchangebox_merchant_paybutton_'.$this->name, array($this,'merchant_pay_button'),99,3);
			add_filter('get_merchant_admin_options_'.$this->name,array($this, 'get_merchant_admin_options'),1,2);
			add_action('myaction_merchant_'. $this->name .'_add', array($this,'myaction_merchant_add'));
			add_action('myaction_merchant_'. $this->name .'_status', array($this,'myaction_merchant_status'));
		}	
		
		function merchants_settingtext(){
			$text = '| <span class="bred">'. __('Not config merchant file','pn') .'</span>';
			if(
				is_deffin($this->m_data,'LIVECOIN_KEY')  
				and is_deffin($this->m_data,'LIVECOIN_SECRET') 
			){
				$text = '';
			}
			
			return $text;
		}

		function get_merchant_admin_options($options, $data){
							
			if(isset($options['note'])){
				unset($options['note']);
			}			
			
			return $options;
		}				

		function merchant_pay_button($temp, $naps, $item){

			$temp = '
			<form action="'. get_merchant_link($this->name.'_add') .'" method="get" target="_blank">
				<input type="hidden" name="hash" value="'. is_bid_hash($item->hashed) .'" />
				<input type="submit" formtarget="_top" value="'. __('Continue','pn') .'" />
			</form>				
			';

			return $temp;
		}

 		function myaction_merchant_add(){
			global $wpdb;	

			$hashed = is_bid_hash(is_param_get('hash'));
			$err = is_param_get('err');
			
			if($hashed){
				$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE hashed='$hashed'");
				if(isset($item->id)){
					$item_id = $item->id;
					$status = $item->status;
					$valut1i = intval($item->valut1i);
					$valut1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1i'");
					$xzt = $valut1->xzt;
					$m_id = apply_filters('get_merchant_id','' , $xzt, $item);
					if($status=='new' and $m_id and $m_id == $this->name){
						$sum = pn_strip_input($item->summ1);
					?>
					
						<div style="border: 1px solid #8eaed5; padding: 10px 15px; font: 13px Arial; width: 400px; border-radius: 3px; margin: 0 auto; text-align: center;">
							
							<p><?php _e('In order to pay an ID order','pn'); ?> <b><?php echo $item_id; ?></b>,<br /> <?php _e('enter coupon code valued','pn'); ?> <b><?php echo $sum; ?> LiveCoin <?php echo is_site_value($item->valut1type); ?></b>:</p>
							<form action="<?php echo get_merchant_link($this->name.'_status'); ?>" method="post">
								<input type="hidden" name="hash" value="<?php echo $hashed; ?>" />
								<p><input type="text" placeholder="<?php _e('Code','pn'); ?>" required name="code" style="border: 1px solid #ddd; border-radius: 3px; padding: 5px 10px;" value="" /></p>
								<p><input type="submit" formtarget="_top" style="padding: 5px 10px;" value="<?php _e('Submit code','pn'); ?>" /></p>
							</form>				
							
							<?php if($err == '-1'){ ?>
								<div style="border: 1px solid #ff0000; padding: 10px 15px; font: bold 13px Arial; border-radius: 3px;">
									<?php _e('Invalid code','pn'); ?>
								</div>
							<?php } ?>
							<?php if($err == '-2'){ ?>
								<div style="border: 1px solid #ff0000; padding: 10px 15px; font: bold 13px Arial; border-radius: 3px;">
									<?php _e('Coupon is not valid','pn'); ?>
								</div>
							<?php } ?>
							<?php if($err == '-3'){ ?>
								<div style="border: 1px solid #ff0000; padding: 10px 15px; font: bold 13px Arial; border-radius: 3px;">
									<?php _e('Payment error: invalid code or coupon is not valid or invalid amount.','pn'); ?>
								</div>
							<?php } ?>
							<?php if($err == '-4'){ ?>
								<div style="border: 1px solid #ff0000; padding: 10px 15px; font: bold 13px Arial; border-radius: 3px;">
									<?php _e('Coupon is not valid','pn'); ?>
								</div>
							<?php } ?>
							<?php if($err == '-5'){ ?>
								<div style="border: 1px solid #ff0000; padding: 10px 15px; font: bold 13px Arial; border-radius: 3px;">
									<?php _e('Invalid type of coupon','pn'); ?>
								</div>
							<?php } ?>							
						</div>
						
					<?php 
					} else {
						wp_redirect(get_bids_url($hashed));
						exit;
					}	 
				} else {
					pn_display_mess(__('Error!','pn'));
				}	
			} else {
				pn_display_mess(__('Error!','pn'));
			}			

		}  
		
		function myaction_merchant_status(){
		global $wpdb;	

			$hashed = is_bid_hash(is_param_post('hash'));
			$code = trim(is_param_post('code'));
			if($hashed){
				$item = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."bids WHERE hashed='$hashed'");
				if(isset($item->id)){
					$item_id = $item->id;
					$status = $item->status;
					$valut1i = intval($item->valut1i);
					$valut1 = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix ."valuts WHERE id='$valut1i'");
					$xzt = $valut1->xzt;
					$m_id = apply_filters('get_merchant_id','' , $xzt, $item);
					if($status=='new' and $m_id and $m_id == $this->name){
						$summ = $item->summ1;
						$currency = strtoupper(str_replace('RUB','RUR',$item->valut1type));
			
						if($code){
							try{
								$res = new LiveCoin(is_deffin($this->m_data,'LIVECOIN_KEY'),is_deffin($this->m_data,'LIVECOIN_SECRET'));
								$info = $res->redeem_voucher($code);
								if($info){
									
									$livecoin_sum = is_isset($info,'amount');
									$livecoin_currency = strtoupper(is_isset($info,'currency'));
									if($livecoin_sum >= $summ){
									
										if($livecoin_currency == $currency){
											
											the_merchant_bid_payed($item_id, $livecoin_sum, '', '', '', 'user');
											
											wp_redirect(get_bids_url($hashed));
											exit;					
											
										} else {
											
											$back = get_merchant_link($this->name.'_add') .'?hash='. $hashed .'&err=-4';
											wp_redirect($back);
											exit;
											
										}

									} else {
										$back = get_merchant_link($this->name.'_add') .'?hash='. $hashed .'&err=-3';
										wp_redirect($back);
										exit;					
									}
									
								} else {
									$back = get_merchant_link($this->name.'_add') .'?hash='. $hashed .'&err=-2';
									wp_redirect($back);
									exit;							
								}
							}
							catch (Exception $e)
							{
								$back = get_merchant_link($this->name.'_add') .'?hash='. $hashed .'&err=-2';
								wp_redirect($back);
								exit;						
							}					
						} else {
							$back = get_merchant_link($this->name.'_add') .'?hash='. $hashed .'&err=-1';
							wp_redirect($back);
							exit;				
						}
					} else {
						wp_redirect(get_bids_url($hashed));
						exit;
					}	 
				} else {
					pn_display_mess(__('Error!','pn'));
				}	
			} else {
				pn_display_mess(__('Error!','pn'));
			}	

		}	 	
		
	}
}

new merchant_livecoin(__FILE__, 'LiveCoin');							