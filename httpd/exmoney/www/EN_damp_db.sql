-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 05 2019 г., 00:14
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `exchangeboxen`
--

-- --------------------------------------------------------

--
-- Структура таблицы `eb_abitcoin`
--

CREATE TABLE `eb_abitcoin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `obmen_id` bigint(20) NOT NULL DEFAULT 0,
  `adress` tinytext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_adminpanelcaptcha`
--

CREATE TABLE `eb_adminpanelcaptcha` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `createdate` datetime NOT NULL,
  `sess_hash` varchar(150) NOT NULL,
  `num1` varchar(10) NOT NULL DEFAULT '0',
  `num2` varchar(10) NOT NULL DEFAULT '0',
  `symbol` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_adminpanelcaptcha`
--

INSERT INTO `eb_adminpanelcaptcha` (`id`, `createdate`, `sess_hash`, `num1`, `num2`, `symbol`) VALUES
(3, '2019-12-05 00:11:07', '138f55f8b26b2fe301750f928dc01d3b', '5', '7', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_archive_data`
--

CREATE TABLE `eb_archive_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `meta_key2` varchar(250) NOT NULL,
  `item_id` bigint(20) NOT NULL DEFAULT 0,
  `meta_value` varchar(20) NOT NULL DEFAULT '0',
  `meta_key3` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_auth_logs`
--

CREATE TABLE `eb_auth_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `auth_date` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(250) NOT NULL,
  `old_user_ip` varchar(250) NOT NULL,
  `old_user_browser` varchar(250) NOT NULL,
  `now_user_ip` varchar(250) NOT NULL,
  `now_user_browser` varchar(250) NOT NULL,
  `auth_status` int(1) NOT NULL DEFAULT 0,
  `auth_status_text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_auth_logs`
--

INSERT INTO `eb_auth_logs` (`id`, `auth_date`, `user_id`, `user_login`, `old_user_ip`, `old_user_browser`, `now_user_ip`, `now_user_browser`, `auth_status`, `auth_status_text`) VALUES
(4, '2019-12-05 00:11:22', 1, 'superadmin', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, '');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bautocurs`
--

CREATE TABLE `eb_bautocurs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nid` bigint(20) NOT NULL DEFAULT 0,
  `parseid` int(3) NOT NULL DEFAULT 0,
  `value1` varchar(10) NOT NULL DEFAULT '0',
  `chto1` int(1) NOT NULL DEFAULT 0,
  `value2` varchar(10) NOT NULL DEFAULT '0',
  `chto2` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_blackip`
--

CREATE TABLE `eb_bcoip_blackip` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `theip` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_country`
--

CREATE TABLE `eb_bcoip_country` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attr` varchar(20) NOT NULL,
  `title` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `wablon_id` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_ipcity`
--

CREATE TABLE `eb_bcoip_ipcity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title1` longtext NOT NULL,
  `title2` longtext NOT NULL,
  `title3` longtext NOT NULL,
  `first_coor` varchar(250) NOT NULL,
  `last_coor` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_iplist`
--

CREATE TABLE `eb_bcoip_iplist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `before_cip` bigint(20) NOT NULL DEFAULT 0,
  `after_cip` bigint(20) NOT NULL DEFAULT 0,
  `before_ip` varchar(250) NOT NULL,
  `after_ip` varchar(250) NOT NULL,
  `country_attr` varchar(20) NOT NULL,
  `place_id` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_themplate`
--

CREATE TABLE `eb_bcoip_themplate` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `temptitle` longtext NOT NULL,
  `title` longtext NOT NULL,
  `content` longtext NOT NULL,
  `info_status` int(1) NOT NULL DEFAULT 0,
  `default_temp` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_whiteip`
--

CREATE TABLE `eb_bcoip_whiteip` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `theip` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bids`
--

CREATE TABLE `eb_bids` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `ref_id` bigint(20) NOT NULL DEFAULT 0,
  `user_skidka` varchar(35) NOT NULL DEFAULT '0',
  `user_sksumm` varchar(35) NOT NULL DEFAULT '0',
  `fio` tinytext NOT NULL,
  `email` tinytext NOT NULL,
  `tel` tinytext NOT NULL,
  `schet1` tinytext NOT NULL,
  `schet2` tinytext NOT NULL,
  `hashed` tinytext NOT NULL,
  `valut1` tinytext NOT NULL,
  `valut2` tinytext NOT NULL,
  `valut1i` bigint(20) NOT NULL DEFAULT 0,
  `valut2i` bigint(20) NOT NULL DEFAULT 0,
  `valut1type` tinytext NOT NULL,
  `valut2type` tinytext NOT NULL,
  `curs1` varchar(35) NOT NULL DEFAULT '0',
  `curs2` varchar(35) NOT NULL DEFAULT '0',
  `summ1` varchar(35) NOT NULL DEFAULT '0',
  `summ2` varchar(35) NOT NULL DEFAULT '0',
  `summz1` varchar(35) NOT NULL DEFAULT '0',
  `summz2` varchar(35) NOT NULL DEFAULT '0',
  `tpersent1` varchar(35) NOT NULL DEFAULT '0',
  `tpersent2` varchar(35) NOT NULL DEFAULT '0',
  `tcommis1` varchar(35) NOT NULL DEFAULT '0',
  `tcommis2` varchar(35) NOT NULL DEFAULT '0',
  `tdate` datetime NOT NULL,
  `status` varchar(35) NOT NULL,
  `naprid` bigint(20) NOT NULL,
  `dop1` longtext NOT NULL,
  `dop2` longtext NOT NULL,
  `userip` longtext NOT NULL,
  `skype` tinytext NOT NULL,
  `dmetas` longtext NOT NULL,
  `acomments` longtext NOT NULL,
  `parthide` int(1) NOT NULL DEFAULT 0,
  `partmax` varchar(25) NOT NULL DEFAULT '0',
  `editdate` datetime NOT NULL,
  `naschet` varchar(150) NOT NULL,
  `pnomer` varchar(250) NOT NULL,
  `fname` tinytext NOT NULL,
  `iname` tinytext NOT NULL,
  `oname` tinytext NOT NULL,
  `schet1_hash` longtext NOT NULL,
  `schet2_hash` longtext NOT NULL,
  `ucomments` longtext NOT NULL,
  `exsum` varchar(30) NOT NULL DEFAULT '0',
  `partpr` varchar(25) NOT NULL DEFAULT '0',
  `ref_sum` varchar(25) NOT NULL DEFAULT '0',
  `p_sum` varchar(25) NOT NULL DEFAULT '0',
  `pcalc` int(1) NOT NULL DEFAULT 0,
  `user_hash` varchar(150) NOT NULL,
  `naschet_h` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bids_meta`
--

CREATE TABLE `eb_bids_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) NOT NULL DEFAULT 0,
  `meta_key` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_blacklist`
--

CREATE TABLE `eb_blacklist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(12) NOT NULL DEFAULT '0',
  `meta_value` tinytext NOT NULL,
  `comment_text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_captcha`
--

CREATE TABLE `eb_captcha` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `createdate` datetime NOT NULL,
  `sess_hash` varchar(150) NOT NULL,
  `num1` varchar(10) NOT NULL DEFAULT '0',
  `num2` varchar(10) NOT NULL DEFAULT '0',
  `symbol` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_captcha`
--

INSERT INTO `eb_captcha` (`id`, `createdate`, `sess_hash`, `num1`, `num2`, `symbol`) VALUES
(8, '2019-12-05 00:10:43', '597e0255616bcbddf2d6e3c21921d402', '6', '8', 2),
(9, '2019-12-05 00:10:43', '138f55f8b26b2fe301750f928dc01d3b', '5', '2', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_change`
--

CREATE TABLE `eb_change` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `meta_key2` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_change`
--

INSERT INTO `eb_change` (`id`, `meta_key`, `meta_key2`, `meta_value`) VALUES
(1, 'admin_panel_url', '', 'xchngbx'),
(2, 'reviews', 'count', '20'),
(3, 'reviews', 'method', 'moderation'),
(4, 'reviews', 'website', '0'),
(5, 'captcha', 'reviewsform', '0'),
(6, 'captcha', 'contactform', '0'),
(7, 'captcha', 'loginform', '0'),
(8, 'captcha', 'registerform', '0'),
(9, 'partners', 'status', '1'),
(10, 'partners', 'minpay', '5'),
(11, 'exchange', 'courez', '2'),
(19, 'exchange', 'techreg', '0'),
(26, 'naps_temp', 'timeline_txt', 'Note: This transaction is performed by the operator in manual mode and takes 5 to 30 minutes during working hours (daily from 9:00 to 24:00 MSK).'),
(27, 'naps_temp', 'description_txt', 'For exchange you need to follow a few steps:\r\n\r\nFill in all the fields of the form submitted. Click «Continue».\r\nRead the terms of the agreement on exchange services, when accepting it, please tick theappropriate field/click «I Agree» («Agree»). Recheck the application data.\r\nPay for the application. To do this, transfer the necessary amount, following the instructions on our website.\r\nAfter this is done, the system will redirect you to the «Application Status» page, where the status of your transferwill be shown.\r\n\r\n<strong>Note</strong>: this operation will require the participation of the operator. The application process takes about 20 minutes.'),
(28, 'naps_temp', 'status_new', 'Log in to the system XXXXXXX;\r\nTurn the amounts shown below on the wallet XXXXXXX;\r\nClick on the \"I paid the order\";\r\nExpect the processing of the application by the operator.'),
(29, 'naps_temp', 'status_payed', 'Confirmation of payment accepted.\r\nYour request is being processed by the operator.'),
(30, 'naps_temp', 'status_success', 'Your application is complete.\r\nThank you for using the services of our service.\r\nPlease leave <a href=\"/reviews/\">review</a> review of the work of our service!'),
(31, 'exchange', 'autodelete', '0'),
(32, 'exchange', 'ad_h', '0'),
(33, 'exchange', 'ad_m', '15'),
(34, 'cron', '', '0'),
(35, 'admincaptcha', '', '1'),
(36, 'letter_auth', '', '1'),
(37, 'naps_temp', 'status_realpay', 'Confirmation of payment accepted.\r\nYour request is being processed by the operator.'),
(38, 'naps_temp', 'status_verify', 'Confirmation of payment accepted.\r\nYour request is being processed by the operator.'),
(39, 'naps_temp', 'status_returned', 'Payment on the bid has been returned to your wallet.'),
(40, 'naps_temp', 'status_delete', 'Bid has been deleted.'),
(41, 'naps_temp', 'status_error', 'In the bid there is an error. Please contact technical support.'),
(42, 'admin', 'w0', '0'),
(43, 'admin', 'w1', '0'),
(44, 'admin', 'w2', '1'),
(45, 'admin', 'w3', '1'),
(46, 'admin', 'w4', '1'),
(47, 'admin', 'w5', '1'),
(48, 'admin', 'w6', '1'),
(49, 'admin', 'w7', '1'),
(50, 'admin', 'w8', '1'),
(51, 'up_mode', '', '0'),
(52, 'txtxml', 'txt', '1'),
(53, 'txtxml', 'xml', '1'),
(54, 'txtxml', 'numtxt', '5'),
(55, 'txtxml', 'numxml', '5'),
(56, 'favicon', '', '/wp-content/uploads/favicon.png'),
(57, 'logo', '', ''),
(58, 'textlogo', '', ''),
(59, 'user_fields', '', 'a:8:{s:5:\"login\";i:1;s:9:\"last_name\";i:1;s:10:\"first_name\";i:1;s:11:\"second_name\";i:1;s:10:\"user_phone\";i:1;s:10:\"user_skype\";i:1;s:7:\"website\";i:1;s:13:\"user_passport\";i:1;}'),
(60, 'user_fields_change', '', 'a:8:{s:10:\"user_email\";i:1;s:9:\"last_name\";i:1;s:10:\"first_name\";i:1;s:11:\"second_name\";i:1;s:10:\"user_phone\";i:1;s:10:\"user_skype\";i:1;s:7:\"website\";i:1;s:13:\"user_passport\";i:1;}'),
(61, 'persdata', 'last_name', '1'),
(62, 'persdata', 'first_name', '1'),
(63, 'persdata', 'second_name', '1'),
(64, 'persdata', 'user_phone', '1'),
(65, 'help', 'last_name', 'Enter the last name as it specified in your passport.'),
(66, 'help', 'first_name', 'Enter the first name as it specified in your passport.'),
(67, 'help', 'second_name', 'Enter the patronymic as it specified in your passport.'),
(68, 'help', 'user_email', 'Enter a valid email address.'),
(69, 'help', 'user_phone', 'Enter your cell phone number in international format to communicate with you. Example: +71234567890.'),
(70, 'help', 'user_skype', 'Enter your Skype name to communicate with you.'),
(71, 'numsybm_count', '', '12'),
(72, 'exchange', 'techregtext', ''),
(73, 'exchange', 'gostnaphide', '0'),
(74, 'exchange', 'an1_hidden', '0'),
(75, 'exchange', 'an2_hidden', '0'),
(76, 'exchange', 'an3_hidden', '0'),
(77, 'exchange', 'admin_mail', '0'),
(78, 'persdata', 'user_skype', '0'),
(79, 'persdata', 'user_passport', '0'),
(80, 'exchange', 'adjust', '0'),
(81, 'exchange', 'beautynum', '0'),
(82, 'exchange', 'maxsymb_all', '4'),
(83, 'exchange', 'maxsymb_reserv', '4'),
(84, 'exchange', 'maxsymb_course', '4'),
(85, 'help', 'user_passport', 'Enter your passport nubmer.'),
(86, 'htmlmap', 'exclude_page', 'a:0:{}'),
(87, 'htmlmap', 'exchanges', '1'),
(88, 'htmlmap', 'pages', '1'),
(89, 'htmlmap', 'news', '1'),
(90, 'xmlmap', 'exclude_page', 'a:0:{}'),
(91, 'xmlmap', 'exchanges', '1'),
(92, 'xmlmap', 'pages', '1'),
(93, 'xmlmap', 'news', '1'),
(94, 'partners', 'wref', '0'),
(95, 'partners', 'payouttext', ''),
(96, 'partners', 'clife', '365'),
(97, 'partners', 'text_banners', '0'),
(98, 'partners', 'calc', '0'),
(99, 'partners', 'reserv', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_commentmeta`
--

CREATE TABLE `eb_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_comments`
--

CREATE TABLE `eb_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_custom_fields_valut`
--

CREATE TABLE `eb_custom_fields_valut` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cf_name` longtext NOT NULL,
  `vid` int(1) NOT NULL DEFAULT 0,
  `valut_id` bigint(20) NOT NULL DEFAULT 0,
  `cf_req` int(1) NOT NULL DEFAULT 0,
  `place_id` int(1) NOT NULL DEFAULT 0,
  `minzn` int(2) NOT NULL DEFAULT 0,
  `maxzn` int(5) NOT NULL DEFAULT 100,
  `firstzn` varchar(20) NOT NULL,
  `helps` longtext NOT NULL,
  `datas` longtext NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `cf_hidden` int(2) NOT NULL DEFAULT 0,
  `cf_order` bigint(20) NOT NULL DEFAULT 0,
  `uniqueid` varchar(250) NOT NULL,
  `tech_name` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_inex_change`
--

CREATE TABLE `eb_inex_change` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `meta_key2` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_inex_deposit`
--

CREATE TABLE `eb_inex_deposit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `createdate` datetime NOT NULL,
  `indate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `outdate` datetime NOT NULL,
  `couday` int(5) NOT NULL DEFAULT 0,
  `pers` varchar(250) NOT NULL,
  `insumm` varchar(250) NOT NULL DEFAULT '0',
  `outsumm` varchar(250) NOT NULL DEFAULT '0',
  `plussumm` varchar(250) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `user_login` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_schet` varchar(250) NOT NULL,
  `gid` bigint(20) NOT NULL DEFAULT 0,
  `gtitle` tinytext NOT NULL,
  `gvalut` varchar(250) NOT NULL,
  `paystatus` int(3) NOT NULL DEFAULT 0,
  `vipstatus` int(3) NOT NULL DEFAULT 0,
  `zakstatus` int(3) NOT NULL DEFAULT 0,
  `locale` varchar(20) NOT NULL,
  `mail1` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_inex_system`
--

CREATE TABLE `eb_inex_system` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` tinytext NOT NULL,
  `valut` varchar(250) NOT NULL,
  `gid` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_inex_tars`
--

CREATE TABLE `eb_inex_tars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` tinytext NOT NULL,
  `minsum` varchar(250) NOT NULL DEFAULT '0',
  `maxsum` varchar(250) NOT NULL DEFAULT '0',
  `gid` bigint(20) NOT NULL DEFAULT 0,
  `gtitle` tinytext NOT NULL,
  `gvalut` varchar(250) NOT NULL,
  `mpers` varchar(250) NOT NULL,
  `cdays` bigint(20) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_links`
--

CREATE TABLE `eb_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_masschange`
--

CREATE TABLE `eb_masschange` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` tinytext NOT NULL,
  `curs1` varchar(50) NOT NULL DEFAULT '0',
  `curs2` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_merchant_logs`
--

CREATE TABLE `eb_merchant_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `createdate` datetime NOT NULL,
  `mdata` longtext NOT NULL,
  `merchant` varchar(150) NOT NULL,
  `ip` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_napobmens`
--

CREATE TABLE `eb_napobmens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `valsid1` bigint(20) NOT NULL DEFAULT 0,
  `valsid2` bigint(20) NOT NULL DEFAULT 0,
  `curs1` varchar(250) NOT NULL DEFAULT '0',
  `curs2` varchar(250) NOT NULL DEFAULT '0',
  `commis1` varchar(250) NOT NULL DEFAULT '0',
  `prorsum1` int(1) NOT NULL DEFAULT 0,
  `commis2` varchar(250) NOT NULL DEFAULT '0',
  `prorsum2` int(1) NOT NULL DEFAULT 0,
  `minsumm1` varchar(250) NOT NULL DEFAULT '0',
  `minsumm2` varchar(250) NOT NULL DEFAULT '0',
  `text1` longtext NOT NULL,
  `text2` longtext NOT NULL,
  `text3` longtext NOT NULL,
  `text4` longtext NOT NULL,
  `text5` longtext NOT NULL,
  `rorder` bigint(20) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1,
  `parser` bigint(20) NOT NULL DEFAULT 0,
  `corecurs1` varchar(250) NOT NULL,
  `corecurs2` varchar(250) NOT NULL,
  `parcommis1` varchar(250) NOT NULL,
  `parcommis2` varchar(250) NOT NULL,
  `parprorsum1` int(1) NOT NULL DEFAULT 0,
  `parprorsum2` int(1) NOT NULL DEFAULT 0,
  `prproc1` varchar(250) NOT NULL,
  `prproc2` varchar(250) NOT NULL,
  `prsum1` varchar(250) NOT NULL,
  `prsum2` varchar(250) NOT NULL,
  `delsk` int(1) NOT NULL DEFAULT 0,
  `maxsumm1` varchar(250) NOT NULL DEFAULT '0',
  `maxsumm1com` varchar(250) NOT NULL DEFAULT '0',
  `maxsumm2com` varchar(250) NOT NULL DEFAULT '0',
  `rorder2` bigint(20) NOT NULL DEFAULT 0,
  `parthide` int(1) NOT NULL DEFAULT 0,
  `partmax` varchar(25) NOT NULL DEFAULT '0',
  `hidegost` int(1) NOT NULL DEFAULT 0,
  `minsumm1com` varchar(50) NOT NULL DEFAULT '0',
  `minsumm2com` varchar(50) NOT NULL DEFAULT '0',
  `combox` varchar(50) NOT NULL DEFAULT '0',
  `x19mod` bigint(20) NOT NULL DEFAULT 0,
  `comboxpers` varchar(50) NOT NULL DEFAULT '0',
  `masschange` bigint(20) NOT NULL DEFAULT 0,
  `text6` longtext NOT NULL,
  `maxsumm2` varchar(250) NOT NULL DEFAULT '0',
  `windowtext` longtext NOT NULL,
  `client_verify` int(1) NOT NULL DEFAULT 0,
  `pmcheck` bigint(20) NOT NULL DEFAULT 0,
  `uf` longtext NOT NULL,
  `show_file` int(1) NOT NULL DEFAULT 1,
  `xml_city` longtext NOT NULL,
  `xml_juridical` int(1) NOT NULL DEFAULT 0,
  `xml_param` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_napobmens`
--

INSERT INTO `eb_napobmens` (`id`, `valsid1`, `valsid2`, `curs1`, `curs2`, `commis1`, `prorsum1`, `commis2`, `prorsum2`, `minsumm1`, `minsumm2`, `text1`, `text2`, `text3`, `text4`, `text5`, `rorder`, `status`, `parser`, `corecurs1`, `corecurs2`, `parcommis1`, `parcommis2`, `parprorsum1`, `parprorsum2`, `prproc1`, `prproc2`, `prsum1`, `prsum2`, `delsk`, `maxsumm1`, `maxsumm1com`, `maxsumm2com`, `rorder2`, `parthide`, `partmax`, `hidegost`, `minsumm1com`, `minsumm2com`, `combox`, `x19mod`, `comboxpers`, `masschange`, `text6`, `maxsumm2`, `windowtext`, `client_verify`, `pmcheck`, `uf`, `show_file`, `xml_city`, `xml_juridical`, `xml_param`) VALUES
(13, 3, 4, '1', '0.851549468879', '0.5', 0, '0.5', 0, '0', '0', '', '', '', '', '', 1, 1, 52, '', '', '0', '-3%', 0, 0, '0.5', '0.5', '0', '0', 0, '0', '0', '0', 1, 0, '0', 0, '0', '0', '0', 0, '0', 0, '', '0', '', 0, 0, 'a:4:{s:9:\"last_name\";s:1:\"1\";s:10:\"first_name\";s:1:\"1\";s:11:\"second_name\";s:1:\"1\";s:10:\"user_phone\";s:1:\"1\";}', 1, '', 0, ''),
(15, 4, 3, '1', '1.104927', '0.5', 0, '0.5', 0, '0', '0', '', '', '', '', '', 2, 1, 51, '', '', '0', '-3%', 0, 0, '0.5', '0.5', '0', '0', 0, '0', '0', '0', 0, 0, '0', 0, '0', '0', '0', 0, '0', 0, '', '0', '', 0, 0, 'a:4:{s:9:\"last_name\";s:1:\"1\";s:10:\"first_name\";s:1:\"1\";s:11:\"second_name\";s:1:\"1\";s:10:\"user_phone\";s:1:\"1\";}', 1, '', 0, ''),
(25, 17, 3, '1', '0.9', '0.8', 0, '0.5', 0, '0', '0', '', '', '', '', '', 3, 1, 0, '', '', '0', '0', 0, 0, '0.8', '0.5', '0', '0', 0, '0', '0', '0', 0, 0, '0', 0, '0', '0', '0', 0, '0', 0, '', '0', '', 0, 0, 'a:4:{s:9:\"last_name\";s:1:\"1\";s:10:\"first_name\";s:1:\"1\";s:11:\"second_name\";s:1:\"1\";s:10:\"user_phone\";s:1:\"1\";}', 1, '', 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_naps_meta`
--

CREATE TABLE `eb_naps_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) NOT NULL DEFAULT 0,
  `meta_key` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_naps_meta`
--

INSERT INTO `eb_naps_meta` (`id`, `item_id`, `meta_key`, `meta_value`) VALUES
(1, 26, 'seo_title', ''),
(2, 26, 'seo_key', ''),
(3, 26, 'seo_descr', ''),
(4, 25, 'seo_title', ''),
(5, 25, 'seo_key', ''),
(6, 25, 'seo_descr', ''),
(7, 23, 'seo_title', ''),
(8, 23, 'seo_key', ''),
(9, 23, 'seo_descr', ''),
(10, 19, 'seo_title', ''),
(11, 19, 'seo_key', ''),
(12, 19, 'seo_descr', ''),
(13, 18, 'seo_title', ''),
(14, 18, 'seo_key', ''),
(15, 18, 'seo_descr', ''),
(16, 16, 'seo_title', ''),
(17, 16, 'seo_key', ''),
(18, 16, 'seo_descr', ''),
(19, 15, 'seo_title', ''),
(20, 15, 'seo_key', ''),
(21, 15, 'seo_descr', ''),
(22, 13, 'seo_title', ''),
(23, 13, 'seo_key', ''),
(24, 13, 'seo_descr', ''),
(25, 11, 'seo_title', ''),
(26, 11, 'seo_key', ''),
(27, 11, 'seo_descr', ''),
(28, 22, 'seo_title', ''),
(29, 22, 'seo_key', ''),
(30, 22, 'seo_descr', '');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_notice_head`
--

CREATE TABLE `eb_notice_head` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `create_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL,
  `auto_status` int(1) NOT NULL DEFAULT 1,
  `edit_user_id` bigint(20) NOT NULL DEFAULT 0,
  `notice_type` int(1) NOT NULL DEFAULT 0,
  `notice_display` int(1) NOT NULL DEFAULT 0,
  `datestart` datetime NOT NULL,
  `dateend` datetime NOT NULL,
  `op_status` int(5) NOT NULL DEFAULT -1,
  `h1` varchar(5) NOT NULL DEFAULT '0',
  `m1` varchar(5) NOT NULL DEFAULT '0',
  `h2` varchar(5) NOT NULL DEFAULT '0',
  `m2` varchar(5) NOT NULL DEFAULT '0',
  `d1` int(1) NOT NULL DEFAULT 0,
  `d2` int(1) NOT NULL DEFAULT 0,
  `d3` int(1) NOT NULL DEFAULT 0,
  `d4` int(1) NOT NULL DEFAULT 0,
  `d5` int(1) NOT NULL DEFAULT 0,
  `d6` int(1) NOT NULL DEFAULT 0,
  `d7` int(1) NOT NULL DEFAULT 0,
  `url` longtext NOT NULL,
  `text` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `theclass` varchar(250) NOT NULL,
  `site_order` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_options`
--

CREATE TABLE `eb_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) DEFAULT NULL,
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_options`
--

INSERT INTO `eb_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(4682, '_site_transient_timeout_theme_roots', '1575495646', 'no'),
(4683, '_site_transient_theme_roots', 'a:1:{s:17:\"exchangeboxtheme2\";s:7:\"/themes\";}', 'no'),
(4, 'siteurl', 'http://exchangeen.best-curs.info', 'yes'),
(5, 'blogname', 'Exchange office', 'yes'),
(6, 'blogdescription', 'Exchange office', 'yes'),
(7, 'users_can_register', '0', 'yes'),
(8, 'admin_email', 'info@best-curs.info', 'yes'),
(9, 'start_of_week', '1', 'yes'),
(10, 'use_balanceTags', '0', 'yes'),
(11, 'use_smilies', '', 'yes'),
(12, 'require_name_email', '1', 'yes'),
(13, 'comments_notify', '', 'yes'),
(14, 'posts_per_rss', '10', 'yes'),
(15, 'rss_use_excerpt', '1', 'yes'),
(16, 'mailserver_url', 'mail.example.com', 'yes'),
(17, 'mailserver_login', 'login@example.com', 'yes'),
(18, 'mailserver_pass', 'password', 'yes'),
(19, 'mailserver_port', '110', 'yes'),
(20, 'default_category', '1', 'yes'),
(21, 'default_comment_status', 'open', 'yes'),
(22, 'default_ping_status', 'closed', 'yes'),
(23, 'default_pingback_flag', '', 'yes'),
(25, 'posts_per_page', '10', 'yes'),
(26, 'date_format', 'd.m.Y', 'yes'),
(27, 'time_format', 'H:i', 'yes'),
(28, 'links_updated_date_format', 'd.m.Y H:i', 'yes'),
(32, 'comment_moderation', '1', 'yes'),
(33, 'moderation_notify', '', 'yes'),
(34, 'permalink_structure', '/%postname%/', 'yes'),
(36, 'hack_file', '0', 'yes'),
(37, 'blog_charset', 'UTF-8', 'yes'),
(38, 'moderation_keys', '', 'no'),
(39, 'active_plugins', 'a:1:{i:0;s:27:\"exchangebox/exchangebox.php\";}', 'yes'),
(40, 'home', 'http://exchangeen.best-curs.info', 'yes'),
(41, 'category_base', '', 'yes'),
(42, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(44, 'comment_max_links', '2', 'yes'),
(45, 'gmt_offset', '3', 'yes'),
(46, 'default_email_category', '1', 'yes'),
(47, 'recently_edited', '', 'no'),
(48, 'template', 'exchangeboxtheme2', 'yes'),
(49, 'stylesheet', 'exchangeboxtheme2', 'yes'),
(50, 'comment_whitelist', '1', 'yes'),
(51, 'blacklist_keys', '', 'no'),
(52, 'comment_registration', '0', 'yes'),
(53, 'html_type', 'text/html', 'yes'),
(54, 'use_trackback', '0', 'yes'),
(55, 'default_role', 'users', 'yes'),
(56, 'db_version', '45805', 'yes'),
(57, 'uploads_use_yearmonth_folders', '', 'yes'),
(58, 'upload_path', '', 'yes'),
(59, 'blog_public', '1', 'yes'),
(60, 'default_link_category', '2', 'yes'),
(61, 'show_on_front', 'page', 'yes'),
(62, 'tag_base', '', 'yes'),
(63, 'show_avatars', '', 'yes'),
(64, 'avatar_rating', 'G', 'yes'),
(65, 'upload_url_path', '', 'yes'),
(66, 'thumbnail_size_w', '150', 'yes'),
(67, 'thumbnail_size_h', '150', 'yes'),
(68, 'thumbnail_crop', '1', 'yes'),
(69, 'medium_size_w', '300', 'yes'),
(70, 'medium_size_h', '300', 'yes'),
(71, 'avatar_default', 'mystery', 'yes'),
(74, 'large_size_w', '1024', 'yes'),
(75, 'large_size_h', '1024', 'yes'),
(76, 'image_default_link_type', 'file', 'yes'),
(77, 'image_default_size', '', 'yes'),
(78, 'image_default_align', '', 'yes'),
(79, 'close_comments_for_old_posts', '0', 'yes'),
(80, 'close_comments_days_old', '14', 'yes'),
(81, 'thread_comments', '1', 'yes'),
(82, 'thread_comments_depth', '5', 'yes'),
(83, 'page_comments', '0', 'yes'),
(84, 'comments_per_page', '50', 'yes'),
(85, 'default_comments_page', 'newest', 'yes'),
(86, 'comment_order', 'asc', 'yes'),
(87, 'sticky_posts', 'a:0:{}', 'yes'),
(88, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(89, 'widget_text', 'a:5:{i:4;a:3:{s:5:\"title\";s:73:\"Приветствуем на сайте обменного пункта!\";s:4:\"text\";s:1787:\"Наш On-line сервис предназначен для тех, кто хочет быстро, безопасно и по выгодному курсу обменять такие виды электронных валют: Webmoney, Perfect Money, LiqPay, Pecunix, Payza, Яндекс. Деньги, Payweb, Альфа-Банк, ВТБ 24, Приват24, Связной банк, Visa/Master Card, Western Union, MoneyGram.</br></br>\r\n\r\nЭтим возможности нашего сервиса не ограничиваются. В рамках проекта действуют программа лояльности, накопительная скидка и партнерская программа, воспользовавшись преимуществами которых, вы сможете совершать обмен электронных валют на более выгодных условиях. Для этого нужно просто <a href=\"/register/\">зарегистрироваться</a> на сайте.</br></br>\r\n\r\nНаш пункт обмена электронных валют – система, созданная на базе современного программного обеспечения и содержащая весь набор необходимых функций для удобной и безопасной конвертации наиболее распространенных видов электронных денег. За время работы мы приобрели репутацию проверенного партнера и делаем все возможное, чтобы ваши впечатления от нашего сервиса были только благоприятными.\";s:6:\"filter\";b:0;}i:5;a:3:{s:5:\"title\";s:16:\"Партнеры\";s:4:\"text\";s:728:\"<table class=\"tpartner\">\r\n<tr>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/ya_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/pm_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/stp_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/egopay_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/bitcoin_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>	\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/okpay_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>					\r\n</tr>\r\n</table>\r\n\";s:6:\"filter\";b:0;}i:8;a:3:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:112:\"Обменный пункт электронных валют.</br>\r\nВсе права защищены © 2014.\";s:6:\"filter\";b:0;}i:9;a:3:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:192:\"<a href=\"/preduprezhdenie/\">Предупреждение</a> &nbsp;&nbsp;&bull;&nbsp;&nbsp; <a href=\"/tos/\">Правила</a> &nbsp;&nbsp;&bull;&nbsp;&nbsp; <a href=\"/sitemap/\">Карта</a>\";s:6:\"filter\";b:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(3718, 'widget_exbloginwidget', 'a:5:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"titlem\";s:0:\"\";}i:3;a:2:{s:5:\"title\";s:0:\"\";s:6:\"titlem\";s:0:\"\";}i:4;a:2:{s:5:\"title\";s:0:\"\";s:6:\"titlem\";s:0:\"\";}i:5;a:2:{s:5:\"title\";s:13:\"Authorization\";s:6:\"titlem\";s:4:\"Menu\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(3720, 'widget_get_rnews', 'a:4:{i:2;a:5:{s:5:\"title\";s:0:\"\";s:4:\"tcat\";s:1:\"0\";s:5:\"count\";s:1:\"3\";s:6:\"sorter\";s:4:\"date\";s:4:\"desc\";s:4:\"desc\";}i:3;a:5:{s:5:\"title\";s:0:\"\";s:4:\"tcat\";s:1:\"0\";s:5:\"count\";s:0:\"\";s:6:\"sorter\";s:4:\"date\";s:4:\"desc\";s:3:\"asc\";}i:4;a:5:{s:5:\"title\";s:0:\"\";s:4:\"tcat\";s:1:\"0\";s:5:\"count\";s:0:\"\";s:6:\"sorter\";s:4:\"date\";s:4:\"desc\";s:4:\"desc\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(90, 'widget_rss', 'a:0:{}', 'yes'),
(91, 'timezone_string', '', 'yes'),
(93, 'embed_size_w', '', 'yes'),
(94, 'embed_size_h', '600', 'yes'),
(97, 'default_post_format', '0', 'yes'),
(98, 'initial_db_version', '19470', 'yes'),
(99, 'eb_user_roles', 'a:2:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:66:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:14:\"manage_ratings\";b:1;s:25:\"user_can_config_downloads\";b:1;s:23:\"user_can_edit_downloads\";b:1;s:25:\"user_can_add_new_download\";b:1;s:27:\"user_can_view_downloads_log\";b:1;}}s:5:\"users\";a:2:{s:4:\"name\";s:24:\"Пользователь\";s:12:\"capabilities\";a:1:{s:7:\"level_0\";b:1;}}}', 'yes'),
(3709, 'theme_mods_exchangeboxtheme', 'a:3:{i:0;s:0:\"\";s:18:\"nav_menu_locations\";a:1:{s:8:\"top_menu\";i:3;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1436944162;s:4:\"data\";a:7:{s:18:\"orphaned_widgets_2\";a:5:{i:0;s:15:\"exbobmen_info-2\";i:1;s:16:\"get_statuswork-3\";i:2;s:16:\"exbloginwidget-2\";i:3;s:16:\"exbotzivs_info-2\";i:4;s:11:\"get_rnews-2\";}s:18:\"orphaned_widgets_3\";a:1:{i:0;s:6:\"text-4\";}s:18:\"orphaned_widgets_4\";a:1:{i:0;s:6:\"text-5\";}s:18:\"orphaned_widgets_5\";a:1:{i:0;s:6:\"text-8\";}s:18:\"orphaned_widgets_6\";a:1:{i:0;s:6:\"text-9\";}s:19:\"wp_inactive_widgets\";a:5:{i:0;s:11:\"get_rnews-4\";i:1;s:16:\"exbotzivs_info-4\";i:2;s:16:\"exbloginwidget-4\";i:3;s:19:\"exblastobmen_info-3\";i:4;s:16:\"get_statuswork-5\";}s:9:\"sidebar-1\";a:5:{i:0;s:16:\"get_statuswork-4\";i:1;s:16:\"exbloginwidget-3\";i:2;s:19:\"exblastobmen_info-2\";i:3;s:16:\"exbotzivs_info-3\";i:4;s:11:\"get_rnews-3\";}}}}', 'yes'),
(100, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(610, 'widget_get_rx_cursrf', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"valuta\";s:2:\"ru\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'sidebars_widgets', 'a:4:{s:9:\"sidebar-1\";a:5:{i:0;s:16:\"get_statuswork-5\";i:1;s:16:\"exbloginwidget-4\";i:2;s:19:\"exblastobmen_info-3\";i:3;s:16:\"exbotzivs_info-4\";i:4;s:11:\"get_rnews-4\";}s:19:\"wp_inactive_widgets\";a:0:{}s:17:\"unique-sidebar-id\";a:4:{i:0;s:16:\"get_statuswork-6\";i:1;s:16:\"exbloginwidget-5\";i:2;s:19:\"exblastobmen_info-4\";i:3;s:16:\"exbotzivs_info-5\";}s:13:\"array_version\";i:3;}', 'yes'),
(3719, 'widget_exbotzivs_info', 'a:5:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:5:\"count\";s:1:\"3\";}i:3;a:2:{s:5:\"title\";s:0:\"\";s:5:\"count\";s:0:\"\";}i:4;a:2:{s:5:\"title\";s:0:\"\";s:5:\"count\";s:0:\"\";}i:5;a:2:{s:5:\"title\";s:7:\"Reviews\";s:5:\"count\";s:1:\"3\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(607, 'widget_get_a_fxlastobmen', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'cron', 'a:7:{i:1575495100;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1575522440;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1575530906;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1575531873;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1575565648;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1575580195;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(2793, 'uninstall_plugins', 'a:1:{s:27:\"exchangebox/exchangebox.php\";s:21:\"exb_plugins_uninstall\";}', 'no'),
(113, 'dashboard_widget_options', 'a:4:{s:25:\"dashboard_recent_comments\";a:1:{s:5:\"items\";i:5;}s:24:\"dashboard_incoming_links\";a:5:{s:4:\"home\";s:30:\"http://exchange.best-curs.info\";s:4:\"link\";s:106:\"http://blogsearch.google.com/blogsearch?scoring=d&partner=wordpress&q=link:http://exchange.best-curs.info/\";s:3:\"url\";s:139:\"http://blogsearch.google.com/blogsearch_feeds?scoring=d&ie=utf-8&num=10&output=rss&partner=wordpress&q=link:http://exchange.best-curs.info/\";s:5:\"items\";i:10;s:9:\"show_date\";b:0;}s:17:\"dashboard_primary\";a:8:{s:5:\"title\";s:18:\"Блог WordPress\";s:3:\"url\";s:31:\"http://wordpress.org/news/feed/\";s:4:\"link\";s:25:\"http://wordpress.org/news\";s:5:\"items\";i:2;s:5:\"error\";b:0;s:12:\"show_summary\";i:1;s:11:\"show_author\";i:0;s:9:\"show_date\";i:1;}s:19:\"dashboard_secondary\";a:7:{s:4:\"link\";s:28:\"http://planet.wordpress.org/\";s:3:\"url\";s:33:\"http://planet.wordpress.org/feed/\";s:5:\"title\";s:37:\"Другие новости WordPress\";s:5:\"items\";i:5;s:12:\"show_summary\";i:0;s:11:\"show_author\";i:0;s:9:\"show_date\";i:0;}}', 'yes'),
(993, 'current_theme', 'ExchangeBoxTheme 2.0', 'yes'),
(832, 'widget_get_a_fxreviews', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:3:\"kol\";s:1:\"2\";s:4:\"link\";s:9:\"/rewiews/\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(4563, 'rewrite_rules', 'a:86:{s:51:\"([\\-_A-Za-z0-9]+)_([A-Za-z0-9]+)_to_([A-Za-z0-9]+)$\";s:68:\"index.php?pagename=$matches[1]&valut1=$matches[2]&valut2=$matches[3]\";s:40:\"([\\-_A-Za-z0-9]+)/hst_([A-Za-z0-9]{35})$\";s:49:\"index.php?pagename=$matches[1]&hashed=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:40:\"index.php?&page_id=151&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(4507, 'banners', 'a:6:{s:4:\"text\";a:2:{i:0;s:706:\"Want to exchange electronic money or withdraw funds to a bank account? Searching for a reliable service for the exchange of electronic money? <a href=\"[partner_link]\">[url]</a> - here are the best exchange rates, security and minimum deadlines for financial transactions. Simple and user-friendly interface eliminates the need to study the structure of the site for a long time, and the opportunity to communicate with the operator at any time would help you to get answers to any questions relating to currency conversion \"on the spot\". In addition, the exchange provides an opportunity to earn on an affiliate program. Registered users get bonuses and discounts, which increase with each new transaction.\";i:1;s:731:\"You need to withdraw electronic money to a bank account or to exchange one type of currency for another one? You knocked down, trying to find the exchange, which offers a favorable rate, high security and maximum speed of the transaction? <a href=\"[partner_link]\">[url]</a> will solve all your problems with the exchange, transfer or withdrawal of funds from the Internet. A wide range of currencies, reduced rates for registered users, considerate technical support and simple interface will make your work with the service comfortable and favourable. And the presence of an affiliate program will not only allow you to conduct transactions without risks, but also to earn money by recommending a proved exchanger to your friends.\";}s:7:\"banner1\";a:1:{i:0;s:188:\"<a href=\"[partner_link]\"><img src=\"[url]/wp-content/plugins/exchangebox/images/banners/468x60_1.gif\" alt=\"Exchange office\" title=\"Exchange office\" width=\"468\" height=\"60\" border=\"0\" /></a>\";}s:7:\"banner2\";a:1:{i:0;s:190:\"<a href=\"[partner_link]\"><img src=\"[url]/wp-content/plugins/exchangebox/images/banners/200x200_1.gif\" alt=\"Exchange office\" title=\"Exchange office\" width=\"200\" height=\"200\" border=\"0\" /></a>\";}s:7:\"banner3\";a:1:{i:0;s:190:\"<a href=\"[partner_link]\"><img src=\"[url]/wp-content/plugins/exchangebox/images/banners/120x600_1.gif\" alt=\"Exchange office\" title=\"Exchange office\" width=\"120\" height=\"600\" border=\"0\" /></a>\";}s:7:\"banner4\";a:1:{i:0;s:214:\"<a href=\"[partner_link]\"><img src=\"[url]/wp-content/plugins/exchangebox/images/banners/100x100_1.gif\" alt=\"Обменный пункт\" title=\"Обменный пункт\" width=\"100\" height=\"100\" border=\"0\" /></a>\";}s:7:\"banner5\";a:1:{i:0;s:186:\"<a href=\"[partner_link]\"><img src=\"[url]/wp-content/plugins/exchangebox/images/banners/88x31_1.gif\" alt=\"Exchange office\" title=\"Exchange office\" width=\"88\" height=\"31\" border=\"0\" /></a>\";}}', 'yes'),
(3761, '_transient_timeout_feed_867bd5c64f85878d03a060509cd2f92c', '1367023035', 'no'),
(151, 'recently_activated', 'a:0:{}', 'yes'),
(3843, 'exchangebox_curs', 'a:4:{s:3:\"EUR\";a:3:{s:3:\"RUB\";s:7:\"75.0469\";s:3:\"USD\";s:6:\"1.1268\";s:3:\"UAH\";s:8:\"24.05418\";}s:3:\"RUB\";a:3:{s:3:\"EUR\";s:6:\"1.3325\";s:3:\"USD\";s:7:\"1.50426\";s:3:\"UAH\";s:8:\"37.03704\";}s:3:\"USD\";a:3:{s:3:\"RUB\";s:7:\"66.4779\";s:3:\"EUR\";s:7:\"0.88747\";s:3:\"UAH\";s:8:\"21.31707\";}s:3:\"UAH\";a:3:{s:3:\"RUB\";s:7:\"27.7662\";s:3:\"EUR\";s:7:\"0.04157\";s:3:\"USD\";s:7:\"0.04691\";}}', 'yes'),
(4037, 'widget_get_statuswork', 'a:5:{i:3;a:11:{s:5:\"title\";s:0:\"\";s:5:\"text1\";s:0:\"\";s:5:\"text2\";s:0:\"\";s:8:\"hidedate\";s:1:\"0\";s:6:\"worked\";s:6:\"online\";s:5:\"wtime\";s:1:\"0\";s:8:\"worktime\";s:3:\"off\";s:3:\"wt1\";s:2:\"00\";s:3:\"wt2\";s:2:\"00\";s:3:\"wt3\";s:2:\"00\";s:3:\"wt4\";s:2:\"00\";}i:4;a:11:{s:5:\"title\";s:0:\"\";s:5:\"text1\";s:15:\"Operator online\";s:5:\"text2\";s:16:\"Operator offline\";s:8:\"hidedate\";s:1:\"0\";s:6:\"worked\";s:7:\"offline\";s:5:\"wtime\";s:1:\"0\";s:8:\"worktime\";s:3:\"off\";s:3:\"wt1\";s:2:\"00\";s:3:\"wt2\";s:2:\"00\";s:3:\"wt3\";s:2:\"00\";s:3:\"wt4\";s:2:\"00\";}i:5;a:11:{s:5:\"title\";s:0:\"\";s:5:\"text1\";s:15:\"Operator online\";s:5:\"text2\";s:16:\"Operator offline\";s:8:\"hidedate\";s:1:\"0\";s:6:\"worked\";s:7:\"offline\";s:5:\"wtime\";s:1:\"0\";s:8:\"worktime\";s:3:\"off\";s:3:\"wt1\";s:2:\"00\";s:3:\"wt2\";s:2:\"00\";s:3:\"wt3\";s:2:\"00\";s:3:\"wt4\";s:2:\"00\";}i:6;a:11:{s:5:\"title\";s:0:\"\";s:5:\"text1\";s:15:\"Operator online\";s:5:\"text2\";s:16:\"Operator offline\";s:8:\"hidedate\";s:1:\"0\";s:6:\"worked\";s:7:\"offline\";s:5:\"wtime\";s:1:\"0\";s:8:\"worktime\";s:3:\"off\";s:3:\"wt1\";s:2:\"00\";s:3:\"wt2\";s:2:\"00\";s:3:\"wt3\";s:2:\"00\";s:3:\"wt4\";s:2:\"00\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(172, '_transient_random_seed', '08259deb90585f9e82b34500033f8bae', 'yes'),
(201, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(448, 'category_children', 'a:0:{}', 'yes'),
(269, 'theme_mods_twentyeleven', 'a:2:{s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:3;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1366924867;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";i:3;s:19:\"fx_browser_plugin-2\";}s:9:\"sidebar-1\";a:5:{i:0;s:15:\"get_rx_cursrf-2\";i:1;s:19:\"get_a_fxlastobmen-2\";i:2;s:20:\"get_a_fxrezervinfo-2\";i:3;s:14:\"get_a_fxinfo-2\";i:4;s:17:\"get_a_fxreviews-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}s:9:\"sidebar-4\";a:0:{}s:9:\"sidebar-5\";a:0:{}}}}', 'yes'),
(364, 'srel_options', 'a:0:{}', 'yes'),
(365, 'srel_main', '', 'yes'),
(366, 'srel_included', 'a:0:{}', 'yes'),
(367, 'srel_excluded', 'a:0:{}', 'yes'),
(368, 'wp_ds_blogmap_array', 'a:11:{s:3:\"ver\";s:3:\"312\";s:6:\"prefix\";s:0:\"\";s:12:\"tags_rep_str\";s:10:\"[tagcloud]\";s:13:\"posts_rep_str\";s:10:\"[postlist]\";s:13:\"pages_rep_str\";s:13:\"[pagesoftree]\";s:11:\"expand_text\";s:25:\"Show&nbsp;all&nbsp;&rarr;\";s:10:\"tags_limit\";i:0;s:11:\"posts_limit\";i:0;s:9:\"page_dept\";i:0;s:17:\"posts_description\";b:1;s:17:\"hidden_categories\";a:1:{i:0;s:47:\"98t4irubfuga76ert2ou3rbpiut8yp4kgjfn87ty349ith3\";}}', 'yes'),
(370, 'tadv_version', '3420', 'yes'),
(371, 'tadv_plugins', 'a:0:{}', 'yes'),
(372, 'tadv_options', 'a:6:{s:8:\"advlink1\";i:0;s:8:\"advimage\";i:1;s:11:\"editorstyle\";i:0;s:11:\"hideclasses\";i:0;s:11:\"contextmenu\";i:0;s:8:\"no_autop\";i:0;}', 'yes'),
(373, 'tadv_toolbars', 'a:4:{s:9:\"toolbar_1\";a:27:{i:0;s:4:\"bold\";i:1;s:6:\"italic\";i:2;s:13:\"strikethrough\";i:3;s:9:\"underline\";i:4;s:10:\"separator1\";i:5;s:7:\"bullist\";i:6;s:7:\"numlist\";i:7;s:7:\"outdent\";i:8;s:6:\"indent\";i:9;s:10:\"separator2\";i:10;s:11:\"justifyleft\";i:11;s:13:\"justifycenter\";i:12;s:12:\"justifyright\";i:13;s:10:\"separator3\";i:14;s:4:\"link\";i:15;s:6:\"unlink\";i:16;s:10:\"separator4\";i:17;s:5:\"image\";i:18;s:10:\"styleprops\";i:19;s:11:\"separator12\";i:20;s:7:\"wp_more\";i:21;s:7:\"wp_page\";i:22;s:10:\"separator5\";i:23;s:12:\"spellchecker\";i:24;s:6:\"search\";i:25;s:10:\"separator6\";i:26;s:10:\"fullscreen\";}s:9:\"toolbar_2\";a:21:{i:0;s:14:\"fontsizeselect\";i:1;s:12:\"formatselect\";i:2;s:9:\"pastetext\";i:3;s:9:\"pasteword\";i:4;s:12:\"removeformat\";i:5;s:10:\"separator8\";i:6;s:7:\"charmap\";i:7;s:5:\"print\";i:8;s:10:\"separator9\";i:9;s:9:\"forecolor\";i:10;s:9:\"backcolor\";i:11;s:8:\"emotions\";i:12;s:11:\"separator10\";i:13;s:3:\"sup\";i:14;s:3:\"sub\";i:15;s:5:\"media\";i:16;s:11:\"separator11\";i:17;s:4:\"undo\";i:18;s:4:\"redo\";i:19;s:7:\"attribs\";i:20;s:7:\"wp_help\";}s:9:\"toolbar_3\";a:0:{}s:9:\"toolbar_4\";a:0:{}}', 'no'),
(374, 'tadv_btns1', 'a:28:{i:0;s:4:\"bold\";i:1;s:6:\"italic\";i:2;s:13:\"strikethrough\";i:3;s:9:\"underline\";i:4;s:9:\"separator\";i:5;s:7:\"bullist\";i:6;s:7:\"numlist\";i:7;s:7:\"outdent\";i:8;s:6:\"indent\";i:9;s:9:\"separator\";i:10;s:11:\"justifyleft\";i:11;s:13:\"justifycenter\";i:12;s:12:\"justifyright\";i:13;s:9:\"separator\";i:14;s:4:\"link\";i:15;s:6:\"unlink\";i:16;s:9:\"separator\";i:17;s:5:\"image\";i:18;s:10:\"styleprops\";i:19;s:9:\"separator\";i:20;s:7:\"wp_more\";i:21;s:7:\"wp_page\";i:22;s:9:\"separator\";i:23;s:12:\"spellchecker\";i:24;s:6:\"search\";i:25;s:9:\"separator\";i:26;s:10:\"fullscreen\";i:27;s:6:\"wp_adv\";}', 'no'),
(375, 'tadv_btns2', 'a:21:{i:0;s:14:\"fontsizeselect\";i:1;s:12:\"formatselect\";i:2;s:9:\"pastetext\";i:3;s:9:\"pasteword\";i:4;s:12:\"removeformat\";i:5;s:9:\"separator\";i:6;s:7:\"charmap\";i:7;s:5:\"print\";i:8;s:9:\"separator\";i:9;s:9:\"forecolor\";i:10;s:9:\"backcolor\";i:11;s:8:\"emotions\";i:12;s:9:\"separator\";i:13;s:3:\"sup\";i:14;s:3:\"sub\";i:15;s:5:\"media\";i:16;s:9:\"separator\";i:17;s:4:\"undo\";i:18;s:4:\"redo\";i:19;s:7:\"attribs\";i:20;s:7:\"wp_help\";}', 'no'),
(376, 'tadv_btns3', 'a:0:{}', 'no'),
(377, 'tadv_btns4', 'a:0:{}', 'no'),
(378, 'tadv_allbtns', 'a:64:{i:0;s:6:\"wp_adv\";i:1;s:4:\"bold\";i:2;s:6:\"italic\";i:3;s:13:\"strikethrough\";i:4;s:9:\"underline\";i:5;s:7:\"bullist\";i:6;s:7:\"numlist\";i:7;s:7:\"outdent\";i:8;s:6:\"indent\";i:9;s:11:\"justifyleft\";i:10;s:13:\"justifycenter\";i:11;s:12:\"justifyright\";i:12;s:11:\"justifyfull\";i:13;s:3:\"cut\";i:14;s:4:\"copy\";i:15;s:5:\"paste\";i:16;s:4:\"link\";i:17;s:6:\"unlink\";i:18;s:5:\"image\";i:19;s:7:\"wp_more\";i:20;s:7:\"wp_page\";i:21;s:6:\"search\";i:22;s:7:\"replace\";i:23;s:10:\"fontselect\";i:24;s:14:\"fontsizeselect\";i:25;s:7:\"wp_help\";i:26;s:10:\"fullscreen\";i:27;s:11:\"styleselect\";i:28;s:12:\"formatselect\";i:29;s:9:\"forecolor\";i:30;s:9:\"backcolor\";i:31;s:9:\"pastetext\";i:32;s:9:\"pasteword\";i:33;s:12:\"removeformat\";i:34;s:7:\"cleanup\";i:35;s:12:\"spellchecker\";i:36;s:7:\"charmap\";i:37;s:5:\"print\";i:38;s:4:\"undo\";i:39;s:4:\"redo\";i:40;s:13:\"tablecontrols\";i:41;s:4:\"cite\";i:42;s:3:\"ins\";i:43;s:3:\"del\";i:44;s:4:\"abbr\";i:45;s:7:\"acronym\";i:46;s:7:\"attribs\";i:47;s:5:\"layer\";i:48;s:5:\"advhr\";i:49;s:4:\"code\";i:50;s:11:\"visualchars\";i:51;s:11:\"nonbreaking\";i:52;s:3:\"sub\";i:53;s:3:\"sup\";i:54;s:9:\"visualaid\";i:55;s:10:\"insertdate\";i:56;s:10:\"inserttime\";i:57;s:6:\"anchor\";i:58;s:10:\"styleprops\";i:59;s:8:\"emotions\";i:60;s:5:\"media\";i:61;s:10:\"blockquote\";i:62;s:9:\"separator\";i:63;s:1:\"|\";}', 'no'),
(380, 'aioseop_options', 'a:37:{s:9:\"aiosp_can\";s:0:\"\";s:12:\"aiosp_donate\";s:0:\"\";s:16:\"aiosp_home_title\";s:61:\"Обменный пункт электронных валют\";s:22:\"aiosp_home_description\";s:61:\"Обменный пункт электронных валют\";s:19:\"aiosp_home_keywords\";s:61:\"Обменный пункт электронных валют\";s:23:\"aiosp_max_words_excerpt\";s:0:\"\";s:20:\"aiosp_rewrite_titles\";s:2:\"on\";s:23:\"aiosp_post_title_format\";s:27:\"%post_title% | %blog_title%\";s:23:\"aiosp_page_title_format\";s:27:\"%page_title% | %blog_title%\";s:27:\"aiosp_category_title_format\";s:31:\"%category_title% | %blog_title%\";s:26:\"aiosp_archive_title_format\";s:21:\"%date% | %blog_title%\";s:22:\"aiosp_tag_title_format\";s:20:\"%tag% | %blog_title%\";s:25:\"aiosp_search_title_format\";s:23:\"%search% | %blog_title%\";s:24:\"aiosp_description_format\";s:13:\"%description%\";s:22:\"aiosp_404_title_format\";s:33:\"Nothing found for %request_words%\";s:18:\"aiosp_paged_format\";s:14:\" - Part %page%\";s:25:\"aiosp_google_analytics_id\";s:0:\"\";s:29:\"aiosp_ga_track_outbound_links\";s:0:\"\";s:20:\"aiosp_use_categories\";s:0:\"\";s:32:\"aiosp_dynamic_postspage_keywords\";s:2:\"on\";s:22:\"aiosp_category_noindex\";s:0:\"\";s:21:\"aiosp_archive_noindex\";s:0:\"\";s:18:\"aiosp_tags_noindex\";s:0:\"\";s:14:\"aiosp_cap_cats\";s:2:\"on\";s:27:\"aiosp_generate_descriptions\";s:0:\"\";s:16:\"aiosp_debug_info\";s:0:\"\";s:20:\"aiosp_post_meta_tags\";s:0:\"\";s:20:\"aiosp_page_meta_tags\";s:0:\"\";s:20:\"aiosp_home_meta_tags\";s:0:\"\";s:13:\"aiosp_enabled\";s:1:\"1\";s:17:\"aiosp_enablecpost\";s:0:\"\";s:26:\"aiosp_use_tags_as_keywords\";s:2:\"on\";s:16:\"aiosp_seopostcol\";s:0:\"\";s:18:\"aiosp_seocustptcol\";s:0:\"\";s:21:\"aiosp_posttypecolumns\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:12:\"aiosp_do_log\";s:0:\"\";s:14:\"aiosp_ex_pages\";s:0:\"\";}', 'yes'),
(439, 'ld_http_auth', 'none', 'yes'),
(440, 'ld_hide_wp_admin', 'yep', 'yes'),
(441, 'ld_login_base', 'iam', 'yes'),
(477, 'WSD-RSS-WGT-DISPLAY', 'no', 'yes'),
(478, 'WSD-COOKIE', 'PHPSESSID=rro39nje4unq1fuol8mcg2liu1; path=/', 'yes'),
(613, 'widget_ca_browser_plugin', 'a:2:{i:2;a:6:{s:12:\"link_firefox\";s:0:\"\";s:11:\"link_chrome\";s:0:\"\";s:10:\"link_opera\";s:0:\"\";s:18:\"image_link_firefox\";s:0:\"\";s:17:\"image_link_chrome\";s:0:\"\";s:16:\"image_link_opera\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(616, 'widget_get_a_fxrezervinfo', 'a:2:{i:2;a:9:{s:5:\"title\";s:0:\"\";s:11:\"fdxWebMoney\";s:1:\"0\";s:9:\"fdxLiqPay\";s:1:\"1\";s:17:\"fdxLibertyReserve\";s:1:\"1\";s:15:\"fdxPerfectMoney\";s:1:\"1\";s:11:\"fdxRBKMoney\";s:1:\"0\";s:7:\"fdxQIWI\";s:1:\"1\";s:14:\"fdxYandexMoney\";s:1:\"1\";s:13:\"fdxMasterTour\";s:1:\"1\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(619, 'widget_get_a_fxinfo', 'a:2:{i:2;a:2:{s:5:\"title\";s:23:\"Наш аттестат\";s:4:\"wmid\";s:12:\"000000000000\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(994, 'theme_mods_exchange_box', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:3:\"top\";i:3;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1366924699;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:5:{i:0;s:15:\"get_rx_cursrf-2\";i:1;s:19:\"get_a_fxlastobmen-2\";i:2;s:20:\"get_a_fxrezervinfo-2\";i:3;s:14:\"get_a_fxinfo-2\";i:4;s:19:\"fx_browser_plugin-2\";}s:9:\"sidebar-2\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:17:\"get_a_fxreviews-2\";i:3;s:16:\"lt_recent_post-2\";}s:9:\"sidebar-3\";a:0:{}s:9:\"sidebar-4\";a:0:{}s:9:\"sidebar-5\";a:1:{i:0;s:6:\"text-4\";}}}}', 'yes'),
(995, 'theme_switched', '', 'yes'),
(997, 'sa_exchangebox_theme_options', 'a:21:{s:7:\"general\";s:0:\"\";s:0:\"\";s:0:\"\";s:4:\"logo\";s:0:\"\";s:9:\"logo_text\";s:0:\"\";s:7:\"favicon\";s:0:\"\";s:9:\"social_on\";s:2:\"on\";s:14:\"vkontakte_link\";s:1:\"#\";s:12:\"twitter_link\";s:1:\"#\";s:13:\"facebook_link\";s:1:\"#\";s:20:\"events_webmoney_link\";s:26:\"http://events.webmoney.ru/\";s:18:\"odnoklassniki_link\";s:1:\"#\";s:16:\"google_plus_link\";s:1:\"#\";s:5:\"style\";s:0:\"\";s:18:\"bg_site_texture_on\";s:0:\"\";s:14:\"body_texture_1\";s:14:\"body_texture_1\";s:8:\"style_on\";s:0:\"\";s:10:\"text_color\";s:6:\"555555\";s:10:\"link_color\";s:6:\"39769c\";s:8:\"browsers\";s:0:\"\";s:7:\"ie_6_on\";s:0:\"\";s:14:\"link_page_ie_6\";s:0:\"\";}', 'yes'),
(1008, 'widget_lt_recent_post', 'a:2:{i:2;a:5:{s:5:\"title\";s:33:\"Последние новости\";s:18:\"category_recent_id\";s:0:\"\";s:6:\"number\";s:1:\"3\";s:9:\"more_link\";s:18:\"/category/novosti/\";s:14:\"more_link_text\";s:27:\"Архив новостей\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(1052, 'widget_fx_browser_plugin', 'a:2:{i:2;a:6:{s:12:\"link_firefox\";s:25:\"/vidzhety-dlya-brauzerov/\";s:11:\"link_chrome\";s:25:\"/vidzhety-dlya-brauzerov/\";s:10:\"link_opera\";s:25:\"/vidzhety-dlya-brauzerov/\";s:18:\"image_link_firefox\";s:59:\"/wp-content/plugins/exchangebox/download/firefox_widget.zip\";s:17:\"image_link_chrome\";s:58:\"/wp-content/plugins/exchangebox/download/chrome_widget.zip\";s:16:\"image_link_opera\";s:57:\"/wp-content/plugins/exchangebox/download/opera_widget.zip\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(2794, 'db_upgraded', '', 'yes'),
(3705, 'link_manager_enabled', '0', 'yes'),
(2813, 'hyper', 'a:9:{s:7:\"comment\";i:1;s:7:\"archive\";i:1;s:7:\"timeout\";i:1440;s:9:\"redirects\";i:1;s:8:\"notfound\";i:1;s:14:\"clean_interval\";i:60;s:4:\"gzip\";i:1;s:16:\"store_compressed\";i:1;s:11:\"expire_type\";s:4:\"post\";}', 'yes'),
(3775, 'first_exb', '1', 'yes'),
(3777, 'page_on_front', '151', 'yes'),
(3778, 'page_for_posts', '155', 'yes'),
(4509, 'curs_parser', 'a:33:{i:8;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"13.4196\";}i:9;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"7451.7869385079\";}i:10;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"30.5327\";}i:1;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"65.4072\";}i:2;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"15.288836702993\";}i:3;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"73.4392\";}i:4;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"13.616706064336\";}i:7;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"17.2088\";}i:11;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"9.73698\";}i:12;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:14:\"0.102701248231\";}i:5;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"244.423\";}i:6;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:15:\"40.912680066933\";}i:51;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"1.1391\";}i:52;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:14:\"0.877886050391\";}i:103;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:6:\"42.617\";}i:104;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"2346.4814510641\";}i:101;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:8:\"2675.521\";}i:102;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"37.375898002669\";}i:105;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:4:\"2670\";}i:106;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"37.453183520599\";}i:107;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:4:\"2985\";}i:108;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"33.500837520938\";}i:151;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"379.84\";}i:152;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:14:\"2.632687447346\";}i:153;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"426.52\";}i:154;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:14:\"2.344555941105\";}i:155;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:4:\"5.81\";}i:156;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:15:\"17.211703958692\";}i:201;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"2.1428\";}i:202;a:2:{s:5:\"curs1\";s:2:\"10\";s:5:\"curs2\";s:13:\"4.66679111443\";}i:203;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:5:\"2.407\";}i:204;a:2:{s:5:\"curs1\";s:2:\"10\";s:5:\"curs2\";s:14:\"4.154549231408\";}i:205;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:6:\"3.2787\";}}', 'yes'),
(4510, 'time_parser', '1575504600', 'yes'),
(4511, 'the_cron', 'a:9:{s:3:\"now\";i:1549993786;s:4:\"2min\";i:1549993703;s:4:\"5min\";i:1549993703;s:5:\"10min\";i:1549993367;s:5:\"30min\";i:1549992682;s:5:\"1hour\";i:1549992682;s:5:\"3hour\";i:1549992682;s:5:\"05day\";i:1549992682;s:4:\"1day\";i:1549992682;}', 'yes'),
(3816, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(3844, 'lastcursup', '1440872343', 'yes'),
(3847, 'widget_exbobmen_info', 'a:2:{i:2;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(3849, 'widget_exbrezerv_info', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(3863, 'theme_mods_exchangeboxthemegreen', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:8:\"top_menu\";i:3;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1405178142;s:4:\"data\";a:9:{s:19:\"wp_inactive_widgets\";a:2:{i:0;s:15:\"exbobmen_info-3\";i:1;s:16:\"get_statuswork-2\";}s:9:\"sidebar-1\";a:1:{i:0;s:6:\"text-7\";}s:9:\"sidebar-2\";a:1:{i:0;s:6:\"text-6\";}s:9:\"sidebar-3\";a:3:{i:0;s:16:\"exbloginwidget-2\";i:1;s:16:\"exbotzivs_info-2\";i:2;s:11:\"get_rnews-2\";}s:9:\"sidebar-4\";a:1:{i:0;s:6:\"text-4\";}s:9:\"sidebar-5\";a:1:{i:0;s:6:\"text-5\";}s:9:\"sidebar-6\";a:1:{i:0;s:6:\"text-8\";}s:9:\"sidebar-7\";a:1:{i:0;s:6:\"text-9\";}s:9:\"sidebar-8\";a:0:{}}}}', 'yes'),
(4017, 'exch_instruction', 'a:1:{s:7:\"epochta\";s:0:\"\";}', 'yes'),
(4387, 'theme_mods_exchangeboxtheme2', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:5:{s:8:\"top_menu\";i:3;s:11:\"bottom_menu\";i:4;s:12:\"the_top_menu\";i:3;s:17:\"the_top_menu_user\";i:3;s:15:\"the_bottom_menu\";i:4;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(4300, 'widget_exblastobmen_info', 'a:4:{i:2;a:0:{}i:3;a:1:{s:5:\"title\";s:13:\"Last exchange\";}i:4;a:2:{s:5:\"title\";s:16:\"Recent exchanges\";s:5:\"count\";s:1:\"3\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(4301, 'themechange', 'a:22:{s:8:\"logotype\";s:0:\"\";s:7:\"favicon\";s:19:\"/images/favicon.png\";s:5:\"skype\";s:11:\"exchangebox\";s:3:\"icq\";s:0:\"\";s:4:\"mail\";s:16:\"info@exchange.ru\";s:6:\"regrab\";s:52:\"Mon - Fri 10 a.m. till 11 p.m.\r\nSat - Sun free time.\";s:9:\"hometitle\";s:25:\"Welcome to exchange site!\";s:8:\"hometext\";s:961:\"Our On-line service is designed for those who wants to exchange these types of electronic currencies quickly, safely and at a favorable rate: Webmoney, Perfect Money, LiqPay, Pecunix, Payza, Yandex.Dengi, Payweb, Alfa-Bank, VTB 24, Privat24, Visa/Master Card, Western uniоn, MoneyGram.\r\n\r\nOur service is not limited to these opportunities only. The project has a loyalty program, cumulative discount and affiliate program, taking advantage of which you will be able to make the exchange of electronic currencies on more favorable terms. To use it you simply need to register on our site.\r\n\r\nOur exchange of electronic currencies is a system created on the basis of modern software and containing all the necessary functions for convenient and safe conversion of the most common types of electronic money. During our work we have gained a reputation of a trusted partner and we do everything possible to make your experience of our service to be only favorable.\";s:8:\"partners\";s:746:\"<div class=\"fwidget\">\r\n<div class=\"fwidgetvn\">\r\n<div class=\"fwtitle\"><div class=\"fwtitlevn\">Partners</div></div>\r\n<table class=\"tpartner\">\r\n<tr>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/ya_bottom.png\" alt=\"\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/pm_bottom.png\" alt=\"\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/stp_bottom.png\" alt=\"\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/egopay_bottom.png\" alt=\"\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/bitcoin_bottom.png\" alt=\"\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/okpay_bottom.png\" alt=\"\" /></a>\r\n</td>							\r\n</tr>\r\n</table>\r\n<div class=\"clear\"></div>\r\n</div>\r\n</div>\";s:7:\"footer1\";s:64:\"Exchange of electronic currencies. \r\nAll rights reserved © 2014\";s:7:\"footer2\";s:121:\"<a href=\"/preduprezhdenie/\">Caution</a>   •   <a href=\"/tos/\">Rules</a>   •   <a href=\"/sitemap/\">Sitemap</a>\";s:7:\"footer3\";s:0:\"\";s:10:\"footercode\";s:0:\"\";s:11:\"hometexttop\";s:0:\"\";s:3:\"tel\";s:17:\"8 (800) 123 45 67\";s:3:\"pwh\";i:1;s:6:\"ptitle\";s:8:\"Partners\";s:6:\"footer\";s:36:\"© 2018 E-currency exchange service.\";s:8:\"telegram\";s:11:\"exchangebox\";s:5:\"viber\";s:0:\"\";s:7:\"whatsup\";s:0:\"\";s:6:\"jabber\";s:0:\"\";}', 'yes'),
(4305, 'exb_version', 'a:2:{s:4:\"text\";s:58:\" Доступно обновление для ExchangeBox \";s:7:\"version\";s:3:\"4.0\";}', 'yes'),
(4369, 'WPLANG', 'ru_RU', 'yes'),
(4451, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1575493845;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:0:{}}', 'no'),
(4441, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1575493847;s:7:\"checked\";a:1:{s:17:\"exchangeboxtheme2\";s:3:\"7.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(4508, 'lcurs_parser', 'a:33:{i:8;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"13.4196\";}i:9;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"7451.7869385079\";}i:10;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"30.5327\";}i:1;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"65.4072\";}i:2;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"15.288836702993\";}i:3;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"73.4392\";}i:4;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"13.616706064336\";}i:7;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"17.2088\";}i:11;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"9.73698\";}i:12;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:14:\"0.102701248231\";}i:5;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"244.423\";}i:6;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:15:\"40.912680066933\";}i:51;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"1.1391\";}i:52;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:14:\"0.877886050391\";}i:103;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:6:\"42.617\";}i:104;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"2346.4814510641\";}i:101;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:8:\"2675.521\";}i:102;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"37.375898002669\";}i:105;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:4:\"2670\";}i:106;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"37.453183520599\";}i:107;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:4:\"2985\";}i:108;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"33.500837520938\";}i:151;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"379.84\";}i:152;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:14:\"2.632687447346\";}i:153;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"426.52\";}i:154;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:14:\"2.344555941105\";}i:155;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:4:\"5.81\";}i:156;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:15:\"17.211703958692\";}i:201;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"2.1428\";}i:202;a:2:{s:5:\"curs1\";s:2:\"10\";s:5:\"curs2\";s:13:\"4.66679111443\";}i:203;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:5:\"2.407\";}i:204;a:2:{s:5:\"curs1\";s:2:\"10\";s:5:\"curs2\";s:14:\"4.154549231408\";}i:205;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:6:\"3.2787\";}}', 'yes'),
(4505, 'the_pages', 'a:22:{s:4:\"home\";i:151;s:7:\"xchange\";i:152;s:12:\"xchangestep2\";i:153;s:12:\"xchangestep3\";i:154;s:4:\"news\";i:155;s:7:\"payouts\";i:156;s:8:\"feedback\";i:157;s:7:\"reviews\";i:158;s:5:\"login\";i:159;s:8:\"register\";i:160;s:8:\"lostpass\";i:161;s:7:\"account\";i:162;s:9:\"userstats\";i:0;s:10:\"usersobmen\";i:164;s:3:\"tos\";i:165;s:11:\"partnersFAQ\";i:166;s:7:\"banners\";i:167;s:12:\"partnerstats\";i:168;s:7:\"sitemap\";i:213;s:6:\"tarifs\";i:214;s:12:\"exchangestep\";i:248;s:19:\"terms_personal_data\";i:338;}', 'yes'),
(4499, 'finished_splitting_shared_terms', '1', 'yes'),
(4557, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4558, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4559, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4553, 'widget_exbregisterwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4538, 'check_new_user', 'a:5:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";}', 'yes'),
(4560, 'site_icon', '0', 'yes'),
(4561, 'medium_large_size_w', '768', 'yes'),
(4562, 'medium_large_size_h', '0', 'yes'),
(4684, 'recovery_keys', 'a:0:{}', 'yes'),
(4591, 'merchants', 'a:0:{}', 'yes'),
(4592, 'merch_data', 'a:0:{}', 'yes');
INSERT INTO `eb_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(4595, 'pn_extended', 'a:1:{s:6:\"moduls\";a:27:{s:16:\"sitecaptcha_plus\";s:16:\"sitecaptcha_plus\";s:5:\"wmess\";s:5:\"wmess\";s:12:\"admincaptcha\";s:12:\"admincaptcha\";s:7:\"captcha\";s:7:\"captcha\";s:4:\"cexp\";s:4:\"cexp\";s:13:\"checkpersdata\";s:13:\"checkpersdata\";s:6:\"cutnum\";s:6:\"cutnum\";s:9:\"discounts\";s:9:\"discounts\";s:6:\"export\";s:6:\"export\";s:3:\"fav\";s:3:\"fav\";s:8:\"finstats\";s:8:\"finstats\";s:7:\"htmlmap\";s:7:\"htmlmap\";s:8:\"mailsmtp\";s:8:\"mailsmtp\";s:9:\"mailtemps\";s:9:\"mailtemps\";s:10:\"masschange\";s:10:\"masschange\";s:12:\"merchantlogs\";s:12:\"merchantlogs\";s:6:\"mobile\";s:6:\"mobile\";s:9:\"mywarning\";s:9:\"mywarning\";s:9:\"napsfiles\";s:9:\"napsfiles\";s:13:\"notice_header\";s:13:\"notice_header\";s:7:\"numsymb\";s:7:\"numsymb\";s:15:\"parser_settings\";s:15:\"parser_settings\";s:8:\"partners\";s:8:\"partners\";s:7:\"pmcheck\";s:7:\"pmcheck\";s:7:\"reviews\";s:7:\"reviews\";s:8:\"setbidid\";s:8:\"setbidid\";s:3:\"x19\";s:3:\"x19\";}}', 'yes'),
(4596, 'bcoip_pages', 'a:0:{}', 'yes'),
(4597, 'fresh_site', '0', 'yes'),
(4598, 'inex_pages', 'a:2:{s:8:\"toinvest\";i:255;s:9:\"indeposit\";i:256;}', 'yes'),
(4599, 'lexb_pages', 'a:0:{}', 'yes'),
(4600, 'usve_pages', 'a:1:{s:10:\"userverify\";i:257;}', 'yes'),
(4601, 'bexb_pages', 'a:0:{}', 'yes'),
(4602, 'widget_bexbcurs_info', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4603, 'widget_get_investbox_menu_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4604, 'widget_get_userverify', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4610, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4611, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4612, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4613, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4655, 'widget_get_pn_news', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4656, 'pn_cron', 'a:3:{s:12:\"update_times\";a:1:{s:4:\"site\";a:9:{s:3:\"now\";i:1575504786;s:4:\"2min\";i:1575504759;s:4:\"5min\";i:1575504600;s:5:\"10min\";i:1575504600;s:5:\"30min\";i:1575504600;s:5:\"1hour\";i:1575504600;s:5:\"3hour\";i:1575504600;s:5:\"05day\";i:1575504600;s:4:\"1day\";i:1575504600;}}s:4:\"site\";a:13:{s:11:\"acp_del_img\";a:2:{s:11:\"last_update\";i:1575504600;s:9:\"work_time\";s:5:\"10min\";}s:16:\"delete_auto_bids\";a:2:{s:11:\"last_update\";i:1575504786;s:9:\"work_time\";s:3:\"now\";}s:18:\"delete_notpay_bids\";a:2:{s:11:\"last_update\";i:1575504786;s:9:\"work_time\";s:3:\"now\";}s:15:\"captcha_del_img\";a:2:{s:11:\"last_update\";i:1575504600;s:9:\"work_time\";s:5:\"10min\";}s:18:\"parser_upload_data\";a:2:{s:11:\"last_update\";i:1575504600;s:9:\"work_time\";s:5:\"1hour\";}s:12:\"del_autologs\";a:2:{s:11:\"last_update\";i:1575504600;s:9:\"work_time\";s:4:\"1day\";}s:14:\"archive_plinks\";a:2:{s:11:\"last_update\";i:1575504600;s:9:\"work_time\";s:4:\"1day\";}s:16:\"del_merchantlogs\";a:2:{s:11:\"last_update\";i:1575504600;s:9:\"work_time\";s:4:\"1day\";}s:23:\"delete_auto_notice_head\";a:2:{s:11:\"last_update\";i:1575504600;s:9:\"work_time\";s:4:\"1day\";}s:20:\"delete_auto_partners\";a:2:{s:11:\"last_update\";i:1575504600;s:9:\"work_time\";s:4:\"1day\";}s:19:\"delete_auto_reviews\";a:2:{s:11:\"last_update\";i:1575504600;s:9:\"work_time\";s:4:\"1day\";}s:16:\"exchangebox_chkv\";a:2:{s:11:\"last_update\";i:1575504600;s:9:\"work_time\";s:5:\"3hour\";}s:0:\"\";a:1:{s:9:\"work_time\";s:0:\"\";}}s:4:\"file\";a:13:{s:0:\"\";a:1:{s:9:\"work_time\";s:0:\"\";}s:12:\"del_autologs\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:14:\"archive_plinks\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:16:\"delete_auto_bids\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:18:\"delete_notpay_bids\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:11:\"acp_del_img\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:15:\"captcha_del_img\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:16:\"del_merchantlogs\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:23:\"delete_auto_notice_head\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:18:\"parser_upload_data\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:20:\"delete_auto_partners\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:19:\"delete_auto_reviews\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:16:\"exchangebox_chkv\";a:1:{s:9:\"work_time\";s:4:\"none\";}}}', 'yes'),
(4657, 'widget_get_pn_cbr', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4658, 'widget_get_pn_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4659, 'pn_notify', 'a:1:{s:5:\"email\";a:2:{s:6:\"payout\";a:6:{s:4:\"mail\";b:0;s:4:\"send\";i:0;s:5:\"title\";s:0:\"\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:0:\"\";s:6:\"tomail\";s:0:\"\";}s:7:\"zreserv\";a:6:{s:4:\"mail\";b:0;s:4:\"send\";i:0;s:5:\"title\";s:0:\"\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:0:\"\";s:6:\"tomail\";s:0:\"\";}}}', 'yes'),
(4686, '_site_transient_update_core', 'O:8:\"stdClass\":3:{s:7:\"updates\";a:0:{}s:15:\"version_checked\";s:3:\"5.3\";s:12:\"last_checked\";i:1575493983;}', 'no'),
(4626, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4638, 'work_parser', 'a:33:{i:1;i:1;i:2;i:1;i:3;i:1;i:4;i:1;i:5;i:1;i:6;i:1;i:7;i:1;i:8;i:1;i:9;i:1;i:10;i:1;i:11;i:1;i:12;i:1;i:51;i:1;i:52;i:1;i:101;i:1;i:102;i:1;i:103;i:1;i:104;i:1;i:105;i:1;i:106;i:1;i:107;i:1;i:108;i:1;i:151;i:1;i:152;i:1;i:153;i:1;i:154;i:1;i:155;i:1;i:156;i:1;i:201;i:1;i:202;i:1;i:203;i:1;i:204;i:1;i:205;i:1;}', 'yes'),
(4651, 'wp_page_for_privacy_policy', '0', 'yes'),
(4652, 'show_comments_cookies_opt_in', '0', 'yes'),
(4685, 'admin_email_lifespan', '1591045965', 'yes'),
(4687, 'can_compress_scripts', '1', 'no');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_partners`
--

CREATE TABLE `eb_partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` tinytext NOT NULL,
  `link` tinytext NOT NULL,
  `img` longtext NOT NULL,
  `rsort` bigint(20) NOT NULL DEFAULT 0,
  `site_order` bigint(20) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL,
  `auto_status` int(1) NOT NULL DEFAULT 1,
  `edit_user_id` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_partners`
--

INSERT INTO `eb_partners` (`id`, `title`, `link`, `img`, `rsort`, `site_order`, `status`, `create_date`, `edit_date`, `auto_status`, `edit_user_id`) VALUES
(6, 'Bestchange.ru', 'https://bestchange.ru', '/wp-content/uploads/bestchange.gif', 0, 0, 0, '2019-02-12 16:43:27', '2019-02-12 16:49:47', 1, 0),
(7, 'Okchanger.ru', 'https://www.okchanger.ru/', '/wp-content/uploads/okchanger.png', 0, 0, 0, '2019-02-12 16:44:07', '2019-02-12 16:49:42', 1, 0),
(8, 'Kurs.expert', 'https://kurs.expert', '/wp-content/uploads/kursexpert.png', 0, 0, 0, '2019-02-12 16:45:19', '2019-02-12 16:49:33', 1, 0),
(9, 'Glazok.org', 'https://glazok.org', '/wp-content/uploads/glazok.gif', 0, 0, 0, '2019-02-12 16:46:31', '2019-02-12 16:49:28', 1, 0),
(10, 'Kurses.com.ua', 'http://kurses.com.ua/', '/wp-content/uploads/kurses.gif', 0, 0, 0, '2019-02-12 16:47:10', '2019-02-12 16:49:22', 1, 0),
(11, 'Webmoney.ru', 'https://webmoney.ru', '/wp-content/uploads/webmoney-1.png', 0, 0, 0, '2019-02-12 16:48:48', '2019-02-12 16:48:48', 1, 0),
(12, 'Bestexchangers.ru', 'https://bestexchangers.ru', '/wp-content/uploads/bestexchangers.gif', 0, 0, 0, '2019-02-12 16:51:33', '2019-02-12 16:51:33', 1, 0),
(13, 'Good-kurs.ru', 'https://good-kurs.ru', '/wp-content/uploads/goodkurs.gif', 0, 0, 0, '2019-02-12 16:53:55', '2019-02-12 16:53:55', 1, 0),
(14, 'Monitorkursov.com', 'http://monitorkursov.com', '/wp-content/uploads/monitorkursov.gif', 0, 0, 0, '2019-02-12 16:56:37', '2019-02-12 16:56:37', 1, 0),
(15, 'Obmenvse.ru', 'http://obmenvse.ru', '/wp-content/uploads/obmenvse.gif', 0, 0, 0, '2019-02-12 16:57:11', '2019-02-12 16:57:11', 1, 0),
(16, 'Okku.ru', 'https://okku.ru/', 'http://exchange.best-curs.info/wp-content/uploads/okku.gif', 0, 0, 0, '2019-02-12 16:57:52', '2019-02-12 16:57:52', 1, 0),
(17, 'Pro-obmen.ru', 'https://pro-obmen.ru', '/wp-content/uploads/proobmen.gif', 0, 0, 0, '2019-02-12 16:58:31', '2019-02-12 16:58:31', 1, 0),
(18, 'Scanmoney.net', 'http://scanmoney.net', '/wp-content/uploads/scanmoney.png', 0, 0, 0, '2019-02-12 16:59:25', '2019-02-12 16:59:25', 1, 0),
(19, 'Secretovobmena.net', 'https://www.secretovobmena.net', '/wp-content/uploads/secretovobmena.gif', 0, 0, 0, '2019-02-12 17:00:46', '2019-02-12 17:00:46', 1, 0),
(20, 'Udifo.com', 'https://udifo.com', '/wp-content/uploads/udifo.png', 0, 0, 0, '2019-02-12 17:02:15', '2019-02-12 17:02:15', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_partner_pers`
--

CREATE TABLE `eb_partner_pers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sumec` varchar(50) NOT NULL DEFAULT '0',
  `pers` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_partner_pers`
--

INSERT INTO `eb_partner_pers` (`id`, `sumec`, `pers`) VALUES
(1, '0', '0.1'),
(2, '500', '0.2'),
(3, '5000', '0.3'),
(4, '10000', '0.4'),
(5, '15000', '0.5');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_payoutuser`
--

CREATE TABLE `eb_payoutuser` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bdate` datetime NOT NULL,
  `userid` bigint(20) NOT NULL DEFAULT 0,
  `bsumm` varchar(250) NOT NULL DEFAULT '0',
  `bstatus` int(1) NOT NULL DEFAULT 0,
  `bcomment` longtext NOT NULL,
  `valuts` varchar(250) NOT NULL,
  `schet` varchar(250) NOT NULL,
  `valsid` bigint(20) NOT NULL DEFAULT 0,
  `user_login` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_plinks`
--

CREATE TABLE `eb_plinks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `user_login` varchar(250) NOT NULL,
  `pdate` datetime NOT NULL,
  `pbrowser` longtext NOT NULL,
  `pip` longtext NOT NULL,
  `prefer` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_pn_options`
--

CREATE TABLE `eb_pn_options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `meta_key2` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_pn_options`
--

INSERT INTO `eb_pn_options` (`id`, `meta_key`, `meta_key2`, `meta_value`) VALUES
(1, 'up_mode', '', '0'),
(2, 'admin_panel_url', '', 'xchngbx'),
(3, 'reviews', 'count', '20'),
(4, 'reviews', 'method', 'moderation'),
(5, 'reviews', 'website', '0'),
(6, 'captcha', 'reviewsform', '0'),
(7, 'captcha', 'contactform', '0'),
(8, 'captcha', 'loginform', '0'),
(9, 'captcha', 'registerform', '0'),
(10, 'partners', 'status', '1'),
(11, 'partners', 'minpay', '5'),
(12, 'exchange', 'courez', '2'),
(13, 'exchange', 'techreg', '0'),
(14, 'naps_temp', 'timeline_txt', 'Note: This transaction is performed by the operator in manual mode and takes 5 to 30 minutes during working hours (daily from 9:00 to 24:00 MSK).'),
(15, 'naps_temp', 'description_txt', 'For exchange you need to follow a few steps:\r\n\r\nFill in all the fields of the form submitted. Click «Continue».\r\nRead the terms of the agreement on exchange services, when accepting it, please tick theappropriate field/click «I Agree» («Agree»). Recheck the application data.\r\nPay for the application. To do this, transfer the necessary amount, following the instructions on our website.\r\nAfter this is done, the system will redirect you to the «Application Status» page, where the status of your transferwill be shown.\r\n\r\n<strong>Note</strong>: this operation will require the participation of the operator. The application process takes about 20 minutes.'),
(16, 'naps_temp', 'status_new', 'Log in to the system XXXXXXX;\r\nTurn the amounts shown below on the wallet XXXXXXX;\r\nClick on the \"I paid the order\";\r\nExpect the processing of the application by the operator.'),
(17, 'naps_temp', 'status_payed', 'Confirmation of payment accepted.\r\nYour request is being processed by the operator.'),
(18, 'naps_temp', 'status_success', 'Your application is complete.\r\nThank you for using the services of our service.\r\nPlease leave <a href=\"/reviews/\">review</a> review of the work of our service!'),
(19, 'exchange', 'autodelete', '0'),
(20, 'exchange', 'ad_h', '0'),
(21, 'exchange', 'ad_m', '15'),
(22, 'cron', '', '0'),
(23, 'admincaptcha', '', '1'),
(24, 'letter_auth', '', '1'),
(25, 'naps_temp', 'status_realpay', 'Confirmation of payment accepted.\r\nYour request is being processed by the operator.'),
(26, 'naps_temp', 'status_verify', 'Confirmation of payment accepted.\r\nYour request is being processed by the operator.'),
(27, 'naps_temp', 'status_returned', 'Payment on the bid has been returned to your wallet.'),
(28, 'naps_temp', 'status_delete', 'Bid has been deleted.'),
(29, 'naps_temp', 'status_error', 'In the bid there is an error. Please contact technical support.'),
(30, 'admin', 'w0', '0'),
(31, 'admin', 'w1', '0'),
(32, 'admin', 'w2', '1'),
(33, 'admin', 'w3', '1'),
(34, 'admin', 'w4', '1'),
(35, 'admin', 'w5', '1'),
(36, 'admin', 'w6', '1'),
(37, 'admin', 'w7', '1'),
(38, 'admin', 'w8', '1'),
(39, 'txtxml', 'txt', '1'),
(40, 'txtxml', 'xml', '1'),
(41, 'txtxml', 'numtxt', '5'),
(42, 'txtxml', 'numxml', '5'),
(43, 'favicon', '', '/wp-content/uploads/favicon.png'),
(44, 'logo', '', ''),
(45, 'textlogo', '', ''),
(46, 'user_fields', '', 'a:8:{s:5:\"login\";i:1;s:9:\"last_name\";i:1;s:10:\"first_name\";i:1;s:11:\"second_name\";i:1;s:10:\"user_phone\";i:1;s:10:\"user_skype\";i:1;s:7:\"website\";i:1;s:13:\"user_passport\";i:1;}'),
(47, 'user_fields_change', '', 'a:8:{s:10:\"user_email\";i:1;s:9:\"last_name\";i:1;s:10:\"first_name\";i:1;s:11:\"second_name\";i:1;s:10:\"user_phone\";i:1;s:10:\"user_skype\";i:1;s:7:\"website\";i:1;s:13:\"user_passport\";i:1;}'),
(48, 'persdata', 'last_name', '1'),
(49, 'persdata', 'first_name', '1'),
(50, 'persdata', 'second_name', '1'),
(51, 'persdata', 'user_phone', '1'),
(52, 'help', 'last_name', 'Enter the last name as it specified in your passport.'),
(53, 'help', 'first_name', 'Enter the first name as it specified in your passport.'),
(54, 'help', 'second_name', 'Enter the patronymic as it specified in your passport.'),
(55, 'help', 'user_email', 'Enter a valid email address.'),
(56, 'help', 'user_phone', 'Enter your cell phone number in international format to communicate with you. Example: +71234567890.'),
(57, 'help', 'user_skype', 'Enter your Skype name to communicate with you.'),
(58, 'numsybm_count', '', '12'),
(59, 'exchange', 'techregtext', ''),
(60, 'exchange', 'gostnaphide', '0'),
(61, 'exchange', 'an1_hidden', '0'),
(62, 'exchange', 'an2_hidden', '0'),
(63, 'exchange', 'an3_hidden', '0'),
(64, 'exchange', 'admin_mail', '0'),
(65, 'persdata', 'user_skype', '0'),
(66, 'persdata', 'user_passport', '0'),
(67, 'exchange', 'adjust', '0'),
(68, 'exchange', 'beautynum', '0'),
(69, 'exchange', 'maxsymb_all', '4'),
(70, 'exchange', 'maxsymb_reserv', '4'),
(71, 'exchange', 'maxsymb_course', '4'),
(72, 'help', 'user_passport', 'Enter your passport nubmer.'),
(73, 'htmlmap', 'exclude_page', 'a:0:{}'),
(74, 'htmlmap', 'exchanges', '1'),
(75, 'htmlmap', 'pages', '1'),
(76, 'htmlmap', 'news', '1'),
(77, 'xmlmap', 'exclude_page', 'a:0:{}'),
(78, 'xmlmap', 'exchanges', '1'),
(79, 'xmlmap', 'pages', '1'),
(80, 'xmlmap', 'news', '1'),
(81, 'partners', 'wref', '0'),
(82, 'partners', 'payouttext', ''),
(83, 'partners', 'clife', '365'),
(84, 'partners', 'text_banners', '0'),
(85, 'partners', 'calc', '0'),
(86, 'partners', 'reserv', '0'),
(87, 'txtxml', 'decimal', '4');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_postmeta`
--

CREATE TABLE `eb_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_postmeta`
--

INSERT INTO `eb_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(299, 171, '_menu_item_menu_item_parent', '0'),
(300, 171, '_menu_item_object_id', '171'),
(301, 171, '_menu_item_object', 'custom'),
(302, 171, '_menu_item_target', ''),
(275, 151, '_wp_page_template', 'exb-pluginpage.php'),
(276, 152, '_wp_page_template', 'exb-pluginpage.php'),
(459, 154, '_wp_page_template', 'exb-pluginpage.php'),
(279, 156, '_wp_page_template', 'exb-pluginpage.php'),
(280, 169, '_menu_item_type', 'post_type'),
(281, 169, '_menu_item_menu_item_parent', '0'),
(282, 169, '_menu_item_object_id', '157'),
(283, 169, '_menu_item_object', 'page'),
(309, 160, '_edit_lock', '1431270185:1'),
(310, 173, '_edit_last', '1'),
(311, 173, '_edit_lock', '1412192810:1'),
(312, 173, '_wp_page_template', 'default'),
(322, 166, '_edit_lock', '1412192726:1'),
(323, 166, '_edit_last', '1'),
(324, 166, '_wp_page_template', 'default'),
(325, 160, '_edit_last', '1'),
(326, 160, '_wp_page_template', 'default'),
(327, 188, '_menu_item_type', 'post_type'),
(328, 188, '_menu_item_menu_item_parent', '171'),
(329, 188, '_menu_item_object_id', '160'),
(330, 188, '_menu_item_object', 'page'),
(331, 188, '_menu_item_target', ''),
(332, 188, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(333, 188, '_menu_item_xfn', ''),
(334, 188, '_menu_item_url', ''),
(336, 189, '_menu_item_type', 'post_type'),
(337, 189, '_menu_item_menu_item_parent', '171'),
(338, 189, '_menu_item_object_id', '159'),
(339, 189, '_menu_item_object', 'page'),
(340, 189, '_menu_item_target', ''),
(341, 189, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(342, 189, '_menu_item_xfn', ''),
(343, 189, '_menu_item_url', ''),
(345, 159, '_edit_lock', '1412192502:1'),
(346, 159, '_edit_last', '1'),
(347, 159, '_wp_page_template', 'default'),
(348, 165, '_edit_lock', '1412192787:1'),
(349, 165, '_edit_last', '1'),
(350, 165, '_wp_page_template', 'default'),
(351, 194, '_menu_item_type', 'post_type'),
(352, 194, '_menu_item_menu_item_parent', '0'),
(353, 194, '_menu_item_object_id', '165'),
(354, 194, '_menu_item_object', 'page'),
(355, 194, '_menu_item_target', ''),
(356, 194, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(357, 194, '_menu_item_xfn', ''),
(358, 194, '_menu_item_url', ''),
(305, 171, '_menu_item_url', '#'),
(296, 170, '_menu_item_url', ''),
(295, 170, '_menu_item_xfn', ''),
(294, 170, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(293, 170, '_menu_item_target', ''),
(292, 170, '_menu_item_object', 'page'),
(291, 170, '_menu_item_object_id', '158'),
(290, 170, '_menu_item_menu_item_parent', '0'),
(289, 170, '_menu_item_type', 'post_type'),
(298, 171, '_menu_item_type', 'custom'),
(287, 169, '_menu_item_url', ''),
(284, 169, '_menu_item_target', ''),
(285, 169, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(286, 169, '_menu_item_xfn', ''),
(215, 130, '_menu_item_object', 'custom'),
(212, 130, '_menu_item_type', 'custom'),
(213, 130, '_menu_item_menu_item_parent', '0'),
(214, 130, '_menu_item_object_id', '130'),
(304, 171, '_menu_item_xfn', ''),
(303, 171, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(219, 130, '_menu_item_url', '/'),
(216, 130, '_menu_item_target', ''),
(217, 130, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(218, 130, '_menu_item_xfn', ''),
(369, 155, '_edit_lock', '1412192661:1'),
(372, 213, '_wp_page_template', 'exb-pluginpage.php'),
(373, 215, '_menu_item_type', 'post_type'),
(374, 215, '_menu_item_menu_item_parent', '0'),
(375, 215, '_menu_item_object_id', '214'),
(376, 215, '_menu_item_object', 'page'),
(377, 215, '_menu_item_target', ''),
(378, 215, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(379, 215, '_menu_item_xfn', ''),
(380, 215, '_menu_item_url', ''),
(384, 164, '_edit_lock', '1412192537:1'),
(385, 157, '_wp_page_template', 'exb-pluginpage.php'),
(386, 158, '_wp_page_template', 'exb-pluginpage.php'),
(387, 162, '_wp_page_template', 'exb-pluginpage.php'),
(388, 164, '_wp_page_template', 'exb-pluginpage.php'),
(389, 167, '_wp_page_template', 'exb-pluginpage.php'),
(390, 168, '_wp_page_template', 'exb-pluginpage.php'),
(391, 214, '_wp_page_template', 'exb-pluginpage.php'),
(392, 157, '_edit_lock', '1433942891:1'),
(393, 157, '_edit_last', '1'),
(394, 164, '_edit_last', '1'),
(395, 161, '_edit_lock', '1412192564:1'),
(396, 161, '_edit_last', '1'),
(397, 161, '_wp_page_template', 'default'),
(398, 156, '_edit_last', '1'),
(399, 156, '_edit_lock', '1412192721:1'),
(400, 151, '_edit_lock', '1412192594:1'),
(401, 151, '_edit_last', '1'),
(402, 213, '_edit_last', '1'),
(403, 213, '_edit_lock', '1412192760:1'),
(404, 162, '_edit_last', '1'),
(405, 162, '_edit_lock', '1412192793:1'),
(406, 155, '_edit_last', '1'),
(407, 155, '_wp_page_template', 'default'),
(411, 152, '_edit_lock', '1412192828:1'),
(410, 152, '_edit_last', '1'),
(458, 153, '_wp_page_template', 'exb-pluginpage.php'),
(416, 158, '_edit_last', '1'),
(417, 158, '_edit_lock', '1412192848:1'),
(418, 168, '_edit_last', '1'),
(419, 168, '_edit_lock', '1412192895:1'),
(420, 167, '_edit_last', '1'),
(421, 167, '_edit_lock', '1412192969:1'),
(422, 214, '_edit_last', '1'),
(423, 214, '_edit_lock', '1412192978:1'),
(424, 242, '_menu_item_type', 'post_type'),
(425, 242, '_menu_item_menu_item_parent', '0'),
(426, 242, '_menu_item_object_id', '213'),
(427, 242, '_menu_item_object', 'page'),
(428, 242, '_menu_item_target', ''),
(429, 242, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(430, 242, '_menu_item_xfn', ''),
(431, 242, '_menu_item_url', ''),
(433, 243, '_menu_item_type', 'post_type'),
(434, 243, '_menu_item_menu_item_parent', '0'),
(435, 243, '_menu_item_object_id', '173'),
(436, 243, '_menu_item_object', 'page'),
(437, 243, '_menu_item_target', ''),
(438, 243, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(439, 243, '_menu_item_xfn', ''),
(440, 243, '_menu_item_url', ''),
(442, 244, '_menu_item_type', 'post_type'),
(443, 244, '_menu_item_menu_item_parent', '0'),
(444, 244, '_menu_item_object_id', '165'),
(445, 244, '_menu_item_object', 'page'),
(446, 244, '_menu_item_target', ''),
(447, 244, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(448, 244, '_menu_item_xfn', ''),
(449, 244, '_menu_item_url', ''),
(451, 248, '_wp_page_template', 'exb-pluginpage.php'),
(456, 248, '_edit_lock', '1447418848:1'),
(457, 248, '_edit_last', '1'),
(463, 258, '_wp_attached_file', 'Advcash.png'),
(464, 258, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"Advcash.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(465, 259, '_wp_attached_file', 'Alfabank.png'),
(466, 259, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Alfabank.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(467, 260, '_wp_attached_file', 'Alipay.png'),
(468, 260, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Alipay.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(469, 261, '_wp_attached_file', 'Avangardbank.png'),
(470, 261, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:16:\"Avangardbank.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(471, 262, '_wp_attached_file', 'Bank-perevod.png'),
(472, 262, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:16:\"Bank-perevod.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(473, 263, '_wp_attached_file', 'Bitcoin.png'),
(474, 263, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"Bitcoin.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(582, 316, '_wp_attached_file', 'bestexchangers.gif'),
(583, 316, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:18:\"bestexchangers.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(572, 311, '_wp_attached_file', 'monero.png'),
(573, 311, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"monero.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(479, 266, '_wp_attached_file', 'Cash-EUR.png'),
(480, 266, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Cash-EUR.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(481, 267, '_wp_attached_file', 'Cash-RUB.png'),
(482, 267, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Cash-RUB.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(483, 268, '_wp_attached_file', 'Cash-USD.png'),
(484, 268, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Cash-USD.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(485, 269, '_wp_attached_file', 'E-dinar.png'),
(486, 269, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"E-dinar.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(487, 270, '_wp_attached_file', 'exmo.png'),
(488, 270, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:8:\"exmo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(489, 271, '_wp_attached_file', 'favicon.png'),
(490, 271, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:54;s:6:\"height\";i:38;s:4:\"file\";s:11:\"favicon.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(574, 312, '_wp_attached_file', 'ether.png'),
(575, 312, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"ether.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(493, 273, '_wp_attached_file', 'Liqpay.png'),
(494, 273, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Liqpay.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(495, 274, '_wp_attached_file', 'Litecoin.png'),
(496, 274, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Litecoin.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(497, 275, '_wp_attached_file', 'Livecoin.png'),
(498, 275, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Livecoin.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(499, 276, '_wp_attached_file', 'NixMoney.png'),
(500, 276, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"NixMoney.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(576, 313, '_wp_attached_file', 'dash.png'),
(577, 313, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:8:\"dash.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(568, 309, '_wp_attached_file', 'zcash.png'),
(569, 309, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"zcash.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(570, 310, '_wp_attached_file', 'wawes.png'),
(571, 310, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"wawes.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(507, 280, '_wp_attached_file', 'Paxum.png'),
(508, 280, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"Paxum.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(509, 281, '_wp_attached_file', 'Payeer.png'),
(510, 281, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Payeer.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(511, 282, '_wp_attached_file', 'Paymer.png'),
(512, 282, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Paymer.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(513, 283, '_wp_attached_file', 'Paypal.png'),
(514, 283, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Paypal.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(515, 284, '_wp_attached_file', 'Payza.png'),
(516, 284, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"Payza.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(517, 285, '_wp_attached_file', 'Perfect-Money.png'),
(518, 285, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:17:\"Perfect-Money.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(584, 317, '_wp_attached_file', 'exchangesumo.png'),
(585, 317, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:16:\"exchangesumo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(521, 287, '_wp_attached_file', 'PMe-voucher.png'),
(522, 287, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:15:\"PMe-voucher.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(523, 288, '_wp_attached_file', 'Privatbank.png'),
(524, 288, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:14:\"Privatbank.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(525, 289, '_wp_attached_file', 'Promsvyazbank.png'),
(526, 289, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:17:\"Promsvyazbank.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(527, 290, '_wp_attached_file', 'Qiwi.png'),
(528, 290, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:8:\"Qiwi.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(529, 291, '_wp_attached_file', 'Russstandart.png'),
(530, 291, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:16:\"Russstandart.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(531, 292, '_wp_attached_file', 'Sberbank.png'),
(532, 292, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Sberbank.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(533, 293, '_wp_attached_file', 'Skrill.png'),
(534, 293, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Skrill.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(535, 294, '_wp_attached_file', 'SolidTrustPay.png'),
(536, 294, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:17:\"SolidTrustPay.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(588, 319, '_wp_attached_file', 'monitorkursov.gif'),
(589, 319, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:17:\"monitorkursov.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(539, 296, '_wp_attached_file', 'Tinkoff.png'),
(540, 296, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"Tinkoff.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(541, 297, '_wp_attached_file', 'Visa-MasterCard.png'),
(542, 297, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:19:\"Visa-MasterCard.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(543, 298, '_wp_attached_file', 'VTB24.png'),
(544, 298, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"VTB24.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(545, 299, '_wp_attached_file', 'WebMoney.png'),
(546, 299, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"WebMoney.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(566, 308, '_wp_attached_file', 'ripple.png'),
(567, 308, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"ripple.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(590, 320, '_wp_attached_file', 'obmenvse.gif'),
(591, 320, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:12:\"obmenvse.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(586, 318, '_wp_attached_file', 'goodkurs.gif'),
(587, 318, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:12:\"goodkurs.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(553, 303, '_wp_attached_file', 'Yandex.png'),
(554, 303, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Yandex.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(555, 304, '_wp_attached_file', 'Z-payment.png'),
(556, 304, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:13:\"Z-payment.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(578, 314, '_wp_attached_file', 'bitcoincash.png'),
(579, 314, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:15:\"bitcoincash.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(580, 315, '_wp_attached_file', 'wex.png'),
(581, 315, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:7:\"wex.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(592, 321, '_wp_attached_file', 'okku.gif'),
(593, 321, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:8:\"okku.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(594, 322, '_wp_attached_file', 'proobmen.gif'),
(595, 322, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:12:\"proobmen.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(596, 323, '_wp_attached_file', 'scanmoney.png'),
(597, 323, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:86;s:6:\"height\";i:27;s:4:\"file\";s:13:\"scanmoney.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(598, 324, '_wp_attached_file', 'secretovobmena.gif'),
(599, 324, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:18:\"secretovobmena.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(600, 325, '_wp_attached_file', 'udifo.png'),
(601, 325, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:9:\"udifo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(602, 326, '_wp_attached_file', 'kurses.gif'),
(603, 326, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:10:\"kurses.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(604, 327, '_wp_attached_file', 'webmoney-1.png'),
(605, 327, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:14:\"webmoney-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(606, 328, '_wp_attached_file', 'okchanger.png'),
(607, 328, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:90;s:6:\"height\";i:32;s:4:\"file\";s:13:\"okchanger.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(608, 329, '_wp_attached_file', 'kursexpert.png'),
(609, 329, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:14:\"kursexpert.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(610, 330, '_wp_attached_file', 'glazok.gif'),
(611, 330, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:10:\"glazok.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(612, 331, '_wp_attached_file', 'bestchange.gif'),
(613, 331, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:14:\"bestchange.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(614, 332, '_wp_attached_file', 'Stellar.png'),
(615, 332, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"Stellar.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(616, 333, '_wp_attached_file', 'Ethereum-Classic.png'),
(617, 333, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:20:\"Ethereum-Classic.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(618, 334, '_wp_attached_file', 'EOS.png'),
(619, 334, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:7:\"EOS.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(620, 335, '_wp_attached_file', 'DigiByte.png'),
(621, 335, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"DigiByte.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(622, 336, '_wp_attached_file', 'Cardano.png'),
(623, 336, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"Cardano.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(624, 337, '_menu_item_type', 'post_type'),
(625, 337, '_menu_item_menu_item_parent', '0'),
(626, 337, '_menu_item_object_id', '155'),
(627, 337, '_menu_item_object', 'page'),
(628, 337, '_menu_item_target', ''),
(629, 337, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(630, 337, '_menu_item_xfn', ''),
(631, 337, '_menu_item_url', ''),
(633, 339, '_wp_attached_file', 'bitex.png'),
(634, 339, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"bitex.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_posts`
--

CREATE TABLE `eb_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(255) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_posts`
--

INSERT INTO `eb_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(188, 1, '2013-04-27 15:34:56', '2013-04-27 11:34:56', '', 'Terms and conditions', '', 'publish', 'open', 'closed', '', 'usloviya-i-registratsii', '', '', '2019-02-12 17:40:35', '2019-02-12 14:40:35', '', 0, 'http://exchangeen.best-curs.info/?p=188', 7, 'nav_menu_item', '', 0),
(189, 1, '2013-04-27 15:35:14', '2013-04-27 11:35:14', ' ', '', '', 'publish', 'open', 'closed', '', '189', '', '', '2019-02-12 17:40:35', '2019-02-12 14:40:35', '', 0, 'http://exchangeen.best-curs.info/?p=189', 6, 'nav_menu_item', '', 0),
(194, 1, '2013-04-27 16:00:13', '2013-04-27 12:00:13', '', 'Rules', '', 'publish', 'open', 'closed', '', 'pravila', '', '', '2019-02-12 17:40:35', '2019-02-12 14:40:35', '', 0, 'http://exchangeen.best-curs.info/?p=194', 4, 'nav_menu_item', '', 0),
(151, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[changetable]', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2014-10-01 23:45:33', '2014-10-01 19:45:33', '', 0, 'http://exchangeen.best-curs.info/home/', 0, 'page', '', 0),
(152, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[xchange]', 'Exchange', '', 'publish', 'closed', 'closed', '', 'xchange', '', '', '2014-10-01 23:47:08', '2014-10-01 19:47:08', '', 0, 'http://exchangeen.best-curs.info/xchange/', 0, 'page', '', 0),
(155, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 'News', '', 'publish', 'closed', 'closed', '', 'news', '', '', '2014-10-01 23:46:42', '2014-10-01 19:46:42', '', 0, 'http://exchangeen.best-curs.info/news/', 0, 'page', '', 0),
(156, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_payouts]', 'Affiliate money withdrawal', '', 'publish', 'closed', 'closed', '', 'payouts', '', '', '2014-10-01 23:45:21', '2014-10-01 19:45:21', '', 0, 'http://exchangeen.best-curs.info/payouts/', 0, 'page', '', 0),
(157, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', 'We are always ready to answer your questions and listen to your suggestions on improvement of our service.\r\n\r\nUse this form to ask us a question, or to report an error. Please make your message more detailed, then we can solve the problem much faster.\r\n\r\n[exb_contact_form]', 'Feedback', '', 'publish', 'closed', 'closed', '', 'feedback', '', '', '2015-06-10 16:24:53', '2015-06-10 13:24:53', '', 0, 'http://exchangeen.best-curs.info/feedback/', 0, 'page', '', 0),
(158, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_reviewspage]', 'Reviews', '', 'publish', 'closed', 'closed', '', 'reviews', '', '', '2014-10-01 23:47:28', '2014-10-01 19:47:28', '', 0, 'http://exchangeen.best-curs.info/reviews/', 0, 'page', '', 0),
(159, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '<h3>For Affiliates</h3>\r\nWe invite you to register a personal account to activate the progressive discount system, track the status of your applications and exchange statistics.\r\n\r\nAlso we offer you to participate in an affiliate program to attract customers. By signing up for our affiliate program, you will receive an interest on the amount exchanged. After registration you will get a wide variety of promotional materials (texts, banners), as well as rates export file for monitoring to choose. All you need is to invite visitors to our website, placing promotional materials on your home pages, blogs, forums, question and answer services, message boards and other resources. Your link will contain a unique code that will identify that a visitor came using your link.\r\n<h3>Registration</h3>\r\nTo register an account, follow <a href=\"/register/\">this link</a> and fill out a simple form. Before registering, you will need to get acquainted with the conditions and accept the user agreement.\r\n<h3>Logging in</h3>\r\nIf you are a registered user of our website, please log in using the login form below:\r\n\r\n[exb_loginform]', 'Authorization', '', 'publish', 'closed', 'closed', '', 'login', '', '', '2014-10-01 23:43:49', '2014-10-01 19:43:49', '', 0, 'http://exchangeen.best-curs.info/login/', 0, 'page', '', 0),
(160, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '1. Registered users have the right to use a progressive discount system when committing the exchange:\r\n<ul>\r\n	<li>0-99 USD — 1%</li>\r\n	<li>100-999 USD — 2%</li>\r\n	<li>1000-4999 USD — 3%</li>\r\n	<li>5000- 9999 USD — 4%</li>\r\n	<li>10000-19999 USD — 5%</li>\r\n	<li>over 20000 USD — 6%</li>\r\n</ul>\r\n2. Calculation and payoff within an affiliate program are maintained in United States dollars (USD).\r\n\r\n3. The minimum amount for the withdrawal of money earned from an affiliate account is 5 USD.\r\n\r\n4. For every exchange made using your affiliate link, you receive a reward at a rate of 1% to 6% of the amount exchanged. The percentage depends on the amount of exchanges made using your affiliate link:\r\n<ul>\r\n	<li>0-99 USD — 1%</li>\r\n	<li>100-999 USD — 2%</li>\r\n	<li>1000-4999 USD — 3%</li>\r\n	<li>5000- 9999 USD — 4%</li>\r\n	<li>10000-19999 USD — 5%</li>\r\n	<li>over 20000 USD — 6%</li>\r\n</ul>\r\n4.1. These values ​​of affiliate rewards may be changed from time to time. In this case, all earnings are retained in the account taking into account the previous rates.\r\n\r\n5. It should be clearly stated on the services provided by our site on the page where you post information about us. In advertising texts, it is prohibited to mention of a \"free bonus\" being available on our site.\r\n\r\n6. It is prohibited to place an affiliate link:\r\n<ul>\r\n	<li>in mass mail (spam);</li>\r\n	<li>on sites, forcing to open a browser window, or opening sites in a hidden frame;</li>\r\n	<li>on sites distributing any materials that, directly or indirectly, violate the laws of the Russian Federation;</li>\r\n	<li>on sites publishing lists of sites with \"free bonuses\";</li>\r\n	<li>on web pages, enclosed from public view by means of authorization (various social networks, closed sections of forums, etc.).</li>\r\n	<li>Sites that violate one or more of the above rules will be blacklisted for our affiliate program. There will be no payoffs for visitors linked from these sites.</li>\r\n</ul>\r\n7 . When failing to comply with these terms and conditions, your account will be blocked without making payoffs and explanation.\r\n\r\n8. An affiliate is solely responsible for the safety of its credentials (username and password) to access the account.\r\n\r\n9. These terms and conditions may be changed unilaterally without notifying participants of the program, but with the publication on this page.\r\n\r\n[exb_registerform]', 'Terms and conditions for registering personal and affiliate accounts', '    \r\n    \r\n    \r\n    \r\n    \r\n    ', 'publish', 'closed', 'closed', '', 'register', '', '', '2015-05-10 18:05:27', '2015-05-10 15:05:27', '', 0, 'http://exchangeen.best-curs.info/register/', 0, 'page', '', 0),
(161, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_lostpassform]', 'Password reset', '', 'publish', 'closed', 'closed', '', 'lostpass', '', '', '2014-10-01 23:44:51', '2014-10-01 19:44:51', '', 0, 'http://exchangeen.best-curs.info/lostpass/', 0, 'page', '', 0),
(162, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_account]', 'Personal account', '', 'publish', 'closed', 'closed', '', 'account', '', '', '2014-10-01 23:46:32', '2014-10-01 19:46:32', '', 0, 'http://exchangeen.best-curs.info/account/', 0, 'page', '', 0),
(164, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_usersobmen]', 'Your transactions', '', 'publish', 'closed', 'closed', '', 'usersobmen', '', '', '2014-10-01 23:44:25', '2014-10-01 19:44:25', '', 0, 'http://exchangeen.best-curs.info/usersobmen/', 0, 'page', '', 0),
(165, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '<strong>1. Parties to the agreement.</strong>\r\n\r\nThe Agreement is concluded between the online service for the exchange of digital currency, hereinafter referred to as the Contractor - for one part, and the Customer, represented by a person who used the services of the Contractor, - for the other part.\r\n\r\n<strong>2. List of terms.</strong>\r\n\r\n2.1. Digital Currency Exchange - automated product of the online service, which is provided by the Contractor under these rules.\r\n\r\n2.2. Customer - a natural person, agreeing to the terms of the Contractor and this agreement that it enters into.\r\n\r\n2.3. Digital currency - a standard unit of a particular payment system, which corresponds to the calculations of electronic systems and indicates the scope of rights corresponding to a specific agreement on electronic payment system and its Customer.\r\n\r\n2.4. Application - information transmitted by the Customer for use of the Contractor\'s funds in electronic form and indicating that he accepts the terms of use of the service offered by the Contractor herein.\r\n\r\n<strong>3. Terms and conditions of the agreement.</strong>\r\n\r\nThese rules are considered to be subject to the conditions of the public offer, which enters into force at the time of submission of an application by the Customer and is one of the main components of this agreement. The information about the conditions of application submission specified by the Contractor, is a Public offer. The main part of a public offer are actions made in the completion of the application submission by the Customer showing his exact intentions to make a transaction on the terms proposed by the Contractor before the end of this application. Time, date, and parameters of the application are created automatically by the Contractor by the end of application submission. The proposal should be accepted by the Customer within 24 hours before the end of formation of the application. Service agreement comes into force from the moment of receipt of digital currency in the full amount specified in the application, from the Customer according to the details set forth by the Contractor. Transactions with digital currency are accounted according to the rules, regulations and format of electronic payment systems/ The agreement is valid for a period which is set from the date of submitting the application and continued until terminated by either party.\r\n\r\n<strong>4. Matter of the agreement.</strong>\r\n\r\nUsing technical methods, the Contractor undertakes to perform digital currency exchange for a commission from the Customer, after the submitting the application by this person, and makes it through the sale of digital currency to persons wishing to purchase it for the money amount which is not lower than that in the application submitted by the Customer. The Contractor undertakes to transfer money according to the details specified by the Customer. In case when a profit occurs at the time of exchange, it remains on the account of the Contractor, as an additional benefit and a premium for commission services.\r\n\r\n<strong>5. Additional provisions.</strong>\r\n\r\n5.1. If the Contractor receives an amount on its account that differs from that indicated in the application, the Contractor makes a resettlement, which corresponds to the actual receipt of digital currency. Should this amount exceed the amount specified in the application for more than 10%, the Contractor terminates the contract unilaterally and all funds are returned to the Customer\'s details, taking into account the amount deducted for commission expenses during the transfer.\r\n\r\n5.2. Should the digital currency not be sent by the Contractor to the specified details of the Customer within 24 hours, the Customer has the full right to demand the termination of the agreement and cancel the application, thereby making the return of digital currency on its account in full. Application for termination of the agreement and return of digital currency is performed by the Contractor in the event that the money has not yet been transferred according to the details of the Customer. In case of terminating the agreement, the return of e-currencies is made within 24 hours of receipt of the application for termination of the agreement. If a delay in the return occurred through no fault of the Contractor, it will not take responsibility for it.\r\n\r\n5.3. If no digital currency arrives from the Customer to the Contractor within the specified period from the date of submitting the application by the Customer, the agreement between the parties shall be terminated by the Contractor unilaterally, since the agreement does not enter into force. There may be no notice about it sent to the Customer. Shall no digital currency arrive to the details of the Contractor after the deadline, then such funds are transferred back to the account of the Customer, and all commission expenses associated with the transfer are deducted from that amount.\r\n\r\n5.4. If there is a delay in the transfer of funds to the account details specified by the Customer, through a fault of a payment system, the Contractor shall not be liable for any damage caused as a result of a delayed transfer. In this case, the Customer shall agree that all claims would be referred to the payment system, and the Contractor shall provide assistance as far as possible under the law.\r\n\r\n5.5. In case of forgery of communication flows, or due to influence in order to degrade the performance of the Contractor, namely its software code, the application is suspended, and the money transferred are subject to resettlement in accordance with the agreement in effect. Shall the Customer not agree to the resettlement, he has every right to terminate the agreement and the digital currency shall be transferred to the account details specified by the Customer.\r\n\r\n5.6. In the case of using the services of the Contractor, the Customer fully agrees that the Contractor shall bear a limited liability corresponding to these rules for obtaining digital currency and give no additional guarantees to the Customer and shall have no additional liability before the Customer. Accordingly, the Customer shall not bear an additional liability to the Contractor.\r\n\r\n5.7. The Customer agrees to comply with applicable laws and not to tamper any communication flows as well as create any obstacles to the normal operation of the program code of the Contractor.\r\n\r\n5.8. The Contractor shall not be liable for any damage or consequences of an erroneous transfer of e-currency in the event that Customer have specified wrong details during application submission.\r\n\r\n<strong>6. Warranty period</strong>\r\n\r\nWithin 24 hours of the execution of the digital currency exchange, the Contractor warrants for services provided, unless otherwise noted.\r\n\r\n<strong>7. Contingencies.</strong>\r\n\r\nIn the case where unforeseen circumstances that contribute to non-compliance with terms of the agreement by the Contractor during the processing of the Customer\'s application, the timing of application accomplishment are delayed for the corresponding period of the duration of the force majeure. The Contractor is not responsible for overdue obligations.\r\n\r\n<strong>8. Form of agreement.</strong>\r\n\r\nBoth parties, represented by the Contractor and the Customer, shall take this agreement as an agreement equivalent to the validity of the contract designated in writing.\r\n\r\n<strong>9. Usage of cards of England, Germany and the United States.</strong>\r\n\r\nFor cardholders from England, Germany and the United States, the arrangements for the transfer of digital currency are extended for an indefinite period, corresponding to the period required for full verification of cardholder data. For the whole period the money is not subject to any transactions and are retained in full in the account of the Contractor.\r\n\r\n<strong>10 Claims and disputes.</strong>\r\nClaims under this agreement are received by the Contractor in the form of e-mail where the Customer specifies the essence of the claim. This mail is sent to the details specified on site of the Contractor.\r\n\r\n<strong>11. Exchange transactions performance.</strong>\r\n\r\n11.1. It is expressly prohibited to use the services of the Contractor to carry out illegal transfers and fraud. At the conclusion of this agreement, the Customer agrees to comply with these requirements and to be criminally liable in the case of fraud under the laws in force.\r\n\r\n11.2. In case of inability to fulfill orders automatically, through no fault of the Contractor, such as lack of communication, lack of funds, or erroneous data of the Customer, the money is transferred to the account within the next 24 hours or returned to the account details of the Customer, net commission expense.\r\n\r\n11.3. On demand the Contractor is entitled to release information on the transfer of electronic currency to law enforcement bodies, administration of payment systems, as well as to victims of misconduct, victims of proven judicial fraud.\r\n\r\n11.4. The Customer agrees to submit all the documents proving his identity, in case of suspicion of fraud and money laundering.\r\n\r\n11.5. The Customer agrees not to interfere with the work of the Contractor and not to cause damage to its hardware and software, as well as the Customer undertakes to provide accurate information to ensure compliance with all terms of the agreement by the Contractor.\r\n\r\n<strong>12. Liability disclaimer.</strong>\r\n\r\nThe Contractor shall have the right to refuse to sign the agreement and accomplish the application without explanation. This paragraph shall apply with respect to any client.', 'Site rules', '', 'publish', 'closed', 'closed', '', 'tos', '', '', '2014-10-01 23:48:47', '2014-10-01 19:48:47', '', 0, 'http://exchangeen.best-curs.info/tos/', 0, 'page', '', 0),
(166, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[from_user]\r\n<b>Question: How does the affiliate program work?</b>\r\n\r\nAnswer: By registering in our affiliate program, you will get a unique affiliate ID, which is added to all your links (?exbpid=777) and the HTML-code. You can post links on any pages of our service on your website, blog, page, in communities and social networks.\r\n\r\n<b>Question: How much will I earn by participating in your affiliate program?</b>\r\n\r\nAnswer: It depends on many factors such as:\r\n<ol>\r\n	<li>1. Traffic to your web site or sites where you post information about us.</li>\r\n	<li>Compliance of the subject of the site with the target audience, which may be interested in the services of currency exchange. Simply put, do not rely on a large number of clicks on your affiliate link posted on the website dedicated to parrot breeding.</li>\r\n	<li>Proper presentation of information. For example, very few people will like only one reference to \"foreign exchange\" without any description somewhere in the corner of the web page.</li>\r\n</ol>\r\n<b>Question: If I put my affiliate link and a signature, will the transitions and all other conditions of the AP also be considered?</b>\r\n\r\nAnswer: Yes, they certainly will.\r\n\r\n<b>Question: There are other affiliate programs installed on my site. May I be your affiliate?</b>\r\n\r\nAnswer: Yes, you may. We impose no restrictions on working with other affiliate programs.\r\n\r\n<b>Question: Is the qualify of my site sufficient to participate in an affiliate program?</b>\r\n\r\nAnswer: We welcome any sites that do not contradict the conditions of our affiliate program. You can see the list of conditions <a href=\"/register/\"><span style=\"color: #1155cc;\"><span style=\"text-decoration: underline;\">here</span></span></a> (paragraph 6).\r\n\r\n<b>Question: How many levels are there in your affiliate program? Is there any reward for involving new affiliates?</b>\r\n\r\nAnswer: Our affiliate program has 6 levels. There is no reward for involving any further affiliates.\r\n\r\n<b>Question: I can not log in to my affiliate account. It shows \"Invalid combination of username and password\". But, in this case, I\'m sure I enter the password correctly.</b>\r\n\r\nAnswer: Make sure that when you enter a password you Russian keyboard layout or Caps Lock are not turned on. If you just remember only login - use the <a href=\"/lostpass/\"><span style=\"color: #1155cc;\"><span style=\"text-decoration: underline;\">password reminder</span></span></a>. The password will be sent to your e-mail, specified during registration.\r\n\r\n<b>Question: How are the earned money paid off?</b>\r\n\r\nAnswer: Affiliate payments are made via WebMoney in the currency WMZ onto a wallet, said by the affiliate during a registration in affiliate program. As a rule, it takes no more than 2-3 hours. Do not rush to send us a message if it has not passed 48 hours from the filing date yet - the administrator sees all application and will process yours anyway.\r\n[/from_user]', 'Affiliate\'s FAQ', '', 'publish', 'closed', 'closed', '', 'partnersfaq', '', '', '2014-10-01 23:47:45', '2014-10-01 19:47:45', '', 0, 'http://exchangeen.best-curs.info/partnersfaq/', 0, 'page', '', 0),
(167, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_banners]', 'Promotional materials', '', 'publish', 'closed', 'closed', '', 'banners', '', '', '2014-10-01 23:49:29', '2014-10-01 19:49:29', '', 0, 'http://exchangeen.best-curs.info/banners/', 0, 'page', '', 0),
(168, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_partnerstats]', 'Affiliate account', '', 'publish', 'closed', 'closed', '', 'partnerstats', '', '', '2014-10-01 23:48:15', '2014-10-01 19:48:15', '', 0, 'http://exchangeen.best-curs.info/partnerstats/', 0, 'page', '', 0),
(169, 1, '2013-04-26 16:47:46', '2013-04-26 12:47:46', ' ', '', '', 'publish', 'open', 'closed', '', '169', '', '', '2019-02-12 17:40:35', '2019-02-12 14:40:35', '', 0, 'http://exchangeen.best-curs.info/?p=169', 9, 'nav_menu_item', '', 0),
(170, 1, '2013-04-26 16:47:46', '2013-04-26 12:47:46', ' ', '', '', 'publish', 'open', 'closed', '', '170', '', '', '2019-02-12 17:40:34', '2019-02-12 14:40:34', '', 0, 'http://exchangeen.best-curs.info/?p=170', 2, 'nav_menu_item', '', 0),
(130, 1, '2013-04-26 16:42:14', '2013-04-26 12:42:14', '', 'Home', '', 'publish', 'open', 'closed', '', 'obmen', '', '', '2019-02-12 17:40:34', '2019-02-12 14:40:34', '', 0, 'http://exchangeen.best-curs.info/?p=130', 1, 'nav_menu_item', '', 0),
(171, 1, '2013-04-27 12:34:38', '2013-04-27 08:34:38', '', 'Account', '', 'publish', 'open', 'closed', '', 'partneram', '', '', '2019-02-12 17:40:35', '2019-02-12 14:40:35', '', 0, 'http://exchangeen.best-curs.info/?p=171', 5, 'nav_menu_item', '', 0),
(173, 1, '2013-04-27 14:14:37', '2013-04-27 10:14:37', 'Dear customers! Security of transactions can be compromised due to circumstances independent from our service. To avoid this, we recommend that you read the following e-currency conversion rules:\r\n<ul>\r\n	<li>Always ask for confirmation of identity of the person on the details of which you are going to transfer the money. This can be done through personal call on skype, icq or by requesting information on the status of the digital wallet of the opponent on the site of payment system;</li>\r\n	<li>Be very careful when filling out the field \"Account Number\" of the offeree. If you have made a mistake, you are sending your own money in an unknown direction without the possibility of its return;</li>\r\n	<li>Never provide loans using \"irrevocable\" electronic payment systems. In this case, the risk to face the fact of fraud is extremely high;</li>\r\n	<li>If you are offered to make a payment in a manner different from that specified in the instructions for use of our service, you shall refuse to execute the payment and report the incident to our expert. The same applies to payments on applications that were not created by you;</li>\r\n	<li>Give up of transacting the funds, which are owned by third parties, through your own bank accounts. There are cases when carrying out such transaction for a fee led to the fact that the account holder became an accomplice of a financial crime, being unaware of malice on the part of scams;</li>\r\n	<li>Always verify information that comes to your mail with the exchange office employee.</li>\r\n</ul>\r\nOur services and similar services do not provide loans, do not take money from people under debt or interest, and do not accept donations. When receiving messages of suspicious nature on our behalf with details similar to our or other details, please refrain from the implementation of these requirements there and tell about what happened to our <a href=\"/feedback/\">Support Service</a>.\r\n\r\nTaking care of your financial well-being.', 'Caution', '', 'publish', 'open', 'closed', '', 'preduprezhdenie', '', '', '2014-10-01 23:49:10', '2014-10-01 19:49:10', '', 0, 'http://exchangeen.best-curs.info/?page_id=173', 0, 'page', '', 0),
(213, 1, '2013-05-19 19:34:15', '2013-05-19 15:34:15', '[exb_sitemap]', 'Sitemap', '', 'publish', 'closed', 'closed', '', 'sitemap', '', '', '2014-10-01 23:46:00', '2014-10-01 19:46:00', '', 0, 'http://exchangeen.best-curs.info/sitemap/', 0, 'page', '', 0),
(214, 1, '2013-05-19 19:34:15', '2013-05-19 15:34:15', '[exb_tarifs]', 'Tariffs', '', 'publish', 'closed', 'closed', '', 'tarifs', '', '', '2014-10-01 23:49:38', '2014-10-01 19:49:38', '', 0, 'http://exchangeen.best-curs.info/tarifs/', 0, 'page', '', 0),
(215, 1, '2013-05-19 19:34:56', '2013-05-19 15:34:56', ' ', '', '', 'publish', 'open', 'closed', '', '215', '', '', '2019-02-12 17:40:35', '2019-02-12 14:40:35', '', 0, 'http://exchangeen.best-curs.info/?p=215', 8, 'nav_menu_item', '', 0),
(242, 1, '2014-12-25 16:16:18', '2014-12-25 12:16:18', ' ', '', '', 'publish', 'open', 'closed', '', '242', '', '', '2014-12-25 16:16:19', '2014-12-25 12:16:19', '', 0, 'http://exchangeen.best-curs.info/?p=242', 1, 'nav_menu_item', '', 0),
(243, 1, '2014-12-25 16:16:19', '2014-12-25 12:16:19', ' ', '', '', 'publish', 'open', 'closed', '', '243', '', '', '2014-12-25 16:16:19', '2014-12-25 12:16:19', '', 0, 'http://exchangeen.best-curs.info/?p=243', 2, 'nav_menu_item', '', 0),
(244, 1, '2014-12-25 16:16:19', '2014-12-25 12:16:19', ' ', '', '', 'publish', 'open', 'closed', '', '244', '', '', '2014-12-25 16:16:19', '2014-12-25 12:16:19', '', 0, 'http://exchangeen.best-curs.info/?p=244', 3, 'nav_menu_item', '', 0),
(247, 1, '2015-06-10 16:24:53', '2015-06-10 13:24:53', 'We are always ready to answer your questions and listen to your suggestions on improvement of our service.\r\n\r\nUse this form to ask us a question, or to report an error. Please make your message more detailed, then we can solve the problem much faster.\r\n\r\n[exb_contact_form]', 'Feedback', '', 'inherit', 'open', 'closed', '', '157-revision-v1', '', '', '2015-06-10 16:24:53', '2015-06-10 13:24:53', '', 157, 'http://exchangeen.best-curs.info/157-revision-v1/', 0, 'revision', '', 0),
(248, 1, '2015-08-29 18:21:55', '2015-08-29 15:21:55', '[exchangestep]', 'Exchange steps', '', 'publish', 'closed', 'closed', '', 'exchangestep', '', '', '2015-11-13 15:41:43', '2015-11-13 12:41:43', '', 0, 'http://exchangeen.best-curs.info/exchangestep/', 0, 'page', '', 0),
(253, 1, '2015-11-13 15:41:43', '2015-11-13 12:41:43', '[exchangestep]', 'Exchange steps', '', 'inherit', 'closed', 'closed', '', '248-revision-v1', '', '', '2015-11-13 15:41:43', '2015-11-13 12:41:43', '', 248, 'http://exchangeen.best-curs.info/248-revision-v1/', 0, 'revision', '', 0),
(324, 1, '2019-02-12 17:37:26', '2019-02-12 14:37:26', '', 'secretovobmena', '', 'inherit', 'open', 'closed', '', 'secretovobmena', '', '', '2019-02-12 17:37:26', '2019-02-12 14:37:26', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/secretovobmena.gif', 0, 'attachment', 'image/gif', 0),
(323, 1, '2019-02-12 17:37:23', '2019-02-12 14:37:23', '', 'scanmoney', '', 'inherit', 'open', 'closed', '', 'scanmoney', '', '', '2019-02-12 17:37:23', '2019-02-12 14:37:23', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/scanmoney.png', 0, 'attachment', 'image/png', 0),
(322, 1, '2019-02-12 17:37:21', '2019-02-12 14:37:21', '', 'proobmen', '', 'inherit', 'open', 'closed', '', 'proobmen', '', '', '2019-02-12 17:37:21', '2019-02-12 14:37:21', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/proobmen.gif', 0, 'attachment', 'image/gif', 0),
(258, 1, '2017-05-02 19:52:38', '2017-05-02 16:52:38', '', 'Advcash', '', 'inherit', 'open', 'closed', '', 'advcash', '', '', '2017-05-02 19:52:38', '2017-05-02 16:52:38', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Advcash.png', 0, 'attachment', 'image/png', 0),
(259, 1, '2017-05-02 19:52:40', '2017-05-02 16:52:40', '', 'Alfabank', '', 'inherit', 'open', 'closed', '', 'alfabank', '', '', '2017-05-02 19:52:40', '2017-05-02 16:52:40', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Alfabank.png', 0, 'attachment', 'image/png', 0),
(260, 1, '2017-05-02 19:52:42', '2017-05-02 16:52:42', '', 'Alipay', '', 'inherit', 'open', 'closed', '', 'alipay', '', '', '2017-05-02 19:52:42', '2017-05-02 16:52:42', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Alipay.png', 0, 'attachment', 'image/png', 0),
(261, 1, '2017-05-02 19:52:44', '2017-05-02 16:52:44', '', 'Avangardbank', '', 'inherit', 'open', 'closed', '', 'avangardbank', '', '', '2017-05-02 19:52:44', '2017-05-02 16:52:44', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Avangardbank.png', 0, 'attachment', 'image/png', 0),
(262, 1, '2017-05-02 19:52:46', '2017-05-02 16:52:46', '', 'Bank-perevod', '', 'inherit', 'open', 'closed', '', 'bank-perevod', '', '', '2017-05-02 19:52:46', '2017-05-02 16:52:46', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Bank-perevod.png', 0, 'attachment', 'image/png', 0),
(263, 1, '2017-05-02 19:52:48', '2017-05-02 16:52:48', '', 'Bitcoin', '', 'inherit', 'open', 'closed', '', 'bitcoin', '', '', '2017-05-02 19:52:48', '2017-05-02 16:52:48', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Bitcoin.png', 0, 'attachment', 'image/png', 0),
(316, 1, '2019-02-12 17:37:06', '2019-02-12 14:37:06', '', 'bestexchangers', '', 'inherit', 'open', 'closed', '', 'bestexchangers', '', '', '2019-02-12 17:37:06', '2019-02-12 14:37:06', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/bestexchangers.gif', 0, 'attachment', 'image/gif', 0),
(311, 1, '2018-08-17 17:34:10', '2018-08-17 14:34:10', '', 'monero', '', 'inherit', 'open', 'closed', '', 'monero', '', '', '2018-08-17 17:34:10', '2018-08-17 14:34:10', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/monero.png', 0, 'attachment', 'image/png', 0),
(266, 1, '2017-05-02 19:52:54', '2017-05-02 16:52:54', '', 'Cash-EUR', '', 'inherit', 'open', 'closed', '', 'cash-eur', '', '', '2017-05-02 19:52:54', '2017-05-02 16:52:54', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Cash-EUR.png', 0, 'attachment', 'image/png', 0),
(267, 1, '2017-05-02 19:52:56', '2017-05-02 16:52:56', '', 'Cash-RUB', '', 'inherit', 'open', 'closed', '', 'cash-rub', '', '', '2017-05-02 19:52:56', '2017-05-02 16:52:56', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Cash-RUB.png', 0, 'attachment', 'image/png', 0),
(268, 1, '2017-05-02 19:52:58', '2017-05-02 16:52:58', '', 'Cash-USD', '', 'inherit', 'open', 'closed', '', 'cash-usd', '', '', '2017-05-02 19:52:58', '2017-05-02 16:52:58', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Cash-USD.png', 0, 'attachment', 'image/png', 0),
(269, 1, '2017-05-02 19:53:01', '2017-05-02 16:53:01', '', 'E-dinar', '', 'inherit', 'open', 'closed', '', 'e-dinar', '', '', '2017-05-02 19:53:01', '2017-05-02 16:53:01', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/E-dinar.png', 0, 'attachment', 'image/png', 0),
(270, 1, '2017-05-02 19:53:03', '2017-05-02 16:53:03', '', 'exmo', '', 'inherit', 'open', 'closed', '', 'exmo', '', '', '2017-05-02 19:53:03', '2017-05-02 16:53:03', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/exmo.png', 0, 'attachment', 'image/png', 0),
(271, 1, '2017-05-02 19:53:05', '2017-05-02 16:53:05', '', 'favicon', '', 'inherit', 'open', 'closed', '', 'favicon', '', '', '2017-05-02 19:53:05', '2017-05-02 16:53:05', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/favicon.png', 0, 'attachment', 'image/png', 0),
(312, 1, '2018-08-17 17:34:13', '2018-08-17 14:34:13', '', 'ether', '', 'inherit', 'open', 'closed', '', 'ether', '', '', '2018-08-17 17:34:13', '2018-08-17 14:34:13', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/ether.png', 0, 'attachment', 'image/png', 0),
(273, 1, '2017-05-02 19:53:09', '2017-05-02 16:53:09', '', 'Liqpay', '', 'inherit', 'open', 'closed', '', 'liqpay', '', '', '2017-05-02 19:53:09', '2017-05-02 16:53:09', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Liqpay.png', 0, 'attachment', 'image/png', 0),
(274, 1, '2017-05-02 19:53:11', '2017-05-02 16:53:11', '', 'Litecoin', '', 'inherit', 'open', 'closed', '', 'litecoin', '', '', '2017-05-02 19:53:11', '2017-05-02 16:53:11', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Litecoin.png', 0, 'attachment', 'image/png', 0),
(275, 1, '2017-05-02 19:53:13', '2017-05-02 16:53:13', '', 'Livecoin', '', 'inherit', 'open', 'closed', '', 'livecoin', '', '', '2017-05-02 19:53:13', '2017-05-02 16:53:13', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Livecoin.png', 0, 'attachment', 'image/png', 0),
(276, 1, '2017-05-02 19:53:15', '2017-05-02 16:53:15', '', 'NixMoney', '', 'inherit', 'open', 'closed', '', 'nixmoney', '', '', '2017-05-02 19:53:15', '2017-05-02 16:53:15', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/NixMoney.png', 0, 'attachment', 'image/png', 0),
(313, 1, '2018-08-17 17:34:15', '2018-08-17 14:34:15', '', 'dash', '', 'inherit', 'open', 'closed', '', 'dash', '', '', '2018-08-17 17:34:15', '2018-08-17 14:34:15', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/dash.png', 0, 'attachment', 'image/png', 0),
(309, 1, '2018-08-17 17:34:04', '2018-08-17 14:34:04', '', 'zcash', '', 'inherit', 'open', 'closed', '', 'zcash', '', '', '2018-08-17 17:34:04', '2018-08-17 14:34:04', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/zcash.png', 0, 'attachment', 'image/png', 0),
(310, 1, '2018-08-17 17:34:07', '2018-08-17 14:34:07', '', 'wawes', '', 'inherit', 'open', 'closed', '', 'wawes', '', '', '2018-08-17 17:34:07', '2018-08-17 14:34:07', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/wawes.png', 0, 'attachment', 'image/png', 0),
(280, 1, '2017-05-02 19:53:23', '2017-05-02 16:53:23', '', 'Paxum', '', 'inherit', 'open', 'closed', '', 'paxum', '', '', '2017-05-02 19:53:23', '2017-05-02 16:53:23', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Paxum.png', 0, 'attachment', 'image/png', 0),
(281, 1, '2017-05-02 19:53:26', '2017-05-02 16:53:26', '', 'Payeer', '', 'inherit', 'open', 'closed', '', 'payeer', '', '', '2017-05-02 19:53:26', '2017-05-02 16:53:26', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Payeer.png', 0, 'attachment', 'image/png', 0),
(282, 1, '2017-05-02 19:53:28', '2017-05-02 16:53:28', '', 'Paymer', '', 'inherit', 'open', 'closed', '', 'paymer', '', '', '2017-05-02 19:53:28', '2017-05-02 16:53:28', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Paymer.png', 0, 'attachment', 'image/png', 0),
(283, 1, '2017-05-02 19:53:30', '2017-05-02 16:53:30', '', 'Paypal', '', 'inherit', 'open', 'closed', '', 'paypal', '', '', '2017-05-02 19:53:30', '2017-05-02 16:53:30', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Paypal.png', 0, 'attachment', 'image/png', 0),
(284, 1, '2017-05-02 19:53:32', '2017-05-02 16:53:32', '', 'Payza', '', 'inherit', 'open', 'closed', '', 'payza', '', '', '2017-05-02 19:53:32', '2017-05-02 16:53:32', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Payza.png', 0, 'attachment', 'image/png', 0),
(285, 1, '2017-05-02 19:53:35', '2017-05-02 16:53:35', '', 'Perfect-Money', '', 'inherit', 'open', 'closed', '', 'perfect-money', '', '', '2017-05-02 19:53:35', '2017-05-02 16:53:35', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Perfect-Money.png', 0, 'attachment', 'image/png', 0),
(317, 1, '2019-02-12 17:37:08', '2019-02-12 14:37:08', '', 'exchangesumo', '', 'inherit', 'open', 'closed', '', 'exchangesumo', '', '', '2019-02-12 17:37:08', '2019-02-12 14:37:08', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/exchangesumo.png', 0, 'attachment', 'image/png', 0),
(287, 1, '2017-05-02 19:53:39', '2017-05-02 16:53:39', '', 'PMe-voucher', '', 'inherit', 'open', 'closed', '', 'pme-voucher', '', '', '2017-05-02 19:53:39', '2017-05-02 16:53:39', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/PMe-voucher.png', 0, 'attachment', 'image/png', 0),
(288, 1, '2017-05-02 19:53:41', '2017-05-02 16:53:41', '', 'Privatbank', '', 'inherit', 'open', 'closed', '', 'privatbank', '', '', '2017-05-02 19:53:41', '2017-05-02 16:53:41', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Privatbank.png', 0, 'attachment', 'image/png', 0),
(289, 1, '2017-05-02 19:53:43', '2017-05-02 16:53:43', '', 'Promsvyazbank', '', 'inherit', 'open', 'closed', '', 'promsvyazbank', '', '', '2017-05-02 19:53:43', '2017-05-02 16:53:43', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Promsvyazbank.png', 0, 'attachment', 'image/png', 0),
(290, 1, '2017-05-02 19:53:45', '2017-05-02 16:53:45', '', 'Qiwi', '', 'inherit', 'open', 'closed', '', 'qiwi', '', '', '2017-05-02 19:53:45', '2017-05-02 16:53:45', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Qiwi.png', 0, 'attachment', 'image/png', 0),
(291, 1, '2017-05-02 19:53:47', '2017-05-02 16:53:47', '', 'Russstandart', '', 'inherit', 'open', 'closed', '', 'russstandart', '', '', '2017-05-02 19:53:47', '2017-05-02 16:53:47', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Russstandart.png', 0, 'attachment', 'image/png', 0),
(292, 1, '2017-05-02 19:53:49', '2017-05-02 16:53:49', '', 'Sberbank', '', 'inherit', 'open', 'closed', '', 'sberbank', '', '', '2017-05-02 19:53:49', '2017-05-02 16:53:49', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Sberbank.png', 0, 'attachment', 'image/png', 0),
(293, 1, '2017-05-02 19:53:52', '2017-05-02 16:53:52', '', 'Skrill', '', 'inherit', 'open', 'closed', '', 'skrill', '', '', '2017-05-02 19:53:52', '2017-05-02 16:53:52', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Skrill.png', 0, 'attachment', 'image/png', 0),
(294, 1, '2017-05-02 19:53:54', '2017-05-02 16:53:54', '', 'SolidTrustPay', '', 'inherit', 'open', 'closed', '', 'solidtrustpay', '', '', '2017-05-02 19:53:54', '2017-05-02 16:53:54', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/SolidTrustPay.png', 0, 'attachment', 'image/png', 0),
(319, 1, '2019-02-12 17:37:13', '2019-02-12 14:37:13', '', 'monitorkursov', '', 'inherit', 'open', 'closed', '', 'monitorkursov', '', '', '2019-02-12 17:37:13', '2019-02-12 14:37:13', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/monitorkursov.gif', 0, 'attachment', 'image/gif', 0),
(296, 1, '2017-05-02 19:53:59', '2017-05-02 16:53:59', '', 'Tinkoff', '', 'inherit', 'open', 'closed', '', 'tinkoff', '', '', '2017-05-02 19:53:59', '2017-05-02 16:53:59', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Tinkoff.png', 0, 'attachment', 'image/png', 0),
(297, 1, '2017-05-02 19:54:01', '2017-05-02 16:54:01', '', 'Visa-MasterCard', '', 'inherit', 'open', 'closed', '', 'visa-mastercard', '', '', '2017-05-02 19:54:01', '2017-05-02 16:54:01', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Visa-MasterCard.png', 0, 'attachment', 'image/png', 0),
(298, 1, '2017-05-02 19:54:03', '2017-05-02 16:54:03', '', 'VTB24', '', 'inherit', 'open', 'closed', '', 'vtb24', '', '', '2017-05-02 19:54:03', '2017-05-02 16:54:03', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/VTB24.png', 0, 'attachment', 'image/png', 0),
(299, 1, '2017-05-02 19:54:05', '2017-05-02 16:54:05', '', 'WebMoney', '', 'inherit', 'open', 'closed', '', 'webmoney', '', '', '2017-05-02 19:54:05', '2017-05-02 16:54:05', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/WebMoney.png', 0, 'attachment', 'image/png', 0),
(308, 1, '2018-08-17 17:34:02', '2018-08-17 14:34:02', '', 'ripple', '', 'inherit', 'open', 'closed', '', 'ripple', '', '', '2018-08-17 17:34:02', '2018-08-17 14:34:02', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/ripple.png', 0, 'attachment', 'image/png', 0),
(320, 1, '2019-02-12 17:37:16', '2019-02-12 14:37:16', '', 'obmenvse', '', 'inherit', 'open', 'closed', '', 'obmenvse', '', '', '2019-02-12 17:37:16', '2019-02-12 14:37:16', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/obmenvse.gif', 0, 'attachment', 'image/gif', 0),
(318, 1, '2019-02-12 17:37:11', '2019-02-12 14:37:11', '', 'goodkurs', '', 'inherit', 'open', 'closed', '', 'goodkurs', '', '', '2019-02-12 17:37:11', '2019-02-12 14:37:11', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/goodkurs.gif', 0, 'attachment', 'image/gif', 0),
(303, 1, '2017-05-02 19:54:14', '2017-05-02 16:54:14', '', 'Yandex', '', 'inherit', 'open', 'closed', '', 'yandex', '', '', '2017-05-02 19:54:14', '2017-05-02 16:54:14', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Yandex.png', 0, 'attachment', 'image/png', 0),
(304, 1, '2017-05-02 19:54:16', '2017-05-02 16:54:16', '', 'Z-payment', '', 'inherit', 'open', 'closed', '', 'z-payment', '', '', '2017-05-02 19:54:16', '2017-05-02 16:54:16', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Z-payment.png', 0, 'attachment', 'image/png', 0),
(314, 1, '2018-08-17 17:34:17', '2018-08-17 14:34:17', '', 'bitcoincash', '', 'inherit', 'open', 'closed', '', 'bitcoincash', '', '', '2018-08-17 17:34:17', '2018-08-17 14:34:17', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/bitcoincash.png', 0, 'attachment', 'image/png', 0),
(315, 1, '2018-08-17 17:34:19', '2018-08-17 14:34:19', '', 'wex', '', 'inherit', 'open', 'closed', '', 'wex', '', '', '2018-08-17 17:34:19', '2018-08-17 14:34:19', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/wex.png', 0, 'attachment', 'image/png', 0),
(321, 1, '2019-02-12 17:37:18', '2019-02-12 14:37:18', '', 'okku', '', 'inherit', 'open', 'closed', '', 'okku', '', '', '2019-02-12 17:37:18', '2019-02-12 14:37:18', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/okku.gif', 0, 'attachment', 'image/gif', 0),
(325, 1, '2019-02-12 17:37:29', '2019-02-12 14:37:29', '', 'udifo', '', 'inherit', 'open', 'closed', '', 'udifo', '', '', '2019-02-12 17:37:29', '2019-02-12 14:37:29', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/udifo.png', 0, 'attachment', 'image/png', 0),
(326, 1, '2019-02-12 17:37:32', '2019-02-12 14:37:32', '', 'kurses', '', 'inherit', 'open', 'closed', '', 'kurses', '', '', '2019-02-12 17:37:32', '2019-02-12 14:37:32', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/kurses.gif', 0, 'attachment', 'image/gif', 0),
(327, 1, '2019-02-12 17:37:35', '2019-02-12 14:37:35', '', 'webmoney-1', '', 'inherit', 'open', 'closed', '', 'webmoney-1', '', '', '2019-02-12 17:37:35', '2019-02-12 14:37:35', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/webmoney-1.png', 0, 'attachment', 'image/png', 0),
(328, 1, '2019-02-12 17:37:38', '2019-02-12 14:37:38', '', 'okchanger', '', 'inherit', 'open', 'closed', '', 'okchanger', '', '', '2019-02-12 17:37:38', '2019-02-12 14:37:38', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/okchanger.png', 0, 'attachment', 'image/png', 0),
(329, 1, '2019-02-12 17:37:41', '2019-02-12 14:37:41', '', 'kursexpert', '', 'inherit', 'open', 'closed', '', 'kursexpert', '', '', '2019-02-12 17:37:41', '2019-02-12 14:37:41', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/kursexpert.png', 0, 'attachment', 'image/png', 0),
(330, 1, '2019-02-12 17:37:44', '2019-02-12 14:37:44', '', 'glazok', '', 'inherit', 'open', 'closed', '', 'glazok', '', '', '2019-02-12 17:37:44', '2019-02-12 14:37:44', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/glazok.gif', 0, 'attachment', 'image/gif', 0),
(331, 1, '2019-02-12 17:37:47', '2019-02-12 14:37:47', '', 'bestchange', '', 'inherit', 'open', 'closed', '', 'bestchange', '', '', '2019-02-12 17:37:47', '2019-02-12 14:37:47', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/bestchange.gif', 0, 'attachment', 'image/gif', 0),
(332, 1, '2019-02-12 17:37:50', '2019-02-12 14:37:50', '', 'Stellar', '', 'inherit', 'open', 'closed', '', 'stellar', '', '', '2019-02-12 17:37:50', '2019-02-12 14:37:50', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Stellar.png', 0, 'attachment', 'image/png', 0),
(333, 1, '2019-02-12 17:37:53', '2019-02-12 14:37:53', '', 'Ethereum-Classic', '', 'inherit', 'open', 'closed', '', 'ethereum-classic', '', '', '2019-02-12 17:37:53', '2019-02-12 14:37:53', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Ethereum-Classic.png', 0, 'attachment', 'image/png', 0),
(334, 1, '2019-02-12 17:37:56', '2019-02-12 14:37:56', '', 'EOS', '', 'inherit', 'open', 'closed', '', 'eos', '', '', '2019-02-12 17:37:56', '2019-02-12 14:37:56', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/EOS.png', 0, 'attachment', 'image/png', 0),
(335, 1, '2019-02-12 17:37:59', '2019-02-12 14:37:59', '', 'DigiByte', '', 'inherit', 'open', 'closed', '', 'digibyte', '', '', '2019-02-12 17:37:59', '2019-02-12 14:37:59', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/DigiByte.png', 0, 'attachment', 'image/png', 0),
(336, 1, '2019-02-12 17:38:02', '2019-02-12 14:38:02', '', 'Cardano', '', 'inherit', 'open', 'closed', '', 'cardano', '', '', '2019-02-12 17:38:02', '2019-02-12 14:38:02', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/Cardano.png', 0, 'attachment', 'image/png', 0),
(337, 1, '2019-02-12 17:40:34', '2019-02-12 14:40:34', ' ', '', '', 'publish', 'closed', 'closed', '', '337', '', '', '2019-02-12 17:40:34', '2019-02-12 14:40:34', '', 0, 'http://exchangeen.best-curs.info/?p=337', 3, 'nav_menu_item', '', 0),
(338, 1, '2019-02-12 18:04:34', '2019-02-12 15:04:34', '', 'Пользовательское соглашение по обработке персональных данных', '', 'publish', 'closed', 'closed', '', 'terms-personal-data', '', '', '2019-02-12 18:04:34', '2019-02-12 15:04:34', '', 0, 'http://exchangeen.best-curs.info/terms-personal-data/', 0, 'page', '', 0),
(339, 1, '2019-04-02 16:39:51', '2019-04-02 13:39:51', '', 'bitex', '', 'inherit', 'open', 'closed', '', 'bitex', '', '', '2019-04-02 16:39:51', '2019-04-02 13:39:51', '', 0, 'http://exchangeen.best-curs.info/wp-content/uploads/bitex.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_reviews`
--

CREATE TABLE `eb_reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `create_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL,
  `auto_status` int(1) NOT NULL DEFAULT 1,
  `edit_user_id` bigint(20) NOT NULL DEFAULT 0,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `user_name` tinytext NOT NULL,
  `user_email` tinytext NOT NULL,
  `user_site` tinytext NOT NULL,
  `review_date` datetime NOT NULL,
  `review_hash` tinytext NOT NULL,
  `review_text` longtext NOT NULL,
  `review_status` varchar(150) NOT NULL DEFAULT 'moderation'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_reviews_meta`
--

CREATE TABLE `eb_reviews_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) NOT NULL DEFAULT 0,
  `meta_key` longtext NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_scrtransaction`
--

CREATE TABLE `eb_scrtransaction` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bdate` datetime NOT NULL,
  `bidid` bigint(20) NOT NULL DEFAULT 0,
  `valsid` bigint(20) NOT NULL DEFAULT 0,
  `bsumm` varchar(250) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_termmeta`
--

CREATE TABLE `eb_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_terms`
--

CREATE TABLE `eb_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_terms`
--

INSERT INTO `eb_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Новости', 'novosti', 0),
(2, 'Ссылки', '%d1%81%d1%81%d1%8b%d0%bb%d0%ba%d0%b8', 0),
(3, 'Верхнее меню', 'verhnee-menyu', 0),
(4, 'Нижнее меню', 'nizhnee-menyu', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_term_relationships`
--

CREATE TABLE `eb_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_term_relationships`
--

INSERT INTO `eb_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(169, 3, 0),
(130, 3, 0),
(170, 3, 0),
(171, 3, 0),
(188, 3, 0),
(189, 3, 0),
(194, 3, 0),
(215, 3, 0),
(242, 4, 0),
(243, 4, 0),
(244, 4, 0),
(337, 3, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_term_taxonomy`
--

CREATE TABLE `eb_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_term_taxonomy`
--

INSERT INTO `eb_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'link_category', '', 0, 0),
(3, 3, 'nav_menu', '', 0, 9),
(4, 4, 'nav_menu', '', 0, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_transactionrezerv`
--

CREATE TABLE `eb_transactionrezerv` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vdate` datetime NOT NULL,
  `valsid` bigint(20) NOT NULL DEFAULT 0,
  `vsumm` varchar(250) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_transactionuser`
--

CREATE TABLE `eb_transactionuser` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bdate` datetime NOT NULL,
  `bidid` bigint(20) NOT NULL DEFAULT 0,
  `userid` bigint(20) NOT NULL DEFAULT 0,
  `bsumm` varchar(250) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_usermeta`
--

CREATE TABLE `eb_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_usermeta`
--

INSERT INTO `eb_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'first_name', 'Администратор'),
(2, 1, 'last_name', ''),
(3, 1, 'nickname', 'superadmin'),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'eb_capabilities', 'a:1:{s:13:\"administrator\";s:1:\"1\";}'),
(11, 1, 'eb_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp330_toolbar,wp330_media_uploader,wp330_saving_widgets,wp340_customize_current_theme_link,wp350_media,wp390_widgets,wp410_dfw,wp496_privacy'),
(13, 1, 'show_welcome_panel', '0'),
(14, 1, 'eb_dashboard_quick_press_last_post_id', '254'),
(15, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(16, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:8:\"add-post\";i:1;s:12:\"add-post_tag\";}'),
(17, 1, 'nav_menu_recently_edited', '3'),
(18, 1, 'aim', ''),
(19, 1, 'yim', ''),
(20, 1, 'jabber', ''),
(36, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(37, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(38, 1, 'eb_user-settings', 'editor=html&ed_size=760&hidetb=1&libraryContent=browse&mfold=o'),
(39, 1, 'eb_user-settings-time', '1447418434'),
(67, 1, 'second_name', ''),
(58, 1, 'wmz', ''),
(57, 1, 'user_phone', ''),
(52, 1, 'googleplus', ''),
(61, 1, 'user_skype', ''),
(64, 1, 'session_tokens', 'a:1:{s:64:\"d04c9066adc4fcb545644ce2235a311f8786d02a74843c4d7a9df8c9ac570815\";a:4:{s:10:\"expiration\";i:1575753082;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36\";s:5:\"login\";i:1575493882;}}'),
(69, 1, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_users`
--

CREATE TABLE `eb_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(255) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) NOT NULL DEFAULT '',
  `user_hash` varchar(15) NOT NULL,
  `auto_login1` varchar(250) NOT NULL,
  `auto_login2` varchar(250) NOT NULL,
  `user_discount` varchar(50) NOT NULL DEFAULT '0',
  `ref_id` bigint(20) NOT NULL DEFAULT 0,
  `partner_pers` varchar(50) NOT NULL DEFAULT '0',
  `sec_lostpass` int(1) NOT NULL DEFAULT 1,
  `sec_login` int(1) NOT NULL DEFAULT 0,
  `email_login` int(1) NOT NULL DEFAULT 0,
  `user_verify` int(1) NOT NULL DEFAULT 0,
  `enable_ips` longtext NOT NULL,
  `user_browser` varchar(250) NOT NULL,
  `user_ip` varchar(250) NOT NULL,
  `user_bann` int(1) NOT NULL DEFAULT 0,
  `admin_comment` longtext NOT NULL,
  `last_adminpanel` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_users`
--

INSERT INTO `eb_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`, `user_hash`, `auto_login1`, `auto_login2`, `user_discount`, `ref_id`, `partner_pers`, `sec_lostpass`, `sec_login`, `email_login`, `user_verify`, `enable_ips`, `user_browser`, `user_ip`, `user_bann`, `admin_comment`, `last_adminpanel`) VALUES
(1, 'superadmin', '$P$Bkz1LSXGIqormtKmg821.npthnT9a5/', 'superadmin', 'info@best-curs.info', '', '2012-05-16 17:07:12', '', 0, 'Администратор', 'rwmLF8ajJgk7zoK', '', '', '0', 0, '0', 1, 0, 0, 0, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', '127.0.0.1', 0, '', '1575504787');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_userverify`
--

CREATE TABLE `eb_userverify` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `createdate` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `user_login` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `fio` varchar(250) NOT NULL,
  `tel` varchar(250) NOT NULL,
  `skype` varchar(250) NOT NULL,
  `file1` longtext NOT NULL,
  `file2` longtext NOT NULL,
  `file3` longtext NOT NULL,
  `file4` longtext NOT NULL,
  `textstatus` longtext NOT NULL,
  `theip` varchar(150) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_user_discounts`
--

CREATE TABLE `eb_user_discounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sumec` varchar(50) NOT NULL DEFAULT '0',
  `discount` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_user_discounts`
--

INSERT INTO `eb_user_discounts` (`id`, `sumec`, `discount`) VALUES
(1, '0', '0'),
(2, '100', '0.1'),
(3, '500', '0.2'),
(4, '1000', '0.3'),
(5, '5000', '0.4');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_user_fav`
--

CREATE TABLE `eb_user_fav` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `link` varchar(250) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '0',
  `menu_order` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_usve_change`
--

CREATE TABLE `eb_usve_change` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `meta_key2` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_valuts`
--

CREATE TABLE `eb_valuts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vname` longtext NOT NULL,
  `vlogo` longtext NOT NULL,
  `xname` longtext NOT NULL,
  `vtype` longtext NOT NULL,
  `numleng` int(5) NOT NULL DEFAULT 0,
  `xzt` varchar(150) NOT NULL DEFAULT '',
  `helps` longtext NOT NULL,
  `txt1` longtext NOT NULL,
  `txt2` longtext NOT NULL,
  `rxzt` int(5) NOT NULL DEFAULT 0,
  `vactive` int(1) NOT NULL DEFAULT 1,
  `pvivod` int(1) NOT NULL DEFAULT 1,
  `show1` int(1) NOT NULL DEFAULT 1,
  `show2` int(1) NOT NULL DEFAULT 1,
  `maxnumleng` int(5) NOT NULL DEFAULT 100,
  `xml_value` varchar(250) NOT NULL,
  `firstzn` varchar(20) NOT NULL,
  `valut_reserv` varchar(50) NOT NULL DEFAULT '0',
  `site_order` bigint(20) NOT NULL DEFAULT 0,
  `reserv_order` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_valuts`
--

INSERT INTO `eb_valuts` (`id`, `vname`, `vlogo`, `xname`, `vtype`, `numleng`, `xzt`, `helps`, `txt1`, `txt2`, `rxzt`, `vactive`, `pvivod`, `show1`, `show2`, `maxnumleng`, `xml_value`, `firstzn`, `valut_reserv`, `site_order`, `reserv_order`) VALUES
(3, 'Perfect Money', '/wp-content/uploads/Perfect-Money.png', 'PMUSD', 'USD', 8, '0', 'Example: U1234567.', '', '', 0, 1, 1, 1, 1, 100, 'PMUSD', '', '0', 0, 0),
(4, 'Perfect Money', '/wp-content/uploads/Perfect-Money.png', 'PMEUR', 'EUR', 8, '0', 'Example: E1234567.', '', '', 0, 1, 1, 1, 1, 100, 'PMEUR', '', '0', 0, 0),
(17, 'WebMoney', '/wp-content/uploads/WebMoney.png', 'WMZ', 'USD', 13, '0', 'Пример: Z123456789000.', '', '', 0, 1, 1, 1, 1, 100, 'WMZ', '', '0', 0, 0),
(19, 'Bitcoin', '/wp-content/uploads/Bitcoin.png', 'BTC', 'BTC', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'BTC', '', '0', 0, 0),
(20, 'Ethereum', '/wp-content/uploads/ether.png', 'ETH', 'ETH', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'ETH', '', '0', 0, 0),
(21, 'Dash', '/wp-content/uploads/dash.png', 'DASH', 'DASH', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'DASH', '', '0', 0, 0),
(22, 'Bitcoin Cash', 'http://exchangeen.best-curs.info/wp-content/uploads/bitcoincash.png', 'BCH', 'BCH', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'BCH', '', '0', 0, 0),
(23, 'Ripple', 'http://exchangeen.best-curs.info/wp-content/uploads/ripple.png', 'XRP', 'XRP', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'XRP', '', '0', 0, 0),
(24, 'Monero', 'http://exchangeen.best-curs.info/wp-content/uploads/monero.png', 'XMR', 'XMR', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'XMR', '', '0', 0, 0),
(25, 'Zcash', 'http://exchangeen.best-curs.info/wp-content/uploads/zcash.png', 'ZEC', 'ZEC', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'ZEC', '', '0', 0, 0),
(26, 'Waves', 'http://exchangeen.best-curs.info/wp-content/uploads/wawes.png', 'WAVES', 'WAVES', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'WAVES', '', '0', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_valuts_meta`
--

CREATE TABLE `eb_valuts_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) NOT NULL DEFAULT 0,
  `meta_key` longtext NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_vschets`
--

CREATE TABLE `eb_vschets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `valid` bigint(20) NOT NULL DEFAULT 0,
  `title` varchar(250) NOT NULL DEFAULT '0',
  `visib` int(1) NOT NULL DEFAULT 0,
  `prosm` int(5) NOT NULL DEFAULT 0,
  `text_comment` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_vtypes`
--

CREATE TABLE `eb_vtypes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `xname` tinytext NOT NULL,
  `vncurs` varchar(50) NOT NULL DEFAULT '0',
  `parser` bigint(20) NOT NULL DEFAULT 0,
  `nums` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_vtypes`
--

INSERT INTO `eb_vtypes` (`id`, `xname`, `vncurs`, `parser`, `nums`) VALUES
(1, 'RUB', '65.4072', 1, '0'),
(2, 'EUR', '0.877886050391', 52, '0'),
(3, 'USD', '1', 0, '0'),
(4, 'UAH', '26.75521', 101, '0'),
(6, 'KZT', '379.84', 151, '0'),
(10, 'BTC', '0.000086206897', 0, '0'),
(12, 'BYN', '2.1428', 201, '0'),
(13, 'ETH', '0.0033564931', 0, '0'),
(14, 'DASH', '0.0063083523', 0, '0'),
(15, 'ZEC', '0.0069778801', 0, '0'),
(16, 'XMR', '0.0104046363', 0, '0'),
(17, 'XRP', '3.1529827217', 0, '0'),
(18, 'BCH', '0.0018390466', 0, '0'),
(19, 'LTC', '0.0170604794', 0, '0'),
(20, 'WAVES', '0.5376344086', 0, '0'),
(21, 'AMD', '1', 0, '0'),
(22, 'GLD', '1', 0, '0'),
(23, 'UZS', '1', 0, '0'),
(24, 'TRY', '1', 0, '0');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_zapros_rezerv`
--

CREATE TABLE `eb_zapros_rezerv` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(250) NOT NULL,
  `valid` bigint(20) NOT NULL DEFAULT 0,
  `summk` varchar(250) NOT NULL DEFAULT '0',
  `comment` longtext NOT NULL,
  `zdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `eb_abitcoin`
--
ALTER TABLE `eb_abitcoin`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_adminpanelcaptcha`
--
ALTER TABLE `eb_adminpanelcaptcha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createdate` (`createdate`),
  ADD KEY `sess_hash` (`sess_hash`);

--
-- Индексы таблицы `eb_archive_data`
--
ALTER TABLE `eb_archive_data`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_auth_logs`
--
ALTER TABLE `eb_auth_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `auth_date` (`auth_date`),
  ADD KEY `auth_status` (`auth_status`);

--
-- Индексы таблицы `eb_bautocurs`
--
ALTER TABLE `eb_bautocurs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_blackip`
--
ALTER TABLE `eb_bcoip_blackip`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_country`
--
ALTER TABLE `eb_bcoip_country`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_ipcity`
--
ALTER TABLE `eb_bcoip_ipcity`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_iplist`
--
ALTER TABLE `eb_bcoip_iplist`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_themplate`
--
ALTER TABLE `eb_bcoip_themplate`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_whiteip`
--
ALTER TABLE `eb_bcoip_whiteip`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bids`
--
ALTER TABLE `eb_bids`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bids_meta`
--
ALTER TABLE `eb_bids_meta`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_blacklist`
--
ALTER TABLE `eb_blacklist`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_captcha`
--
ALTER TABLE `eb_captcha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createdate` (`createdate`),
  ADD KEY `sess_hash` (`sess_hash`);

--
-- Индексы таблицы `eb_change`
--
ALTER TABLE `eb_change`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_commentmeta`
--
ALTER TABLE `eb_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `eb_comments`
--
ALTER TABLE `eb_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Индексы таблицы `eb_custom_fields_valut`
--
ALTER TABLE `eb_custom_fields_valut`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_inex_change`
--
ALTER TABLE `eb_inex_change`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_inex_deposit`
--
ALTER TABLE `eb_inex_deposit`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_inex_system`
--
ALTER TABLE `eb_inex_system`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_inex_tars`
--
ALTER TABLE `eb_inex_tars`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_links`
--
ALTER TABLE `eb_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Индексы таблицы `eb_masschange`
--
ALTER TABLE `eb_masschange`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_merchant_logs`
--
ALTER TABLE `eb_merchant_logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_napobmens`
--
ALTER TABLE `eb_napobmens`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_naps_meta`
--
ALTER TABLE `eb_naps_meta`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_notice_head`
--
ALTER TABLE `eb_notice_head`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_options`
--
ALTER TABLE `eb_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Индексы таблицы `eb_partners`
--
ALTER TABLE `eb_partners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_partner_pers`
--
ALTER TABLE `eb_partner_pers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_payoutuser`
--
ALTER TABLE `eb_payoutuser`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_plinks`
--
ALTER TABLE `eb_plinks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_pn_options`
--
ALTER TABLE `eb_pn_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_key` (`meta_key`),
  ADD KEY `meta_key2` (`meta_key2`);

--
-- Индексы таблицы `eb_postmeta`
--
ALTER TABLE `eb_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `eb_posts`
--
ALTER TABLE `eb_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`),
  ADD KEY `post_name` (`post_name`(191));

--
-- Индексы таблицы `eb_reviews`
--
ALTER TABLE `eb_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_reviews_meta`
--
ALTER TABLE `eb_reviews_meta`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_scrtransaction`
--
ALTER TABLE `eb_scrtransaction`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_termmeta`
--
ALTER TABLE `eb_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `eb_terms`
--
ALTER TABLE `eb_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Индексы таблицы `eb_term_relationships`
--
ALTER TABLE `eb_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Индексы таблицы `eb_term_taxonomy`
--
ALTER TABLE `eb_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Индексы таблицы `eb_transactionrezerv`
--
ALTER TABLE `eb_transactionrezerv`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_transactionuser`
--
ALTER TABLE `eb_transactionuser`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_usermeta`
--
ALTER TABLE `eb_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `eb_users`
--
ALTER TABLE `eb_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Индексы таблицы `eb_userverify`
--
ALTER TABLE `eb_userverify`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_user_discounts`
--
ALTER TABLE `eb_user_discounts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_user_fav`
--
ALTER TABLE `eb_user_fav`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_usve_change`
--
ALTER TABLE `eb_usve_change`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_valuts`
--
ALTER TABLE `eb_valuts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_valuts_meta`
--
ALTER TABLE `eb_valuts_meta`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_vschets`
--
ALTER TABLE `eb_vschets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_vtypes`
--
ALTER TABLE `eb_vtypes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_zapros_rezerv`
--
ALTER TABLE `eb_zapros_rezerv`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `eb_abitcoin`
--
ALTER TABLE `eb_abitcoin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_adminpanelcaptcha`
--
ALTER TABLE `eb_adminpanelcaptcha`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `eb_archive_data`
--
ALTER TABLE `eb_archive_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_auth_logs`
--
ALTER TABLE `eb_auth_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `eb_bautocurs`
--
ALTER TABLE `eb_bautocurs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_blackip`
--
ALTER TABLE `eb_bcoip_blackip`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_country`
--
ALTER TABLE `eb_bcoip_country`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_ipcity`
--
ALTER TABLE `eb_bcoip_ipcity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_iplist`
--
ALTER TABLE `eb_bcoip_iplist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_themplate`
--
ALTER TABLE `eb_bcoip_themplate`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_whiteip`
--
ALTER TABLE `eb_bcoip_whiteip`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bids`
--
ALTER TABLE `eb_bids`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bids_meta`
--
ALTER TABLE `eb_bids_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_blacklist`
--
ALTER TABLE `eb_blacklist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_captcha`
--
ALTER TABLE `eb_captcha`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `eb_change`
--
ALTER TABLE `eb_change`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT для таблицы `eb_commentmeta`
--
ALTER TABLE `eb_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `eb_comments`
--
ALTER TABLE `eb_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `eb_custom_fields_valut`
--
ALTER TABLE `eb_custom_fields_valut`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_inex_change`
--
ALTER TABLE `eb_inex_change`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_inex_deposit`
--
ALTER TABLE `eb_inex_deposit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_inex_system`
--
ALTER TABLE `eb_inex_system`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_inex_tars`
--
ALTER TABLE `eb_inex_tars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_links`
--
ALTER TABLE `eb_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `eb_masschange`
--
ALTER TABLE `eb_masschange`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_merchant_logs`
--
ALTER TABLE `eb_merchant_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_napobmens`
--
ALTER TABLE `eb_napobmens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `eb_naps_meta`
--
ALTER TABLE `eb_naps_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `eb_notice_head`
--
ALTER TABLE `eb_notice_head`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_options`
--
ALTER TABLE `eb_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4688;

--
-- AUTO_INCREMENT для таблицы `eb_partners`
--
ALTER TABLE `eb_partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `eb_partner_pers`
--
ALTER TABLE `eb_partner_pers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `eb_payoutuser`
--
ALTER TABLE `eb_payoutuser`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_plinks`
--
ALTER TABLE `eb_plinks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_pn_options`
--
ALTER TABLE `eb_pn_options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT для таблицы `eb_postmeta`
--
ALTER TABLE `eb_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=635;

--
-- AUTO_INCREMENT для таблицы `eb_posts`
--
ALTER TABLE `eb_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=340;

--
-- AUTO_INCREMENT для таблицы `eb_reviews`
--
ALTER TABLE `eb_reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_reviews_meta`
--
ALTER TABLE `eb_reviews_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_scrtransaction`
--
ALTER TABLE `eb_scrtransaction`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_termmeta`
--
ALTER TABLE `eb_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_terms`
--
ALTER TABLE `eb_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `eb_term_taxonomy`
--
ALTER TABLE `eb_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `eb_transactionrezerv`
--
ALTER TABLE `eb_transactionrezerv`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_transactionuser`
--
ALTER TABLE `eb_transactionuser`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_usermeta`
--
ALTER TABLE `eb_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT для таблицы `eb_users`
--
ALTER TABLE `eb_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `eb_userverify`
--
ALTER TABLE `eb_userverify`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_user_discounts`
--
ALTER TABLE `eb_user_discounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `eb_user_fav`
--
ALTER TABLE `eb_user_fav`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_usve_change`
--
ALTER TABLE `eb_usve_change`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_valuts`
--
ALTER TABLE `eb_valuts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT для таблицы `eb_valuts_meta`
--
ALTER TABLE `eb_valuts_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_vschets`
--
ALTER TABLE `eb_vschets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_vtypes`
--
ALTER TABLE `eb_vtypes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT для таблицы `eb_zapros_rezerv`
--
ALTER TABLE `eb_zapros_rezerv`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
