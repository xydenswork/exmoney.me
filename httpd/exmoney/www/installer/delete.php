<?php
header("Content-Type: text/html; charset=UTF-8");
define( 'WP_INSTALLING', false );
define('INSTALLER_PATH',dirname(__FILE__) . '/');
define('DIR_PATH', str_replace('installer/','',INSTALLER_PATH));

if(!function_exists('full_del_dir')){
	function full_del_dir($directory){
		if(is_dir($directory)){
			$dir = opendir($directory);
			while(($file = readdir($dir))){
				if ( is_file($directory."/".$file)){
					unlink($directory."/".$file);
				} else if ( is_dir ($directory."/".$file) && ($file != ".") && ($file != "..")){
					full_del_dir($directory."/".$file);  
				}
			}
			closedir ($dir);
			rmdir ($directory);
		}
	}
}

$files_or_directory = array( 
	'installer/', 
	'damp_db.sql',
	'EN_damp_db.sql',
	'RU_damp_db.sql',
	'index.html',
    'license.zip',
	'license(1).zip',
	'license(2).zip',
	'license(3).zip',
	'license(4).zip',
	'wp-info.php',
	'exchangebox7.0_php5.6_ioncube10.zip',
	'exchangebox7.0_php7.1_ioncube10.zip',
	'exchangebox7.0_php7.2_ioncube10.zip',
	
	
);

foreach($files_or_directory as $file){
	$file_path = DIR_PATH . $file;
	if(is_dir($file_path)){
		full_del_dir($file_path);
	} elseif(is_file($file_path)) {
		unlink($file_path);
	}
}

header('Location: /');