-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 05 2019 г., 00:14
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `exchangebox`
--

-- --------------------------------------------------------

--
-- Структура таблицы `eb_abitcoin`
--

CREATE TABLE `eb_abitcoin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `obmen_id` bigint(20) NOT NULL DEFAULT 0,
  `adress` tinytext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_adminpanelcaptcha`
--

CREATE TABLE `eb_adminpanelcaptcha` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `createdate` datetime NOT NULL,
  `sess_hash` varchar(150) NOT NULL,
  `num1` varchar(10) NOT NULL DEFAULT '0',
  `num2` varchar(10) NOT NULL DEFAULT '0',
  `symbol` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_adminpanelcaptcha`
--

INSERT INTO `eb_adminpanelcaptcha` (`id`, `createdate`, `sess_hash`, `num1`, `num2`, `symbol`) VALUES
(4, '2019-12-05 00:08:51', 'cb6d993e992b230b992360d47ef9cb5c', '1', '3', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_archive_data`
--

CREATE TABLE `eb_archive_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `meta_key2` varchar(250) NOT NULL,
  `item_id` bigint(20) NOT NULL DEFAULT 0,
  `meta_value` varchar(20) NOT NULL DEFAULT '0',
  `meta_key3` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_auth_logs`
--

CREATE TABLE `eb_auth_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `auth_date` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(250) NOT NULL,
  `old_user_ip` varchar(250) NOT NULL,
  `old_user_browser` varchar(250) NOT NULL,
  `now_user_ip` varchar(250) NOT NULL,
  `now_user_browser` varchar(250) NOT NULL,
  `auth_status` int(1) NOT NULL DEFAULT 0,
  `auth_status_text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_auth_logs`
--

INSERT INTO `eb_auth_logs` (`id`, `auth_date`, `user_id`, `user_login`, `old_user_ip`, `old_user_browser`, `now_user_ip`, `now_user_browser`, `auth_status`, `auth_status_text`) VALUES
(4, '2019-12-05 00:09:03', 1, 'superadmin', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1, '');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bautocurs`
--

CREATE TABLE `eb_bautocurs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nid` bigint(20) NOT NULL DEFAULT 0,
  `parseid` int(3) NOT NULL DEFAULT 0,
  `value1` varchar(10) NOT NULL DEFAULT '0',
  `chto1` int(1) NOT NULL DEFAULT 0,
  `value2` varchar(10) NOT NULL DEFAULT '0',
  `chto2` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_blackip`
--

CREATE TABLE `eb_bcoip_blackip` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `theip` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_country`
--

CREATE TABLE `eb_bcoip_country` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attr` varchar(20) NOT NULL,
  `title` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `wablon_id` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_ipcity`
--

CREATE TABLE `eb_bcoip_ipcity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title1` longtext NOT NULL,
  `title2` longtext NOT NULL,
  `title3` longtext NOT NULL,
  `first_coor` varchar(250) NOT NULL,
  `last_coor` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_iplist`
--

CREATE TABLE `eb_bcoip_iplist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `before_cip` bigint(20) NOT NULL DEFAULT 0,
  `after_cip` bigint(20) NOT NULL DEFAULT 0,
  `before_ip` varchar(250) NOT NULL,
  `after_ip` varchar(250) NOT NULL,
  `country_attr` varchar(20) NOT NULL,
  `place_id` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_themplate`
--

CREATE TABLE `eb_bcoip_themplate` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `temptitle` longtext NOT NULL,
  `title` longtext NOT NULL,
  `content` longtext NOT NULL,
  `info_status` int(1) NOT NULL DEFAULT 0,
  `default_temp` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bcoip_whiteip`
--

CREATE TABLE `eb_bcoip_whiteip` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `theip` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bids`
--

CREATE TABLE `eb_bids` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `ref_id` bigint(20) NOT NULL DEFAULT 0,
  `user_skidka` varchar(35) NOT NULL DEFAULT '0',
  `user_sksumm` varchar(35) NOT NULL DEFAULT '0',
  `fio` tinytext NOT NULL,
  `email` tinytext NOT NULL,
  `tel` tinytext NOT NULL,
  `schet1` tinytext NOT NULL,
  `schet2` tinytext NOT NULL,
  `hashed` tinytext NOT NULL,
  `valut1` tinytext NOT NULL,
  `valut2` tinytext NOT NULL,
  `valut1i` bigint(20) NOT NULL DEFAULT 0,
  `valut2i` bigint(20) NOT NULL DEFAULT 0,
  `valut1type` tinytext NOT NULL,
  `valut2type` tinytext NOT NULL,
  `curs1` varchar(35) NOT NULL DEFAULT '0',
  `curs2` varchar(35) NOT NULL DEFAULT '0',
  `summ1` varchar(35) NOT NULL DEFAULT '0',
  `summ2` varchar(35) NOT NULL DEFAULT '0',
  `summz1` varchar(35) NOT NULL DEFAULT '0',
  `summz2` varchar(35) NOT NULL DEFAULT '0',
  `tpersent1` varchar(35) NOT NULL DEFAULT '0',
  `tpersent2` varchar(35) NOT NULL DEFAULT '0',
  `tcommis1` varchar(35) NOT NULL DEFAULT '0',
  `tcommis2` varchar(35) NOT NULL DEFAULT '0',
  `tdate` datetime NOT NULL,
  `status` varchar(35) NOT NULL,
  `naprid` bigint(20) NOT NULL,
  `dop1` longtext NOT NULL,
  `dop2` longtext NOT NULL,
  `userip` longtext NOT NULL,
  `skype` tinytext NOT NULL,
  `dmetas` longtext NOT NULL,
  `acomments` longtext NOT NULL,
  `parthide` int(1) NOT NULL DEFAULT 0,
  `partmax` varchar(25) NOT NULL DEFAULT '0',
  `editdate` datetime NOT NULL,
  `naschet` varchar(150) NOT NULL,
  `fname` tinytext NOT NULL,
  `iname` tinytext NOT NULL,
  `oname` tinytext NOT NULL,
  `pnomer` varchar(250) NOT NULL,
  `schet1_hash` longtext NOT NULL,
  `schet2_hash` longtext NOT NULL,
  `ucomments` longtext NOT NULL,
  `exsum` varchar(30) NOT NULL DEFAULT '0',
  `partpr` varchar(25) NOT NULL DEFAULT '0',
  `ref_sum` varchar(25) NOT NULL DEFAULT '0',
  `p_sum` varchar(25) NOT NULL DEFAULT '0',
  `pcalc` int(1) NOT NULL DEFAULT 0,
  `user_hash` varchar(150) NOT NULL,
  `naschet_h` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_bids_meta`
--

CREATE TABLE `eb_bids_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) NOT NULL DEFAULT 0,
  `meta_key` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_blacklist`
--

CREATE TABLE `eb_blacklist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(12) NOT NULL DEFAULT '0',
  `meta_value` tinytext NOT NULL,
  `comment_text` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_captcha`
--

CREATE TABLE `eb_captcha` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `createdate` datetime NOT NULL,
  `sess_hash` varchar(150) NOT NULL,
  `num1` varchar(10) NOT NULL DEFAULT '0',
  `num2` varchar(10) NOT NULL DEFAULT '0',
  `symbol` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_captcha`
--

INSERT INTO `eb_captcha` (`id`, `createdate`, `sess_hash`, `num1`, `num2`, `symbol`) VALUES
(10, '2019-12-05 00:08:42', '4bf5199f1081fd498054d2b857f761f9', '3', '8', 2),
(11, '2019-12-05 00:08:42', 'cb6d993e992b230b992360d47ef9cb5c', '8', '9', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_change`
--

CREATE TABLE `eb_change` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `meta_key2` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_change`
--

INSERT INTO `eb_change` (`id`, `meta_key`, `meta_key2`, `meta_value`) VALUES
(1, 'admin_panel_url', '', 'xchngbx'),
(2, 'reviews', 'count', '20'),
(3, 'reviews', 'method', 'moderation'),
(4, 'reviews', 'website', '0'),
(5, 'captcha', 'reviewsform', '0'),
(6, 'captcha', 'contactform', '0'),
(7, 'captcha', 'loginform', '0'),
(8, 'captcha', 'registerform', '0'),
(9, 'partners', 'status', '1'),
(10, 'partners', 'minpay', '5'),
(11, 'exchange', 'courez', '4'),
(19, 'exchange', 'techreg', '0'),
(20, 'exchange', 'techregtext', ''),
(27, 'naps_temp', 'timeline_txt', 'Данная операция производится оператором в ручном режиме и занимает от 5 до 30 минут в рабочее время (см. статус оператора).'),
(28, 'naps_temp', 'description_txt', 'Для обмена Вам необходимо выполнить несколько шагов:\r\n\r\nЗаполните все поля представленной формы. Нажмите кнопку «Продолжить».\r\nОзнакомьтесь с условиями договора на оказание услуг обмена, если вы принимаете их, поставьте галочку в соответствующем поле/нажмите кнопку «Принимаю» («Согласен»). Еще раз проверьте данные заявки.\r\n3. Оплатите заявку.  Для этого следует совершить перевод необходимой суммы, следуя инструкциям на нашем сайте.\r\n4. После выполнения указанных действий, система переместит Вас на страницу «Состояние заявки», где будет указан статус вашего перевода.\r\n\r\n<strong>Внимание</strong>: для выполнения данной операции потребуется участие оператора (см. статус оператора).'),
(29, 'naps_temp', 'status_new', 'Авторизуйтесь в платежной системе XXXXXXX;\r\nПереведите указанную ниже сумму на кошелек XXXXXXX;\r\nНажмите на кнопку \"Я оплатил заявку\";\r\nОжидайте обработку заявки оператором.'),
(30, 'naps_temp', 'status_payed', 'Подтверждение оплаты принято.\r\nВаша заявка обрабатывается оператором.'),
(31, 'naps_temp', 'status_success', 'Ваша заявка выполнена.\r\nБлагодарим за то, что воспользовались услугами нашего сервиса.\r\nОставьте, пожалуйста, <a href=\"/reviews/\">отзыв</a> о работе нашего сервиса!'),
(32, 'naps_temp', 'status_new_mobile', '<ol>\r\n	<li>Авторизуйтесь в платежной системе или запустите мобильное приложение XXXXXXX;</li>\r\n	<li>Переведите указанную ниже сумму на кошелек XXXXXXX;</li>\r\n	<li>Нажмите на кнопку \"Я оплатил заявку\";</li>\r\n	<li>Ожидайте обработку заявки оператором.</li>\r\n</ol>'),
(33, 'exchange', 'autodelete', '0'),
(34, 'exchange', 'ad_h', '0'),
(35, 'exchange', 'ad_m', '15'),
(36, 'cron', '', '0'),
(37, 'admincaptcha', '', '1'),
(38, 'letter_auth', '', '1'),
(39, 'naps_temp', 'status_realpay', 'Подтверждение оплаты принято.\r\nВаша заявка обрабатывается оператором.'),
(40, 'naps_temp', 'status_verify', 'Подтверждение оплаты принято.\r\nВаша заявка обрабатывается оператором.'),
(41, 'naps_temp', 'status_returned', 'Оплата по заявке была возвращена на ваш кошелек.'),
(42, 'naps_temp', 'status_delete', 'Заявка была удалена.'),
(43, 'naps_temp', 'status_error', 'В заявке есть ошибки. Обратитесь в техническую поддержку.'),
(44, 'up_mode', '', '0'),
(45, 'txtxml', 'txt', '1'),
(46, 'txtxml', 'xml', '1'),
(47, 'txtxml', 'numtxt', '5'),
(48, 'txtxml', 'numxml', '5'),
(49, 'favicon', '', '/wp-content/uploads/favicon.png'),
(50, 'logo', '', ''),
(51, 'textlogo', '', ''),
(52, 'exchange', 'gostnaphide', '0'),
(53, 'exchange', 'an1_hidden', '0'),
(54, 'exchange', 'an2_hidden', '0'),
(55, 'exchange', 'an3_hidden', '0'),
(56, 'exchange', 'admin_mail', '0'),
(57, 'numsybm_count', '', '12'),
(58, 'user_fields', '', 'a:8:{s:5:\"login\";i:1;s:9:\"last_name\";i:1;s:10:\"first_name\";i:1;s:11:\"second_name\";i:1;s:10:\"user_phone\";i:1;s:10:\"user_skype\";i:1;s:7:\"website\";i:1;s:13:\"user_passport\";i:1;}'),
(59, 'user_fields_change', '', 'a:8:{s:10:\"user_email\";i:1;s:9:\"last_name\";i:1;s:10:\"first_name\";i:1;s:11:\"second_name\";i:1;s:10:\"user_phone\";i:1;s:10:\"user_skype\";i:1;s:7:\"website\";i:1;s:13:\"user_passport\";i:1;}'),
(60, 'persdata', 'last_name', '1'),
(61, 'persdata', 'first_name', '1'),
(62, 'persdata', 'second_name', '1'),
(63, 'persdata', 'user_phone', '1'),
(64, 'help', 'last_name', 'Введите фамилию, как у вас в паспорте.'),
(65, 'help', 'first_name', 'Введите имя, как у вас в паспорте.'),
(66, 'help', 'second_name', 'Введите отчество, как у вас в паспорте.'),
(67, 'help', 'user_email', 'Пожалуйста, укажите действующий адрес электронный почты.'),
(68, 'help', 'user_phone', 'Введите номер мобильного телефона в международном формате для связи с вами. Пример: +71234567890.'),
(69, 'help', 'user_skype', 'Введите логин Skype для связи с вами.'),
(70, 'persdata', 'user_skype', '0'),
(71, 'persdata', 'user_passport', '0'),
(72, 'exchange', 'adjust', '0'),
(73, 'exchange', 'beautynum', '0'),
(74, 'exchange', 'maxsymb_all', '4'),
(75, 'exchange', 'maxsymb_reserv', '4'),
(76, 'exchange', 'maxsymb_course', '4'),
(77, 'help', 'user_passport', 'Введите номер паспорта.'),
(78, 'htmlmap', 'exclude_page', 'a:0:{}'),
(79, 'htmlmap', 'exchanges', '1'),
(80, 'htmlmap', 'pages', '1'),
(81, 'htmlmap', 'news', '1'),
(82, 'xmlmap', 'exclude_page', 'a:0:{}'),
(83, 'xmlmap', 'exchanges', '1'),
(84, 'xmlmap', 'pages', '1'),
(85, 'xmlmap', 'news', '1'),
(86, 'captcha', 'lostpass1form', '0'),
(87, 'captcha', 'reservform', '0'),
(88, 'captcha', 'exchangeform', '0'),
(89, 'partners', 'wref', '0'),
(90, 'partners', 'payouttext', ''),
(91, 'partners', 'clife', '365'),
(92, 'partners', 'text_banners', '0'),
(93, 'partners', 'calc', '0'),
(94, 'partners', 'reserv', '0'),
(95, 'seo', 'home_title', ''),
(96, 'seo', 'home_key', ''),
(97, 'seo', 'home_descr', ''),
(98, 'seo', 'news_title', ''),
(99, 'seo', 'news_key', ''),
(100, 'seo', 'news_descr', ''),
(101, 'seo', 'news_temp', ''),
(102, 'seo', 'page_temp', ''),
(103, 'seo', 'exch_temp', '');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_commentmeta`
--

CREATE TABLE `eb_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_comments`
--

CREATE TABLE `eb_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_custom_fields_valut`
--

CREATE TABLE `eb_custom_fields_valut` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cf_name` longtext NOT NULL,
  `vid` int(1) NOT NULL DEFAULT 0,
  `valut_id` bigint(20) NOT NULL DEFAULT 0,
  `cf_req` int(1) NOT NULL DEFAULT 0,
  `place_id` int(1) NOT NULL DEFAULT 0,
  `minzn` int(2) NOT NULL DEFAULT 0,
  `maxzn` int(5) NOT NULL DEFAULT 100,
  `firstzn` varchar(20) NOT NULL,
  `helps` longtext NOT NULL,
  `datas` longtext NOT NULL,
  `status` int(2) NOT NULL DEFAULT 1,
  `cf_hidden` int(2) NOT NULL DEFAULT 0,
  `cf_order` bigint(20) NOT NULL DEFAULT 0,
  `uniqueid` varchar(250) NOT NULL,
  `tech_name` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_inex_change`
--

CREATE TABLE `eb_inex_change` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `meta_key2` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_inex_deposit`
--

CREATE TABLE `eb_inex_deposit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `createdate` datetime NOT NULL,
  `indate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `outdate` datetime NOT NULL,
  `couday` int(5) NOT NULL DEFAULT 0,
  `pers` varchar(250) NOT NULL,
  `insumm` varchar(250) NOT NULL DEFAULT '0',
  `outsumm` varchar(250) NOT NULL DEFAULT '0',
  `plussumm` varchar(250) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `user_login` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_schet` varchar(250) NOT NULL,
  `gid` bigint(20) NOT NULL DEFAULT 0,
  `gtitle` tinytext NOT NULL,
  `gvalut` varchar(250) NOT NULL,
  `paystatus` int(3) NOT NULL DEFAULT 0,
  `vipstatus` int(3) NOT NULL DEFAULT 0,
  `zakstatus` int(3) NOT NULL DEFAULT 0,
  `locale` varchar(20) NOT NULL,
  `mail1` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_inex_system`
--

CREATE TABLE `eb_inex_system` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` tinytext NOT NULL,
  `valut` varchar(250) NOT NULL,
  `gid` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_inex_tars`
--

CREATE TABLE `eb_inex_tars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` tinytext NOT NULL,
  `minsum` varchar(250) NOT NULL DEFAULT '0',
  `maxsum` varchar(250) NOT NULL DEFAULT '0',
  `gid` bigint(20) NOT NULL DEFAULT 0,
  `gtitle` tinytext NOT NULL,
  `gvalut` varchar(250) NOT NULL,
  `mpers` varchar(250) NOT NULL,
  `cdays` bigint(20) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_links`
--

CREATE TABLE `eb_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_masschange`
--

CREATE TABLE `eb_masschange` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` tinytext NOT NULL,
  `curs1` varchar(50) NOT NULL DEFAULT '0',
  `curs2` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_merchant_logs`
--

CREATE TABLE `eb_merchant_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `createdate` datetime NOT NULL,
  `mdata` longtext NOT NULL,
  `merchant` varchar(150) NOT NULL,
  `ip` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_napobmens`
--

CREATE TABLE `eb_napobmens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `valsid1` bigint(20) NOT NULL DEFAULT 0,
  `valsid2` bigint(20) NOT NULL DEFAULT 0,
  `curs1` varchar(250) NOT NULL DEFAULT '0',
  `curs2` varchar(250) NOT NULL DEFAULT '0',
  `commis1` varchar(250) NOT NULL DEFAULT '0',
  `prorsum1` int(1) NOT NULL DEFAULT 0,
  `commis2` varchar(250) NOT NULL DEFAULT '0',
  `prorsum2` int(1) NOT NULL DEFAULT 0,
  `minsumm1` varchar(250) NOT NULL DEFAULT '0',
  `minsumm2` varchar(250) NOT NULL DEFAULT '0',
  `text1` longtext NOT NULL,
  `text2` longtext NOT NULL,
  `text3` longtext NOT NULL,
  `text4` longtext NOT NULL,
  `text5` longtext NOT NULL,
  `rorder` bigint(20) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1,
  `parser` bigint(20) NOT NULL DEFAULT 0,
  `corecurs1` varchar(250) NOT NULL,
  `corecurs2` varchar(250) NOT NULL,
  `parcommis1` varchar(250) NOT NULL,
  `parcommis2` varchar(250) NOT NULL,
  `parprorsum1` int(1) NOT NULL DEFAULT 0,
  `parprorsum2` int(1) NOT NULL DEFAULT 0,
  `prproc1` varchar(250) NOT NULL,
  `prproc2` varchar(250) NOT NULL,
  `prsum1` varchar(250) NOT NULL,
  `prsum2` varchar(250) NOT NULL,
  `delsk` int(1) NOT NULL DEFAULT 0,
  `maxsumm1` varchar(250) NOT NULL DEFAULT '0',
  `maxsumm1com` varchar(250) NOT NULL DEFAULT '0',
  `maxsumm2com` varchar(250) NOT NULL DEFAULT '0',
  `rorder2` bigint(20) NOT NULL DEFAULT 0,
  `parthide` int(1) NOT NULL DEFAULT 0,
  `partmax` varchar(25) NOT NULL DEFAULT '0',
  `hidegost` int(1) NOT NULL DEFAULT 0,
  `minsumm1com` varchar(50) NOT NULL DEFAULT '0',
  `minsumm2com` varchar(50) NOT NULL DEFAULT '0',
  `combox` varchar(50) NOT NULL DEFAULT '0',
  `x19mod` bigint(20) NOT NULL DEFAULT 0,
  `comboxpers` varchar(50) NOT NULL DEFAULT '0',
  `masschange` bigint(20) NOT NULL DEFAULT 0,
  `text6` longtext NOT NULL,
  `maxsumm2` varchar(250) NOT NULL DEFAULT '0',
  `windowtext` longtext NOT NULL,
  `client_verify` int(1) NOT NULL DEFAULT 0,
  `pmcheck` bigint(20) NOT NULL DEFAULT 0,
  `uf` longtext NOT NULL,
  `show_file` int(1) NOT NULL DEFAULT 1,
  `xml_city` longtext NOT NULL,
  `xml_juridical` int(1) NOT NULL DEFAULT 0,
  `xml_param` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_napobmens`
--

INSERT INTO `eb_napobmens` (`id`, `valsid1`, `valsid2`, `curs1`, `curs2`, `commis1`, `prorsum1`, `commis2`, `prorsum2`, `minsumm1`, `minsumm2`, `text1`, `text2`, `text3`, `text4`, `text5`, `rorder`, `status`, `parser`, `corecurs1`, `corecurs2`, `parcommis1`, `parcommis2`, `parprorsum1`, `parprorsum2`, `prproc1`, `prproc2`, `prsum1`, `prsum2`, `delsk`, `maxsumm1`, `maxsumm1com`, `maxsumm2com`, `rorder2`, `parthide`, `partmax`, `hidegost`, `minsumm1com`, `minsumm2com`, `combox`, `x19mod`, `comboxpers`, `masschange`, `text6`, `maxsumm2`, `windowtext`, `client_verify`, `pmcheck`, `uf`, `show_file`, `xml_city`, `xml_juridical`, `xml_param`) VALUES
(11, 11, 3, '1000', '15.11025815175', '0', 1, '0.5', 0, '0', '0', '', '', '', '', '', 5, 1, 2, '', '', '0', '-3%', 0, 0, '0', '0.5', '0', '0', 0, '0', '0', '0', 0, 0, '0', 0, '0', '0', '0', 0, '0', 0, '', '0', '', 0, 0, 'a:4:{s:9:\"last_name\";s:1:\"1\";s:10:\"first_name\";s:1:\"1\";s:11:\"second_name\";s:1:\"1\";s:10:\"user_phone\";s:1:\"1\";}', 1, '', 0, ''),
(13, 3, 4, '1', '0.851549468879', '0.5', 0, '0.5', 0, '0', '0', '', '', '', '', '', 1, 1, 52, '', '', '0', '-3%', 0, 0, '0.5', '0.5', '0', '0', 0, '0', '0', '0', 1, 0, '0', 0, '0', '0', '0', 0, '0', 0, '', '0', '', 0, 0, 'a:4:{s:9:\"last_name\";s:1:\"1\";s:10:\"first_name\";s:1:\"1\";s:11:\"second_name\";s:1:\"1\";s:10:\"user_phone\";s:1:\"1\";}', 1, '', 0, ''),
(15, 4, 3, '1', '1.104927', '0.5', 0, '0.5', 0, '0', '0', '', '', '', '', '', 2, 1, 51, '', '', '0', '-3%', 0, 0, '0.5', '0.5', '0', '0', 0, '0', '0', '0', 1, 0, '0', 0, '0', '0', '0', 0, '0', 0, '', '0', '', 0, 0, 'a:4:{s:9:\"last_name\";s:1:\"1\";s:10:\"first_name\";s:1:\"1\";s:11:\"second_name\";s:1:\"1\";s:10:\"user_phone\";s:1:\"1\";}', 1, '', 0, ''),
(16, 12, 3, '1000', '15.11025815175', '0', 1, '0.5', 0, '0', '0', '', '', '', '', '', 6, 1, 2, '', '', '0', '-3%', 0, 0, '0', '0.5', '0', '0', 0, '0', '0', '0', 0, 0, '0', 0, '0', '0', '0', 0, '0', 0, '', '0', '', 0, 0, 'a:4:{s:9:\"last_name\";s:1:\"1\";s:10:\"first_name\";s:1:\"1\";s:11:\"second_name\";s:1:\"1\";s:10:\"user_phone\";s:1:\"1\";}', 1, '', 0, ''),
(19, 3, 11, '1', '62.268956', '0.5', 0, '0', 1, '0', '0', '', '', '', '', '', 1, 1, 1, '', '', '0', '-3%', 0, 0, '0.5', '0', '0', '0', 0, '0', '0', '0', 3, 0, '0', 0, '0', '0', '0', 0, '0', 0, '', '0', '', 0, 0, 'a:4:{s:9:\"last_name\";s:1:\"1\";s:10:\"first_name\";s:1:\"1\";s:11:\"second_name\";s:1:\"1\";s:10:\"user_phone\";s:1:\"1\";}', 1, '', 0, ''),
(23, 3, 12, '1', '62.268956', '0.5', 0, '0', 1, '0', '0', '', '', '', '', '', 1, 1, 1, '', '', '0', '-3%', 0, 0, '0.5', '1', '0', '0', 0, '0', '0', '0', 5, 0, '0', 0, '0', '0', '0', 0, '0', 0, '', '0', '', 0, 0, 'a:4:{s:9:\"last_name\";s:1:\"1\";s:10:\"first_name\";s:1:\"1\";s:11:\"second_name\";s:1:\"1\";s:10:\"user_phone\";s:1:\"1\";}', 1, '', 0, ''),
(26, 18, 3, '1000', '15.11025815175', '0.5', 0, '0.5', 0, '0', '0', '', '', '', '', '', 3, 1, 2, '', '', '0', '-3%', 0, 0, '0.5', '0.5', '0', '0', 0, '0', '0', '0', 0, 0, '0', 0, '0', '0', '0', 0, '0', 0, '', '0', '', 0, 0, 'a:4:{s:9:\"last_name\";s:1:\"1\";s:10:\"first_name\";s:1:\"1\";s:11:\"second_name\";s:1:\"1\";s:10:\"user_phone\";s:1:\"1\";}', 1, '', 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_naps_meta`
--

CREATE TABLE `eb_naps_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) NOT NULL DEFAULT 0,
  `meta_key` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_naps_meta`
--

INSERT INTO `eb_naps_meta` (`id`, `item_id`, `meta_key`, `meta_value`) VALUES
(1, 26, 'seo_title', ''),
(2, 26, 'seo_key', ''),
(3, 26, 'seo_descr', ''),
(4, 23, 'seo_title', ''),
(5, 23, 'seo_key', ''),
(6, 23, 'seo_descr', ''),
(7, 22, 'seo_title', ''),
(8, 22, 'seo_key', ''),
(9, 22, 'seo_descr', ''),
(10, 19, 'seo_title', ''),
(11, 19, 'seo_key', ''),
(12, 19, 'seo_descr', ''),
(13, 18, 'seo_title', ''),
(14, 18, 'seo_key', ''),
(15, 18, 'seo_descr', ''),
(16, 16, 'seo_title', ''),
(17, 16, 'seo_key', ''),
(18, 16, 'seo_descr', ''),
(19, 15, 'seo_title', ''),
(20, 15, 'seo_key', ''),
(21, 15, 'seo_descr', ''),
(22, 13, 'seo_title', ''),
(23, 13, 'seo_key', ''),
(24, 13, 'seo_descr', ''),
(25, 11, 'seo_title', ''),
(26, 11, 'seo_key', ''),
(27, 11, 'seo_descr', '');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_notice_head`
--

CREATE TABLE `eb_notice_head` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `create_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL,
  `auto_status` int(1) NOT NULL DEFAULT 1,
  `edit_user_id` bigint(20) NOT NULL DEFAULT 0,
  `notice_type` int(1) NOT NULL DEFAULT 0,
  `notice_display` int(1) NOT NULL DEFAULT 0,
  `datestart` datetime NOT NULL,
  `dateend` datetime NOT NULL,
  `op_status` int(5) NOT NULL DEFAULT -1,
  `h1` varchar(5) NOT NULL DEFAULT '0',
  `m1` varchar(5) NOT NULL DEFAULT '0',
  `h2` varchar(5) NOT NULL DEFAULT '0',
  `m2` varchar(5) NOT NULL DEFAULT '0',
  `d1` int(1) NOT NULL DEFAULT 0,
  `d2` int(1) NOT NULL DEFAULT 0,
  `d3` int(1) NOT NULL DEFAULT 0,
  `d4` int(1) NOT NULL DEFAULT 0,
  `d5` int(1) NOT NULL DEFAULT 0,
  `d6` int(1) NOT NULL DEFAULT 0,
  `d7` int(1) NOT NULL DEFAULT 0,
  `url` longtext NOT NULL,
  `text` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `theclass` varchar(250) NOT NULL,
  `site_order` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_options`
--

CREATE TABLE `eb_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) DEFAULT NULL,
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_options`
--

INSERT INTO `eb_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(4, 'siteurl', 'http://exchange.best-curs.info', 'yes'),
(5, 'blogname', 'Обменный пункт', 'yes'),
(6, 'blogdescription', 'Обменный пункт', 'yes'),
(7, 'users_can_register', '0', 'yes'),
(8, 'admin_email', 'info@best-curs.info', 'yes'),
(9, 'start_of_week', '1', 'yes'),
(10, 'use_balanceTags', '0', 'yes'),
(11, 'use_smilies', '', 'yes'),
(12, 'require_name_email', '1', 'yes'),
(13, 'comments_notify', '', 'yes'),
(14, 'posts_per_rss', '10', 'yes'),
(15, 'rss_use_excerpt', '1', 'yes'),
(16, 'mailserver_url', 'mail.example.com', 'yes'),
(17, 'mailserver_login', 'login@example.com', 'yes'),
(18, 'mailserver_pass', 'password', 'yes'),
(19, 'mailserver_port', '110', 'yes'),
(20, 'default_category', '1', 'yes'),
(21, 'default_comment_status', 'open', 'yes'),
(22, 'default_ping_status', 'closed', 'yes'),
(23, 'default_pingback_flag', '', 'yes'),
(25, 'posts_per_page', '10', 'yes'),
(26, 'date_format', 'd.m.Y', 'yes'),
(27, 'time_format', 'H:i', 'yes'),
(28, 'links_updated_date_format', 'd.m.Y H:i', 'yes'),
(32, 'comment_moderation', '1', 'yes'),
(33, 'moderation_notify', '', 'yes'),
(34, 'permalink_structure', '/%postname%/', 'yes'),
(36, 'hack_file', '0', 'yes'),
(37, 'blog_charset', 'UTF-8', 'yes'),
(38, 'moderation_keys', '', 'no'),
(39, 'active_plugins', 'a:1:{i:0;s:27:\"exchangebox/exchangebox.php\";}', 'yes'),
(40, 'home', 'http://exchange.best-curs.info', 'yes'),
(41, 'category_base', '', 'yes'),
(42, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(44, 'comment_max_links', '2', 'yes'),
(45, 'gmt_offset', '3', 'yes'),
(46, 'default_email_category', '1', 'yes'),
(47, 'recently_edited', '', 'no'),
(48, 'template', 'exchangeboxtheme2', 'yes'),
(49, 'stylesheet', 'exchangeboxtheme2', 'yes'),
(50, 'comment_whitelist', '1', 'yes'),
(51, 'blacklist_keys', '', 'no'),
(52, 'comment_registration', '0', 'yes'),
(53, 'html_type', 'text/html', 'yes'),
(54, 'use_trackback', '0', 'yes'),
(55, 'default_role', 'users', 'yes'),
(56, 'db_version', '45805', 'yes'),
(57, 'uploads_use_yearmonth_folders', '', 'yes'),
(58, 'upload_path', '', 'yes'),
(59, 'blog_public', '1', 'yes'),
(60, 'default_link_category', '2', 'yes'),
(61, 'show_on_front', 'page', 'yes'),
(62, 'tag_base', '', 'yes'),
(63, 'show_avatars', '', 'yes'),
(64, 'avatar_rating', 'G', 'yes'),
(65, 'upload_url_path', '', 'yes'),
(66, 'thumbnail_size_w', '150', 'yes'),
(67, 'thumbnail_size_h', '150', 'yes'),
(68, 'thumbnail_crop', '1', 'yes'),
(69, 'medium_size_w', '300', 'yes'),
(70, 'medium_size_h', '300', 'yes'),
(71, 'avatar_default', 'mystery', 'yes'),
(74, 'large_size_w', '1024', 'yes'),
(75, 'large_size_h', '1024', 'yes'),
(76, 'image_default_link_type', 'file', 'yes'),
(77, 'image_default_size', '', 'yes'),
(78, 'image_default_align', '', 'yes'),
(79, 'close_comments_for_old_posts', '0', 'yes'),
(80, 'close_comments_days_old', '14', 'yes'),
(81, 'thread_comments', '1', 'yes'),
(82, 'thread_comments_depth', '5', 'yes'),
(83, 'page_comments', '0', 'yes'),
(84, 'comments_per_page', '50', 'yes'),
(85, 'default_comments_page', 'newest', 'yes'),
(86, 'comment_order', 'asc', 'yes'),
(87, 'sticky_posts', 'a:0:{}', 'yes'),
(88, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(89, 'widget_text', 'a:5:{i:4;a:3:{s:5:\"title\";s:73:\"Приветствуем на сайте обменного пункта!\";s:4:\"text\";s:1787:\"Наш On-line сервис предназначен для тех, кто хочет быстро, безопасно и по выгодному курсу обменять такие виды электронных валют: Webmoney, Perfect Money, LiqPay, Pecunix, Payza, Яндекс. Деньги, Payweb, Альфа-Банк, ВТБ 24, Приват24, Связной банк, Visa/Master Card, Western Union, MoneyGram.</br></br>\r\n\r\nЭтим возможности нашего сервиса не ограничиваются. В рамках проекта действуют программа лояльности, накопительная скидка и партнерская программа, воспользовавшись преимуществами которых, вы сможете совершать обмен электронных валют на более выгодных условиях. Для этого нужно просто <a href=\"/register/\">зарегистрироваться</a> на сайте.</br></br>\r\n\r\nНаш пункт обмена электронных валют – система, созданная на базе современного программного обеспечения и содержащая весь набор необходимых функций для удобной и безопасной конвертации наиболее распространенных видов электронных денег. За время работы мы приобрели репутацию проверенного партнера и делаем все возможное, чтобы ваши впечатления от нашего сервиса были только благоприятными.\";s:6:\"filter\";b:0;}i:5;a:3:{s:5:\"title\";s:16:\"Партнеры\";s:4:\"text\";s:728:\"<table class=\"tpartner\">\r\n<tr>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/ya_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/pm_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/stp_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/egopay_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/bitcoin_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>	\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/okpay_bottom.png\" alt=\"\" height=\"31\" width=\"88\" /></a>\r\n</td>					\r\n</tr>\r\n</table>\r\n\";s:6:\"filter\";b:0;}i:8;a:3:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:112:\"Обменный пункт электронных валют.</br>\r\nВсе права защищены © 2014.\";s:6:\"filter\";b:0;}i:9;a:3:{s:5:\"title\";s:0:\"\";s:4:\"text\";s:192:\"<a href=\"/preduprezhdenie/\">Предупреждение</a> &nbsp;&nbsp;&bull;&nbsp;&nbsp; <a href=\"/tos/\">Правила</a> &nbsp;&nbsp;&bull;&nbsp;&nbsp; <a href=\"/sitemap/\">Карта</a>\";s:6:\"filter\";b:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(3718, 'widget_exbloginwidget', 'a:5:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"titlem\";s:0:\"\";}i:3;a:2:{s:5:\"title\";s:0:\"\";s:6:\"titlem\";s:0:\"\";}i:4;a:2:{s:5:\"title\";s:0:\"\";s:6:\"titlem\";s:0:\"\";}i:5;a:2:{s:5:\"title\";s:0:\"\";s:6:\"titlem\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(3720, 'widget_get_rnews', 'a:5:{i:2;a:5:{s:5:\"title\";s:0:\"\";s:4:\"tcat\";s:1:\"0\";s:5:\"count\";s:1:\"3\";s:6:\"sorter\";s:4:\"date\";s:4:\"desc\";s:4:\"desc\";}i:3;a:5:{s:5:\"title\";s:0:\"\";s:4:\"tcat\";s:1:\"0\";s:5:\"count\";s:0:\"\";s:6:\"sorter\";s:4:\"date\";s:4:\"desc\";s:3:\"asc\";}i:4;a:5:{s:5:\"title\";s:0:\"\";s:4:\"tcat\";s:1:\"0\";s:5:\"count\";s:0:\"\";s:6:\"sorter\";s:4:\"date\";s:4:\"desc\";s:3:\"asc\";}i:5;a:5:{s:5:\"title\";s:0:\"\";s:4:\"tcat\";s:1:\"0\";s:5:\"count\";s:0:\"\";s:6:\"sorter\";s:4:\"date\";s:4:\"desc\";s:4:\"desc\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(90, 'widget_rss', 'a:0:{}', 'yes'),
(91, 'timezone_string', '', 'yes'),
(93, 'embed_size_w', '', 'yes'),
(94, 'embed_size_h', '600', 'yes'),
(97, 'default_post_format', '0', 'yes'),
(98, 'initial_db_version', '19470', 'yes'),
(99, 'eb_user_roles', 'a:2:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:66:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:14:\"manage_ratings\";b:1;s:25:\"user_can_config_downloads\";b:1;s:23:\"user_can_edit_downloads\";b:1;s:25:\"user_can_add_new_download\";b:1;s:27:\"user_can_view_downloads_log\";b:1;}}s:5:\"users\";a:2:{s:4:\"name\";s:24:\"Пользователь\";s:12:\"capabilities\";a:1:{s:7:\"level_0\";b:1;}}}', 'yes'),
(3709, 'theme_mods_exchangeboxtheme', 'a:3:{i:0;s:0:\"\";s:18:\"nav_menu_locations\";a:1:{s:8:\"top_menu\";i:3;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1436943454;s:4:\"data\";a:7:{s:18:\"orphaned_widgets_2\";a:5:{i:0;s:15:\"exbobmen_info-2\";i:1;s:16:\"get_statuswork-3\";i:2;s:16:\"exbloginwidget-2\";i:3;s:16:\"exbotzivs_info-2\";i:4;s:11:\"get_rnews-2\";}s:18:\"orphaned_widgets_3\";a:1:{i:0;s:6:\"text-4\";}s:18:\"orphaned_widgets_4\";a:1:{i:0;s:6:\"text-5\";}s:18:\"orphaned_widgets_5\";a:1:{i:0;s:6:\"text-8\";}s:18:\"orphaned_widgets_6\";a:1:{i:0;s:6:\"text-9\";}s:19:\"wp_inactive_widgets\";a:5:{i:0;s:11:\"get_rnews-4\";i:1;s:16:\"exbotzivs_info-4\";i:2;s:16:\"exbloginwidget-4\";i:3;s:19:\"exblastobmen_info-3\";i:4;s:16:\"get_statuswork-5\";}s:9:\"sidebar-1\";a:5:{i:0;s:16:\"get_statuswork-4\";i:1;s:16:\"exbloginwidget-3\";i:2;s:19:\"exblastobmen_info-2\";i:3;s:16:\"exbotzivs_info-3\";i:4;s:11:\"get_rnews-3\";}}}}', 'yes'),
(100, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(610, 'widget_get_rx_cursrf', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"valuta\";s:2:\"ru\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'sidebars_widgets', 'a:4:{s:9:\"sidebar-1\";a:3:{i:0;s:16:\"get_statuswork-5\";i:1;s:16:\"exbloginwidget-4\";i:2;s:19:\"exblastobmen_info-3\";}s:19:\"wp_inactive_widgets\";a:0:{}s:17:\"unique-sidebar-id\";a:4:{i:0;s:16:\"get_statuswork-6\";i:1;s:16:\"exbloginwidget-5\";i:2;s:19:\"exblastobmen_info-4\";i:3;s:16:\"get_pn_reviews-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(3719, 'widget_exbotzivs_info', 'a:5:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:5:\"count\";s:1:\"3\";}i:3;a:2:{s:5:\"title\";s:0:\"\";s:5:\"count\";s:0:\"\";}i:4;a:2:{s:5:\"title\";s:0:\"\";s:5:\"count\";s:0:\"\";}i:5;a:2:{s:5:\"title\";s:0:\"\";s:5:\"count\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(607, 'widget_get_a_fxlastobmen', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'cron', 'a:7:{i:1575495679;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1575522440;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1575530906;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1575563613;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1575565648;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1575580110;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(2793, 'uninstall_plugins', 'a:1:{s:27:\"exchangebox/exchangebox.php\";s:21:\"exb_plugins_uninstall\";}', 'no'),
(113, 'dashboard_widget_options', 'a:4:{s:25:\"dashboard_recent_comments\";a:1:{s:5:\"items\";i:5;}s:24:\"dashboard_incoming_links\";a:5:{s:4:\"home\";s:30:\"http://exchange.best-curs.info\";s:4:\"link\";s:106:\"http://blogsearch.google.com/blogsearch?scoring=d&partner=wordpress&q=link:http://exchange.best-curs.info/\";s:3:\"url\";s:139:\"http://blogsearch.google.com/blogsearch_feeds?scoring=d&ie=utf-8&num=10&output=rss&partner=wordpress&q=link:http://exchange.best-curs.info/\";s:5:\"items\";i:10;s:9:\"show_date\";b:0;}s:17:\"dashboard_primary\";a:8:{s:5:\"title\";s:18:\"Блог WordPress\";s:3:\"url\";s:31:\"http://wordpress.org/news/feed/\";s:4:\"link\";s:25:\"http://wordpress.org/news\";s:5:\"items\";i:2;s:5:\"error\";b:0;s:12:\"show_summary\";i:1;s:11:\"show_author\";i:0;s:9:\"show_date\";i:1;}s:19:\"dashboard_secondary\";a:7:{s:4:\"link\";s:28:\"http://planet.wordpress.org/\";s:3:\"url\";s:33:\"http://planet.wordpress.org/feed/\";s:5:\"title\";s:37:\"Другие новости WordPress\";s:5:\"items\";i:5;s:12:\"show_summary\";i:0;s:11:\"show_author\";i:0;s:9:\"show_date\";i:0;}}', 'yes'),
(993, 'current_theme', 'ExchangeBoxTheme 2.0', 'yes'),
(832, 'widget_get_a_fxreviews', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:3:\"kol\";s:1:\"2\";s:4:\"link\";s:9:\"/rewiews/\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(4650, 'rewrite_rules', 'a:86:{s:51:\"([\\-_A-Za-z0-9]+)_([A-Za-z0-9]+)_to_([A-Za-z0-9]+)$\";s:68:\"index.php?pagename=$matches[1]&valut1=$matches[2]&valut2=$matches[3]\";s:40:\"([\\-_A-Za-z0-9]+)/hst_([A-Za-z0-9]{35})$\";s:49:\"index.php?pagename=$matches[1]&hashed=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:40:\"index.php?&page_id=151&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(4504, 'banners', 'a:6:{s:4:\"text\";a:2:{i:0;s:1198:\"Хотите обменять электронные деньги или вывести средства на банковский счет? Ищете надежный сервис для обмена электронных денег? <a href=\"[partner_link]\">[url]</a> – это выгодные курсы обмена, безопасность и минимальные сроки выполнения финансовых операций. Простой и удобный интерфейс избавит от необходимости долго изучать структуру сайта, а возможность в любое время связаться с оператором позволит «на месте» получить ответы на любые вопросы, касающиеся конвертации валют. Кроме того, обменник предусматривает возможность заработка на партнерской программе. Зарегистрированные пользователи получают бонусы и скидки, которые растут с каждой новой транзакцией.\";i:1;s:1257:\"Вам нужно вывести электронные деньги на банковский счет или обменять один вид электронной валюты на другой? Вы сбились с ног, пытаясь отыскать пункт обмена, где вам предложат выгодный курс, высокий уровень безопасности и максимальную оперативность проведения транзакций? <a href=\"[partner_link]\">[url]</a> решит все ваши проблемы с обменом, переводом или выводом средств из сети. Широкий спектр валют, льготные тарифы для зарегистрированных пользователей, внимательная техподдержка и простой интерфейс делают работу с сервисом комфортной и выгодной. А наличие партнерской программы позволит не только проводить сделки без рисков, но и зарабатывать, рекомендуя проверенный обменник своим знакомым.\";}s:7:\"banner1\";a:1:{i:0;s:212:\"<a href=\"[partner_link]\"><img src=\"[url]/wp-content/plugins/exchangebox/images/banners/468x60_1.gif\" alt=\"Обменный пункт\" title=\"Обменный пункт\" width=\"468\" height=\"60\" border=\"0\" /></a>\";}s:7:\"banner2\";a:1:{i:0;s:214:\"<a href=\"[partner_link]\"><img src=\"[url]/wp-content/plugins/exchangebox/images/banners/200x200_1.gif\" alt=\"Обменный пункт\" title=\"Обменный пункт\" width=\"200\" height=\"200\" border=\"0\" /></a>\";}s:7:\"banner3\";a:1:{i:0;s:214:\"<a href=\"[partner_link]\"><img src=\"[url]/wp-content/plugins/exchangebox/images/banners/120x600_1.gif\" alt=\"Обменный пункт\" title=\"Обменный пункт\" width=\"120\" height=\"600\" border=\"0\" /></a>\";}s:7:\"banner4\";a:1:{i:0;s:214:\"<a href=\"[partner_link]\"><img src=\"[url]/wp-content/plugins/exchangebox/images/banners/100x100_1.gif\" alt=\"Обменный пункт\" title=\"Обменный пункт\" width=\"100\" height=\"100\" border=\"0\" /></a>\";}s:7:\"banner5\";a:1:{i:0;s:210:\"<a href=\"[partner_link]\"><img src=\"[url]/wp-content/plugins/exchangebox/images/banners/88x31_1.gif\" alt=\"Обменный пункт\" title=\"Обменный пункт\" width=\"88\" height=\"31\" border=\"0\" /></a>\";}}', 'yes'),
(3761, '_transient_timeout_feed_867bd5c64f85878d03a060509cd2f92c', '1367023035', 'no'),
(151, 'recently_activated', 'a:0:{}', 'yes'),
(3843, 'exchangebox_curs', 'a:4:{s:3:\"EUR\";a:3:{s:3:\"RUB\";s:7:\"81.1533\";s:3:\"USD\";s:6:\"1.1497\";s:3:\"UAH\";s:8:\"24.63701\";}s:3:\"RUB\";a:3:{s:3:\"EUR\";s:7:\"1.23224\";s:3:\"USD\";s:6:\"1.4135\";s:3:\"UAH\";s:8:\"33.33333\";}s:3:\"USD\";a:3:{s:3:\"RUB\";s:7:\"70.7465\";s:3:\"EUR\";s:7:\"0.86979\";s:3:\"UAH\";s:8:\"22.03077\";}s:3:\"UAH\";a:3:{s:3:\"RUB\";s:7:\"30.0333\";s:3:\"EUR\";s:7:\"0.04059\";s:3:\"USD\";s:7:\"0.04539\";}}', 'yes'),
(4037, 'widget_get_statuswork', 'a:5:{i:3;a:11:{s:5:\"title\";s:0:\"\";s:5:\"text1\";s:0:\"\";s:5:\"text2\";s:0:\"\";s:8:\"hidedate\";s:1:\"0\";s:6:\"worked\";s:6:\"online\";s:5:\"wtime\";s:1:\"0\";s:8:\"worktime\";s:3:\"off\";s:3:\"wt1\";s:2:\"00\";s:3:\"wt2\";s:2:\"00\";s:3:\"wt3\";s:2:\"00\";s:3:\"wt4\";s:2:\"00\";}i:4;a:11:{s:5:\"title\";s:0:\"\";s:5:\"text1\";s:0:\"\";s:5:\"text2\";s:0:\"\";s:8:\"hidedate\";s:1:\"0\";s:6:\"worked\";s:7:\"offline\";s:5:\"wtime\";s:1:\"0\";s:8:\"worktime\";s:3:\"off\";s:3:\"wt1\";s:2:\"00\";s:3:\"wt2\";s:2:\"00\";s:3:\"wt3\";s:2:\"00\";s:3:\"wt4\";s:2:\"00\";}i:5;a:11:{s:5:\"title\";s:0:\"\";s:5:\"text1\";s:0:\"\";s:5:\"text2\";s:0:\"\";s:8:\"hidedate\";s:1:\"0\";s:6:\"worked\";s:7:\"offline\";s:5:\"wtime\";s:1:\"0\";s:8:\"worktime\";s:3:\"off\";s:3:\"wt1\";s:2:\"00\";s:3:\"wt2\";s:2:\"00\";s:3:\"wt3\";s:2:\"00\";s:3:\"wt4\";s:2:\"00\";}i:6;a:11:{s:5:\"title\";s:0:\"\";s:5:\"text1\";s:0:\"\";s:5:\"text2\";s:0:\"\";s:8:\"hidedate\";s:1:\"0\";s:6:\"worked\";s:7:\"offline\";s:5:\"wtime\";s:1:\"0\";s:8:\"worktime\";s:3:\"off\";s:3:\"wt1\";s:2:\"00\";s:3:\"wt2\";s:2:\"00\";s:3:\"wt3\";s:2:\"00\";s:3:\"wt4\";s:2:\"00\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(172, '_transient_random_seed', '5386b7ba218c5901ca99f6ca0f2eca4f', 'yes'),
(201, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(448, 'category_children', 'a:0:{}', 'yes'),
(269, 'theme_mods_twentyeleven', 'a:2:{s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:3;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1366924867;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";i:3;s:19:\"fx_browser_plugin-2\";}s:9:\"sidebar-1\";a:5:{i:0;s:15:\"get_rx_cursrf-2\";i:1;s:19:\"get_a_fxlastobmen-2\";i:2;s:20:\"get_a_fxrezervinfo-2\";i:3;s:14:\"get_a_fxinfo-2\";i:4;s:17:\"get_a_fxreviews-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}s:9:\"sidebar-4\";a:0:{}s:9:\"sidebar-5\";a:0:{}}}}', 'yes'),
(364, 'srel_options', 'a:0:{}', 'yes'),
(365, 'srel_main', '', 'yes'),
(366, 'srel_included', 'a:0:{}', 'yes'),
(367, 'srel_excluded', 'a:0:{}', 'yes'),
(368, 'wp_ds_blogmap_array', 'a:11:{s:3:\"ver\";s:3:\"312\";s:6:\"prefix\";s:0:\"\";s:12:\"tags_rep_str\";s:10:\"[tagcloud]\";s:13:\"posts_rep_str\";s:10:\"[postlist]\";s:13:\"pages_rep_str\";s:13:\"[pagesoftree]\";s:11:\"expand_text\";s:25:\"Show&nbsp;all&nbsp;&rarr;\";s:10:\"tags_limit\";i:0;s:11:\"posts_limit\";i:0;s:9:\"page_dept\";i:0;s:17:\"posts_description\";b:1;s:17:\"hidden_categories\";a:1:{i:0;s:47:\"98t4irubfuga76ert2ou3rbpiut8yp4kgjfn87ty349ith3\";}}', 'yes'),
(370, 'tadv_version', '3420', 'yes'),
(371, 'tadv_plugins', 'a:0:{}', 'yes'),
(372, 'tadv_options', 'a:6:{s:8:\"advlink1\";i:0;s:8:\"advimage\";i:1;s:11:\"editorstyle\";i:0;s:11:\"hideclasses\";i:0;s:11:\"contextmenu\";i:0;s:8:\"no_autop\";i:0;}', 'yes'),
(373, 'tadv_toolbars', 'a:4:{s:9:\"toolbar_1\";a:27:{i:0;s:4:\"bold\";i:1;s:6:\"italic\";i:2;s:13:\"strikethrough\";i:3;s:9:\"underline\";i:4;s:10:\"separator1\";i:5;s:7:\"bullist\";i:6;s:7:\"numlist\";i:7;s:7:\"outdent\";i:8;s:6:\"indent\";i:9;s:10:\"separator2\";i:10;s:11:\"justifyleft\";i:11;s:13:\"justifycenter\";i:12;s:12:\"justifyright\";i:13;s:10:\"separator3\";i:14;s:4:\"link\";i:15;s:6:\"unlink\";i:16;s:10:\"separator4\";i:17;s:5:\"image\";i:18;s:10:\"styleprops\";i:19;s:11:\"separator12\";i:20;s:7:\"wp_more\";i:21;s:7:\"wp_page\";i:22;s:10:\"separator5\";i:23;s:12:\"spellchecker\";i:24;s:6:\"search\";i:25;s:10:\"separator6\";i:26;s:10:\"fullscreen\";}s:9:\"toolbar_2\";a:21:{i:0;s:14:\"fontsizeselect\";i:1;s:12:\"formatselect\";i:2;s:9:\"pastetext\";i:3;s:9:\"pasteword\";i:4;s:12:\"removeformat\";i:5;s:10:\"separator8\";i:6;s:7:\"charmap\";i:7;s:5:\"print\";i:8;s:10:\"separator9\";i:9;s:9:\"forecolor\";i:10;s:9:\"backcolor\";i:11;s:8:\"emotions\";i:12;s:11:\"separator10\";i:13;s:3:\"sup\";i:14;s:3:\"sub\";i:15;s:5:\"media\";i:16;s:11:\"separator11\";i:17;s:4:\"undo\";i:18;s:4:\"redo\";i:19;s:7:\"attribs\";i:20;s:7:\"wp_help\";}s:9:\"toolbar_3\";a:0:{}s:9:\"toolbar_4\";a:0:{}}', 'no'),
(374, 'tadv_btns1', 'a:28:{i:0;s:4:\"bold\";i:1;s:6:\"italic\";i:2;s:13:\"strikethrough\";i:3;s:9:\"underline\";i:4;s:9:\"separator\";i:5;s:7:\"bullist\";i:6;s:7:\"numlist\";i:7;s:7:\"outdent\";i:8;s:6:\"indent\";i:9;s:9:\"separator\";i:10;s:11:\"justifyleft\";i:11;s:13:\"justifycenter\";i:12;s:12:\"justifyright\";i:13;s:9:\"separator\";i:14;s:4:\"link\";i:15;s:6:\"unlink\";i:16;s:9:\"separator\";i:17;s:5:\"image\";i:18;s:10:\"styleprops\";i:19;s:9:\"separator\";i:20;s:7:\"wp_more\";i:21;s:7:\"wp_page\";i:22;s:9:\"separator\";i:23;s:12:\"spellchecker\";i:24;s:6:\"search\";i:25;s:9:\"separator\";i:26;s:10:\"fullscreen\";i:27;s:6:\"wp_adv\";}', 'no'),
(375, 'tadv_btns2', 'a:21:{i:0;s:14:\"fontsizeselect\";i:1;s:12:\"formatselect\";i:2;s:9:\"pastetext\";i:3;s:9:\"pasteword\";i:4;s:12:\"removeformat\";i:5;s:9:\"separator\";i:6;s:7:\"charmap\";i:7;s:5:\"print\";i:8;s:9:\"separator\";i:9;s:9:\"forecolor\";i:10;s:9:\"backcolor\";i:11;s:8:\"emotions\";i:12;s:9:\"separator\";i:13;s:3:\"sup\";i:14;s:3:\"sub\";i:15;s:5:\"media\";i:16;s:9:\"separator\";i:17;s:4:\"undo\";i:18;s:4:\"redo\";i:19;s:7:\"attribs\";i:20;s:7:\"wp_help\";}', 'no'),
(376, 'tadv_btns3', 'a:0:{}', 'no'),
(377, 'tadv_btns4', 'a:0:{}', 'no'),
(378, 'tadv_allbtns', 'a:64:{i:0;s:6:\"wp_adv\";i:1;s:4:\"bold\";i:2;s:6:\"italic\";i:3;s:13:\"strikethrough\";i:4;s:9:\"underline\";i:5;s:7:\"bullist\";i:6;s:7:\"numlist\";i:7;s:7:\"outdent\";i:8;s:6:\"indent\";i:9;s:11:\"justifyleft\";i:10;s:13:\"justifycenter\";i:11;s:12:\"justifyright\";i:12;s:11:\"justifyfull\";i:13;s:3:\"cut\";i:14;s:4:\"copy\";i:15;s:5:\"paste\";i:16;s:4:\"link\";i:17;s:6:\"unlink\";i:18;s:5:\"image\";i:19;s:7:\"wp_more\";i:20;s:7:\"wp_page\";i:21;s:6:\"search\";i:22;s:7:\"replace\";i:23;s:10:\"fontselect\";i:24;s:14:\"fontsizeselect\";i:25;s:7:\"wp_help\";i:26;s:10:\"fullscreen\";i:27;s:11:\"styleselect\";i:28;s:12:\"formatselect\";i:29;s:9:\"forecolor\";i:30;s:9:\"backcolor\";i:31;s:9:\"pastetext\";i:32;s:9:\"pasteword\";i:33;s:12:\"removeformat\";i:34;s:7:\"cleanup\";i:35;s:12:\"spellchecker\";i:36;s:7:\"charmap\";i:37;s:5:\"print\";i:38;s:4:\"undo\";i:39;s:4:\"redo\";i:40;s:13:\"tablecontrols\";i:41;s:4:\"cite\";i:42;s:3:\"ins\";i:43;s:3:\"del\";i:44;s:4:\"abbr\";i:45;s:7:\"acronym\";i:46;s:7:\"attribs\";i:47;s:5:\"layer\";i:48;s:5:\"advhr\";i:49;s:4:\"code\";i:50;s:11:\"visualchars\";i:51;s:11:\"nonbreaking\";i:52;s:3:\"sub\";i:53;s:3:\"sup\";i:54;s:9:\"visualaid\";i:55;s:10:\"insertdate\";i:56;s:10:\"inserttime\";i:57;s:6:\"anchor\";i:58;s:10:\"styleprops\";i:59;s:8:\"emotions\";i:60;s:5:\"media\";i:61;s:10:\"blockquote\";i:62;s:9:\"separator\";i:63;s:1:\"|\";}', 'no'),
(380, 'aioseop_options', 'a:37:{s:9:\"aiosp_can\";s:0:\"\";s:12:\"aiosp_donate\";s:0:\"\";s:16:\"aiosp_home_title\";s:61:\"Обменный пункт электронных валют\";s:22:\"aiosp_home_description\";s:61:\"Обменный пункт электронных валют\";s:19:\"aiosp_home_keywords\";s:61:\"Обменный пункт электронных валют\";s:23:\"aiosp_max_words_excerpt\";s:0:\"\";s:20:\"aiosp_rewrite_titles\";s:2:\"on\";s:23:\"aiosp_post_title_format\";s:27:\"%post_title% | %blog_title%\";s:23:\"aiosp_page_title_format\";s:27:\"%page_title% | %blog_title%\";s:27:\"aiosp_category_title_format\";s:31:\"%category_title% | %blog_title%\";s:26:\"aiosp_archive_title_format\";s:21:\"%date% | %blog_title%\";s:22:\"aiosp_tag_title_format\";s:20:\"%tag% | %blog_title%\";s:25:\"aiosp_search_title_format\";s:23:\"%search% | %blog_title%\";s:24:\"aiosp_description_format\";s:13:\"%description%\";s:22:\"aiosp_404_title_format\";s:33:\"Nothing found for %request_words%\";s:18:\"aiosp_paged_format\";s:14:\" - Part %page%\";s:25:\"aiosp_google_analytics_id\";s:0:\"\";s:29:\"aiosp_ga_track_outbound_links\";s:0:\"\";s:20:\"aiosp_use_categories\";s:0:\"\";s:32:\"aiosp_dynamic_postspage_keywords\";s:2:\"on\";s:22:\"aiosp_category_noindex\";s:0:\"\";s:21:\"aiosp_archive_noindex\";s:0:\"\";s:18:\"aiosp_tags_noindex\";s:0:\"\";s:14:\"aiosp_cap_cats\";s:2:\"on\";s:27:\"aiosp_generate_descriptions\";s:0:\"\";s:16:\"aiosp_debug_info\";s:0:\"\";s:20:\"aiosp_post_meta_tags\";s:0:\"\";s:20:\"aiosp_page_meta_tags\";s:0:\"\";s:20:\"aiosp_home_meta_tags\";s:0:\"\";s:13:\"aiosp_enabled\";s:1:\"1\";s:17:\"aiosp_enablecpost\";s:0:\"\";s:26:\"aiosp_use_tags_as_keywords\";s:2:\"on\";s:16:\"aiosp_seopostcol\";s:0:\"\";s:18:\"aiosp_seocustptcol\";s:0:\"\";s:21:\"aiosp_posttypecolumns\";a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}s:12:\"aiosp_do_log\";s:0:\"\";s:14:\"aiosp_ex_pages\";s:0:\"\";}', 'yes'),
(439, 'ld_http_auth', 'none', 'yes'),
(440, 'ld_hide_wp_admin', 'yep', 'yes'),
(441, 'ld_login_base', 'iam', 'yes'),
(477, 'WSD-RSS-WGT-DISPLAY', 'no', 'yes'),
(478, 'WSD-COOKIE', 'PHPSESSID=rro39nje4unq1fuol8mcg2liu1; path=/', 'yes'),
(613, 'widget_ca_browser_plugin', 'a:2:{i:2;a:6:{s:12:\"link_firefox\";s:0:\"\";s:11:\"link_chrome\";s:0:\"\";s:10:\"link_opera\";s:0:\"\";s:18:\"image_link_firefox\";s:0:\"\";s:17:\"image_link_chrome\";s:0:\"\";s:16:\"image_link_opera\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(616, 'widget_get_a_fxrezervinfo', 'a:2:{i:2;a:9:{s:5:\"title\";s:0:\"\";s:11:\"fdxWebMoney\";s:1:\"0\";s:9:\"fdxLiqPay\";s:1:\"1\";s:17:\"fdxLibertyReserve\";s:1:\"1\";s:15:\"fdxPerfectMoney\";s:1:\"1\";s:11:\"fdxRBKMoney\";s:1:\"0\";s:7:\"fdxQIWI\";s:1:\"1\";s:14:\"fdxYandexMoney\";s:1:\"1\";s:13:\"fdxMasterTour\";s:1:\"1\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(619, 'widget_get_a_fxinfo', 'a:2:{i:2;a:2:{s:5:\"title\";s:23:\"Наш аттестат\";s:4:\"wmid\";s:12:\"000000000000\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(994, 'theme_mods_exchange_box', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:3:\"top\";i:3;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1366924699;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:5:{i:0;s:15:\"get_rx_cursrf-2\";i:1;s:19:\"get_a_fxlastobmen-2\";i:2;s:20:\"get_a_fxrezervinfo-2\";i:3;s:14:\"get_a_fxinfo-2\";i:4;s:19:\"fx_browser_plugin-2\";}s:9:\"sidebar-2\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:17:\"get_a_fxreviews-2\";i:3;s:16:\"lt_recent_post-2\";}s:9:\"sidebar-3\";a:0:{}s:9:\"sidebar-4\";a:0:{}s:9:\"sidebar-5\";a:1:{i:0;s:6:\"text-4\";}}}}', 'yes'),
(995, 'theme_switched', '', 'yes'),
(997, 'sa_exchangebox_theme_options', 'a:21:{s:7:\"general\";s:0:\"\";s:0:\"\";s:0:\"\";s:4:\"logo\";s:0:\"\";s:9:\"logo_text\";s:0:\"\";s:7:\"favicon\";s:0:\"\";s:9:\"social_on\";s:2:\"on\";s:14:\"vkontakte_link\";s:1:\"#\";s:12:\"twitter_link\";s:1:\"#\";s:13:\"facebook_link\";s:1:\"#\";s:20:\"events_webmoney_link\";s:26:\"http://events.webmoney.ru/\";s:18:\"odnoklassniki_link\";s:1:\"#\";s:16:\"google_plus_link\";s:1:\"#\";s:5:\"style\";s:0:\"\";s:18:\"bg_site_texture_on\";s:0:\"\";s:14:\"body_texture_1\";s:14:\"body_texture_1\";s:8:\"style_on\";s:0:\"\";s:10:\"text_color\";s:6:\"555555\";s:10:\"link_color\";s:6:\"39769c\";s:8:\"browsers\";s:0:\"\";s:7:\"ie_6_on\";s:0:\"\";s:14:\"link_page_ie_6\";s:0:\"\";}', 'yes'),
(1008, 'widget_lt_recent_post', 'a:2:{i:2;a:5:{s:5:\"title\";s:33:\"Последние новости\";s:18:\"category_recent_id\";s:0:\"\";s:6:\"number\";s:1:\"3\";s:9:\"more_link\";s:18:\"/category/novosti/\";s:14:\"more_link_text\";s:27:\"Архив новостей\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(1052, 'widget_fx_browser_plugin', 'a:2:{i:2;a:6:{s:12:\"link_firefox\";s:25:\"/vidzhety-dlya-brauzerov/\";s:11:\"link_chrome\";s:25:\"/vidzhety-dlya-brauzerov/\";s:10:\"link_opera\";s:25:\"/vidzhety-dlya-brauzerov/\";s:18:\"image_link_firefox\";s:59:\"/wp-content/plugins/exchangebox/download/firefox_widget.zip\";s:17:\"image_link_chrome\";s:58:\"/wp-content/plugins/exchangebox/download/chrome_widget.zip\";s:16:\"image_link_opera\";s:57:\"/wp-content/plugins/exchangebox/download/opera_widget.zip\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(2794, 'db_upgraded', '', 'yes'),
(3705, 'link_manager_enabled', '0', 'yes'),
(2813, 'hyper', 'a:9:{s:7:\"comment\";i:1;s:7:\"archive\";i:1;s:7:\"timeout\";i:1440;s:9:\"redirects\";i:1;s:8:\"notfound\";i:1;s:14:\"clean_interval\";i:60;s:4:\"gzip\";i:1;s:16:\"store_compressed\";i:1;s:11:\"expire_type\";s:4:\"post\";}', 'yes'),
(3775, 'first_exb', '1', 'yes'),
(3777, 'page_on_front', '151', 'yes'),
(3778, 'page_for_posts', '155', 'yes'),
(4788, '_site_transient_update_core', 'O:8:\"stdClass\":3:{s:7:\"updates\";a:0:{}s:15:\"version_checked\";s:3:\"5.3\";s:12:\"last_checked\";i:1575493985;}', 'no'),
(4782, 'recovery_keys', 'a:0:{}', 'yes'),
(4784, '_site_transient_browser_2894fb4dbf964f58ccf3d2e4e372b316', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"78.0.3904.108\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(4785, '_site_transient_timeout_php_check_b3655adab148a1a6c8391bd3893ea554', '1576098545', 'no'),
(4786, '_site_transient_php_check_b3655adab148a1a6c8391bd3893ea554', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(4562, 'check_new_user', 'a:5:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";i:3;s:1:\"3\";i:4;s:1:\"4\";}', 'yes'),
(4506, 'curs_parser', 'a:33:{i:8;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"13.4299\";}i:9;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"7446.0718248088\";}i:10;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"30.302\";}i:1;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"64.1948\";}i:2;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"15.577585723454\";}i:3;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"71.1086\";}i:4;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"14.062996599567\";}i:7;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"16.5653\";}i:5;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"268.301\";}i:6;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:12:\"37.271571854\";}i:51;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"1.1391\";}i:52;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:14:\"0.877886050391\";}i:101;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:8:\"2675.521\";}i:102;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"37.375898002669\";}i:103;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:6:\"42.617\";}i:104;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"2346.4814510641\";}i:151;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"387.48\";}i:152;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:14:\"2.580778362754\";}i:153;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"428.98\";}i:154;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:14:\"2.331111007506\";}i:155;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:4:\"6.05\";}i:156;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:15:\"16.528925619835\";}i:201;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"2.1183\";}i:202;a:2:{s:5:\"curs1\";s:2:\"10\";s:5:\"curs2\";s:14:\"4.720766652504\";}i:203;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"2.3464\";}i:204;a:2:{s:5:\"curs1\";s:2:\"10\";s:5:\"curs2\";s:14:\"4.261847937266\";}i:205;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:6:\"3.3008\";}i:11;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"9.08002\";}i:12;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:14:\"0.110131916009\";}i:105;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:4:\"2375\";}i:106;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"42.105263157895\";}i:107;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:4:\"2610\";}i:108;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"38.314176245211\";}}', 'yes'),
(4507, 'time_parser', '1575504514', 'yes'),
(4508, 'the_cron', 'a:9:{s:3:\"now\";i:1549982834;s:4:\"2min\";i:1549982826;s:4:\"5min\";i:1549982605;s:5:\"10min\";i:1549982605;s:5:\"30min\";i:1549982605;s:5:\"1hour\";i:1549982605;s:5:\"3hour\";i:1549982605;s:5:\"05day\";i:1549982605;s:4:\"1day\";i:1549982605;}', 'yes'),
(4510, 'work_parser', 'a:33:{i:1;i:1;i:2;i:1;i:3;i:1;i:4;i:1;i:5;i:1;i:6;i:1;i:7;i:1;i:8;i:1;i:9;i:1;i:10;i:1;i:51;i:1;i:52;i:1;i:101;i:1;i:102;i:1;i:103;i:1;i:104;i:1;i:151;i:1;i:152;i:1;i:153;i:1;i:154;i:1;i:155;i:1;i:156;i:1;i:201;i:1;i:202;i:1;i:203;i:1;i:204;i:1;i:205;i:1;i:11;i:1;i:12;i:1;i:105;i:1;i:106;i:1;i:107;i:1;i:108;i:1;}', 'yes'),
(3816, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(3844, 'lastcursup', '1440431194', 'yes'),
(3847, 'widget_exbobmen_info', 'a:2:{i:2;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(3849, 'widget_exbrezerv_info', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(3863, 'theme_mods_exchangeboxthemegreen', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:8:\"top_menu\";i:3;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1405178142;s:4:\"data\";a:9:{s:19:\"wp_inactive_widgets\";a:2:{i:0;s:15:\"exbobmen_info-3\";i:1;s:16:\"get_statuswork-2\";}s:9:\"sidebar-1\";a:1:{i:0;s:6:\"text-7\";}s:9:\"sidebar-2\";a:1:{i:0;s:6:\"text-6\";}s:9:\"sidebar-3\";a:3:{i:0;s:16:\"exbloginwidget-2\";i:1;s:16:\"exbotzivs_info-2\";i:2;s:11:\"get_rnews-2\";}s:9:\"sidebar-4\";a:1:{i:0;s:6:\"text-4\";}s:9:\"sidebar-5\";a:1:{i:0;s:6:\"text-5\";}s:9:\"sidebar-6\";a:1:{i:0;s:6:\"text-8\";}s:9:\"sidebar-7\";a:1:{i:0;s:6:\"text-9\";}s:9:\"sidebar-8\";a:0:{}}}}', 'yes'),
(4017, 'exch_instruction', 'a:1:{s:7:\"epochta\";s:0:\"\";}', 'yes'),
(4300, 'widget_exblastobmen_info', 'a:4:{i:2;a:0:{}i:3;a:0:{}i:4;a:2:{s:5:\"title\";s:0:\"\";s:5:\"count\";s:1:\"3\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(4301, 'themechange', 'a:24:{s:8:\"logotype\";s:0:\"\";s:7:\"favicon\";s:19:\"/images/favicon.png\";s:5:\"skype\";s:11:\"exchangebox\";s:3:\"icq\";s:0:\"\";s:4:\"mail\";s:16:\"info@exchange.ru\";s:6:\"regrab\";s:96:\"Пн. - Пт. с 10:00 до 23:00 по мск.\r\nСб. - Вск. свободный график.\";s:9:\"hometitle\";s:73:\"Приветствуем на сайте обменного пункта!\";s:8:\"hometext\";s:1708:\"Наш On-line сервис предназначен для тех, кто хочет быстро, безопасно и по выгодному курсу обменять такие виды электронных валют: Webmoney, Perfect Money, LiqPay, Pecunix, Payza, Яндекс. Деньги, Альфа-Банк, ВТБ 24, Приват24, Visa/Master Card, Western uniоn, MoneyGram.\r\n\r\nЭтим возможности нашего сервиса не ограничиваются. В рамках проекта действуют программа лояльности, накопительная скидка и партнерская программа, воспользовавшись преимуществами которых, вы сможете совершать обмен электронных валют на более выгодных условиях. Для этого нужно просто зарегистрироваться на сайте.\r\n\r\nНаш пункт обмена электронных валют – система, созданная на базе современного программного обеспечения и содержащая весь набор необходимых функций для удобной и безопасной конвертации наиболее распространенных видов электронных денег. За время работы мы приобрели репутацию проверенного партнера и делаем все возможное, чтобы ваши впечатления от нашего сервиса были только благоприятными.\";s:8:\"partners\";s:754:\"<div class=\"fwidget\">\r\n<div class=\"fwidgetvn\">\r\n<div class=\"fwtitle\"><div class=\"fwtitlevn\">Партнеры</div></div>\r\n<table class=\"tpartner\">\r\n<tr>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/ya_bottom.png\" alt=\"\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/pm_bottom.png\" alt=\"\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/stp_bottom.png\" alt=\"\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/egopay_bottom.png\" alt=\"\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/bitcoin_bottom.png\" alt=\"\" /></a>\r\n</td>\r\n<td>\r\n<a href=\"#\"><img src=\"/images/payment_icons/okpay_bottom.png\" alt=\"\" /></a>\r\n</td>							\r\n</tr>\r\n</table>\r\n<div class=\"clear\"></div>\r\n</div>\r\n</div>\";s:7:\"footer1\";s:112:\"Обменный пункт электронных валют.</br>\r\nВсе права защищены © 2015.\";s:7:\"footer2\";s:154:\"<a href=\"/preduprezhdenie/\">Предупреждение</a>   •   <a href=\"/tos/\">Правила</a>   •   <a href=\"/sitemap/\">Карта</a>\";s:7:\"footer3\";s:0:\"\";s:10:\"footercode\";s:0:\"\";s:10:\"checktheme\";s:4:\"blue\";s:11:\"hometexttop\";s:0:\"\";s:3:\"tel\";s:17:\"8 (800) 123 45 67\";s:3:\"pwh\";i:1;s:6:\"ptitle\";s:16:\"Партнеры\";s:6:\"footer\";s:68:\"© 2019 Сервис обмена электронных валют.\";s:11:\"regrabtitle\";s:51:\"ВРЕМЯ РАБОТЫ ТЕХ. ПОДДЕРЖКИ:\";s:8:\"telegram\";s:11:\"exchangebox\";s:5:\"viber\";s:0:\"\";s:7:\"whatsup\";s:0:\"\";s:6:\"jabber\";s:0:\"\";}', 'yes'),
(4305, 'exb_version', 'a:2:{s:4:\"text\";s:58:\" Доступно обновление для ExchangeBox \";s:7:\"version\";s:3:\"4.0\";}', 'yes'),
(4347, 'WPLANG', 'ru_RU', 'yes'),
(4364, 'theme_mods_exchangeboxtheme2', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:5:{s:8:\"top_menu\";i:3;s:11:\"bottom_menu\";i:4;s:12:\"the_top_menu\";i:3;s:17:\"the_top_menu_user\";i:3;s:15:\"the_bottom_menu\";i:4;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(4458, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1575493728;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:0:{}}', 'no'),
(4459, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1575493730;s:7:\"checked\";a:1:{s:17:\"exchangeboxtheme2\";s:3:\"7.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(4505, 'lcurs_parser', 'a:33:{i:8;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"13.4196\";}i:9;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"7451.7869385079\";}i:10;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"30.5327\";}i:1;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"65.4072\";}i:2;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"15.288836702993\";}i:3;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"73.4392\";}i:4;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"13.616706064336\";}i:7;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"17.2088\";}i:5;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:7:\"244.423\";}i:6;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:15:\"40.912680066933\";}i:51;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"1.1391\";}i:52;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:14:\"0.877886050391\";}i:101;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:8:\"2675.521\";}i:102;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"37.375898002669\";}i:103;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:6:\"42.617\";}i:104;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"2346.4814510641\";}i:151;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"379.84\";}i:152;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:14:\"2.632687447346\";}i:153;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"426.52\";}i:154;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:14:\"2.344555941105\";}i:155;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:4:\"5.81\";}i:156;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:15:\"17.211703958692\";}i:201;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:6:\"2.1428\";}i:202;a:2:{s:5:\"curs1\";s:2:\"10\";s:5:\"curs2\";s:13:\"4.66679111443\";}i:203;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:5:\"2.407\";}i:204;a:2:{s:5:\"curs1\";s:2:\"10\";s:5:\"curs2\";s:14:\"4.154549231408\";}i:205;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:6:\"3.2787\";}i:11;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:7:\"9.73698\";}i:12;a:2:{s:5:\"curs1\";s:1:\"1\";s:5:\"curs2\";s:14:\"0.102701248231\";}i:105;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:4:\"2670\";}i:106;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"37.453183520599\";}i:107;a:2:{s:5:\"curs1\";s:3:\"100\";s:5:\"curs2\";s:4:\"2985\";}i:108;a:2:{s:5:\"curs1\";s:4:\"1000\";s:5:\"curs2\";s:15:\"33.500837520938\";}}', 'yes'),
(4502, 'the_pages', 'a:22:{s:4:\"home\";i:151;s:7:\"xchange\";i:152;s:12:\"xchangestep2\";i:153;s:12:\"xchangestep3\";i:154;s:4:\"news\";i:155;s:7:\"payouts\";i:156;s:8:\"feedback\";i:157;s:7:\"reviews\";i:158;s:5:\"login\";i:159;s:8:\"register\";i:160;s:8:\"lostpass\";i:161;s:7:\"account\";i:162;s:9:\"userstats\";i:0;s:10:\"usersobmen\";i:164;s:3:\"tos\";i:165;s:11:\"partnersFAQ\";i:166;s:7:\"banners\";i:167;s:12:\"partnerstats\";i:168;s:7:\"sitemap\";i:213;s:6:\"tarifs\";i:214;s:12:\"exchangestep\";i:226;s:19:\"terms_personal_data\";i:302;}', 'yes'),
(4490, 'finished_splitting_shared_terms', '1', 'yes'),
(4625, '_transient_timeout_feed_d117b5738fbd35bd8c0391cda1f2b5d9', '1447395206', 'no'),
(4683, 'lexb_pages', 'a:0:{}', 'yes'),
(4684, 'fresh_site', '0', 'yes');
INSERT INTO `eb_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(4685, 'usve_pages', 'a:1:{s:10:\"userverify\";i:284;}', 'yes'),
(4686, 'widget_get_userverify', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4687, 'bexb_pages', 'a:0:{}', 'yes'),
(4643, 'widget_exbregisterwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4644, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4645, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4646, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4647, 'site_icon', '0', 'yes'),
(4648, 'medium_large_size_w', '768', 'yes'),
(4649, 'medium_large_size_h', '0', 'yes'),
(4688, 'widget_bexbcurs_info', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4690, 'widget_get_investbox_menu_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4672, 'merch_data', 'a:0:{}', 'yes'),
(4679, 'pn_extended', 'a:2:{s:6:\"moduls\";a:19:{s:12:\"admincaptcha\";s:12:\"admincaptcha\";s:7:\"captcha\";s:7:\"captcha\";s:4:\"cexp\";s:4:\"cexp\";s:6:\"cutnum\";s:6:\"cutnum\";s:9:\"discounts\";s:9:\"discounts\";s:3:\"fav\";s:3:\"fav\";s:8:\"finstats\";s:8:\"finstats\";s:7:\"htmlmap\";s:7:\"htmlmap\";s:8:\"mailsmtp\";s:8:\"mailsmtp\";s:9:\"mailtemps\";s:9:\"mailtemps\";s:10:\"masschange\";s:10:\"masschange\";s:9:\"mywarning\";s:9:\"mywarning\";s:9:\"napsfiles\";s:9:\"napsfiles\";s:13:\"notice_header\";s:13:\"notice_header\";s:7:\"numsymb\";s:7:\"numsymb\";s:15:\"parser_settings\";s:15:\"parser_settings\";s:8:\"partners\";s:8:\"partners\";s:7:\"reviews\";s:7:\"reviews\";s:8:\"setbidid\";s:8:\"setbidid\";}s:9:\"merchants\";a:0:{}}', 'yes'),
(4689, 'inex_pages', 'a:2:{s:8:\"toinvest\";i:285;s:9:\"indeposit\";i:286;}', 'yes'),
(4682, 'bcoip_pages', 'a:0:{}', 'yes'),
(4671, 'merchants', 'a:1:{s:4:\"qiwi\";i:0;}', 'yes'),
(4699, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4700, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4701, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4702, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4749, 'widget_get_pn_news', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4750, 'pn_cron', 'a:3:{s:12:\"update_times\";a:1:{s:4:\"site\";a:9:{s:3:\"now\";i:1575504789;s:4:\"2min\";i:1575504760;s:4:\"5min\";i:1575504514;s:5:\"10min\";i:1575504514;s:5:\"30min\";i:1575504514;s:5:\"1hour\";i:1575504514;s:5:\"3hour\";i:1575504514;s:5:\"05day\";i:1575504514;s:4:\"1day\";i:1575504514;}}s:4:\"site\";a:12:{s:11:\"acp_del_img\";a:2:{s:11:\"last_update\";i:1575504514;s:9:\"work_time\";s:5:\"10min\";}s:16:\"delete_auto_bids\";a:2:{s:11:\"last_update\";i:1575504789;s:9:\"work_time\";s:3:\"now\";}s:18:\"delete_notpay_bids\";a:2:{s:11:\"last_update\";i:1575504789;s:9:\"work_time\";s:3:\"now\";}s:15:\"captcha_del_img\";a:2:{s:11:\"last_update\";i:1575504514;s:9:\"work_time\";s:5:\"10min\";}s:18:\"parser_upload_data\";a:2:{s:11:\"last_update\";i:1575504514;s:9:\"work_time\";s:5:\"1hour\";}s:12:\"del_autologs\";a:2:{s:11:\"last_update\";i:1575504514;s:9:\"work_time\";s:4:\"1day\";}s:14:\"archive_plinks\";a:2:{s:11:\"last_update\";i:1575504514;s:9:\"work_time\";s:4:\"1day\";}s:23:\"delete_auto_notice_head\";a:2:{s:11:\"last_update\";i:1575504514;s:9:\"work_time\";s:4:\"1day\";}s:20:\"delete_auto_partners\";a:2:{s:11:\"last_update\";i:1575504514;s:9:\"work_time\";s:4:\"1day\";}s:19:\"delete_auto_reviews\";a:2:{s:11:\"last_update\";i:1575504514;s:9:\"work_time\";s:4:\"1day\";}s:16:\"exchangebox_chkv\";a:2:{s:11:\"last_update\";i:1575504514;s:9:\"work_time\";s:5:\"3hour\";}s:0:\"\";a:1:{s:9:\"work_time\";s:0:\"\";}}s:4:\"file\";a:12:{s:0:\"\";a:1:{s:9:\"work_time\";s:0:\"\";}s:12:\"del_autologs\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:14:\"archive_plinks\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:16:\"delete_auto_bids\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:18:\"delete_notpay_bids\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:11:\"acp_del_img\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:15:\"captcha_del_img\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:23:\"delete_auto_notice_head\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:18:\"parser_upload_data\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:20:\"delete_auto_partners\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:19:\"delete_auto_reviews\";a:1:{s:9:\"work_time\";s:4:\"none\";}s:16:\"exchangebox_chkv\";a:1:{s:9:\"work_time\";s:4:\"none\";}}}', 'yes'),
(4780, '_site_transient_timeout_theme_roots', '1575495529', 'no'),
(4781, '_site_transient_theme_roots', 'a:1:{s:17:\"exchangeboxtheme2\";s:7:\"/themes\";}', 'no'),
(4745, 'wp_page_for_privacy_policy', '0', 'yes'),
(4746, 'show_comments_cookies_opt_in', '0', 'yes'),
(4751, 'widget_get_pn_cbr', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4752, 'widget_get_pn_reviews', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:5:\"count\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(4753, 'pn_notify', 'a:1:{s:5:\"email\";a:30:{s:6:\"payout\";a:6:{s:4:\"mail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:85:\"Заказан вывод средств в партнерской программе\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:139:\"Пользователь [name] заказ выплату партнерского вознаграждения на сумму [summ] USD.\";s:6:\"tomail\";s:0:\"\";}s:7:\"zreserv\";a:6:{s:4:\"mail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:29:\"Резерв доступен\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:210:\"На сайте [sitename] вы оставляли запрос на резерв в размере [sum] [valut]. В данный момент доступен резерв в размере [summrez] [valut].\";s:6:\"tomail\";s:0:\"\";}s:9:\"new_bids1\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:33:\"Заявка на обмен [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:582:\"<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br>\r\nСсылка на заявку: [bidurl] <br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:12:\"delete_bids1\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:32:\"Удалена заявка [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:582:\"<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br>\r\nСсылка на заявку: [bidurl] <br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:14:\"returned_bids1\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:52:\"Возврат средств по заявке [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:582:\"<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br>\r\nСсылка на заявку: [bidurl] <br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:12:\"verify_bids1\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:54:\"Оплата заявки [id] (на проверке)\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:582:\"<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br>\r\nСсылка на заявку: [bidurl] <br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:11:\"error_bids1\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:33:\"Ошибка в заявке [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:582:\"<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br>\r\nСсылка на заявку: [bidurl] <br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:13:\"success_bids1\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:40:\"Выполненная заявка [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:582:\"<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br>\r\nСсылка на заявку: [bidurl] <br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:16:\"realdelete_bids1\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:51:\"Полностью удалена заявка [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:582:\"<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br>\r\nСсылка на заявку: [bidurl] <br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:9:\"newreview\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:21:\"Новый отзыв\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:171:\"Добавлен новый отзыв пользователем [user]. <br>\r\nСтатус отзыва: [status].<br>\r\nУправление отзывом: [management]\";}s:11:\"contactform\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:27:\"Обратная связь\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:215:\"Поступил запрос от пользователя через форму обратной связи<br>\r\nИмя: [name]<br>\r\nE-mai:l [email]<br>\r\nID обмена: [idz]<br>\r\nСообщение:<br>\r\n[text]\";}s:13:\"confirmreview\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:33:\"Подтвердите отзыв\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:90:\"Для подтверждения отзывы перейдите по ссылке [link]\";}s:10:\"partprofit\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:70:\"Начислено партнерское вознаграждение\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:148:\"На сайте [sitename] вам было начислено партнерского вознаграждение в размере [sum] [ctype].\";}s:12:\"lostpassform\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:41:\"Восстановление пароля\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:92:\"Для восстановления пароля перейдите по ссылке [link]\";}s:12:\"registerform\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:39:\"Успешная регистрация\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:181:\"Вы успешно зарегистрировались на сайте [sitename].<br>\r\nВаш логин: [login]<br>\r\nВаш пароль: [pass]<br>\r\nВаш email:  [email]<br>\";}s:9:\"new_bids2\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:33:\"Заявка на обмен [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:680:\"Здравствуйте.<br><br>\r\n\r\nСтатус заявки: новая заявка<br>\r\nСсылка на заявку: [bidurl]<br><br>\r\n\r\n<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:13:\"realpay_bids1\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:47:\"Оплата заявки [id] (мерчант)\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:582:\"<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br>\r\nСсылка на заявку: [bidurl] <br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:11:\"payed_bids1\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:47:\"Оплата заявки [id] (вручную)\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:582:\"<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br>\r\nСсылка на заявку: [bidurl] <br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:13:\"realpay_bids2\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:30:\"Оплата заявки [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:765:\"Здравствуйте.<br><br>\r\n\r\nСтатус заявки: уведомление об оплате получено, обрабатывается оператором<br>\r\nСсылка на заявку: [bidurl]<br><br>\r\n\r\n<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:11:\"payed_bids2\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:30:\"Оплата заявки [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:765:\"Здравствуйте.<br><br>\r\n\r\nСтатус заявки: уведомление об оплате получено, обрабатывается оператором<br>\r\nСсылка на заявку: [bidurl]<br><br>\r\n\r\n<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:12:\"verify_bids2\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:30:\"Оплата заявки [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:765:\"Здравствуйте.<br><br>\r\n\r\nСтатус заявки: уведомление об оплате получено, обрабатывается оператором<br>\r\nСсылка на заявку: [bidurl]<br><br>\r\n\r\n<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:12:\"delete_bids2\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:32:\"Удалена заявка [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:671:\"Здравствуйте.<br><br>\r\n\r\nСтатус заявки: удалена<br>\r\nСсылка на заявку: [bidurl]<br><br>\r\n\r\n<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:14:\"returned_bids2\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:52:\"Возврат средств по заявке [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:767:\"Здравствуйте.<br><br>\r\n\r\nСтатус заявки: оплаченные средства возвращены обратно на счет плательщика<br>\r\nСсылка на заявку: [bidurl]<br><br>\r\n\r\n<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:11:\"error_bids2\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:33:\"Ошибка в заявке [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:763:\"Здравствуйте.<br><br>\r\n\r\nСтатус заявки: ошибка в заявке, свяжитесь с нашей технической поддержкой<br>\r\nСсылка на заявку: [bidurl]<br><br>\r\n\r\n<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:13:\"success_bids2\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:40:\"Выполненная заявка [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:704:\"Здравствуйте.<br><br>\r\n\r\nСтатус заявки: выполнена в полном объеме<br>\r\nСсылка на заявку: [bidurl]<br><br>\r\n\r\n<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:16:\"realdelete_bids2\";a:6:{s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"send\";i:1;s:5:\"title\";s:51:\"Полностью удалена заявка [id]\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:659:\"Здравствуйте.<br><br>\r\n\r\nСтатус заявки: заявка удалена безвозвратно<br>\r\n\r\n<strong>Информация о заявке</strong><br>\r\nID [id] от [createdate]<br>\r\nКурс обмена: <strong>[curs1] [valut1] [vtype1] -> [curs2] [valut2] [vtype2]</strong><br>\r\nСумма обмена: <strong>[summ1] [valut1] [vtype1] со счета [account1] -> [summ2c] [valut2] [vtype2] на счет [account2]</strong><br><br>\r\n\r\n<strong>Информация о клиенте</strong><br>\r\n[last_name] [first_name] [second_name]<br>\r\nТелефон [user_phone]<br>\r\nEmail [user_email]<br>\r\nSkype [user_skype]<br>\";}s:5:\"alogs\";a:6:{s:4:\"send\";i:1;s:5:\"title\";s:67:\"Уведомление о входе в личный кабинет\";s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:124:\"[date] совершен вход в личный кабинет с IP адреса [ip] через браузер [browser].\";}s:10:\"letterauth\";a:6:{s:4:\"send\";i:1;s:5:\"title\";s:49:\"Подтверждение авторизации\";s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:73:\"Для авторизации перейдите по ссылке [link]\";}s:14:\"newreview_auto\";a:6:{s:4:\"send\";i:0;s:5:\"title\";s:0:\"\";s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:0:\"\";}s:16:\"contactform_auto\";a:6:{s:4:\"send\";i:0;s:5:\"title\";s:0:\"\";s:4:\"mail\";s:0:\"\";s:6:\"tomail\";s:0:\"\";s:4:\"name\";s:0:\"\";s:4:\"text\";s:0:\"\";}}}', 'yes'),
(4718, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(4778, 'pn_version', 'a:3:{s:4:\"text\";s:200:\"Вы используете устаревшую версию скрипта. Выполните обновление по инструкции https://best-curs.info/upinstruction/plugin-exchangebox/\";s:7:\"version\";s:3:\"7.0\";s:4:\"news\";a:0:{}}', 'yes'),
(4783, '_site_transient_timeout_browser_2894fb4dbf964f58ccf3d2e4e372b316', '1576098544', 'no'),
(4787, 'admin_email_lifespan', '1591045963', 'yes'),
(4789, 'can_compress_scripts', '1', 'no');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_partners`
--

CREATE TABLE `eb_partners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` tinytext NOT NULL,
  `link` tinytext NOT NULL,
  `img` longtext NOT NULL,
  `rsort` bigint(20) NOT NULL DEFAULT 0,
  `site_order` bigint(20) NOT NULL DEFAULT 0,
  `status` int(1) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL,
  `auto_status` int(1) NOT NULL DEFAULT 1,
  `edit_user_id` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_partners`
--

INSERT INTO `eb_partners` (`id`, `title`, `link`, `img`, `rsort`, `site_order`, `status`, `create_date`, `edit_date`, `auto_status`, `edit_user_id`) VALUES
(6, 'Bestchange.ru', 'https://bestchange.ru', '/wp-content/uploads/bestchange.gif', 0, 0, 0, '2019-02-12 16:43:27', '2019-02-12 16:49:47', 1, 0),
(7, 'Okchanger.ru', 'https://www.okchanger.ru/', '/wp-content/uploads/okchanger.png', 0, 0, 0, '2019-02-12 16:44:07', '2019-02-12 16:49:42', 1, 0),
(8, 'Kurs.expert', 'https://kurs.expert', '/wp-content/uploads/kursexpert.png', 0, 0, 0, '2019-02-12 16:45:19', '2019-02-12 16:49:33', 1, 0),
(9, 'Glazok.org', 'https://glazok.org', '/wp-content/uploads/glazok.gif', 0, 0, 0, '2019-02-12 16:46:31', '2019-02-12 16:49:28', 1, 0),
(10, 'Kurses.com.ua', 'http://kurses.com.ua/', '/wp-content/uploads/kurses.gif', 0, 0, 0, '2019-02-12 16:47:10', '2019-02-12 16:49:22', 1, 0),
(11, 'Webmoney.ru', 'https://webmoney.ru', '/wp-content/uploads/webmoney-1.png', 0, 0, 0, '2019-02-12 16:48:48', '2019-02-12 16:48:48', 1, 0),
(12, 'Bestexchangers.ru', 'https://bestexchangers.ru', '/wp-content/uploads/bestexchangers.gif', 0, 0, 0, '2019-02-12 16:51:33', '2019-02-12 16:51:33', 1, 0),
(13, 'Good-kurs.ru', 'https://good-kurs.ru', '/wp-content/uploads/goodkurs.gif', 0, 0, 0, '2019-02-12 16:53:55', '2019-02-12 16:53:55', 1, 0),
(14, 'Monitorkursov.com', 'http://monitorkursov.com', '/wp-content/uploads/monitorkursov.gif', 0, 0, 0, '2019-02-12 16:56:37', '2019-02-12 16:56:37', 1, 0),
(15, 'Obmenvse.ru', 'http://obmenvse.ru', '/wp-content/uploads/obmenvse.gif', 0, 0, 0, '2019-02-12 16:57:11', '2019-02-12 16:57:11', 1, 0),
(16, 'Okku.ru', 'https://okku.ru/', 'http://exchange.best-curs.info/wp-content/uploads/okku.gif', 0, 0, 0, '2019-02-12 16:57:52', '2019-02-12 16:57:52', 1, 0),
(17, 'Pro-obmen.ru', 'https://pro-obmen.ru', '/wp-content/uploads/proobmen.gif', 0, 0, 0, '2019-02-12 16:58:31', '2019-02-12 16:58:31', 1, 0),
(18, 'Scanmoney.net', 'http://scanmoney.net', '/wp-content/uploads/scanmoney.png', 0, 0, 0, '2019-02-12 16:59:25', '2019-02-12 16:59:25', 1, 0),
(19, 'Secretovobmena.net', 'https://www.secretovobmena.net', '/wp-content/uploads/secretovobmena.gif', 0, 0, 0, '2019-02-12 17:00:46', '2019-02-12 17:00:46', 1, 0),
(20, 'Udifo.com', 'https://udifo.com', '/wp-content/uploads/udifo.png', 0, 0, 0, '2019-02-12 17:02:15', '2019-02-12 17:02:15', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_partner_pers`
--

CREATE TABLE `eb_partner_pers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sumec` varchar(50) NOT NULL DEFAULT '0',
  `pers` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_partner_pers`
--

INSERT INTO `eb_partner_pers` (`id`, `sumec`, `pers`) VALUES
(1, '0', '0.1'),
(2, '500', '0.2'),
(3, '5000', '0.3'),
(4, '10000', '0.4'),
(5, '15000', '0.5');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_payoutuser`
--

CREATE TABLE `eb_payoutuser` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bdate` datetime NOT NULL,
  `userid` bigint(20) NOT NULL DEFAULT 0,
  `bsumm` varchar(250) NOT NULL DEFAULT '0',
  `bstatus` int(1) NOT NULL DEFAULT 0,
  `bcomment` longtext NOT NULL,
  `valuts` varchar(250) NOT NULL,
  `schet` varchar(250) NOT NULL,
  `valsid` bigint(20) NOT NULL DEFAULT 0,
  `user_login` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_plinks`
--

CREATE TABLE `eb_plinks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `user_login` varchar(250) NOT NULL,
  `pdate` datetime NOT NULL,
  `pbrowser` longtext NOT NULL,
  `pip` longtext NOT NULL,
  `prefer` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_pn_options`
--

CREATE TABLE `eb_pn_options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `meta_key2` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_pn_options`
--

INSERT INTO `eb_pn_options` (`id`, `meta_key`, `meta_key2`, `meta_value`) VALUES
(1, 'up_mode', '', '0'),
(2, 'admin_panel_url', '', 'xchngbx'),
(3, 'reviews', 'count', '20'),
(4, 'reviews', 'method', 'moderation'),
(5, 'reviews', 'website', '0'),
(6, 'captcha', 'reviewsform', '0'),
(7, 'captcha', 'contactform', '0'),
(8, 'captcha', 'loginform', '0'),
(9, 'captcha', 'registerform', '0'),
(10, 'partners', 'status', '1'),
(11, 'partners', 'minpay', '5'),
(12, 'exchange', 'courez', '4'),
(13, 'exchange', 'techreg', '0'),
(14, 'exchange', 'techregtext', ''),
(15, 'naps_temp', 'timeline_txt', 'Данная операция производится оператором в ручном режиме и занимает от 5 до 30 минут в рабочее время (см. статус оператора).'),
(16, 'naps_temp', 'description_txt', 'Для обмена Вам необходимо выполнить несколько шагов:\r\n\r\nЗаполните все поля представленной формы. Нажмите кнопку «Продолжить».\r\nОзнакомьтесь с условиями договора на оказание услуг обмена, если вы принимаете их, поставьте галочку в соответствующем поле/нажмите кнопку «Принимаю» («Согласен»). Еще раз проверьте данные заявки.\r\n3. Оплатите заявку.  Для этого следует совершить перевод необходимой суммы, следуя инструкциям на нашем сайте.\r\n4. После выполнения указанных действий, система переместит Вас на страницу «Состояние заявки», где будет указан статус вашего перевода.\r\n\r\n<strong>Внимание</strong>: для выполнения данной операции потребуется участие оператора (см. статус оператора).'),
(17, 'naps_temp', 'status_new', 'Авторизуйтесь в платежной системе XXXXXXX;\r\nПереведите указанную ниже сумму на кошелек XXXXXXX;\r\nНажмите на кнопку \"Я оплатил заявку\";\r\nОжидайте обработку заявки оператором.'),
(18, 'naps_temp', 'status_payed', 'Подтверждение оплаты принято.\r\nВаша заявка обрабатывается оператором.'),
(19, 'naps_temp', 'status_success', 'Ваша заявка выполнена.\r\nБлагодарим за то, что воспользовались услугами нашего сервиса.\r\nОставьте, пожалуйста, <a href=\"/reviews/\">отзыв</a> о работе нашего сервиса!'),
(20, 'naps_temp', 'status_new_mobile', '<ol>\r\n	<li>Авторизуйтесь в платежной системе или запустите мобильное приложение XXXXXXX;</li>\r\n	<li>Переведите указанную ниже сумму на кошелек XXXXXXX;</li>\r\n	<li>Нажмите на кнопку \"Я оплатил заявку\";</li>\r\n	<li>Ожидайте обработку заявки оператором.</li>\r\n</ol>'),
(21, 'exchange', 'autodelete', '0'),
(22, 'exchange', 'ad_h', '0'),
(23, 'exchange', 'ad_m', '15'),
(24, 'cron', '', '0'),
(25, 'admincaptcha', '', '1'),
(26, 'letter_auth', '', '1'),
(27, 'naps_temp', 'status_realpay', 'Подтверждение оплаты принято.\r\nВаша заявка обрабатывается оператором.'),
(28, 'naps_temp', 'status_verify', 'Подтверждение оплаты принято.\r\nВаша заявка обрабатывается оператором.'),
(29, 'naps_temp', 'status_returned', 'Оплата по заявке была возвращена на ваш кошелек.'),
(30, 'naps_temp', 'status_delete', 'Заявка была удалена.'),
(31, 'naps_temp', 'status_error', 'В заявке есть ошибки. Обратитесь в техническую поддержку.'),
(32, 'txtxml', 'txt', '1'),
(33, 'txtxml', 'xml', '1'),
(34, 'txtxml', 'numtxt', '5'),
(35, 'txtxml', 'numxml', '5'),
(36, 'favicon', '', '/wp-content/uploads/favicon.png'),
(37, 'logo', '', ''),
(38, 'textlogo', '', ''),
(39, 'exchange', 'gostnaphide', '0'),
(40, 'exchange', 'an1_hidden', '0'),
(41, 'exchange', 'an2_hidden', '0'),
(42, 'exchange', 'an3_hidden', '0'),
(43, 'exchange', 'admin_mail', '0'),
(44, 'numsybm_count', '', '12'),
(45, 'user_fields', '', 'a:8:{s:5:\"login\";i:1;s:9:\"last_name\";i:1;s:10:\"first_name\";i:1;s:11:\"second_name\";i:1;s:10:\"user_phone\";i:1;s:10:\"user_skype\";i:1;s:7:\"website\";i:1;s:13:\"user_passport\";i:1;}'),
(46, 'user_fields_change', '', 'a:8:{s:10:\"user_email\";i:1;s:9:\"last_name\";i:1;s:10:\"first_name\";i:1;s:11:\"second_name\";i:1;s:10:\"user_phone\";i:1;s:10:\"user_skype\";i:1;s:7:\"website\";i:1;s:13:\"user_passport\";i:1;}'),
(47, 'persdata', 'last_name', '1'),
(48, 'persdata', 'first_name', '1'),
(49, 'persdata', 'second_name', '1'),
(50, 'persdata', 'user_phone', '1'),
(51, 'help', 'last_name', 'Введите фамилию, как у вас в паспорте.'),
(52, 'help', 'first_name', 'Введите имя, как у вас в паспорте.'),
(53, 'help', 'second_name', 'Введите отчество, как у вас в паспорте.'),
(54, 'help', 'user_email', 'Пожалуйста, укажите действующий адрес электронный почты.'),
(55, 'help', 'user_phone', 'Введите номер мобильного телефона в международном формате для связи с вами. Пример: +71234567890.'),
(56, 'help', 'user_skype', 'Введите логин Skype для связи с вами.'),
(57, 'persdata', 'user_skype', '0'),
(58, 'persdata', 'user_passport', '0'),
(59, 'exchange', 'adjust', '0'),
(60, 'exchange', 'beautynum', '0'),
(61, 'exchange', 'maxsymb_all', '4'),
(62, 'exchange', 'maxsymb_reserv', '4'),
(63, 'exchange', 'maxsymb_course', '4'),
(64, 'help', 'user_passport', 'Введите номер паспорта.'),
(65, 'htmlmap', 'exclude_page', 'a:0:{}'),
(66, 'htmlmap', 'exchanges', '1'),
(67, 'htmlmap', 'pages', '1'),
(68, 'htmlmap', 'news', '1'),
(69, 'xmlmap', 'exclude_page', 'a:0:{}'),
(70, 'xmlmap', 'exchanges', '1'),
(71, 'xmlmap', 'pages', '1'),
(72, 'xmlmap', 'news', '1'),
(73, 'captcha', 'lostpass1form', '0'),
(74, 'captcha', 'reservform', '0'),
(75, 'captcha', 'exchangeform', '0'),
(76, 'partners', 'wref', '0'),
(77, 'partners', 'payouttext', ''),
(78, 'partners', 'clife', '365'),
(79, 'partners', 'text_banners', '0'),
(80, 'partners', 'calc', '0'),
(81, 'partners', 'reserv', '0'),
(82, 'seo', 'home_title', ''),
(83, 'seo', 'home_key', ''),
(84, 'seo', 'home_descr', ''),
(85, 'seo', 'news_title', ''),
(86, 'seo', 'news_key', ''),
(87, 'seo', 'news_descr', ''),
(88, 'seo', 'news_temp', ''),
(89, 'seo', 'page_temp', ''),
(90, 'seo', 'exch_temp', ''),
(91, 'txtxml', 'decimal', '4'),
(92, 'admin', 'w0', '0'),
(93, 'admin', 'w1', '0'),
(94, 'admin', 'w2', '0'),
(95, 'admin', 'w3', '0'),
(96, 'admin', 'w4', '0'),
(97, 'admin', 'w5', '0'),
(98, 'admin', 'w6', '0'),
(99, 'admin', 'w7', '0'),
(100, 'admin', 'w8', '0'),
(101, 'admin', 'ws0', '0'),
(102, 'admin', 'ws1', '0'),
(103, 'admin', 'wm0', '1'),
(104, 'admin', 'wm1', '1'),
(105, 'admin', 'wm2', '0'),
(106, 'iconbar', 'pp_user_payouts', '1'),
(107, 'iconbar', 'zreserv', '1'),
(108, 'iconbar', 'reviews', '1'),
(109, 'admin_panel_captcha', '', '1'),
(110, 'site_captcha', '', '1');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_postmeta`
--

CREATE TABLE `eb_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_postmeta`
--

INSERT INTO `eb_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(299, 171, '_menu_item_menu_item_parent', '0'),
(300, 171, '_menu_item_object_id', '171'),
(301, 171, '_menu_item_object', 'custom'),
(302, 171, '_menu_item_target', ''),
(275, 151, '_wp_page_template', 'exb-pluginpage.php'),
(276, 152, '_wp_page_template', 'exb-pluginpage.php'),
(431, 153, '_wp_page_template', 'exb-pluginpage.php'),
(279, 156, '_wp_page_template', 'exb-pluginpage.php'),
(280, 169, '_menu_item_type', 'post_type'),
(281, 169, '_menu_item_menu_item_parent', '0'),
(282, 169, '_menu_item_object_id', '157'),
(283, 169, '_menu_item_object', 'page'),
(309, 160, '_edit_lock', '1445589960:1'),
(310, 173, '_edit_last', '1'),
(311, 173, '_edit_lock', '1445542932:1'),
(312, 173, '_wp_page_template', 'default'),
(322, 166, '_edit_lock', '1445542831:1'),
(323, 166, '_edit_last', '1'),
(324, 166, '_wp_page_template', 'default'),
(325, 160, '_edit_last', '1'),
(326, 160, '_wp_page_template', 'default'),
(327, 188, '_menu_item_type', 'post_type'),
(328, 188, '_menu_item_menu_item_parent', '171'),
(329, 188, '_menu_item_object_id', '160'),
(330, 188, '_menu_item_object', 'page'),
(331, 188, '_menu_item_target', ''),
(332, 188, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(333, 188, '_menu_item_xfn', ''),
(334, 188, '_menu_item_url', ''),
(336, 189, '_menu_item_type', 'post_type'),
(337, 189, '_menu_item_menu_item_parent', '171'),
(338, 189, '_menu_item_object_id', '159'),
(339, 189, '_menu_item_object', 'page'),
(340, 189, '_menu_item_target', ''),
(341, 189, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(342, 189, '_menu_item_xfn', ''),
(343, 189, '_menu_item_url', ''),
(345, 159, '_edit_lock', '1367062872:1'),
(346, 159, '_edit_last', '1'),
(347, 159, '_wp_page_template', 'default'),
(348, 165, '_edit_lock', '1445542886:1'),
(349, 165, '_edit_last', '1'),
(350, 165, '_wp_page_template', 'default'),
(351, 194, '_menu_item_type', 'post_type'),
(352, 194, '_menu_item_menu_item_parent', '0'),
(353, 194, '_menu_item_object_id', '165'),
(354, 194, '_menu_item_object', 'page'),
(355, 194, '_menu_item_target', ''),
(356, 194, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(357, 194, '_menu_item_xfn', ''),
(358, 194, '_menu_item_url', ''),
(305, 171, '_menu_item_url', '#'),
(296, 170, '_menu_item_url', ''),
(295, 170, '_menu_item_xfn', ''),
(294, 170, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(293, 170, '_menu_item_target', ''),
(292, 170, '_menu_item_object', 'page'),
(291, 170, '_menu_item_object_id', '158'),
(290, 170, '_menu_item_menu_item_parent', '0'),
(289, 170, '_menu_item_type', 'post_type'),
(298, 171, '_menu_item_type', 'custom'),
(287, 169, '_menu_item_url', ''),
(284, 169, '_menu_item_target', ''),
(285, 169, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(286, 169, '_menu_item_xfn', ''),
(215, 130, '_menu_item_object', 'custom'),
(212, 130, '_menu_item_type', 'custom'),
(213, 130, '_menu_item_menu_item_parent', '0'),
(214, 130, '_menu_item_object_id', '130'),
(304, 171, '_menu_item_xfn', ''),
(303, 171, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(219, 130, '_menu_item_url', '/'),
(216, 130, '_menu_item_target', ''),
(217, 130, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(218, 130, '_menu_item_xfn', ''),
(369, 155, '_edit_lock', '1367132165:1'),
(372, 213, '_wp_page_template', 'exb-pluginpage.php'),
(373, 215, '_menu_item_type', 'post_type'),
(374, 215, '_menu_item_menu_item_parent', '0'),
(375, 215, '_menu_item_object_id', '214'),
(376, 215, '_menu_item_object', 'page'),
(377, 215, '_menu_item_target', ''),
(378, 215, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(379, 215, '_menu_item_xfn', ''),
(380, 215, '_menu_item_url', ''),
(384, 164, '_edit_lock', '1368981983:1'),
(385, 157, '_wp_page_template', 'default'),
(386, 158, '_wp_page_template', 'exb-pluginpage.php'),
(387, 162, '_wp_page_template', 'exb-pluginpage.php'),
(388, 164, '_wp_page_template', 'exb-pluginpage.php'),
(389, 167, '_wp_page_template', 'exb-pluginpage.php'),
(390, 168, '_wp_page_template', 'exb-pluginpage.php'),
(391, 214, '_wp_page_template', 'exb-pluginpage.php'),
(392, 157, '_edit_lock', '1440425452:1'),
(393, 157, '_edit_last', '1'),
(394, 220, '_menu_item_type', 'post_type'),
(395, 220, '_menu_item_menu_item_parent', '0'),
(396, 220, '_menu_item_object_id', '213'),
(397, 220, '_menu_item_object', 'page'),
(398, 220, '_menu_item_target', ''),
(399, 220, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(400, 220, '_menu_item_xfn', ''),
(401, 220, '_menu_item_url', ''),
(403, 221, '_menu_item_type', 'post_type'),
(404, 221, '_menu_item_menu_item_parent', '0'),
(405, 221, '_menu_item_object_id', '173'),
(406, 221, '_menu_item_object', 'page'),
(407, 221, '_menu_item_target', ''),
(408, 221, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(409, 221, '_menu_item_xfn', ''),
(410, 221, '_menu_item_url', ''),
(412, 222, '_menu_item_type', 'post_type'),
(413, 222, '_menu_item_menu_item_parent', '0'),
(414, 222, '_menu_item_object_id', '165'),
(415, 222, '_menu_item_object', 'page'),
(416, 222, '_menu_item_target', ''),
(417, 222, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(418, 222, '_menu_item_xfn', ''),
(419, 222, '_menu_item_url', ''),
(423, 226, '_wp_page_template', 'exb-pluginpage.php'),
(424, 151, '_edit_lock', '1445542842:1'),
(432, 154, '_wp_page_template', 'exb-pluginpage.php'),
(429, 226, '_edit_lock', '1447418187:1'),
(430, 226, '_edit_last', '1'),
(433, 237, '_wp_attached_file', 'Advcash.png'),
(434, 237, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"Advcash.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(435, 238, '_wp_attached_file', 'Alfabank.png'),
(436, 238, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Alfabank.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(437, 239, '_wp_attached_file', 'Alipay.png'),
(438, 239, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Alipay.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(439, 240, '_wp_attached_file', 'Avangardbank.png'),
(440, 240, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:16:\"Avangardbank.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(441, 241, '_wp_attached_file', 'Bank-perevod.png'),
(442, 241, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:16:\"Bank-perevod.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(443, 242, '_wp_attached_file', 'Bitcoin.png'),
(444, 242, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"Bitcoin.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(544, 297, '_wp_attached_file', 'ether.png'),
(545, 297, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"ether.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(447, 244, '_wp_attached_file', 'Cash-EUR.png'),
(448, 244, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Cash-EUR.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(449, 245, '_wp_attached_file', 'Cash-RUB.png'),
(450, 245, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Cash-RUB.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(451, 246, '_wp_attached_file', 'Cash-USD.png'),
(452, 246, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Cash-USD.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(567, 310, '_wp_attached_file', 'Stellar.png'),
(568, 310, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"Stellar.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(455, 248, '_wp_attached_file', 'exmo.png'),
(456, 248, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:8:\"exmo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(536, 293, '_wp_attached_file', 'ripple.png'),
(537, 293, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"ripple.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(459, 250, '_wp_attached_file', 'Liqpay.png'),
(460, 250, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Liqpay.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(461, 251, '_wp_attached_file', 'Litecoin.png'),
(462, 251, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Litecoin.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(463, 252, '_wp_attached_file', 'Livecoin.png'),
(464, 252, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Livecoin.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(465, 253, '_wp_attached_file', 'NixMoney.png'),
(466, 253, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"NixMoney.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(540, 295, '_wp_attached_file', 'wawes.png'),
(541, 295, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"wawes.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(542, 296, '_wp_attached_file', 'monero.png'),
(543, 296, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"monero.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(471, 256, '_wp_attached_file', 'Paxum.png'),
(472, 256, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"Paxum.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(473, 257, '_wp_attached_file', 'Payeer.png'),
(474, 257, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Payeer.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(475, 258, '_wp_attached_file', 'Paymer.png'),
(476, 258, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Paymer.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(477, 259, '_wp_attached_file', 'Paypal.png'),
(478, 259, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Paypal.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(479, 260, '_wp_attached_file', 'Payza.png'),
(480, 260, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"Payza.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(481, 261, '_wp_attached_file', 'Perfect-Money.png'),
(482, 261, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:17:\"Perfect-Money.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(483, 262, '_wp_attached_file', 'PMe-voucher.png'),
(484, 262, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:15:\"PMe-voucher.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(485, 263, '_wp_attached_file', 'Privatbank.png'),
(486, 263, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:14:\"Privatbank.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(487, 264, '_wp_attached_file', 'Promsvyazbank.png'),
(488, 264, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:17:\"Promsvyazbank.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(489, 265, '_wp_attached_file', 'Qiwi.png'),
(490, 265, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:8:\"Qiwi.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(491, 266, '_wp_attached_file', 'Russstandart.png'),
(492, 266, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:16:\"Russstandart.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(493, 267, '_wp_attached_file', 'Sberbank.png'),
(494, 267, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"Sberbank.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(495, 268, '_wp_attached_file', 'Skrill.png'),
(496, 268, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Skrill.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(497, 269, '_wp_attached_file', 'SolidTrustPay.png'),
(498, 269, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:17:\"SolidTrustPay.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(499, 270, '_wp_attached_file', 'Tinkoff.png'),
(500, 270, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"Tinkoff.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(501, 271, '_wp_attached_file', 'Visa-MasterCard.png'),
(502, 271, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:19:\"Visa-MasterCard.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(503, 272, '_wp_attached_file', 'VTB24.png'),
(504, 272, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"VTB24.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(505, 273, '_wp_attached_file', 'WebMoney.png'),
(506, 273, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"WebMoney.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(538, 294, '_wp_attached_file', 'zcash.png'),
(539, 294, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"zcash.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(509, 275, '_wp_attached_file', 'Yandex.png'),
(510, 275, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:10:\"Yandex.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(511, 276, '_wp_attached_file', 'Z-payment.png'),
(512, 276, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:13:\"Z-payment.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(513, 277, '_wp_attached_file', 'favicon.png'),
(514, 277, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:54;s:6:\"height\";i:38;s:4:\"file\";s:11:\"favicon.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(555, 304, '_wp_attached_file', 'kurses.gif'),
(556, 304, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:10:\"kurses.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(557, 305, '_wp_attached_file', 'webmoney-1.png'),
(558, 305, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:14:\"webmoney-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(559, 306, '_wp_attached_file', 'okchanger.png'),
(560, 306, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:90;s:6:\"height\";i:32;s:4:\"file\";s:13:\"okchanger.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(561, 307, '_wp_attached_file', 'kursexpert.png'),
(562, 307, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:14:\"kursexpert.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(563, 308, '_wp_attached_file', 'glazok.gif'),
(564, 308, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:10:\"glazok.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(565, 309, '_wp_attached_file', 'bestchange.gif'),
(566, 309, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:14:\"bestchange.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(547, 298, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:8:\"dash.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(546, 298, '_wp_attached_file', 'dash.png'),
(548, 299, '_wp_attached_file', 'bitcoincash.png'),
(549, 299, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:15:\"bitcoincash.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(569, 311, '_wp_attached_file', 'Ethereum-Classic.png'),
(570, 311, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:20:\"Ethereum-Classic.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(571, 312, '_wp_attached_file', 'EOS.png'),
(572, 312, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:7:\"EOS.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(573, 313, '_wp_attached_file', 'DigiByte.png'),
(574, 313, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:12:\"DigiByte.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(575, 314, '_wp_attached_file', 'Cardano.png'),
(576, 314, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:11:\"Cardano.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(577, 315, '_wp_attached_file', 'bestexchangers.gif'),
(578, 315, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:18:\"bestexchangers.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(607, 327, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:50;s:6:\"height\";i:50;s:4:\"file\";s:9:\"bitex.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(597, 325, '_menu_item_type', 'post_type'),
(598, 325, '_menu_item_menu_item_parent', '0'),
(599, 325, '_menu_item_object_id', '155'),
(600, 325, '_menu_item_object', 'page'),
(601, 325, '_menu_item_target', ''),
(602, 325, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(603, 325, '_menu_item_xfn', ''),
(604, 325, '_menu_item_url', ''),
(606, 327, '_wp_attached_file', 'bitex.png'),
(581, 317, '_wp_attached_file', 'goodkurs.gif'),
(582, 317, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:12:\"goodkurs.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(583, 318, '_wp_attached_file', 'monitorkursov.gif'),
(584, 318, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:17:\"monitorkursov.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(585, 319, '_wp_attached_file', 'obmenvse.gif'),
(586, 319, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:12:\"obmenvse.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(587, 320, '_wp_attached_file', 'okku.gif'),
(588, 320, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:8:\"okku.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(589, 321, '_wp_attached_file', 'proobmen.gif'),
(590, 321, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:12:\"proobmen.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(591, 322, '_wp_attached_file', 'scanmoney.png'),
(592, 322, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:86;s:6:\"height\";i:27;s:4:\"file\";s:13:\"scanmoney.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(593, 323, '_wp_attached_file', 'secretovobmena.gif'),
(594, 323, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:18:\"secretovobmena.gif\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(595, 324, '_wp_attached_file', 'udifo.png'),
(596, 324, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:88;s:6:\"height\";i:31;s:4:\"file\";s:9:\"udifo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_posts`
--

CREATE TABLE `eb_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(255) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_posts`
--

INSERT INTO `eb_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(188, 1, '2013-04-27 15:34:56', '2013-04-27 11:34:56', '', 'Условия и регистрации', '', 'publish', 'open', 'closed', '', 'usloviya-i-registratsii', '', '', '2019-02-12 17:11:20', '2019-02-12 14:11:20', '', 0, 'http://exchange.best-curs.info/?p=188', 7, 'nav_menu_item', '', 0),
(189, 1, '2013-04-27 15:35:14', '2013-04-27 11:35:14', ' ', '', '', 'publish', 'open', 'closed', '', '189', '', '', '2019-02-12 17:11:20', '2019-02-12 14:11:20', '', 0, 'http://exchange.best-curs.info/?p=189', 6, 'nav_menu_item', '', 0),
(194, 1, '2013-04-27 16:00:13', '2013-04-27 12:00:13', '', 'Правила', '', 'publish', 'open', 'closed', '', 'pravila', '', '', '2019-02-12 17:11:20', '2019-02-12 14:11:20', '', 0, 'http://exchange.best-curs.info/?p=194', 4, 'nav_menu_item', '', 0),
(151, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[changetable]', 'Главная', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 0, 'http://exchange.best-curs.info/home/', 0, 'page', '', 0),
(152, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[xchange]', 'Обмен', '', 'publish', 'closed', 'closed', '', 'xchange', '', '', '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 0, 'http://exchange.best-curs.info/xchange/', 0, 'page', '', 0),
(155, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 'Новости', '', 'publish', 'closed', 'closed', '', 'news', '', '', '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 0, 'http://exchange.best-curs.info/news/', 0, 'page', '', 0),
(156, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_payouts]', 'Вывод партнерских средств', '', 'publish', 'closed', 'closed', '', 'payouts', '', '', '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 0, 'http://exchange.best-curs.info/payouts/', 0, 'page', '', 0),
(157, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', 'Мы всегда готовы ответить на интересующие Вас вопросы, а также выслушать Ваши предложения по улучшению нашего сервиса.\r\n\r\nИспользуйте данную форму, если хотите задать нам вопрос, или сообщить о ошибке. Пожалуйста, делайте Ваше сообщение как можно более развернутым, тогда мы сможем решить проблему намного быстрее.\r\n\r\n[exb_contact_form]', 'Контакты', '', 'publish', 'closed', 'closed', '', 'feedback', '', '', '2015-08-24 17:13:11', '2015-08-24 14:13:11', '', 0, 'http://exchange.best-curs.info/feedback/', 0, 'page', '', 0),
(158, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_reviewspage]', 'Отзывы', '', 'publish', 'closed', 'closed', '', 'reviews', '', '', '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 0, 'http://exchange.best-curs.info/reviews/', 0, 'page', '', 0),
(159, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '<h3>Партнёрам</h3>\r\nМы предлагаем вам зарегистрироваться в личном кабинет, чтобы активировать накопительную систему скидок, отслеживать статус ваших заявок и статистику обменов.\r\n\r\nТакже мы предлагает вам поучаствовать в партнерской программе по привлечению клиентов. Зарегистрировавшись в нашей партнерской программе, вы будете получать процент от суммы обмена. После регистрации вы получите на выбор большое количество различных промо-материалов (тексты, баннеры), а также файл экспорта курсов для мониторингов. Все, что потребуется – это приглашать посетителей на наш сайт, размещая промо-материалы на своих домашних страницах, блогах, форумах, сервисах вопросов и ответов, досках объявлений и на других ресурсах. Ваша ссылка будет содержать уникальный код, который позволит определить, что посетитель пришел по ссылке именно от вас.\r\n<h3>Регистрация</h3>\r\nДля регистрации в кабинете, пройдите <a href=\"/register/\">по этой ссылке</a> и заполните простую форму. Перед регистрацией вам будет необходимо ознакомиться с условиями работы и принять пользовательское соглашение.\r\n<h3>Вход</h3>\r\nЕсли вы уже являетесь зарегистрированным пользователем нашего сайта, выполните вход, используя следующую форму авторизации:\r\n\r\n[exb_loginform]', 'Авторизация', '', 'publish', 'closed', 'closed', '', 'login', '', '', '2013-04-27 15:43:02', '2013-04-27 11:43:02', '', 0, 'http://exchange.best-curs.info/login/', 0, 'page', '', 0),
(160, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '1. Зарегистрированные пользователи получают право использовать накопительную систему скидок при совершение обмене:\r\n<ul>\r\n	<li>0-99 USD - 1%</li>\r\n	<li>100-999 USD - 2%</li>\r\n	<li>1000-4999 USD - 3%</li>\r\n	<li>5000- 9999 USD - 4%</li>\r\n	<li>10000-19999 USD - 5%</li>\r\n	<li>свыше 20000 USD - 6%</li>\r\n</ul>\r\n2. Начисления и выплаты по партнерской программе ведутся в долларах (WebMoney WMZ).\r\n\r\n3. Минимальная сумма для снятия заработанных денег с партнерского счета составляет 5 USD.\r\n\r\n4. За каждый совершенный обмен по вашей партнерской ссылке вы получает вознаграждение в размере от 1% до 6% от суммы обмена. Процент отчислений зависит от суммы совершенных обменов по вашей партнерской ссылке:\r\n<ul>\r\n	<li>0-99 USD - 1%</li>\r\n	<li>100-999 USD - 2%</li>\r\n	<li>1000-4999 USD - 3%</li>\r\n	<li>5000- 9999 USD - 4%</li>\r\n	<li>10000-19999 USD - 5%</li>\r\n	<li>свыше 20000 USD - 6%</li>\r\n</ul>\r\n4.1. Указанные значения партнерских вознаграждений быть со временем изменены. При этом все заработанные средства сохраняются на счете с учетом действовавших ранее ставок.\r\n\r\n5. На странице, где вы публикуете о нас информацию должно быть четко указано об услугах, предоставляемых нашим сайтом. В рекламных текстах запрещаются любые упоминания о наличии «бесплатных бонусов» на нашем сайте.\r\n\r\n6. Запрещается размещать партнерскую ссылку:\r\n<ul>\r\n	<li>в массовых рассылках писем (СПАМ);</li>\r\n	<li>на сайтах, принудительно открывающих окна браузера, либо открывающих сайты в скрытых фреймах;</li>\r\n	<li>на сайтах, распространяющих любые материалы, прямо или косвенно нарушающие законодательство РФ;</li>\r\n	<li>на сайтах, публикующих списки сайтов с «бесплатными бонусами»;</li>\r\n	<li>на веб-страницах, закрытых от публичного просмотра с помощью авторизации (различные социальные сети, закрытые разделы форумов и т.п.).</li>\r\n</ul>\r\nСайты, нарушающие одно или несколько вышеперечисленных правил, будут занесены в черный список нашей партнерской программы. Оплата за посетителей, пришедших с подобных сайтов производиться не будет.\r\n\r\n7 . При несоблюдении данных условий аккаунт нарушителя будет заблокирован без выплат и объяснения причин.\r\n\r\n8. Партнер несет полную ответственность за сохранность своих аутентификационных данных (логина и пароля) для доступа к аккаунту.\r\n\r\n9. Данные условия могут изменяться в одностороннем порядке без оповещения участников программы, но с публикацией на этой странице.\r\n<h1>Регистрация</h1>\r\nПожалуйста, внимательно и аккуратно заполните все поля регистрационной формы. На указанный вами e-mail будет выслано уведомление о регистрации.\r\n\r\n[exb_registerform]', 'Условия регистрации личного и партнерского кабинета', '    \r\n    \r\n    \r\n    \r\n    \r\n    ', 'publish', 'closed', 'closed', '', 'register', '', '', '2013-07-03 19:49:41', '2013-07-03 15:49:41', '', 0, 'http://exchange.best-curs.info/register/', 0, 'page', '', 0),
(161, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_lostpassform]', 'Восстановление пароля', '', 'publish', 'closed', 'closed', '', 'lostpass', '', '', '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 0, 'http://exchange.best-curs.info/lostpass/', 0, 'page', '', 0),
(162, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_account]', 'Личный кабинет', '', 'publish', 'closed', 'closed', '', 'account', '', '', '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 0, 'http://exchange.best-curs.info/account/', 0, 'page', '', 0),
(164, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_usersobmen]', 'Ваши операции', '', 'publish', 'closed', 'closed', '', 'usersobmen', '', '', '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 0, 'http://exchange.best-curs.info/usersobmen/', 0, 'page', '', 0),
(165, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '<strong>1. Стороны соглашения.</strong>\r\n\r\nДоговор заключается между интернет сервисом по обмену титульных знаков, далее Исполнитель, — с одной стороны, и Заказчик, в лице того, кто воспользовался услугами Исполнителя, — с другой стороны.\r\n\r\n<strong>2. Список терминов.</strong>\r\n\r\n2.1. Обмен титульных знаков — автоматизированный продукт интернет обслуживания, который предоставляется Исполнителем на основании данных правил.\r\n2.2. Заказчик — физическое лицо, соглашающееся с условиями Исполнителя и данного соглашения, к которому присоединяется.\r\n2.3. Титульный знак — условная единица той или иной платежной системы, которая соответствует расчетам электронных систем и обозначает объем прав, соответствующих договору системы электронной оплаты и ее Заказчика.\r\n2.4. Заявка — сведения, переданные Заказчиком для использования средств Исполнителя в электронном виде и свидетельствующие о том, что он принимает условия пользования сервисом, которые предлагаются Исполнителем в данной заявке.\r\n\r\n<strong>3. Условия соглашения.</strong>\r\n\r\nДанные правила считаются организованными за счет условий общественной оферты, которая образуется во время подачи Заказчиком заявки и является одной из главных составляющих настоящего договора. Общественной офертой именуются отображаемые исполнителем сведения об условиях подачи заявки. Главным составляющим общественной оферты являются действия, сделанные в завершении подачи заявки Заказчиком и говорящие о его точных намерениях совершить сделку на условиях предложенных Исполнителем перед завершением данной заявки. Время, дата, и параметры заявки создаются Исполнителем автоматически в момент окончания формирования данной заявки. Предложение должно приняться Заказчиком в течение 24 часов от окончания формирования заявки. Договор по обслуживанию вступает в силу с момента поступления титульных знаков в полном размере, указанном в заявке, от Заказчика на реквизиты Исполнителя. Операции с титульными знаками учитываются согласно правилам, регламенту и формату электронных систем по расчетам. Договор действителен в течение срока , который устанавливается с момента подачи заявки до расторжения по инициативе одной из сторон.\r\n\r\n<strong>4. Предмет соглашения.</strong>\r\n\r\nПутем использования технических методов Исполнитель обязуется выполнять обмен титульных знаков за комиссионное вознаграждение от Заказчика, после подачи данным лицом заявки и совершает это путем продажи титульных знаков лицам, желающим их приобрести по сумме, указанной не ниже, чем в заявке поданной Заказчиком. Денежные средства Исполнитель обязуется переводить на указанные Заказчиком реквизиты. В случае возникновения во время обмена прибыли, она остается на счету Исполнителя, как дополнительная выгода и премия за комиссионные услуги.\r\n\r\n<strong>5. В дополнение.</strong>\r\n\r\n5.1. Если на счет Исполнителя поступает сумма, отличающаяся от указанной в заявке, Исполнитель делает перерасчет, который соответствует фактическому поступлению титульных знаков. Если данная сумма превышает указанную в заявке более чем на 10%, Исполнитель расторгает договор в одностороннем порядке и все средства возвращаются на реквизиты Заказчика, с учетом вычтенной суммы на комиссионные расходы во время перевода.\r\n5.2. В случае, когда титульные знаки не отправляются Исполнителем на указанные реквизиты Заказчика в течение 24 часов, Заказчик имеет полное право потребовать расторжение соглашения и аннулировать свою заявку, тем самым совершая возврат титульных знаков на свой счет в полном объеме. Заявка на расторжение соглашения и возврата титульных знаков выполняется Исполнителем в том случае, если денежные средства еще не были переведены на указанные реквизиты Заказчика. В случае аннулирования договора, возврат электронной валюты производится в течение 24 часов с момента получения требовании о расторжении договора. Если задержки при возврате возникли не по вине Исполнителя, он не несет за них ответственности.\r\n5.3. Если титульные знаки не поступаеют от Заказчика на счет Исполнителя в течение указанного срока, с момента подачи заявки Заказчиком, соглашение между сторонами расторгается Исполнителем с одной стороны, так как договор не вступает в действие. Заказчик может об этом не уведомляться. Если титульные знаки поступает на реквизиты Исполнителя после указанного срока, то такие средства переводятся обратно на счет Заказчика, причем все комиссионные расходы, связанные с переводом, вычитаются из данных средств.\r\n5.4. Если происходит задержка перевода средств на реквизиты, указанные Заказчиком, по вине расчетной системы, Исполнитель не несет ответственности за ущерб, возникающий в результате долгого поступления денежных средств. В этом случае Заказчик должен согласиться с тем, что все претензии будут предъявляться к расчетной системе, а Исполнитель оказывает свою помощь по мере своих возможностей в рамках закона.\r\n5.5. В случае обнаружения подделки коммуникационных потоков или оказания воздействия, с целью ухудшить работу Исполнителя, а именно его программного кода, заявка приостанавливается, а переведенные средства подвергаются перерасчету в соответствии с действующим соглашением. Если Заказчик не согласен с перерасчетом, он имеет полное право расторгнуть договор и титульные знаки отправятся на реквизиты указанные Заказчиком.\r\n5.6. В случае пользования услугами Исполнителя, Заказчик полностью соглашается с тем, что Исполнитель несет ограниченную ответственность соответствующую рамкам настоящих правил полученных титульных знаков и не дает дополнительных гарантий Заказчику, а также не несет перед ним дополнительной ответственности. Соответственно Заказчик  не несет дополнительной ответственности перед Исполнителем.\r\n5.7. Заказчик обязуется выполнять нормы соответствующие законодательству, а также не подделывать коммуникационные потоки и не создавать препятствий для нормальной работы программного кода Исполнителя.\r\n5.8.Исполнитель не несет ответственности за ущерб и последствия при ошибочном переводе электронной валюты в том случае, если Заказчик указал при подаче заявки неверные реквизиты.\r\n\r\n<strong>6. Гарантийный срок</strong>\r\n\r\nВ течение 24 часов с момента исполнения обмена титульных знаков Исполнитель дает гарантию на оказываемые услуги при условии, если не оговорены иные сроки.\r\n\r\n<strong>7. Непредвиденные обстоятельства.</strong>\r\n\r\nВ случае, когда в процессе обработки заявки Заказчика возникают непредвиденные обстоятельства, способствующие невыполнению Исполнителем условий договора, сроки выполнения заявки переносятся на соответствующий срок длительности форс-мажора. За просроченные обязательства Исполнитель ответственности не несет.\r\n\r\n<strong>8. Форма соглашения.</strong>\r\n\r\nДанное соглашение обе стороны, в лице Исполнителя и Заказчика, принимают как равноценный по юридической силе договор, обозначенный в письменной форме.\r\n\r\n<strong>9. Работа с картами Англии, Германии и США.</strong>\r\n\r\nДля владельцев карт стран Англии, Германии и США условия перевода титульных знаков продляются на неопределенный срок, соответствующий полной проверке данных владельца карты. Денежные средства в течение всего срока не подвергаются никаким операциям и в полном размере находятся на счете Исполнителя.\r\n\r\n<strong>10 Претензии и споры.</strong>\r\n\r\nПретензии по настоящему соглашению принимаются Исполнителем в форме электронного письма, в котором Заказчик указывает суть претензии. Данное письмо отправляется на указанные на сайте реквизиты Исполнителя.\r\n\r\n<strong>11. Проведение обменных операций.</strong>\r\n\r\n11.1.Категорически запрещается пользоваться услугами Исполнителя для проведения незаконных переводов и мошеннических действий. При заключении настоящего договора, Заказчик обязуется выполнять эти требования и в случае мошенничества нести уголовную ответственность, установленную законодательством на данный момент.\r\n11.2. В случае невозможности выполнения заявки автоматически, по не зависящим от Исполнителя обстоятельствам, таким как отсутствие связи, нехватка средств, или же ошибочные данные Заказчика, средства поступают на счет в течение последующих 24 часов или же возвращается на реквизиты Заказчика за вычетом комиссионных расходов.\r\n11.3.По первому требованию Исполнитель вправе передавать информацию о переводе электронной валюты правоохранительным органам, администрации расчетных систем, а также жертвам неправомерных действий, пострадавшим в результате доказанного судебными органами мошенничества.\r\n11.4. Заказчик обязуется представить все документы, удостоверяющие его личность, в случае подозрения о мошенничестве и отмывании денег.\r\n11.5. Заказчик обязуется не вмешиваться в работу Исполнителя и не наносить урон его программной и аппаратной части, а также Заказчик обязуется передавать точные сведения для обеспечения выполнения Исполнителем всех условий договора.\r\n\r\n<strong>12.Отказ от обязательств.</strong>\r\n\r\nИсполнитель имеет право отказа на заключение договора и выполнение заявки, причем без объяснения причин. Данный пункт применяется по отношению к любому клиенту.', 'Правила сайта', '', 'publish', 'closed', 'closed', '', 'tos', '', '', '2013-04-27 15:59:31', '2013-04-27 11:59:31', '', 0, 'http://exchange.best-curs.info/tos/', 0, 'page', '', 0),
(166, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[from_user]\r\n<strong>Вопрос: Как работает партнерская программа?</strong>\r\n\r\nОтвет: Зарегистрировавшись в нашей партнерской программе, Вы получаете уникальный партнерский идентификатор, который добавляется во все Ваши ссылки (?exbpid=777) и HTML-код. Вы можете размещать ссылки на любые страницы нашего сервиса  на своем сайте, блоге, страничке, в сообществах и социальных сетях.<strong>   </strong>\r\n\r\n<strong>Вопрос: Сколько я буду зарабатывать, участвуя в Вашей партнерской программе?</strong>\r\n\r\nОтвет: Это зависит от многих факторов, таких как:\r\n\r\n1. Посещаемость Вашего веб-сайта или сайтов, где Вы размещаете о нас информацию.\r\n\r\n2. Соответствие тематики сайта той целевой аудитории, которая может заинтересоваться услугами обмена валют. Проще говоря, не стоит рассчитывать на большое количество переходов по Вашей партнерской ссылке, размещенной на сайте, посвященном разведению попугаев.\r\n\r\n3. Правильная подача информации. Например, мало кого привлечет одна лишь ссылка \"обмен валют\" без всяких описаний где-нибудь в углу веб-страницы.\r\n\r\n<strong>Вопрос: Если я поставлю свою партнерскую ссылку в подпись на форуме, будут ли учитываться переходы и все остальные условия ПП?</strong>\r\n\r\n<strong></strong>Ответ: Да, конечно будут.\r\n\r\n<strong>Вопрос: На моем сайте уже установлены другие партнерские программы. Могу ли я быть Вашим партнером?</strong>\r\n\r\nОтвет: Да, можете. У нас нет ограничений на работу с другими партнерскими программами.\r\n\r\n<strong>Вопрос: Подходит ли мой сайт для участия в партнерской программе?</strong>\r\n\r\nОтвет: Мы приветствуем любые сайты, которые не противоречат условиям нашей партнерской программы. Посмотреть список условий можно <a href=\"/register/\">здесь</a> (пункт 6).\r\n\r\n<strong>Вопрос: Сколько уровней в Вашей партнерской программе? Оплачивается ли привлечение новых партнеров?</strong>\r\n\r\nОтвет: В нашей партнерской программе 6-ть уровней. Привлечение новых партнеров не оплачивается.\r\n\r\n<strong>Вопрос: Не могу войти в свой аккаунт партнера. Пишет \"Неверное сочетание логина и пароля\". При этом я уверен, что ввожу пароль правильно.</strong>\r\n\r\nОтвет: Убедитесь, что при вводе пароля у Вас не включена русская раскладка клавиатуры или Caps Lock. Если Вы точно помните только логин – воспользуйтесь функцией <a href=\"/lostpass/\">Напоминания пароля</a>. Пароль будет выслан на Ваш e-mail, указанный при регистрации.\r\n\r\n<strong>Вопрос: Как выплачиваются заработанные деньги?</strong>\r\n\r\nОтвет: Партнерские выплаты производятся через систему WebMoney в валюте WMZ на кошелек, указанный партнером при регистрации в партнерской программе. Как правило, на это уходит не более 2-3 часов. Не спешите отправлять нам сообщения, если с момента подачи заявки не прошло 48 часов – администратор видит все заявки и обработает Вашу в любом случае.\r\n\r\n[/from_user]', 'Партнёрский FAQ', '', 'publish', 'closed', 'closed', '', 'partnersfaq', '', '', '2013-06-09 02:15:27', '2013-06-08 22:15:27', '', 0, 'http://exchange.best-curs.info/partnersfaq/', 0, 'page', '', 0),
(167, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_banners]', 'Рекламные материалы', '', 'publish', 'closed', 'closed', '', 'banners', '', '', '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 0, 'http://exchange.best-curs.info/banners/', 0, 'page', '', 0),
(168, 1, '2013-04-26 16:43:36', '2013-04-26 12:43:36', '[exb_partnerstats]', 'Партнёрский аккаунт', '', 'publish', 'closed', 'closed', '', 'partnerstats', '', '', '2013-04-26 16:43:36', '2013-04-26 12:43:36', '', 0, 'http://exchange.best-curs.info/partnerstats/', 0, 'page', '', 0),
(169, 1, '2013-04-26 16:47:46', '2013-04-26 12:47:46', ' ', '', '', 'publish', 'open', 'closed', '', '169', '', '', '2019-02-12 17:11:20', '2019-02-12 14:11:20', '', 0, 'http://exchange.best-curs.info/?p=169', 9, 'nav_menu_item', '', 0),
(170, 1, '2013-04-26 16:47:46', '2013-04-26 12:47:46', ' ', '', '', 'publish', 'open', 'closed', '', '170', '', '', '2019-02-12 17:11:19', '2019-02-12 14:11:19', '', 0, 'http://exchange.best-curs.info/?p=170', 2, 'nav_menu_item', '', 0),
(130, 1, '2013-04-26 16:42:14', '2013-04-26 12:42:14', '', 'Обмен', '', 'publish', 'open', 'closed', '', 'obmen', '', '', '2019-02-12 17:11:19', '2019-02-12 14:11:19', '', 0, 'http://exchange.best-curs.info/?p=130', 1, 'nav_menu_item', '', 0),
(171, 1, '2013-04-27 12:34:38', '2013-04-27 08:34:38', '', 'Кабинет', '', 'publish', 'open', 'closed', '', 'partneram', '', '', '2019-02-12 17:11:20', '2019-02-12 14:11:20', '', 0, 'http://exchange.best-curs.info/?p=171', 5, 'nav_menu_item', '', 0),
(173, 1, '2013-04-27 14:14:37', '2013-04-27 10:14:37', 'Уважаемые клиенты! Безопасность проведения транзакций может быть поставлена под угрозу, в связи с независящими от нашего сервиса обстоятельствами. Чтобы этого не произошло, рекомендуем ознакомиться со следующими правилами конвертации электронной валюты:\r\n<ul>\r\n	<li> Всегда требуйте подтверждения личности лица, на реквизиты которого вы собираетесь выполнить перевод средств. Сделать это можно посредством личного звонка на skype, icq либо посредством запроса информации о статусе кошелька оппонента на сайте платежной системы;</li>\r\n	<li>Будьте предельно внимательны при заполнении поля «Номер счета» адресата. Допустив ошибку, вы отправляете собственные средства в неизвестном направлении без возможности их возврата;</li>\r\n	<li>Никогда не предоставляете займы, используя «безотзывные» электронные системы оплаты. В данном случае шанс столкнуться с фактом мошенничества чрезвычайно велик;</li>\r\n	<li>Если вам предлагается сделать оплату способом, отличным от указанного в инструкции к использованию нашего сервиса, откажитесь от выполнения платежа и сообщите о случившемся нашему специалисту. То же касается выплат по заявкам, созданным не лично вами;</li>\r\n	<li>Откажитесь от проведения средств, собственниками которых являются третьи лица, через собственные банковские счета. Известны случаи, когда проведение таких транзакций за вознаграждение, приводило к тому, что владелец счета становился соучастником финансового преступления, не подозревая о злом умысле со стороны мошенников;</li>\r\n	<li>Всегда уточняйте у сотрудника обменного пункта информацию, приходящую на вашу почту.</li>\r\n</ul>\r\nНаш и подобные сервисы не предоставляют займов, не берут средства у пользователей в долг или под проценты, не принимают пожертвований. При получении сообщений подозрительного характера от нашего имени с похожих на наши либо иных реквизитов, воздержитесь от выполнения указанных там требований и сообщите о произошедшем в нашу <a href=\"/feedback/\">службы поддержки</a>.\r\n\r\nС заботой о вашем финансовом благополучии.', 'Предупреждение', '', 'publish', 'open', 'closed', '', 'preduprezhdenie', '', '', '2013-04-27 14:14:37', '2013-04-27 10:14:37', '', 0, 'http://exchange.best-curs.info/?page_id=173', 0, 'page', '', 0),
(213, 1, '2013-05-19 19:34:15', '2013-05-19 15:34:15', '[exb_sitemap]', 'Карта сайта', '', 'publish', 'closed', 'closed', '', 'sitemap', '', '', '2013-05-19 19:34:15', '2013-05-19 15:34:15', '', 0, 'http://exchange.best-curs.info/sitemap/', 0, 'page', '', 0),
(214, 1, '2013-05-19 19:34:15', '2013-05-19 15:34:15', '[exb_tarifs]', 'Тарифы', '', 'publish', 'closed', 'closed', '', 'tarifs', '', '', '2013-05-19 19:34:15', '2013-05-19 15:34:15', '', 0, 'http://exchange.best-curs.info/tarifs/', 0, 'page', '', 0),
(215, 1, '2013-05-19 19:34:56', '2013-05-19 15:34:56', ' ', '', '', 'publish', 'open', 'closed', '', '215', '', '', '2019-02-12 17:11:20', '2019-02-12 14:11:20', '', 0, 'http://exchange.best-curs.info/?p=215', 8, 'nav_menu_item', '', 0),
(220, 1, '2014-12-04 23:32:32', '2014-12-04 19:32:32', ' ', '', '', 'publish', 'open', 'closed', '', '220', '', '', '2014-12-04 23:32:32', '2014-12-04 19:32:32', '', 0, 'http://exchange.best-curs.info/?p=220', 1, 'nav_menu_item', '', 0),
(221, 1, '2014-12-04 23:32:32', '2014-12-04 19:32:32', ' ', '', '', 'publish', 'open', 'closed', '', '221', '', '', '2014-12-04 23:32:32', '2014-12-04 19:32:32', '', 0, 'http://exchange.best-curs.info/?p=221', 3, 'nav_menu_item', '', 0),
(222, 1, '2014-12-04 23:32:32', '2014-12-04 19:32:32', ' ', '', '', 'publish', 'open', 'closed', '', '222', '', '', '2014-12-04 23:32:32', '2014-12-04 19:32:32', '', 0, 'http://exchange.best-curs.info/?p=222', 2, 'nav_menu_item', '', 0),
(226, 1, '2015-08-24 15:53:51', '2015-08-24 12:53:51', '[exchangestep]', 'Шаги обмена', '', 'publish', 'closed', 'closed', '', 'exchangestep', '', '', '2015-11-13 15:38:37', '2015-11-13 12:38:37', '', 0, 'http://exchange.best-curs.info/exchangestep/', 0, 'page', '', 0),
(328, 1, '2019-12-05 00:09:05', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2019-12-05 00:09:05', '0000-00-00 00:00:00', '', 0, 'http://exchange.best-curs.info/?p=328', 0, 'post', '', 0),
(237, 1, '2017-04-29 23:15:53', '2017-04-29 20:15:53', '', 'Advcash', '', 'inherit', 'open', 'closed', '', 'advcash', '', '', '2017-04-29 23:15:53', '2017-04-29 20:15:53', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Advcash.png', 0, 'attachment', 'image/png', 0),
(238, 1, '2017-04-29 23:15:55', '2017-04-29 20:15:55', '', 'Alfabank', '', 'inherit', 'open', 'closed', '', 'alfabank', '', '', '2017-04-29 23:15:55', '2017-04-29 20:15:55', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Alfabank.png', 0, 'attachment', 'image/png', 0),
(239, 1, '2017-04-29 23:15:57', '2017-04-29 20:15:57', '', 'Alipay', '', 'inherit', 'open', 'closed', '', 'alipay', '', '', '2017-04-29 23:15:57', '2017-04-29 20:15:57', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Alipay.png', 0, 'attachment', 'image/png', 0),
(240, 1, '2017-04-29 23:15:59', '2017-04-29 20:15:59', '', 'Avangardbank', '', 'inherit', 'open', 'closed', '', 'avangardbank', '', '', '2017-04-29 23:15:59', '2017-04-29 20:15:59', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Avangardbank.png', 0, 'attachment', 'image/png', 0),
(241, 1, '2017-04-29 23:16:01', '2017-04-29 20:16:01', '', 'Bank-perevod', '', 'inherit', 'open', 'closed', '', 'bank-perevod', '', '', '2017-04-29 23:16:01', '2017-04-29 20:16:01', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Bank-perevod.png', 0, 'attachment', 'image/png', 0),
(242, 1, '2017-04-29 23:16:02', '2017-04-29 20:16:02', '', 'Bitcoin', '', 'inherit', 'open', 'closed', '', 'bitcoin', '', '', '2017-04-29 23:16:02', '2017-04-29 20:16:02', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Bitcoin.png', 0, 'attachment', 'image/png', 0),
(297, 1, '2018-08-17 17:02:34', '2018-08-17 14:02:34', '', 'ether', '', 'inherit', 'open', 'closed', '', 'ether', '', '', '2018-08-17 17:02:34', '2018-08-17 14:02:34', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/ether.png', 0, 'attachment', 'image/png', 0),
(244, 1, '2017-04-29 23:16:06', '2017-04-29 20:16:06', '', 'Cash-EUR', '', 'inherit', 'open', 'closed', '', 'cash-eur', '', '', '2017-04-29 23:16:06', '2017-04-29 20:16:06', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Cash-EUR.png', 0, 'attachment', 'image/png', 0),
(245, 1, '2017-04-29 23:16:08', '2017-04-29 20:16:08', '', 'Cash-RUB', '', 'inherit', 'open', 'closed', '', 'cash-rub', '', '', '2017-04-29 23:16:08', '2017-04-29 20:16:08', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Cash-RUB.png', 0, 'attachment', 'image/png', 0),
(246, 1, '2017-04-29 23:16:10', '2017-04-29 20:16:10', '', 'Cash-USD', '', 'inherit', 'open', 'closed', '', 'cash-usd', '', '', '2017-04-29 23:16:10', '2017-04-29 20:16:10', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Cash-USD.png', 0, 'attachment', 'image/png', 0),
(311, 1, '2019-02-12 16:36:34', '2019-02-12 13:36:34', '', 'Ethereum-Classic', '', 'inherit', 'open', 'closed', '', 'ethereum-classic', '', '', '2019-02-12 16:36:34', '2019-02-12 13:36:34', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Ethereum-Classic.png', 0, 'attachment', 'image/png', 0),
(248, 1, '2017-04-29 23:16:13', '2017-04-29 20:16:13', '', 'exmo', '', 'inherit', 'open', 'closed', '', 'exmo', '', '', '2017-04-29 23:16:13', '2017-04-29 20:16:13', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/exmo.png', 0, 'attachment', 'image/png', 0),
(293, 1, '2018-08-17 17:02:26', '2018-08-17 14:02:26', '', 'ripple', '', 'inherit', 'open', 'closed', '', 'ripple', '', '', '2018-08-17 17:02:26', '2018-08-17 14:02:26', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/ripple.png', 0, 'attachment', 'image/png', 0),
(250, 1, '2017-04-29 23:16:17', '2017-04-29 20:16:17', '', 'Liqpay', '', 'inherit', 'open', 'closed', '', 'liqpay', '', '', '2017-04-29 23:16:17', '2017-04-29 20:16:17', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Liqpay.png', 0, 'attachment', 'image/png', 0),
(251, 1, '2017-04-29 23:16:19', '2017-04-29 20:16:19', '', 'Litecoin', '', 'inherit', 'open', 'closed', '', 'litecoin', '', '', '2017-04-29 23:16:19', '2017-04-29 20:16:19', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Litecoin.png', 0, 'attachment', 'image/png', 0),
(252, 1, '2017-04-29 23:16:21', '2017-04-29 20:16:21', '', 'Livecoin', '', 'inherit', 'open', 'closed', '', 'livecoin', '', '', '2017-04-29 23:16:21', '2017-04-29 20:16:21', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Livecoin.png', 0, 'attachment', 'image/png', 0),
(253, 1, '2017-04-29 23:16:22', '2017-04-29 20:16:22', '', 'NixMoney', '', 'inherit', 'open', 'closed', '', 'nixmoney', '', '', '2017-04-29 23:16:22', '2017-04-29 20:16:22', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/NixMoney.png', 0, 'attachment', 'image/png', 0),
(295, 1, '2018-08-17 17:02:30', '2018-08-17 14:02:30', '', 'wawes', '', 'inherit', 'open', 'closed', '', 'wawes', '', '', '2018-08-17 17:02:30', '2018-08-17 14:02:30', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/wawes.png', 0, 'attachment', 'image/png', 0),
(296, 1, '2018-08-17 17:02:32', '2018-08-17 14:02:32', '', 'monero', '', 'inherit', 'open', 'closed', '', 'monero', '', '', '2018-08-17 17:02:32', '2018-08-17 14:02:32', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/monero.png', 0, 'attachment', 'image/png', 0),
(256, 1, '2017-04-29 23:16:28', '2017-04-29 20:16:28', '', 'Paxum', '', 'inherit', 'open', 'closed', '', 'paxum', '', '', '2017-04-29 23:16:28', '2017-04-29 20:16:28', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Paxum.png', 0, 'attachment', 'image/png', 0),
(257, 1, '2017-04-29 23:16:29', '2017-04-29 20:16:29', '', 'Payeer', '', 'inherit', 'open', 'closed', '', 'payeer', '', '', '2017-04-29 23:16:29', '2017-04-29 20:16:29', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Payeer.png', 0, 'attachment', 'image/png', 0),
(258, 1, '2017-04-29 23:16:31', '2017-04-29 20:16:31', '', 'Paymer', '', 'inherit', 'open', 'closed', '', 'paymer', '', '', '2017-04-29 23:16:31', '2017-04-29 20:16:31', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Paymer.png', 0, 'attachment', 'image/png', 0),
(259, 1, '2017-04-29 23:16:33', '2017-04-29 20:16:33', '', 'Paypal', '', 'inherit', 'open', 'closed', '', 'paypal', '', '', '2017-04-29 23:16:33', '2017-04-29 20:16:33', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Paypal.png', 0, 'attachment', 'image/png', 0),
(260, 1, '2017-04-29 23:16:35', '2017-04-29 20:16:35', '', 'Payza', '', 'inherit', 'open', 'closed', '', 'payza', '', '', '2017-04-29 23:16:35', '2017-04-29 20:16:35', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Payza.png', 0, 'attachment', 'image/png', 0),
(261, 1, '2017-04-29 23:16:37', '2017-04-29 20:16:37', '', 'Perfect-Money', '', 'inherit', 'open', 'closed', '', 'perfect-money', '', '', '2017-04-29 23:16:37', '2017-04-29 20:16:37', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Perfect-Money.png', 0, 'attachment', 'image/png', 0),
(262, 1, '2017-04-29 23:16:38', '2017-04-29 20:16:38', '', 'PMe-voucher', '', 'inherit', 'open', 'closed', '', 'pme-voucher', '', '', '2017-04-29 23:16:38', '2017-04-29 20:16:38', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/PMe-voucher.png', 0, 'attachment', 'image/png', 0),
(263, 1, '2017-04-29 23:16:40', '2017-04-29 20:16:40', '', 'Privatbank', '', 'inherit', 'open', 'closed', '', 'privatbank', '', '', '2017-04-29 23:16:40', '2017-04-29 20:16:40', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Privatbank.png', 0, 'attachment', 'image/png', 0),
(264, 1, '2017-04-29 23:16:42', '2017-04-29 20:16:42', '', 'Promsvyazbank', '', 'inherit', 'open', 'closed', '', 'promsvyazbank', '', '', '2017-04-29 23:16:42', '2017-04-29 20:16:42', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Promsvyazbank.png', 0, 'attachment', 'image/png', 0),
(265, 1, '2017-04-29 23:16:44', '2017-04-29 20:16:44', '', 'Qiwi', '', 'inherit', 'open', 'closed', '', 'qiwi', '', '', '2017-04-29 23:16:44', '2017-04-29 20:16:44', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Qiwi.png', 0, 'attachment', 'image/png', 0),
(266, 1, '2017-04-29 23:16:46', '2017-04-29 20:16:46', '', 'Russstandart', '', 'inherit', 'open', 'closed', '', 'russstandart', '', '', '2017-04-29 23:16:46', '2017-04-29 20:16:46', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Russstandart.png', 0, 'attachment', 'image/png', 0),
(267, 1, '2017-04-29 23:16:47', '2017-04-29 20:16:47', '', 'Sberbank', '', 'inherit', 'open', 'closed', '', 'sberbank', '', '', '2017-04-29 23:16:47', '2017-04-29 20:16:47', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Sberbank.png', 0, 'attachment', 'image/png', 0),
(268, 1, '2017-04-29 23:16:49', '2017-04-29 20:16:49', '', 'Skrill', '', 'inherit', 'open', 'closed', '', 'skrill', '', '', '2017-04-29 23:16:49', '2017-04-29 20:16:49', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Skrill.png', 0, 'attachment', 'image/png', 0),
(269, 1, '2017-04-29 23:16:51', '2017-04-29 20:16:51', '', 'SolidTrustPay', '', 'inherit', 'open', 'closed', '', 'solidtrustpay', '', '', '2017-04-29 23:16:51', '2017-04-29 20:16:51', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/SolidTrustPay.png', 0, 'attachment', 'image/png', 0),
(270, 1, '2017-04-29 23:16:53', '2017-04-29 20:16:53', '', 'Tinkoff', '', 'inherit', 'open', 'closed', '', 'tinkoff', '', '', '2017-04-29 23:16:53', '2017-04-29 20:16:53', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Tinkoff.png', 0, 'attachment', 'image/png', 0),
(271, 1, '2017-04-29 23:16:55', '2017-04-29 20:16:55', '', 'Visa-MasterCard', '', 'inherit', 'open', 'closed', '', 'visa-mastercard', '', '', '2017-04-29 23:16:55', '2017-04-29 20:16:55', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Visa-MasterCard.png', 0, 'attachment', 'image/png', 0),
(272, 1, '2017-04-29 23:16:56', '2017-04-29 20:16:56', '', 'VTB24', '', 'inherit', 'open', 'closed', '', 'vtb24', '', '', '2017-04-29 23:16:56', '2017-04-29 20:16:56', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/VTB24.png', 0, 'attachment', 'image/png', 0),
(273, 1, '2017-04-29 23:16:58', '2017-04-29 20:16:58', '', 'WebMoney', '', 'inherit', 'open', 'closed', '', 'webmoney', '', '', '2017-04-29 23:16:58', '2017-04-29 20:16:58', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/WebMoney.png', 0, 'attachment', 'image/png', 0),
(294, 1, '2018-08-17 17:02:28', '2018-08-17 14:02:28', '', 'zcash', '', 'inherit', 'open', 'closed', '', 'zcash', '', '', '2018-08-17 17:02:28', '2018-08-17 14:02:28', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/zcash.png', 0, 'attachment', 'image/png', 0),
(275, 1, '2017-04-29 23:17:02', '2017-04-29 20:17:02', '', 'Yandex', '', 'inherit', 'open', 'closed', '', 'yandex', '', '', '2017-04-29 23:17:02', '2017-04-29 20:17:02', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Yandex.png', 0, 'attachment', 'image/png', 0),
(276, 1, '2017-04-29 23:17:04', '2017-04-29 20:17:04', '', 'Z-payment', '', 'inherit', 'open', 'closed', '', 'z-payment', '', '', '2017-04-29 23:17:04', '2017-04-29 20:17:04', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Z-payment.png', 0, 'attachment', 'image/png', 0),
(277, 1, '2017-04-29 23:25:56', '2017-04-29 20:25:56', '', 'favicon', '', 'inherit', 'open', 'closed', '', 'favicon', '', '', '2017-04-29 23:25:56', '2017-04-29 20:25:56', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/favicon.png', 0, 'attachment', 'image/png', 0),
(304, 1, '2019-02-12 16:36:18', '2019-02-12 13:36:18', '', 'kurses', '', 'inherit', 'open', 'closed', '', 'kurses', '', '', '2019-02-12 16:36:18', '2019-02-12 13:36:18', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/kurses.gif', 0, 'attachment', 'image/gif', 0),
(302, 1, '2019-02-12 14:52:27', '2019-02-12 11:52:27', '', 'Пользовательское соглашение по обработке персональных данных', '', 'publish', 'closed', 'closed', '', 'terms-personal-data', '', '', '2019-02-12 14:52:27', '2019-02-12 11:52:27', '', 0, 'http://exchange.best-curs.info/terms-personal-data/', 0, 'page', '', 0),
(305, 1, '2019-02-12 16:36:20', '2019-02-12 13:36:20', '', 'webmoney-1', '', 'inherit', 'open', 'closed', '', 'webmoney-1', '', '', '2019-02-12 16:36:20', '2019-02-12 13:36:20', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/webmoney-1.png', 0, 'attachment', 'image/png', 0),
(306, 1, '2019-02-12 16:36:23', '2019-02-12 13:36:23', '', 'okchanger', '', 'inherit', 'open', 'closed', '', 'okchanger', '', '', '2019-02-12 16:36:23', '2019-02-12 13:36:23', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/okchanger.png', 0, 'attachment', 'image/png', 0),
(307, 1, '2019-02-12 16:36:25', '2019-02-12 13:36:25', '', 'kursexpert', '', 'inherit', 'open', 'closed', '', 'kursexpert', '', '', '2019-02-12 16:36:25', '2019-02-12 13:36:25', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/kursexpert.png', 0, 'attachment', 'image/png', 0),
(308, 1, '2019-02-12 16:36:27', '2019-02-12 13:36:27', '', 'glazok', '', 'inherit', 'open', 'closed', '', 'glazok', '', '', '2019-02-12 16:36:27', '2019-02-12 13:36:27', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/glazok.gif', 0, 'attachment', 'image/gif', 0),
(309, 1, '2019-02-12 16:36:29', '2019-02-12 13:36:29', '', 'bestchange', '', 'inherit', 'open', 'closed', '', 'bestchange', '', '', '2019-02-12 16:36:29', '2019-02-12 13:36:29', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/bestchange.gif', 0, 'attachment', 'image/gif', 0),
(310, 1, '2019-02-12 16:36:32', '2019-02-12 13:36:32', '', 'Stellar', '', 'inherit', 'open', 'closed', '', 'stellar', '', '', '2019-02-12 16:36:32', '2019-02-12 13:36:32', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Stellar.png', 0, 'attachment', 'image/png', 0),
(299, 1, '2018-08-17 17:02:38', '2018-08-17 14:02:38', '', 'bitcoincash', '', 'inherit', 'open', 'closed', '', 'bitcoincash', '', '', '2018-08-17 17:02:38', '2018-08-17 14:02:38', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/bitcoincash.png', 0, 'attachment', 'image/png', 0),
(312, 1, '2019-02-12 16:36:36', '2019-02-12 13:36:36', '', 'EOS', '', 'inherit', 'open', 'closed', '', 'eos', '', '', '2019-02-12 16:36:36', '2019-02-12 13:36:36', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/EOS.png', 0, 'attachment', 'image/png', 0),
(298, 1, '2018-08-17 17:02:36', '2018-08-17 14:02:36', '', 'dash', '', 'inherit', 'open', 'closed', '', 'dash', '', '', '2018-08-17 17:02:36', '2018-08-17 14:02:36', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/dash.png', 0, 'attachment', 'image/png', 0),
(313, 1, '2019-02-12 16:36:38', '2019-02-12 13:36:38', '', 'DigiByte', '', 'inherit', 'open', 'closed', '', 'digibyte', '', '', '2019-02-12 16:36:38', '2019-02-12 13:36:38', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/DigiByte.png', 0, 'attachment', 'image/png', 0),
(314, 1, '2019-02-12 16:36:41', '2019-02-12 13:36:41', '', 'Cardano', '', 'inherit', 'open', 'closed', '', 'cardano', '', '', '2019-02-12 16:36:41', '2019-02-12 13:36:41', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/Cardano.png', 0, 'attachment', 'image/png', 0),
(315, 1, '2019-02-12 16:42:25', '2019-02-12 13:42:25', '', 'bestexchangers', '', 'inherit', 'open', 'closed', '', 'bestexchangers', '', '', '2019-02-12 16:42:25', '2019-02-12 13:42:25', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/bestexchangers.gif', 0, 'attachment', 'image/gif', 0),
(325, 1, '2019-02-12 17:11:19', '2019-02-12 14:11:19', ' ', '', '', 'publish', 'closed', 'closed', '', '325', '', '', '2019-02-12 17:11:19', '2019-02-12 14:11:19', '', 0, 'http://exchange.best-curs.info/?p=325', 3, 'nav_menu_item', '', 0),
(317, 1, '2019-02-12 16:42:30', '2019-02-12 13:42:30', '', 'goodkurs', '', 'inherit', 'open', 'closed', '', 'goodkurs', '', '', '2019-02-12 16:42:30', '2019-02-12 13:42:30', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/goodkurs.gif', 0, 'attachment', 'image/gif', 0),
(318, 1, '2019-02-12 16:42:34', '2019-02-12 13:42:34', '', 'monitorkursov', '', 'inherit', 'open', 'closed', '', 'monitorkursov', '', '', '2019-02-12 16:42:34', '2019-02-12 13:42:34', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/monitorkursov.gif', 0, 'attachment', 'image/gif', 0),
(319, 1, '2019-02-12 16:42:37', '2019-02-12 13:42:37', '', 'obmenvse', '', 'inherit', 'open', 'closed', '', 'obmenvse', '', '', '2019-02-12 16:42:37', '2019-02-12 13:42:37', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/obmenvse.gif', 0, 'attachment', 'image/gif', 0),
(320, 1, '2019-02-12 16:42:40', '2019-02-12 13:42:40', '', 'okku', '', 'inherit', 'open', 'closed', '', 'okku', '', '', '2019-02-12 16:42:40', '2019-02-12 13:42:40', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/okku.gif', 0, 'attachment', 'image/gif', 0),
(321, 1, '2019-02-12 16:42:43', '2019-02-12 13:42:43', '', 'proobmen', '', 'inherit', 'open', 'closed', '', 'proobmen', '', '', '2019-02-12 16:42:43', '2019-02-12 13:42:43', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/proobmen.gif', 0, 'attachment', 'image/gif', 0),
(322, 1, '2019-02-12 16:42:46', '2019-02-12 13:42:46', '', 'scanmoney', '', 'inherit', 'open', 'closed', '', 'scanmoney', '', '', '2019-02-12 16:42:46', '2019-02-12 13:42:46', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/scanmoney.png', 0, 'attachment', 'image/png', 0),
(323, 1, '2019-02-12 16:42:49', '2019-02-12 13:42:49', '', 'secretovobmena', '', 'inherit', 'open', 'closed', '', 'secretovobmena', '', '', '2019-02-12 16:42:49', '2019-02-12 13:42:49', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/secretovobmena.gif', 0, 'attachment', 'image/gif', 0),
(324, 1, '2019-02-12 16:42:52', '2019-02-12 13:42:52', '', 'udifo', '', 'inherit', 'open', 'closed', '', 'udifo', '', '', '2019-02-12 16:42:52', '2019-02-12 13:42:52', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/udifo.png', 0, 'attachment', 'image/png', 0),
(327, 1, '2019-04-02 16:37:33', '2019-04-02 13:37:33', '', 'bitex', '', 'inherit', 'open', 'closed', '', 'bitex', '', '', '2019-04-02 16:37:33', '2019-04-02 13:37:33', '', 0, 'http://exchange.best-curs.info/wp-content/uploads/bitex.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_reviews`
--

CREATE TABLE `eb_reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `create_date` datetime NOT NULL,
  `edit_date` datetime NOT NULL,
  `auto_status` int(1) NOT NULL DEFAULT 1,
  `edit_user_id` bigint(20) NOT NULL DEFAULT 0,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `user_name` tinytext NOT NULL,
  `user_email` tinytext NOT NULL,
  `user_site` tinytext NOT NULL,
  `review_date` datetime NOT NULL,
  `review_hash` tinytext NOT NULL,
  `review_text` longtext NOT NULL,
  `review_status` varchar(150) NOT NULL DEFAULT 'moderation'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_reviews_meta`
--

CREATE TABLE `eb_reviews_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) NOT NULL DEFAULT 0,
  `meta_key` longtext NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_scrtransaction`
--

CREATE TABLE `eb_scrtransaction` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bdate` datetime NOT NULL,
  `bidid` bigint(20) NOT NULL DEFAULT 0,
  `valsid` bigint(20) NOT NULL DEFAULT 0,
  `bsumm` varchar(250) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_termmeta`
--

CREATE TABLE `eb_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_terms`
--

CREATE TABLE `eb_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_terms`
--

INSERT INTO `eb_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Новости', 'novosti', 0),
(2, 'Ссылки', '%d1%81%d1%81%d1%8b%d0%bb%d0%ba%d0%b8', 0),
(3, 'Верхнее меню', 'verhnee-menyu', 0),
(4, 'Нижнее меню', 'nizhnee-menyu', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_term_relationships`
--

CREATE TABLE `eb_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_term_relationships`
--

INSERT INTO `eb_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(169, 3, 0),
(130, 3, 0),
(170, 3, 0),
(171, 3, 0),
(188, 3, 0),
(189, 3, 0),
(194, 3, 0),
(215, 3, 0),
(220, 4, 0),
(222, 4, 0),
(221, 4, 0),
(325, 3, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_term_taxonomy`
--

CREATE TABLE `eb_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_term_taxonomy`
--

INSERT INTO `eb_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'link_category', '', 0, 0),
(3, 3, 'nav_menu', '', 0, 9),
(4, 4, 'nav_menu', '', 0, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_transactionrezerv`
--

CREATE TABLE `eb_transactionrezerv` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vdate` datetime NOT NULL,
  `valsid` bigint(20) NOT NULL DEFAULT 0,
  `vsumm` varchar(250) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_transactionuser`
--

CREATE TABLE `eb_transactionuser` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bdate` datetime NOT NULL,
  `bidid` bigint(20) NOT NULL DEFAULT 0,
  `userid` bigint(20) NOT NULL DEFAULT 0,
  `bsumm` varchar(250) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_usermeta`
--

CREATE TABLE `eb_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_usermeta`
--

INSERT INTO `eb_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'first_name', 'Администратор'),
(2, 1, 'last_name', ''),
(3, 1, 'nickname', 'superadmin'),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'eb_capabilities', 'a:1:{s:13:\"administrator\";s:1:\"1\";}'),
(11, 1, 'eb_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp330_toolbar,wp330_media_uploader,wp330_saving_widgets,wp340_customize_current_theme_link,wp350_media,wp390_widgets,wp410_dfw,wp496_privacy'),
(13, 1, 'show_welcome_panel', '0'),
(14, 1, 'eb_dashboard_quick_press_last_post_id', '328'),
(15, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(16, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:8:\"add-post\";i:1;s:12:\"add-post_tag\";}'),
(17, 1, 'nav_menu_recently_edited', '3'),
(18, 1, 'aim', ''),
(19, 1, 'yim', ''),
(20, 1, 'jabber', ''),
(36, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(37, 1, 'metaboxhidden_dashboard', 'a:3:{i:0;s:18:\"dashboard_activity\";i:1;s:21:\"dashboard_quick_press\";i:2;s:17:\"dashboard_primary\";}'),
(38, 1, 'eb_user-settings', 'editor=html&ed_size=760&hidetb=1&libraryContent=browse&mfold=o'),
(39, 1, 'eb_user-settings-time', '1549981022'),
(68, 1, 'second_name', ''),
(58, 1, 'wmz', ''),
(57, 1, 'user_phone', ''),
(52, 1, 'googleplus', ''),
(61, 1, 'user_skype', ''),
(65, 1, 'session_tokens', 'a:1:{s:64:\"f40ff42904b8af9dc3066d60c2777bbec734cffcf7c894a821e0faac90059310\";a:4:{s:10:\"expiration\";i:1575752942;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36\";s:5:\"login\";i:1575493742;}}'),
(70, 1, '', ''),
(72, 1, 'show_block_parser', 'a:0:{}');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_users`
--

CREATE TABLE `eb_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(255) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) NOT NULL DEFAULT '',
  `user_hash` varchar(15) NOT NULL,
  `auto_login1` varchar(250) NOT NULL,
  `auto_login2` varchar(250) NOT NULL,
  `user_discount` varchar(50) NOT NULL DEFAULT '0',
  `ref_id` bigint(20) NOT NULL DEFAULT 0,
  `partner_pers` varchar(50) NOT NULL DEFAULT '0',
  `sec_lostpass` int(1) NOT NULL DEFAULT 1,
  `sec_login` int(1) NOT NULL DEFAULT 0,
  `email_login` int(1) NOT NULL DEFAULT 0,
  `user_verify` int(1) NOT NULL DEFAULT 0,
  `enable_ips` longtext NOT NULL,
  `user_browser` varchar(250) NOT NULL,
  `user_ip` varchar(250) NOT NULL,
  `user_bann` int(1) NOT NULL DEFAULT 0,
  `admin_comment` longtext NOT NULL,
  `last_adminpanel` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_users`
--

INSERT INTO `eb_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`, `user_hash`, `auto_login1`, `auto_login2`, `user_discount`, `ref_id`, `partner_pers`, `sec_lostpass`, `sec_login`, `email_login`, `user_verify`, `enable_ips`, `user_browser`, `user_ip`, `user_bann`, `admin_comment`, `last_adminpanel`) VALUES
(1, 'superadmin', '$P$BBuxKtBMuuuboC69nC0Q8Q3blI82LT.', 'superadmin', 'info@best-curs.info', '', '2012-05-16 17:07:12', '', 0, 'Администратор', 'LBOKCZXrxNZ3ZlO', '', '', '0', 0, '0', 1, 0, 0, 0, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', '127.0.0.1', 0, '', '1575504789');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_userverify`
--

CREATE TABLE `eb_userverify` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `createdate` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `user_login` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `fio` varchar(250) NOT NULL,
  `tel` varchar(250) NOT NULL,
  `skype` varchar(250) NOT NULL,
  `file1` longtext NOT NULL,
  `file2` longtext NOT NULL,
  `file3` longtext NOT NULL,
  `file4` longtext NOT NULL,
  `textstatus` longtext NOT NULL,
  `theip` varchar(150) NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_user_discounts`
--

CREATE TABLE `eb_user_discounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sumec` varchar(50) NOT NULL DEFAULT '0',
  `discount` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_user_discounts`
--

INSERT INTO `eb_user_discounts` (`id`, `sumec`, `discount`) VALUES
(1, '0', '0'),
(2, '100', '0.1'),
(3, '500', '0.2'),
(4, '1000', '0.3'),
(5, '5000', '0.4');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_user_fav`
--

CREATE TABLE `eb_user_fav` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT 0,
  `link` varchar(250) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '0',
  `menu_order` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_usve_change`
--

CREATE TABLE `eb_usve_change` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  `meta_key2` varchar(250) NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_valuts`
--

CREATE TABLE `eb_valuts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vname` longtext NOT NULL,
  `vlogo` longtext NOT NULL,
  `xname` longtext NOT NULL,
  `vtype` longtext NOT NULL,
  `numleng` int(5) NOT NULL DEFAULT 0,
  `xzt` varchar(150) NOT NULL DEFAULT '',
  `helps` longtext NOT NULL,
  `txt1` longtext NOT NULL,
  `txt2` longtext NOT NULL,
  `rxzt` int(5) NOT NULL DEFAULT 0,
  `vactive` int(1) NOT NULL DEFAULT 1,
  `pvivod` int(1) NOT NULL DEFAULT 1,
  `show1` int(1) NOT NULL DEFAULT 1,
  `show2` int(1) NOT NULL DEFAULT 1,
  `maxnumleng` int(5) NOT NULL DEFAULT 100,
  `xml_value` varchar(250) NOT NULL,
  `firstzn` varchar(20) NOT NULL,
  `valut_reserv` varchar(50) NOT NULL DEFAULT '0',
  `site_order` bigint(20) NOT NULL DEFAULT 0,
  `reserv_order` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_valuts`
--

INSERT INTO `eb_valuts` (`id`, `vname`, `vlogo`, `xname`, `vtype`, `numleng`, `xzt`, `helps`, `txt1`, `txt2`, `rxzt`, `vactive`, `pvivod`, `show1`, `show2`, `maxnumleng`, `xml_value`, `firstzn`, `valut_reserv`, `site_order`, `reserv_order`) VALUES
(3, 'Perfect Money', '/wp-content/uploads/Perfect-Money.png', 'PMUSD', 'USD', 8, '0', 'Пример: U1234567.', '', '', 0, 1, 1, 1, 1, 100, 'PMUSD', '', '0', 0, 0),
(4, 'Perfect Money', '/wp-content/uploads/Perfect-Money.png', 'PMEUR', 'EUR', 8, '0', 'Пример: E1234567.', '', '', 0, 1, 1, 1, 1, 100, 'PMEUR', '', '0', 0, 0),
(11, 'Альфа банк', '/wp-content/uploads/Alfabank.png', 'ACRUB', 'RUB', 20, '0', 'Номер вашего рублевого счета состоит из 20 цифр и начинается с &quot;408&quot;.', '', '', 0, 1, 1, 1, 1, 100, 'ACRUB', '', '0', 0, 0),
(12, 'Сбербанк', '/wp-content/uploads/Sberbank.png', 'SBERRUB', 'RUB', 16, '0', 'Номер счета(карты) состоит из 16 или 18 цифр.', 'С карты', 'На карту', 0, 1, 1, 1, 1, 100, 'SBERRUB', '', '0', 0, 0),
(13, 'Qiwi', '/wp-content/uploads/Qiwi.png', 'QWRUB', 'RUB', 11, '0', 'Номер счета Qiwi совпадает с номером мобильного телефона в международном формате. Пример: +71234567890.', '', '', 0, 1, 1, 1, 1, 100, 'QWRUB', '', '0', 0, 0),
(16, 'Приват24', '/wp-content/uploads/Privatbank.png', 'P24UAH', 'UAH', 11, '0', '', '', '', 0, 1, 1, 1, 1, 100, 'P24UAH', '', '0', 0, 0),
(17, 'WebMoney', '/wp-content/uploads/WebMoney.png', 'WMZ', 'USD', 13, '0', 'Пример: Z123456789000.', '', '', 0, 1, 1, 1, 1, 100, 'WMZ', '', '0', 0, 0),
(18, 'Яндекс.Деньги', '/wp-content/uploads/Yandex.png', 'YAMRUB', 'RUB', 14, '0', 'Пример: 01234567890000', '', '', 0, 1, 1, 1, 1, 100, 'YAMRUB', '', '0', 0, 0),
(19, 'Bitcoin', '/wp-content/uploads/Bitcoin.png', 'BTC', 'BTC', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'BTC', '', '0', 0, 0),
(20, 'Ethereum', '/wp-content/uploads/ether.png', 'ETH', 'ETH', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'ETH', '', '0', 0, 0),
(21, 'Dash', '/wp-content/uploads/dash.png', 'DASH', 'DASH', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'DASH', '', '0', 0, 0),
(22, 'Bitcoin Cash', 'http://exchange.best-curs.info/wp-content/uploads/bitcoincash.png', 'BCH', 'BCH', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'BCH', '', '0', 0, 0),
(23, 'Ripple', 'http://exchange.best-curs.info/wp-content/uploads/ripple.png', 'XRP', 'XRP', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'XRP', '', '0', 0, 0),
(24, 'Monero', 'http://exchange.best-curs.info/wp-content/uploads/monero.png', 'XMR', 'XMR', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'XMR', '', '0', 0, 0),
(25, 'Zcash', 'http://exchange.best-curs.info/wp-content/uploads/zcash.png', 'ZEC', 'ZEC', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'ZEC', '', '0', 0, 0),
(26, 'Waves', 'http://exchange.best-curs.info/wp-content/uploads/wawes.png', 'WAVES', 'WAVES', 0, '0', '', '', '', 0, 1, 1, 1, 1, 0, 'WAVES', '', '0', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eb_valuts_meta`
--

CREATE TABLE `eb_valuts_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` bigint(20) NOT NULL DEFAULT 0,
  `meta_key` longtext NOT NULL,
  `meta_value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_vschets`
--

CREATE TABLE `eb_vschets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `valid` bigint(20) NOT NULL DEFAULT 0,
  `title` varchar(250) NOT NULL DEFAULT '0',
  `visib` int(1) NOT NULL DEFAULT 0,
  `prosm` int(5) NOT NULL DEFAULT 0,
  `text_comment` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `eb_vtypes`
--

CREATE TABLE `eb_vtypes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `xname` tinytext NOT NULL,
  `vncurs` varchar(50) NOT NULL DEFAULT '0',
  `parser` bigint(20) NOT NULL DEFAULT 0,
  `nums` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eb_vtypes`
--

INSERT INTO `eb_vtypes` (`id`, `xname`, `vncurs`, `parser`, `nums`) VALUES
(1, 'RUB', '64.1948', 1, '0'),
(2, 'EUR', '0.877886050391', 52, '0'),
(3, 'USD', '1', 0, '0'),
(4, 'UAH', '26.75521', 101, '0'),
(6, 'KZT', '387.48', 151, '0'),
(10, 'BTC', '0.000086206897', 0, '0'),
(12, 'BYN', '2.1183', 201, '0'),
(13, 'ETH', '0.0033564931', 0, '0'),
(14, 'DASH', '0.0063083523', 0, '0'),
(15, 'ZEC', '0.0069778801', 0, '0'),
(16, 'XMR', '0.0104046363', 0, '0'),
(17, 'XRP', '3.1529827217', 0, '0'),
(18, 'BCH', '0.0018390466', 0, '0'),
(19, 'LTC', '0.0170604794', 0, '0'),
(20, 'WAVES', '0.5376344086', 0, '0');

-- --------------------------------------------------------

--
-- Структура таблицы `eb_zapros_rezerv`
--

CREATE TABLE `eb_zapros_rezerv` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(250) NOT NULL,
  `valid` bigint(20) NOT NULL DEFAULT 0,
  `summk` varchar(250) NOT NULL DEFAULT '0',
  `comment` longtext NOT NULL,
  `zdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `eb_abitcoin`
--
ALTER TABLE `eb_abitcoin`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_adminpanelcaptcha`
--
ALTER TABLE `eb_adminpanelcaptcha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createdate` (`createdate`),
  ADD KEY `sess_hash` (`sess_hash`);

--
-- Индексы таблицы `eb_archive_data`
--
ALTER TABLE `eb_archive_data`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_auth_logs`
--
ALTER TABLE `eb_auth_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `auth_date` (`auth_date`),
  ADD KEY `auth_status` (`auth_status`);

--
-- Индексы таблицы `eb_bautocurs`
--
ALTER TABLE `eb_bautocurs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_blackip`
--
ALTER TABLE `eb_bcoip_blackip`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_country`
--
ALTER TABLE `eb_bcoip_country`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_ipcity`
--
ALTER TABLE `eb_bcoip_ipcity`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_iplist`
--
ALTER TABLE `eb_bcoip_iplist`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_themplate`
--
ALTER TABLE `eb_bcoip_themplate`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bcoip_whiteip`
--
ALTER TABLE `eb_bcoip_whiteip`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bids`
--
ALTER TABLE `eb_bids`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_bids_meta`
--
ALTER TABLE `eb_bids_meta`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_blacklist`
--
ALTER TABLE `eb_blacklist`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_captcha`
--
ALTER TABLE `eb_captcha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createdate` (`createdate`),
  ADD KEY `sess_hash` (`sess_hash`);

--
-- Индексы таблицы `eb_change`
--
ALTER TABLE `eb_change`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_commentmeta`
--
ALTER TABLE `eb_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `eb_comments`
--
ALTER TABLE `eb_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Индексы таблицы `eb_custom_fields_valut`
--
ALTER TABLE `eb_custom_fields_valut`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_inex_change`
--
ALTER TABLE `eb_inex_change`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_inex_deposit`
--
ALTER TABLE `eb_inex_deposit`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_inex_system`
--
ALTER TABLE `eb_inex_system`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_inex_tars`
--
ALTER TABLE `eb_inex_tars`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_links`
--
ALTER TABLE `eb_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Индексы таблицы `eb_masschange`
--
ALTER TABLE `eb_masschange`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_merchant_logs`
--
ALTER TABLE `eb_merchant_logs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_napobmens`
--
ALTER TABLE `eb_napobmens`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_naps_meta`
--
ALTER TABLE `eb_naps_meta`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_notice_head`
--
ALTER TABLE `eb_notice_head`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_options`
--
ALTER TABLE `eb_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Индексы таблицы `eb_partners`
--
ALTER TABLE `eb_partners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_partner_pers`
--
ALTER TABLE `eb_partner_pers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_payoutuser`
--
ALTER TABLE `eb_payoutuser`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_plinks`
--
ALTER TABLE `eb_plinks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_pn_options`
--
ALTER TABLE `eb_pn_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_key` (`meta_key`),
  ADD KEY `meta_key2` (`meta_key2`);

--
-- Индексы таблицы `eb_postmeta`
--
ALTER TABLE `eb_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `eb_posts`
--
ALTER TABLE `eb_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`),
  ADD KEY `post_name` (`post_name`(191));

--
-- Индексы таблицы `eb_reviews`
--
ALTER TABLE `eb_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_reviews_meta`
--
ALTER TABLE `eb_reviews_meta`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_scrtransaction`
--
ALTER TABLE `eb_scrtransaction`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_termmeta`
--
ALTER TABLE `eb_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `eb_terms`
--
ALTER TABLE `eb_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Индексы таблицы `eb_term_relationships`
--
ALTER TABLE `eb_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Индексы таблицы `eb_term_taxonomy`
--
ALTER TABLE `eb_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Индексы таблицы `eb_transactionrezerv`
--
ALTER TABLE `eb_transactionrezerv`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_transactionuser`
--
ALTER TABLE `eb_transactionuser`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_usermeta`
--
ALTER TABLE `eb_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `eb_users`
--
ALTER TABLE `eb_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Индексы таблицы `eb_userverify`
--
ALTER TABLE `eb_userverify`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_user_discounts`
--
ALTER TABLE `eb_user_discounts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_user_fav`
--
ALTER TABLE `eb_user_fav`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_usve_change`
--
ALTER TABLE `eb_usve_change`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_valuts`
--
ALTER TABLE `eb_valuts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_valuts_meta`
--
ALTER TABLE `eb_valuts_meta`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_vschets`
--
ALTER TABLE `eb_vschets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_vtypes`
--
ALTER TABLE `eb_vtypes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `eb_zapros_rezerv`
--
ALTER TABLE `eb_zapros_rezerv`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `eb_abitcoin`
--
ALTER TABLE `eb_abitcoin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_adminpanelcaptcha`
--
ALTER TABLE `eb_adminpanelcaptcha`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `eb_archive_data`
--
ALTER TABLE `eb_archive_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_auth_logs`
--
ALTER TABLE `eb_auth_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `eb_bautocurs`
--
ALTER TABLE `eb_bautocurs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_blackip`
--
ALTER TABLE `eb_bcoip_blackip`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_country`
--
ALTER TABLE `eb_bcoip_country`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_ipcity`
--
ALTER TABLE `eb_bcoip_ipcity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_iplist`
--
ALTER TABLE `eb_bcoip_iplist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_themplate`
--
ALTER TABLE `eb_bcoip_themplate`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bcoip_whiteip`
--
ALTER TABLE `eb_bcoip_whiteip`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bids`
--
ALTER TABLE `eb_bids`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_bids_meta`
--
ALTER TABLE `eb_bids_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_blacklist`
--
ALTER TABLE `eb_blacklist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_captcha`
--
ALTER TABLE `eb_captcha`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `eb_change`
--
ALTER TABLE `eb_change`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT для таблицы `eb_commentmeta`
--
ALTER TABLE `eb_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `eb_comments`
--
ALTER TABLE `eb_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `eb_custom_fields_valut`
--
ALTER TABLE `eb_custom_fields_valut`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_inex_change`
--
ALTER TABLE `eb_inex_change`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_inex_deposit`
--
ALTER TABLE `eb_inex_deposit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_inex_system`
--
ALTER TABLE `eb_inex_system`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_inex_tars`
--
ALTER TABLE `eb_inex_tars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_links`
--
ALTER TABLE `eb_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `eb_masschange`
--
ALTER TABLE `eb_masschange`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_merchant_logs`
--
ALTER TABLE `eb_merchant_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_napobmens`
--
ALTER TABLE `eb_napobmens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT для таблицы `eb_naps_meta`
--
ALTER TABLE `eb_naps_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT для таблицы `eb_notice_head`
--
ALTER TABLE `eb_notice_head`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_options`
--
ALTER TABLE `eb_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4790;

--
-- AUTO_INCREMENT для таблицы `eb_partners`
--
ALTER TABLE `eb_partners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `eb_partner_pers`
--
ALTER TABLE `eb_partner_pers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `eb_payoutuser`
--
ALTER TABLE `eb_payoutuser`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_plinks`
--
ALTER TABLE `eb_plinks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_pn_options`
--
ALTER TABLE `eb_pn_options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT для таблицы `eb_postmeta`
--
ALTER TABLE `eb_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=608;

--
-- AUTO_INCREMENT для таблицы `eb_posts`
--
ALTER TABLE `eb_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=329;

--
-- AUTO_INCREMENT для таблицы `eb_reviews`
--
ALTER TABLE `eb_reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_reviews_meta`
--
ALTER TABLE `eb_reviews_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_scrtransaction`
--
ALTER TABLE `eb_scrtransaction`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_termmeta`
--
ALTER TABLE `eb_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_terms`
--
ALTER TABLE `eb_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `eb_term_taxonomy`
--
ALTER TABLE `eb_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `eb_transactionrezerv`
--
ALTER TABLE `eb_transactionrezerv`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_transactionuser`
--
ALTER TABLE `eb_transactionuser`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_usermeta`
--
ALTER TABLE `eb_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT для таблицы `eb_users`
--
ALTER TABLE `eb_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `eb_userverify`
--
ALTER TABLE `eb_userverify`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_user_discounts`
--
ALTER TABLE `eb_user_discounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `eb_user_fav`
--
ALTER TABLE `eb_user_fav`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_usve_change`
--
ALTER TABLE `eb_usve_change`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_valuts`
--
ALTER TABLE `eb_valuts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT для таблицы `eb_valuts_meta`
--
ALTER TABLE `eb_valuts_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_vschets`
--
ALTER TABLE `eb_vschets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `eb_vtypes`
--
ALTER TABLE `eb_vtypes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `eb_zapros_rezerv`
--
ALTER TABLE `eb_zapros_rezerv`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
