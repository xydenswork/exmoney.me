### Start server

**Notice!** Set resolving `exmoney.me` to `127.0.0.1` in your system `hosts` before start.

**Notice!** Chrome frequently creates own DNS-cache, so if you see the internet site instead of your local [https://www.howtogeek.com/197804/how-to-clear-the-google-chrome-dns-cache-on-windows/](clear chrome DNS cache)
or use another browser.
 
`docker-compose up` will launch *mysql* and two hosts:
- `exmoney.me` for site
- `localhost:8080` for PHPMyAdmin

### Installation (the first start)

The first thing you need to start developing this project is to install exchangebox  script. Go to `exmoney.me` and 
perform installation following [install-guide.docx]().

For database settings use following:
- database name: `exchangebox`
- user name: `exchangebox`
- user password: `password`
- database host: `db`

On the next page (import) perform import of `httpd/exmoney/www/RU_damp_db.sql` and set site URL to `http://exmoney.me`.
On the last page we arrange to use common config values:
- admin email: `admin@exmoney.me`
- site email: `admin@exmoney.me`
- sender name: `Exchange box`
- admin username: `admin`
- admin password: `admin`

### PHPMyAdmin

PHPMyAdmin is available by `http://localhost:8080`. You can use root user credentials for working in PHPMA:
- username: `root`
- password: `root`